#include "stdafx.h"
#include "MatchRuleNII.h"

#include "bcd.h"
#include "Message.h"
#include "StrUtil.h"

#include <stdio.h>

using std::string;

//======================================================================

MatchRuleNII::MatchRuleNII()
:	m_wMatchData(0),
	m_bUseMask(TRUE),
	m_wMask(0x0FFE),
	m_bUseRange(FALSE),
	m_wRangeMin(0),
	m_wRangeMax(999)
{
}

//---------------------------------------------------------

MatchRule* MatchRuleNII::Clone() const
{
	return new MatchRuleNII(*this);
}

//======================================================================
//
//	NII [ MASKED_WITH <mask> ] [ IS ] [ NOT ] <msg_type>
//	NII [ MASKED_WITH <mask> ] [ IS ] [ NOT ] FROM <low_nii> TO <high_nii>
//

BOOL MatchRuleNII::Import(const Line &rLine)
{
	m_bUseMask = FALSE;
	m_bInverted = FALSE;
	m_bUseRange = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "NII") return FALSE;

	if(++i == rLine.end()) return FALSE;

	if((*i) == "MASKED_WITH")
	{
		if(++i == rLine.end()) return FALSE;

		m_wMask = (WORD)strtol((*i).c_str(), NULL, 16);

		if(m_wMask == 0)
			return FALSE;

		m_bUseMask = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	if((*i) == "IS")
		if(++i == rLine.end()) return FALSE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	if((*i) == "FROM")
	{
		m_bUseRange = TRUE;

		if(++i == rLine.end()) return FALSE;

		m_wRangeMin = (WORD)strtol((*i).c_str(), NULL, 10);

		if(++i == rLine.end()) return FALSE;

		if((*i) != "TO")
			return FALSE;

		if(++i == rLine.end()) return FALSE;

		m_wRangeMax = (WORD)strtol((*i).c_str(), NULL, 10);
	}
	else
	{
		m_wMatchData = (WORD)strtol((*i).c_str(), NULL, 10);
	}

	return TRUE;
}

//======================================================================
//
//	NII [ MASKED_WITH <mask> ] [ IS ] [ NOT ] <msg_type>
//	NII [ MASKED_WITH <mask> ] [ IS ] [ NOT ] FROM <low_nii> TO <high_nii>
//

BOOL MatchRuleNII::Export(string &strDesc)
{
	string strTmp;

	strDesc = "NII";

	if(m_bUseMask)
	{
		StrUtil::Format(strTmp, " MASKED_WITH %04X", (int)m_wMask);
		strDesc += strTmp;
	}

	strDesc += " IS";

	if(m_bInverted)
		strDesc += " NOT";

	if(m_bUseRange)
	{
		StrUtil::Format(strTmp, " FROM %04d TO %04d", (int)m_wRangeMin, (int)m_wRangeMax);
	}
	else
	{
		StrUtil::Format(strTmp, " %04d", (int)m_wMatchData);
	}

	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL MatchRuleNII::DoesMatch(WORD wTestData) const
{
	BOOL bResult = FALSE;

	if(m_bUseMask)
	{
		char szTmp[5];

		sprintf_s(szTmp, sizeof(szTmp), "%04d", (int)wTestData);
		wTestData = (WORD)strtol(szTmp, NULL, 16);	// convert to hex
		wTestData &= m_wMask;						// mask
		sprintf_s(szTmp, sizeof(szTmp), "%04X", (int)wTestData);
		wTestData = (WORD)strtol(szTmp, NULL, 10);	// convert back to dec
	}

	if(m_bUseRange)
	{
		if((wTestData >= m_wRangeMin)&&(wTestData <= m_wRangeMax))
			bResult = TRUE;
	}
	else
	{
		if(wTestData == m_wMatchData)
			bResult = TRUE;
	}

	if(m_bInverted)
		return !bResult;
	else
		return bResult;
}

//======================================================================

BOOL MatchRuleNII::DoesMatch(Message &rMsg) const
{
	if(rMsg.GetCommsHdrType() != Message::CHT_TPDU)
		return FALSE;

	WORD wNII = (WORD)BCD::LLLBCD2Bin(rMsg.GetCommsHeader() + 1);

	return DoesMatch(wNII);
}

//======================================================================
