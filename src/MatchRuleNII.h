#ifndef _MATCHRULENII_H
#define _MATCHRULENII_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

class Message;

//==============================================================

class MatchRuleNII : public MatchRule {
public:
	MatchRuleNII();

	MatchRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	BOOL DoesMatch(WORD wTestData) const;

private:
	WORD		m_wMatchData;

	BOOL		m_bUseMask;
	WORD		m_wMask;

	BOOL		m_bUseRange;
	WORD		m_wRangeMin;
	WORD		m_wRangeMax;
};

//==============================================================

#endif // _MATCHRULENII_H
