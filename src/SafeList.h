#ifndef _SAFELIST_H
#define _SAFELIST_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <list>
#include <algorithm>

//==============================================================

template <typename T>
class SafeList {
private:
	typedef std::list<T*> ListType;
	ListType	m_List;
	CritSect	m_Crit;

public:
	inline T* GetHead()	{ Lock lock(&m_Crit);	return m_List.empty()?NULL:m_List.front(); }
	inline T* GetTail()	{ Lock lock(&m_Crit);	return m_List.empty()?NULL:m_List.back(); }

	inline T* RemoveHead()	
	{
		Lock lock(&m_Crit);	
		
		if(m_List.empty()) 
			return NULL; 
		else 
		{ 
			T* p = m_List.front();
			m_List.pop_front(); 
			return p; 
		}
	}

	inline T* RemoveTail()
	{
		Lock lock(&m_Crit);
		
		if(m_List.empty())
			return NULL;
		else
		{
			T* p(m_List.back());
			m_List.pop_back();
			return p;
		}
	}

	inline T* RemoveElem(T* p)
	{
		Lock lock(&m_Crit);
		
		ListType::iterator it = std::find(m_List.begin(), m_List.end(), p);
		
		if(it == m_List.end())
			return NULL;
		
		m_List.erase(it);
		return p;
	}

	inline void AddHead(T* p) { Lock lock(&m_Crit);	m_List.push_front(p); }
	inline void AddTail(T* p) { Lock lock(&m_Crit);	m_List.push_back(p); }

	inline void RemoveAll() { Lock lock(&m_Crit);	m_List.clear(); }

	inline int GetCount() { Lock lock(&m_Crit);	return m_List.size(); }
	inline BOOL IsEmpty() { Lock lock(&m_Crit);	return m_List.empty(); }

};

//==============================================================

#endif // _SAFELIST_H
