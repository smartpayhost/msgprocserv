#ifndef _MUXDOWNSTREAMTHREAD_H
#define _MUXDOWNSTREAMTHREAD_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _STOPPABLETHREAD_H
#include "StoppableThread.h"
#endif

#ifndef _DATAPIPE_H
#include "DataPipe.h"
#endif

#include <vector>

//==============================================================

class MuxDownstreamThread : public StoppableThread {
public:
	MuxDownstreamThread();

	DataPipe& GetPipeUpSrc();
	void SetPipeUpDst(DataPipe *pPipe);
	DataPipe& GetPipeDnSrc();
	void SetPipeDnDst(DataPipe *pPipe);

	void SetNumTrans(int iNumTrans);
	void SetThreadNumber(BYTE bIndex, BYTE bInstance);

protected:
    DWORD ThreadMemberFunc();

private:
	class HdrEntry {
	public:
		HdrEntry();

		BOOL m_fShortHeader;

		WORD m_wUpThread;
		WORD m_wUpConnection;

		WORD m_wDnThread;
		WORD m_wDnConnection;
	};

	std::vector<HdrEntry> m_HdrTable;
	int m_iHdrTableSize;

	BYTE m_bThreadIndex;
	BYTE m_bThreadInstance;

	BYTE m_bPktIndex;
	BYTE m_bPktCycle;

	DataPipe m_pipeUpSrc;
	DataPipe *m_pPipeUpDst;
	DataPipe m_pipeDnSrc;
	DataPipe *m_pPipeDnDst;
};

//==============================================================

#endif // _MUXDOWNSTREAMTHREAD_H
