#ifndef _MATCHRULEEXPDATE_H
#define _MATCHRULEEXPDATE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

class Message;
class BitmapMsg;

//==============================================================

class MatchRuleExpDate : public MatchRule {
public:
	MatchRuleExpDate();

	MatchRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	BOOL DoesMatch(const std::string& strTestData, const BitmapMsg *pMsg) const;

	enum { DEFAULT_PIVOT = 1970 };

	BOOL ValidYYMM(const std::string& strDate, int iPivot = DEFAULT_PIVOT, int* piMonthsSince0000 = NULL) const;
	int CurrentMonthsSince0000() const;

private:
	enum MATCH_WITH {
		MRED_PRESENT,
		MRED_EXPIRED,
		MRED_BEFORE,
		MRED_AFTER,
		MRED_MATCHES,
		MRED_REGEXP
	};

	MATCH_WITH	m_eWith;
	int			m_iPivot;
	std::string	m_strMatchData;
};

//==============================================================

#endif // _MATCHRULEEXPDATE_H
