#ifndef _REPLYRULEMSGTYPE_H
#define _REPLYRULEMSGTYPE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULE_H
#include "ReplyRule.h"
#endif

#ifndef _STRUTIL_H
#include "StrUtil.h"
#endif

//==============================================================

class ReplyRuleMsgType : public ReplyRule {
public:
	explicit ReplyRuleMsgType(const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const;

private:
	enum ReplyRuleMsgTypeSource {
		RRMTS_FIXED,
		RRMTS_FROMORIG,
	};

	ReplyRuleMsgTypeSource	m_eSource;

	WORD		m_wFixedData;

	BOOL		m_bUseMask;
	WORD		m_wMask;

	BOOL		m_bUseOffset;
	WORD		m_wOffset;
};

//==============================================================

#endif // _REPLYRULEMSGTYPE_H
