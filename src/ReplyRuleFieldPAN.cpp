#include "stdafx.h"
#include "ReplyRuleFieldPAN.h"

#include <assert.h>

using std::string;

//======================================================================

ReplyRuleFieldPAN::ReplyRuleFieldPAN(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldPAN::Clone() const
{
	return new ReplyRuleFieldPAN(*this);
}

//======================================================================
//
//	FIELD <field_num> PAN

BOOL ReplyRuleFieldPAN::Import(const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "PAN")
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL ReplyRuleFieldPAN::Export(string &strDesc)
{
	StrUtil::Format(strDesc, "FIELD %d PAN", GetFieldNum());

	return TRUE;
}

//======================================================================
//
//	Build pan field if available.
//

BOOL ReplyRuleFieldPAN::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	string strPAN(rOrigMsg.GetPAN());

	BitmapMsgField *pField;

	if(!strPAN.empty())
	{
		pField = new BitmapMsgField(GetFieldNum(), rOrigMsg.GetMsgStyle());

		assert(pField);

		if(!pField)
			return FALSE;

		if(!pField->SetFieldFromString(strPAN))
		{
			delete pField;
			return FALSE;
		}

		rNewMsg.SetField(pField);
	}

	return TRUE;
}

//======================================================================
