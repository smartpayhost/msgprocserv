#include "stdafx.h"
#include "LoggerDatabase.h"

#include "LogDefaults.h"
#include "jpmsgdll.h"
#include "synchro.h"

#include "OdbcStatement.h"
#include "OdbcConnection.h"

#include "BitmapMsg.h"

#include <stdio.h>
#include <time.h>
#include <process.h>

#ifdef UNIT_TESTS
#include <boost/test/unit_test.hpp>
#endif

using std::string;

//======================================================================

LoggerDatabase::LoggerDatabase(const string& strLogName, int iLogLevel)
:	Logger(strLogName, iLogLevel),
	m_evShutdown(TRUE, FALSE),
	m_fConfigured(FALSE),
	m_dwLogTypes(0)
{
}

//======================================================================

void LoggerDatabase::SetDSN(const char *szDSN)
{
	m_fConfigured = szDSN && strlen(szDSN);
	m_dbt.SetDSN(szDSN);
}

void LoggerDatabase::SetUID(const char *szUID)
{
	m_dbt.SetUID(szUID);
}

void LoggerDatabase::SetAuthStr(const char *szAuthStr)
{
	m_dbt.SetAuthStr(szAuthStr);
}

void LoggerDatabase::SetTimeout(DWORD dwTimeout)
{
	m_dbt.SetTimeout(dwTimeout);
}

void LoggerDatabase::SetLogTypes(DWORD dwLogTypes)
{
	m_dwLogTypes = dwLogTypes;
}

void LoggerDatabase::SetTraceFile(const char *szTraceFile)
{
	m_dbt.SetTraceFile(szTraceFile);
}

//======================================================================

void LoggerDatabase::DBThread::SetDSN(const char *szDSN)
{
	m_pConnectionManager->SetDSN(szDSN);
}

void LoggerDatabase::DBThread::SetUID(const char *szUID)
{
	m_pConnectionManager->SetUID(szUID);
}

void LoggerDatabase::DBThread::SetAuthStr(const char *szAuthStr)
{
	m_pConnectionManager->SetAuthStr(szAuthStr);
}

void LoggerDatabase::DBThread::SetTimeout(DWORD dwTimeout)
{
	m_dwTimeout = dwTimeout;
}

void LoggerDatabase::DBThread::SetTraceFile(const char *szTraceFile)
{
	m_pConnectionManager->SetTraceFile(szTraceFile);
}

//======================================================================

struct LogData {
	struct tm timestamp;
	const char* szLogName;
	int pid;
	DWORD threadid;
	enum LogLevel level;
	char szBuf[1024];

	bool bmmsg;
	char msgtype[5];
	char proccode[7];
	char stan[7];
	char catid[9];
	char caid[16];
};

//======================================================================

int LoggerDatabase::vLog(enum LogLevel level, const char *fmt, va_list ap)
{
	Lock localLock(&m_Lockable);

	if(!m_fConfigured)
		return 0;

	if(!(m_dwLogTypes & LOGDB_TYPE_NORMAL))
		return 0;

	if(GetLogLevel() < level)
		return 0;

	//---------------------------------------------------------------

	LogData ld;

	time_t t;

	time(&t);
	localtime_s(&ld.timestamp, &t);

	ld.szLogName = GetLogName();
	ld.pid = _getpid();
	ld.threadid = GetCurrentThreadId();
	ld.level = level;

	ld.szBuf[sizeof(ld.szBuf) - 1] = 0;

	_vsnprintf_s(ld.szBuf, sizeof(ld.szBuf) - 1, _TRUNCATE, fmt, ap);

	ld.bmmsg = false;

	//---------------------------------------------------------------

	m_dbt.GetPipeLog().Put(sizeof(ld), (const BYTE*)&ld);

	return 0;
}

//======================================================================

int LoggerDatabase::vLogBitmapMsg(enum LogLevel level, const BitmapMsg& msg, const char *fmt, va_list ap)
{
	Lock localLock(&m_Lockable);

	if(!m_fConfigured)
		return 0;

	if(!(m_dwLogTypes & LOGDB_TYPE_MSG))
		return 0;

	if(GetLogLevel() < level)
		return 0;

	//---------------------------------------------------------------

	LogData ld;

	time_t t;

	time(&t);
	localtime_s(&ld.timestamp, &t);

	ld.szLogName = GetLogName();
	ld.pid = _getpid();
	ld.threadid = GetCurrentThreadId();
	ld.level = level;

	ld.szBuf[sizeof(ld.szBuf) - 1] = 0;

	_vsnprintf_s(ld.szBuf, sizeof(ld.szBuf) - 1, _TRUNCATE, fmt, ap);

	ld.bmmsg = true;

	const BitmapMsgField* f;

	_snprintf_s(ld.msgtype, sizeof(ld.msgtype), _TRUNCATE, "%04u", msg.GetMsgType());
	f = msg.GetField(3);
	if(f) _snprintf_s(ld.proccode, sizeof(ld.proccode), _TRUNCATE, "%s", f->GetAsciiRep().c_str());
	f = msg.GetField(11);
	if(f) _snprintf_s(ld.stan, sizeof(ld.stan), _TRUNCATE, "%s", f->GetAsciiRep().c_str());
	f = msg.GetField(41);
	if(f) _snprintf_s(ld.catid, sizeof(ld.catid), _TRUNCATE, "%s", f->GetAsciiRep().c_str());
	f = msg.GetField(42);
	if(f) _snprintf_s(ld.caid, sizeof(ld.caid), _TRUNCATE, "%s", f->GetAsciiRep().c_str());

	//---------------------------------------------------------------

	m_dbt.GetPipeMsgLog().Put(sizeof(ld), (const BYTE*)&ld);

	return 0;
}

//======================================================================

void LoggerDatabase::Start()
{
	SZFN(LoggerDatabase);

	LoggingObject::Log(LOG_DEBUG, "%s: starting", szFn);

	if(m_fConfigured)
	{
		if(m_dbt.Connect())
		{
			m_dbt.SetShutdownEvent(&m_evShutdown);
			m_dbt.StartThread();
		}
		else
		{
			LoggingObject::Log(LOG_WARNING, "%s: Couldn't connect to logging database, check LOGDB* parameters in configuration file.", szFn);
			m_fConfigured = FALSE;
		}
	}
}

void LoggerDatabase::Stop()
{
	if(m_fConfigured)
	{
		m_evShutdown.Set();
		m_dbt.WaitForExit();
	}
}

//======================================================================

LoggerDatabase::DBThread::DBThread()
:	m_dwTimeout(0),
	m_pConnectionManager(new OdbcConnectionManager)
{
}

//======================================================================

BOOL LoggerDatabase::DBThread::Connect()
{
	return m_pConnectionManager->GetConnection()?TRUE:FALSE;
}

//======================================================================

BOOL LoggerDatabase::DBThread::ProcessLogEntries()
{
	OdbcConnection *pConnection = m_pConnectionManager->GetConnection();

	if(!pConnection)
	{
		return FALSE;
	}

	OdbcStatement stmt(pConnection);

	stmt.SetLogger(GetLogger());

	if(!stmt.Prepare("INSERT INTO MsgProcLog (Timestamp, LogName, ProcessId, ThreadId, LogLevel, Message) VALUES (?,?,?,?,?,?)", m_dwTimeout))
	{
		return FALSE;
	}

	// buffers for inserted data
	
	#define MAX_ROWS 20

	SQL_TIMESTAMP_STRUCT ts[MAX_ROWS];
	char logname[MAX_ROWS][64];		// SQLCHAR is actually unsigned char but shouldn't matter here.
	SQLINTEGER pid[MAX_ROWS];
	SQLUINTEGER threadid[MAX_ROWS];
	SQLUSMALLINT loglevel[MAX_ROWS];
	char message[MAX_ROWS][1024];	// SQLCHAR is actually unsigned char but shouldn't matter here.
	
	WORD wMaxRows = MAX_ROWS;

	if(!stmt.SetNumRows(wMaxRows))	// check driver supports multiple rows in one INSERT
		wMaxRows = 1;				// send only one at a time if not

	while(m_pipeLog.GetNumChunks())
	{
		BYTE *pbBuf = NULL;
		int iLen;
		WORD rows = 0;

		while(rows < wMaxRows)
		{
			iLen = m_pipeLog.GetChunkNC(&pbBuf);

			if(!iLen)
				break;

			assert(iLen == sizeof(LogData));

			LogData* pld = (LogData*)pbBuf;

			ts[rows].year = (SQLSMALLINT)(pld->timestamp.tm_year + 1900);
			ts[rows].month = (SQLUSMALLINT)(pld->timestamp.tm_mon + 1);
			ts[rows].day = (SQLUSMALLINT)pld->timestamp.tm_mday;
			ts[rows].hour = (SQLUSMALLINT)pld->timestamp.tm_hour;
			ts[rows].minute = (SQLUSMALLINT)pld->timestamp.tm_min;
			ts[rows].second = (SQLUSMALLINT)pld->timestamp.tm_sec;
			ts[rows].fraction = 0;

			strncpy_s(logname[rows], sizeof(logname[rows]), pld->szLogName, _TRUNCATE);
			pid[rows] = pld->pid;
			threadid[rows] = pld->threadid;
			loglevel[rows] = (SQLUSMALLINT)pld->level;
			strncpy_s(message[rows], sizeof(message[rows]), pld->szBuf, _TRUNCATE);

			++rows;

			delete[] pbBuf;
		}

		// set number of rows

		if(!stmt.SetNumRows(rows))
			return FALSE;

		// bind params

		if(!stmt.BindParam(1, SQL_PARAM_INPUT, SQL_C_TYPE_TIMESTAMP, SQL_TYPE_TIMESTAMP, 22, 0, (SQLPOINTER)ts, sizeof(SQL_TIMESTAMP_STRUCT), NULL))
			return FALSE;

		if(!stmt.BindParam(2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, sizeof(logname[0]), 0, (SQLPOINTER)logname, sizeof(logname[0]), NULL))
			return FALSE;

		if(!stmt.BindParam(3, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER, 0, 0, (SQLPOINTER)pid, 0, NULL))
			return FALSE;

		if(!stmt.BindParam(4, SQL_PARAM_INPUT, SQL_C_ULONG, SQL_INTEGER, 0, 0, (SQLPOINTER)threadid, 0, NULL))
			return FALSE;

		if(!stmt.BindParam(5, SQL_PARAM_INPUT, SQL_C_USHORT, SQL_INTEGER, 0, 0, (SQLPOINTER)loglevel, 0, NULL))
			return FALSE;

		if(!stmt.BindParam(6, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, sizeof(message[0]), 0, (SQLPOINTER)message, sizeof(message[0]), NULL))
			return FALSE;

		// execute

		if(!stmt.Execute(m_dwTimeout))
			return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL LoggerDatabase::DBThread::ProcessMsgLogEntries()
{
	OdbcConnection *pConnection = m_pConnectionManager->GetConnection();

	if(!pConnection)
	{
		return FALSE;
	}

	OdbcStatement stmt(pConnection);

	stmt.SetLogger(GetLogger());

	if(!stmt.Prepare("INSERT INTO MsgProcMsgLog (Timestamp, LogName, ProcessId, ThreadId, LogLevel, MsgType, ProcCode, STAN, CATID, CAID, Message) VALUES (?,?,?,?,?,?,?,?,?,?,?)", m_dwTimeout))
	{
		return FALSE;
	}

	// buffers for inserted data
	
	#define MAX_ROWS 20

	SQL_TIMESTAMP_STRUCT ts[MAX_ROWS];
	char logname[MAX_ROWS][64];		// SQLCHAR is actually unsigned char but shouldn't matter here.
	SQLINTEGER pid[MAX_ROWS];
	SQLUINTEGER threadid[MAX_ROWS];
	SQLUSMALLINT loglevel[MAX_ROWS];
	char msgtype[MAX_ROWS][5];		// SQLCHAR is actually unsigned char but shouldn't matter here.
	char proccode[MAX_ROWS][7];
	char stan[MAX_ROWS][7];
	char catid[MAX_ROWS][9];
	char caid[MAX_ROWS][16];
	char message[MAX_ROWS][1024];	// SQLCHAR is actually unsigned char but shouldn't matter here.
	
	WORD wMaxRows = MAX_ROWS;

	if(!stmt.SetNumRows(wMaxRows))	// check driver supports multiple rows in one INSERT
		wMaxRows = 1;				// send only one at a time if not

	while(m_pipeMsgLog.GetNumChunks())
	{
		BYTE *pbBuf = NULL;
		int iLen;
		WORD rows = 0;

		while(rows < wMaxRows)
		{
			iLen = m_pipeMsgLog.GetChunkNC(&pbBuf);

			if(!iLen)
				break;

			assert(iLen == sizeof(LogData));

			LogData* pld = (LogData*)pbBuf;

			ts[rows].year = (SQLSMALLINT)(pld->timestamp.tm_year + 1900);
			ts[rows].month = (SQLUSMALLINT)(pld->timestamp.tm_mon + 1);
			ts[rows].day = (SQLUSMALLINT)pld->timestamp.tm_mday;
			ts[rows].hour = (SQLUSMALLINT)pld->timestamp.tm_hour;
			ts[rows].minute = (SQLUSMALLINT)pld->timestamp.tm_min;
			ts[rows].second = (SQLUSMALLINT)pld->timestamp.tm_sec;
			ts[rows].fraction = 0;

			strncpy_s(logname[rows], sizeof(logname[rows]), pld->szLogName, _TRUNCATE);
			pid[rows] = pld->pid;
			threadid[rows] = pld->threadid;
			loglevel[rows] = (SQLUSMALLINT)pld->level;
			strncpy_s(msgtype[rows], sizeof(msgtype[rows]), pld->msgtype, _TRUNCATE);
			strncpy_s(proccode[rows], sizeof(proccode[rows]), pld->proccode, _TRUNCATE);
			strncpy_s(stan[rows], sizeof(stan[rows]), pld->stan, _TRUNCATE);
			strncpy_s(catid[rows], sizeof(catid[rows]), pld->catid, _TRUNCATE);
			strncpy_s(caid[rows], sizeof(caid[rows]), pld->caid, _TRUNCATE);
			strncpy_s(message[rows], sizeof(message[rows]), pld->szBuf, _TRUNCATE);

			++rows;

			delete[] pbBuf;
		}

		// set number of rows

		if(!stmt.SetNumRows(rows))
			return FALSE;

		// bind params

		if(!stmt.BindParam(1, SQL_PARAM_INPUT, SQL_C_TYPE_TIMESTAMP, SQL_TYPE_TIMESTAMP, 22, 0, (SQLPOINTER)ts, sizeof(SQL_TIMESTAMP_STRUCT), NULL))
			return FALSE;

		if(!stmt.BindParam(2, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, sizeof(logname[0]), 0, (SQLPOINTER)logname, sizeof(logname[0]), NULL))
			return FALSE;

		if(!stmt.BindParam(3, SQL_PARAM_INPUT, SQL_C_SLONG, SQL_INTEGER, 0, 0, (SQLPOINTER)pid, 0, NULL))
			return FALSE;

		if(!stmt.BindParam(4, SQL_PARAM_INPUT, SQL_C_ULONG, SQL_INTEGER, 0, 0, (SQLPOINTER)threadid, 0, NULL))
			return FALSE;

		if(!stmt.BindParam(5, SQL_PARAM_INPUT, SQL_C_USHORT, SQL_INTEGER, 0, 0, (SQLPOINTER)loglevel, 0, NULL))
			return FALSE;

		if(!stmt.BindParam(6, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, sizeof(msgtype[0]), 0, (SQLPOINTER)msgtype, sizeof(msgtype[0]), NULL))
			return FALSE;

		if(!stmt.BindParam(7, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, sizeof(proccode[0]), 0, (SQLPOINTER)proccode, sizeof(proccode[0]), NULL))
			return FALSE;

		if(!stmt.BindParam(8, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, sizeof(stan[0]), 0, (SQLPOINTER)stan, sizeof(stan[0]), NULL))
			return FALSE;

		if(!stmt.BindParam(9, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, sizeof(catid[0]), 0, (SQLPOINTER)catid, sizeof(catid[0]), NULL))
			return FALSE;

		if(!stmt.BindParam(10, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, sizeof(caid[0]), 0, (SQLPOINTER)caid, sizeof(caid[0]), NULL))
			return FALSE;

		if(!stmt.BindParam(11, SQL_PARAM_INPUT, SQL_C_CHAR, SQL_VARCHAR, sizeof(message[0]), 0, (SQLPOINTER)message, sizeof(message[0]), NULL))
			return FALSE;

		// execute

		if(!stmt.Execute(m_dwTimeout))
			return FALSE;
	}

	return TRUE;
}

//======================================================================

DWORD LoggerDatabase::DBThread::ThreadMemberFunc()
{
	SZFN(LoggerDatabase::DBThread);

	Log(LOG_DEBUG, "%s: starting", szFn);

	BOOL fExit = FALSE;

	while(!fExit)
	{
		WaitableCollection wc;

		wc.push_back(GetShutdownEvent());
		wc.push_back(m_pipeLog);
		wc.push_back(m_pipeMsgLog);

		DWORD e = wc.Wait();

		switch(e)
		{
		case WAIT_OBJECT_0 + 0:		// shutdown
			Log(LOG_DEBUG, "%s: shutdown requested", szFn);
			fExit = TRUE;
			break;

		case WAIT_OBJECT_0 + 1:		// data packet received
			ProcessLogEntries();
			break;

		case WAIT_OBJECT_0 + 2:		// data packet received
			ProcessMsgLogEntries();
			break;

		case WAIT_TIMEOUT:
			break;

		default:
			Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result", szFn);
			fExit = TRUE;
			break;
		}
	}

	Log(LOG_DEBUG, "%s: ending", szFn);

	return 0;
}

//======================================================================

void LoggerDatabase::SetLogger(Logger* pLogger)
{
	LoggingObject::SetLogger(pLogger);
	m_dbt.SetLogger(pLogger);
}

//======================================================================

void LoggerDatabase::DBThread::SetLogger(Logger* pLogger)
{
	LoggingObject::SetLogger(pLogger);
	m_pConnectionManager->SetLogger(pLogger);
}

//======================================================================

#ifdef UNIT_TESTS

BOOST_AUTO_TEST_CASE(LoggerDatabase__Log)
{
/*
	char buf[500];
	int i;

	LoggerDatabase theLogger("test", LOG_DEBUG);


	for(i = 0; i < (sizeof(buf)/sizeof(buf[0])); i++)
	{
		buf[i] = (char)i;
//		buf[i] = (char)'j';
		if(!buf[i])
			buf[i] = ' ';
	}
	
	buf[sizeof(buf) - 1] = 0;

//	theLogger.Log(LOG_DEBUG, "%s", &buf[0]);
	((Logger*)&theLogger)->Log(LOG_DEBUG, "%d %s", 1, buf);	// extra param needed to select correct overload
	BOOST_CHECK(true);
*/
}

#endif
