#include "stdafx.h"

#include "ServiceCfg.h"

#include "Log.h"
#include "LogDefaults.h"
#include "ServDefaults.h"
#include "HostDescription.h"

#include <algorithm>

using std::string;

namespace ConfKeys
{
	const string Encryption = "ENCRYPTION";
	const string LogLevel = "LOGLEVEL";
	const string LogPath = "LOGPATH";
	const string WorkDir = "WORKDIR";
	const string ServiceName = "SERVICENAME";
	const string ServiceDisplay = "SERVICEDISPLAY";
	const string LogDBDSN = "LOGDBDSN";
	const string LogDBUID = "LOGDBUID";
	const string LogDBAuthStr = "LOGDBAUTHSTR";
	const string LogDBTraceFile = "LOGDBTRACEFILE";
	const string LogDBTimeout = "LOGDBTIMEOUT";
	const string LogDBLogTypes = "LOGDBLOGTYPES";
}

//======================================================================

ServiceCfg::ServiceCfg()
:	m_iLogLevel(LOGLEVEL),
	m_strLogFile(LOGPATH),
	m_strWorkDir(DEFAULT_WORK_DIR),
	m_iLogDBTimeout(0),
	m_iLogDBLogTypes(0),
	m_encryptionType(Config::NO_TYPE)
{
}

//======================================================================

BOOL ServiceCfg::LoadEncryptionType()
{
	std::string strType;

	if (m_sCfg.GetStringValue(ConfKeys::Encryption,	strType))
	{
		transform(strType.begin(), strType.end(), strType.begin(), toupper);

		if (strType == Config::ETSL60_LABEL)
		{
			m_encryptionType = Config::ETSL60;
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
		
	return TRUE;
}

//======================================================================

BOOL ServiceCfg::ReadFromFile(const string& strFilename, FILETIME *pftConfigLastChange)
{
	if(!m_sCfg.Load(strFilename, pftConfigLastChange))
		return FALSE;

	m_sCfg.GetIntValue   (ConfKeys::LogLevel,		m_iLogLevel);
	m_sCfg.GetStringValue(ConfKeys::LogPath,		m_strLogFile);
	m_sCfg.GetStringValue(ConfKeys::WorkDir,		m_strWorkDir);
	m_sCfg.GetStringValue(ConfKeys::ServiceName,	m_strServiceName);
	m_sCfg.GetStringValue(ConfKeys::ServiceDisplay,	m_strServiceDisplayName);

	m_sCfg.GetStringValue(ConfKeys::LogDBDSN,		m_strLogDBDSN);
	m_sCfg.GetStringValue(ConfKeys::LogDBUID,		m_strLogDBUID);
	m_sCfg.GetStringValue(ConfKeys::LogDBAuthStr,	m_strLogDBAuthStr);
	m_sCfg.GetStringValue(ConfKeys::LogDBTraceFile,	m_strLogDBTraceFile);
	m_sCfg.GetIntValue	 (ConfKeys::LogDBTimeout,	m_iLogDBTimeout);
	m_sCfg.GetIntValue	 (ConfKeys::LogDBLogTypes,	m_iLogDBLogTypes);

	return LoadEncryptionType();
}
