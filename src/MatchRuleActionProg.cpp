#include "stdafx.h"
#include "MatchRuleActionProg.h"

#include "MsgProcRuleCollection.h"
#include "Message.h"

#include <assert.h>
#include <fstream>
#include <process.h>

using std::string;
using std::vector;
using std::ofstream;
using std::ios_base;

//======================================================================

MatchRule* MatchRuleActionProg::Clone() const
{
	return new MatchRuleActionProg(*this);
}

//======================================================================
//
//	[ACTION] PROG [ NOT ] <cmdline>
//

BOOL MatchRuleActionProg::Import(const Line &rLine)
{
	m_bInverted = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) == "ACTION")
	{
		if(++i == rLine.end()) return FALSE;
	}

	if((*i) != "PROG") return FALSE;

	if(++i == rLine.end()) return FALSE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	m_strCmdline = (*i);

	return TRUE;
}

//======================================================================
//
//	[ACTION] PROG [ NOT ] <cmdline>
//

BOOL MatchRuleActionProg::Export(string &strDesc)
{
	string strTmp;

	strDesc = "PROG";

	if(m_bInverted)
		strDesc += " NOT";

	StrUtil::PurifyString(m_strCmdline, strTmp);
	strDesc += ' ';
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL MatchRuleActionProg::DoesMatch(Message &rMsg) const
{
	assert(m_pMsgProcRules);

	int i;
	int iResult;

	//-----------------------
	// Create source file(s)

	string strExpandedCmdLine;
	Line lineArgs;
	vector<string> vectFiles;

	StrUtil::StringToLine(m_strCmdline, lineArgs);

	// look for % arguments and replace with filenames

	int iNumArgs = (int)lineArgs.size();

	for(i = 0; i < iNumArgs; i++)
	{
		string strArg = lineArgs[i];

		if(strArg[0] == '%')
		{
			int iField = atoi(strArg.c_str() + 1);

			if((iField >= 1) && (iField <= 128))
			{
				char tname[MAX_PATH];
				tmpnam_s(tname, sizeof(tname));
				string strSrcName(tname);

				ofstream of;

				of.open(strSrcName.c_str(), ios_base::out | ios_base::trunc | ios_base::binary);

				if(of.bad())
				{
					char szErr[100];

					strerror_s(szErr, sizeof(szErr), errno);

					Log(LOG_ERROR, "Failed to create temp file %s: %s", strSrcName.c_str(), szErr);
					return FALSE;
				}

				const BitmapMsgField *pField = rMsg.GetField(iField);

				if(pField != NULL)
				{
					if(pField->GetFieldLen() != 0)
					{
						of.write((const char *)(pField->GetField()), pField->GetFieldLen());

						if(of.bad())
						{
							char szErr[100];

							strerror_s(szErr, sizeof(szErr), errno);

							Log(LOG_ERROR, "Error writing to temp file %s: %s", strSrcName.c_str(), szErr);
							of.close();
							return FALSE;
						}
					}
				}

				of.close();
				vectFiles.push_back(strSrcName);
				lineArgs[i] = strSrcName;
			}
		}
	}

	vector<const char *> vArgs;

	for(i = 0; i < iNumArgs; i++)
	{
		vArgs.push_back(lineArgs[i].c_str());
	}

	vArgs.push_back(NULL);

	//------------
	// Run program

	iResult = (int)::_spawnv(_P_WAIT, vArgs[0], &vArgs[0]);

	//--------------
	// Check results
	
	BOOL fResult = FALSE;

	if(iResult >= 0)
	{
		if(iResult == 0)
			fResult = TRUE;

		if(m_bInverted)
			fResult = !fResult;
	}
	else
	{
		Log(LOG_ERROR, "Failed to execute %s, return value = %d", m_strCmdline.c_str(), iResult);
	}

	//---------
	// Clean up

	for(i = 0; i < (int)vectFiles.size(); i++)
	{
		::remove(vectFiles[i].c_str());
	}

	return fResult;
}

//======================================================================
