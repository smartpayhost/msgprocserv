#ifndef _SERVICEAPP_H
#define _SERVICEAPP_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _WIN32SERVICE_H
#include "Win32Service.h"
#endif

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <boost/utility.hpp>

//==============================================================

class ServiceApp : public LoggingObject, public boost::noncopyable {
public:
	ServiceApp();

	virtual int InitialSetup(int argc, char **argv);

	std::string GetName() const { return m_strName; }
	std::string GetDisplayName() const { return m_strDisplayName; }
	std::string GetConfig() const { return m_strConfigFile; }
	std::string GetRegKey() const { return m_strRegKeyMain; }
	std::string GetExePath() const { return m_strExePath; }

	int Run(BOOL fRunningAsService, int argc, char **argv);

	BOOL StopService();
	BOOL StartService();
	BOOL UninstallService();
	BOOL InstallService(const char *szServiceExecutable);
	
protected:
	virtual BOOL Setup() = 0;
	virtual BOOL Main() = 0;
	virtual void Cleanup() = 0;

	Event& GetStopEvent() { return m_evStop; }
	BOOL IsRunningAsService() const { return m_fRunningAsService; }

private:
	BOOL ControlService(DWORD dwCmd, DWORD dwNewState);
	void ServMain(DWORD dwArgc, LPTSTR *lpszArgv);
	void CtrlHandler(DWORD dwCommand);

private:
	Event m_evStop;
	BOOL m_fRunningAsService;

	std::string m_strName;
	std::string m_strDisplayName;
	std::string m_strRegKeyMain;
	std::string m_strConfigFile;
	std::string m_strExePath;

	Win32ComponentService m_thisComponent;

	static ServiceApp *m_pApp;

private:
	friend void WINAPI ServiceAppCtrlHandler(DWORD dwCommand);
	friend void WINAPI ServiceAppServMain(DWORD dwArgc, LPTSTR *lpszArgv);
};

//==============================================================

#endif // _SERVICEAPP_H
