#ifndef _TCPSOCKET_H
#define _TCPSOCKET_H

//=======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _IPSOCKET_H
#include "IpSocket.h"
#endif

#include <string>

//=======================================================================

class TcpSocket : public IpSocket {
public:
	SOCKRC Open();
	SOCKRC Connect();
	SOCKRC Connect(const std::string& strHostname, const std::string& strPort);

	SOCKRC Listen(const std::string& strPort);
	SOCKET Accept();
	SOCKET Accept(unsigned long timeout);

	SOCKRC SetNodelay();
	SOCKRC SetNagle();

	void SetHost(const std::string& strHostname, const std::string& strPort);

	std::string GetPeerName();

private:
	std::string m_strHost;
	std::string m_strPort;
};

//=======================================================================

#endif _TCPSOCKET_H
