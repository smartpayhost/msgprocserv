#include "stdafx.h"
#include "OdbcConnectionManager.h"
#include "OdbcConnection.h"

//======================================================================

OdbcConnectionManager::OdbcConnectionManager()
{
}

//======================================================================

void OdbcConnectionManager::SetDSN(const char *szDSN)
{
	m_strDSN = szDSN;
}

void OdbcConnectionManager::SetUID(const char *szUID)
{
	m_strUID = szUID;
}

void OdbcConnectionManager::SetAuthStr(const char *szAuthStr)
{
	m_strAuthStr = szAuthStr;
}

void OdbcConnectionManager::SetTraceFile(const char *szTraceFile)
{
	m_strTraceFile = szTraceFile;
}

//======================================================================

OdbcConnection* OdbcConnectionManager::GetConnection()
{
	Lock lock(&m_crit);

	DWORD dwThreadId = ::GetCurrentThreadId();

	ConnectionMap::iterator it;

	it = m_Connections.find(dwThreadId);

	if(it != m_Connections.end())
	{
		if(it->second->Connected())
			return (*it).second.get();
	}

	Log(LOG_DEBUG, "OdbcConnectionManager(%p) creating connection", this);

	boost::shared_ptr<OdbcConnection> p(new OdbcConnection);

	p->SetLogger(GetLogger());

	if(!p->Connect(m_strDSN.c_str(), m_strUID.empty()?NULL:m_strUID.c_str(), m_strAuthStr.empty()?NULL:m_strAuthStr.c_str(), m_strTraceFile.empty()?NULL:m_strTraceFile.c_str()))
	{
		return NULL;
	}

	m_Connections[dwThreadId] = p;

	return p.get();
}

//======================================================================
