#include "stdafx.h"
#include "SQLActionQuery.h"

#include "StrUtil.h"
#include "SQLResultTable.h"

using std::string;

//======================================================================

BOOL SQLActionQuery::Export(string &rStrDest) const
{
	string strTmp;

	rStrDest = "ACTION_QUERY ";
	StrUtil::PurifyString(GetName(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "\tDATA_SOURCE ";
	StrUtil::PurifyString(GetDataSource(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "\tQUERY ";
	StrUtil::PurifyString(GetQuery(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "ACTION_QUERY_END\n";

	return TRUE;
}

//======================================================================

BOOL SQLActionQuery::ExecQuery(SQLResultTable &rResult, const BitmapMsg *pOrigMsg, const BitmapMsg *pNewMsg) const
{
	BOOL fResult = SQLMessageQuery::ExecQuery(rResult, pOrigMsg, pNewMsg);

	Log(LOG_INFO, "Action query %s: %d results", GetName().c_str(), rResult.GetNumRows());

	return fResult;
}

//======================================================================
