#include "stdafx.h"
#include "MatchTemplateRuleEntity.h"

#include "Action.h"
#include "MatchRuleActionQuery.h"
#include "MatchTemplateCollection.h"

#include <assert.h>

//======================================================================

MatchTemplateRuleEntity::MatchTemplateRuleEntity()
:	m_fComplete(FALSE),
	m_pCollection(NULL)
{
}

//======================================================================

void MatchTemplateRuleEntity::SetName(const char *szName)
{
	m_mtTemplate.SetName(szName);
}

//======================================================================

void MatchTemplateRuleEntity::SetMatchTemplateCollection(MatchTemplateCollection *pCollection)
{
	m_pCollection = pCollection;
}

//======================================================================

BOOL MatchTemplateRuleEntity::ProcessReplyWith(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_mtTemplate.SetAction(Action::RESPONSE);
	m_mtTemplate.SetReplyTemplateName(rLine[1].c_str());

	return TRUE;
}

//----------------------------------------------------------------------

BOOL MatchTemplateRuleEntity::ProcessSendTo(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_mtTemplate.SetAction(Action::UPSTREAM);
	m_mtTemplate.SetDestinationHost(rLine[1].c_str());

	return TRUE;
}

//----------------------------------------------------------------------

BOOL MatchTemplateRuleEntity::ProcessMatchEnd(const Line & /*rLine*/)
{
	m_fComplete = TRUE;

	assert(m_pCollection);
	m_pCollection->AddTemplate(m_mtTemplate);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL MatchTemplateRuleEntity::ProcessRule(const Line &rLine)
{
	MatchRule *pRule = MatchRule::MakeNew(rLine);

	if(pRule)
	{
		m_mtTemplate.AddRule(pRule);
		return TRUE;
	}

	return FALSE;
}

//----------------------------------------------------------------------

BOOL MatchTemplateRuleEntity::ProcessLine(const Line &rLine)
{
	if(rLine[0] == "REPLY_WITH")
	{
		return ProcessReplyWith(rLine);
	}
	else if(rLine[0] == "SEND_TO")
	{
		return ProcessSendTo(rLine);
	}
	else if(rLine[0] == "MATCH_TEMPLATE_END")
	{
		return ProcessMatchEnd(rLine);
	}
	else
	{
		return ProcessRule(rLine);
	}
}

//======================================================================

BOOL MatchTemplateRuleEntity::IsComplete() const
{
	return m_fComplete;
}

//======================================================================
