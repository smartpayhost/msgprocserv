#ifndef _BITMAPMSG_H
#define _BITMAPMSG_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MAC_H
#include "MAC.h"
#endif

#include <string>
#include <boost/utility.hpp>

//==============================================================

enum BMFieldFormat {
	BMFF_INVALID,
	BMFF_FIXED,
	BMFF_LLVAROZ,
	BMFF_LLVARNZ,
	BMFF_LLLVAROZ,
	BMFF_LLLVARNZ,
};

enum BMFieldAttr {
	BMFA_INVALID,
	BMFA_B,
	BMFA_N,
	BMFA_XN,
	BMFA_NS,
	BMFA_Z,
	BMFA_A,
	BMFA_AN,
	BMFA_ANS,
	BMFA_AORN,
};

enum BMPrintOption {
	BMPS_FULL,
	BMPS_ETSL60
};

//==============================================================
//==============================================================

class BitmapMsgFieldStyle : public boost::noncopyable {
public:
	BitmapMsgFieldStyle();
	BitmapMsgFieldStyle(BMFieldFormat eFormat,
			BMFieldAttr eAttr,
			int iMaxLen,
			const std::string& strShortDesc,
			const std::string& strDesc);

	inline BMFieldFormat	GetFormat()		const	{ return m_eFormat; }
	inline BMFieldAttr		GetAttr()		const	{ return m_eAttr; }
	inline int				GetMaxLen()		const	{ return m_iMaxLen; }
	inline std::string		GetShortDesc()	const	{ return m_strShortDesc; }
	inline std::string		GetDesc()		const	{ return m_strDesc; }

	static const BitmapMsgFieldStyle DefaultFieldStyle;
	static const std::string DefaultShortDesc;
	static const std::string DefaultDesc;

private:
	BMFieldFormat		m_eFormat;
	BMFieldAttr			m_eAttr;
	int					m_iMaxLen;
	const std::string	m_strShortDesc;
	const std::string	m_strDesc;
};

//==============================================================
//==============================================================

class BitmapMsgStyle {
public:
	BitmapMsgStyle();
	BitmapMsgStyle(const std::string& strName, const BitmapMsgFieldStyle* const *pField);

	const BitmapMsgFieldStyle* GetField(int iFieldNum) const;
	BOOL IsValidField(int iFieldNum) const;

private:
	std::string m_strName;
	const BitmapMsgFieldStyle* const *m_pField; // ptr to const ptr to const BitmapMsgFieldStyle
};

//==============================================================
//==============================================================

class BitmapMsgField {
public:
	BitmapMsgField(int iFieldNumber, const BitmapMsgStyle *pMsgStyle);
	BitmapMsgField(const BitmapMsgField& rhs);

	BitmapMsgField& operator=(const BitmapMsgField& rhs);

	BOOL SetFieldContents(BYTE *pData, int iBufLen, BYTE **ppNext = NULL);
	BOOL SetFieldFromString(const std::string& strAsciiRep);

	void Print(std::string &strDest);

	inline int			GetFieldNumber()	const { return m_iFieldNumber; }
	inline int			GetLen()			const { return m_iUnitLen; }
	inline int			GetFieldLen()		const { return m_iLenLen + m_iDataLen; }
	inline int			GetLenLen()			const { return m_iLenLen; }
	inline int			GetDataLen()		const { return m_iDataLen; }
	inline const BYTE*	GetField()			const { return m_pField; }
	inline const BYTE*	GetData()			const { return m_pData; }
	inline const std::string& GetAsciiRep()		const { return m_strAsciiRep; }

	inline const BitmapMsgFieldStyle* GetStyle()	const { return m_pfsFieldStyle; }

private:
	static int CalcUnitLen(const BitmapMsgFieldStyle& fsFieldStyle, const std::string& strAsciiRep);
	static int CalcDataLen(const BitmapMsgFieldStyle& fsFieldStyle, int iUnitLen);
	static int CalcLenLen(const BitmapMsgFieldStyle& fsFieldStyle);

	static void EncodeLen(const BitmapMsgFieldStyle& fsFieldStyle, int iUnitLen, BYTE *pDest);
	static void EncodeNumeric(const std::string& strAscii, BYTE *pDest, int iDestLen, std::string& strFormatted, BOOL fTrailingF);

	int	CalcDataLen();
	int	CalcLenLen();

	int	DecodeLen();
	std::string GenAsciiRep();

private:
	int				m_iFieldNumber;

	const BitmapMsgFieldStyle	*m_pfsFieldStyle;

	int				m_iUnitLen;		// Length of data in units (e.g. 3 for N3, 64 for B64 etc)

	int				m_iFieldLen;	// Length of field in bytes (inc LL/LLL)
	BYTE			*m_pField;		// Pointer to field data as transmitted (in m_abLocalCopy)

	int				m_iLenLen;		// Length of LL/LLL (0 - 3)

	int				m_iDataLen;		// Length of data in bytes	(ex LL/LLL) (e.g. 2 for N3, 8 for B64 etc)
	BYTE			*m_pData;		// Pointer to data (in m_abLocalCopy)

	std::string		m_strAsciiRep;

	enum { MAX_BITMAP_RAW_FIELD_LEN	= 1024 };

	BYTE			m_abLocalCopy[MAX_BITMAP_RAW_FIELD_LEN];
};

//==============================================================
//==============================================================

class BitmapMsg {
public:
	~BitmapMsg();
	BitmapMsg();

	void SetField(BitmapMsgField *pField);

	const BitmapMsgField* GetField(int iFieldNum) const;
	BitmapMsgField* GetField(int iFieldNum);

	void SetMsgStyle(BitmapMsgStyle *pStyle) { m_pStyle = pStyle; }
	const BitmapMsgStyle* GetMsgStyle() const { return m_pStyle; }

	BOOL SetMsg(int iLen, const BYTE *pData, const BYTE **ppNext = NULL);

	void Print(std::string &strDest);
	void Print(std::string &strDest, BMPrintOption printOption);

	BOOL Build();

	int GetMsgType() const { return m_iMsgType; }
	void SetMsgType(int iMsgType) { m_iMsgType = iMsgType; }

	int GetMsgLen() const { return m_iLen; }
	void SetMsgLen(int iLen) { m_iLen = iLen; }

	const BYTE *GetRawMsg() const { return m_abRawMsg; }

	MacValidity	GetMacValidity() const { return m_eMacValid; }
	void SetMacValidity(MacValidity eMacValid) { m_eMacValid = eMacValid; }

	std::string BitmapMsg::GetPAN() const;
	std::string BitmapMsg::GetExpiryYYMM() const;

private:
	BOOL GetBit(int iBitNum);
	void SetBit(int iBitNum, BOOL bSet = TRUE);
	WORD GetFieldOffset(int iFieldNum) const;

	BOOL SetField(int iFieldNum, BYTE **ppPtr, BYTE *pEndPtr);

private:
	BitmapMsgStyle 	*m_pStyle;				// message style (as2805/etsl etc)

	int				m_iMsgType;				// message type (decimal)

	BYTE			*m_pMessage;			// Pointer to start of message
	BYTE			m_abPrimaryBitmap[8];	// Pointer to primary bitmap
	BYTE			m_abSecondaryBitmap[8];	// Pointer to secondary bitmap

	MacValidity		m_eMacValid;

	int				m_iLen;


	enum { MAX_BITMAP_RAW_MSG_LEN = 2048 };

	BYTE			m_abRawMsg[MAX_BITMAP_RAW_MSG_LEN];	// buffer inc. msg type, bitmap, msg

	BitmapMsgField*	m_apFields[128];
	WORD			m_awFieldOffsets[128];
};

//==============================================================

#endif //_BITMAPMSG_H
