#include "stdafx.h"
#include "MatchRuleMsgType.h"

#include "Message.h"
#include "StrUtil.h"

#include <stdio.h>

using std::string;

//======================================================================

MatchRuleMsgType::MatchRuleMsgType()
:	m_wMatchData(0),
	m_bUseMask(TRUE),
	m_wMask(0x0FFE),
	m_bUseRange(FALSE),
	m_wRangeMin(0),
	m_wRangeMax(999)
{
}

//---------------------------------------------------------

MatchRule* MatchRuleMsgType::Clone() const
{
	return new MatchRuleMsgType(*this);
}

//======================================================================
//
//	MSG_TYPE [ MASKED_WITH <mask> ] [ IS ] [ NOT ] <msg_type>
//	MSG_TYPE [ MASKED_WITH <mask> ] [ IS ] [ NOT ] FROM <low_msg_type> TO <high_msg_type>
//

BOOL MatchRuleMsgType::Import(const Line &rLine)
{
	m_bUseMask = FALSE;
	m_bInverted = FALSE;
	m_bUseRange = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "MSG_TYPE") return FALSE;

	if(++i == rLine.end()) return FALSE;

	if((*i) == "MASKED_WITH")
	{
		if(++i == rLine.end()) return FALSE;

		m_wMask = (WORD)strtol((*i).c_str(), NULL, 16);

		if(m_wMask == 0)
			return FALSE;

		m_bUseMask = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	if((*i) == "IS")
		if(++i == rLine.end()) return FALSE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	if((*i) == "FROM")
	{
		m_bUseRange = TRUE;

		if(++i == rLine.end()) return FALSE;

		m_wRangeMin = (WORD)strtol((*i).c_str(), NULL, 10);

		if(++i == rLine.end()) return FALSE;

		if((*i) != "TO")
			return FALSE;

		if(++i == rLine.end()) return FALSE;

		m_wRangeMax = (WORD)strtol((*i).c_str(), NULL, 10);
	}
	else
	{
		m_wMatchData = (WORD)strtol((*i).c_str(), NULL, 10);
	}

	return TRUE;
}

//======================================================================
//
//	MSG_TYPE [ MASKED_WITH <mask> ] [ IS ] [ NOT ] <msg_type>
//	MSG_TYPE [ MASKED_WITH <mask> ] [ IS ] [ NOT ] FROM <low_msg_type> TO <high_msg_type>
//

BOOL MatchRuleMsgType::Export(string &strDesc)
{
	string strTmp;

	strDesc = "MSG_TYPE";

	if(m_bUseMask)
	{
		StrUtil::Format(strTmp, " MASKED_WITH %04X", (int)m_wMask);
		strDesc += strTmp;
	}

	strDesc += " IS";

	if(m_bInverted)
		strDesc += " NOT";

	if(m_bUseRange)
	{
		StrUtil::Format(strTmp, " FROM %04d TO %04d", (int)m_wRangeMin, (int)m_wRangeMax);
	}
	else
	{
		StrUtil::Format(strTmp, " %04d", (int)m_wMatchData);
	}

	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL MatchRuleMsgType::DoesMatch(WORD wTestData) const
{
	BOOL bResult = FALSE;

	if(m_bUseMask)
	{
		char szTmp[5];

		sprintf_s(szTmp, sizeof(szTmp), "%04d", (int)wTestData);
		wTestData = (WORD)strtol(szTmp, NULL, 16);	// convert to hex
		wTestData &= m_wMask;						// mask
		sprintf_s(szTmp, sizeof(szTmp), "%04X", (int)wTestData);
		wTestData = (WORD)strtol(szTmp, NULL, 10);	// convert back to dec
	}

	if(m_bUseRange)
	{
		if((wTestData >= m_wRangeMin)&&(wTestData <= m_wRangeMax))
			bResult = TRUE;
	}
	else
	{
		if(wTestData == m_wMatchData)
			bResult = TRUE;
	}

	if(m_bInverted)
		return !bResult;
	else
		return bResult;
}

//======================================================================

BOOL MatchRuleMsgType::DoesMatch(Message &rMsg) const
{
	return DoesMatch((WORD)(rMsg.m_sBMsg.GetMsgType()));
}

//======================================================================
