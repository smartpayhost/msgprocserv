#ifndef _MATCHTEMPLATECOLLECTION_H
#define _MATCHTEMPLATECOLLECTION_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#include <vector>
#include <map>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

class Message;
class Logger;
class MsgProcRuleCollection;
class MatchTemplate;

//======================================================================

class MatchTemplateCollection :	public RuleEntityDescriptor, public boost::noncopyable {
public:
	MatchTemplateCollection();

	BOOL IsStart(const Line &rLine) const;
	RuleEntity *MakeNew(const Line &rLine);

	BOOL Check() const;

	void AddTemplate(const MatchTemplate &rTemplate);
	void Register(RuleFile &rRuleFile);
	BOOL Export(std::string &rStrDest);
	void MergeFrom(const MatchTemplateCollection& rhs);

	void SetLogger(Logger *pLogger);

	void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray);

	const MatchTemplate* FindByName(const std::string& strName) const;
	const MatchTemplate* FindMatchTemplate(Message& rMsg) const;

private:
	typedef std::vector<boost::shared_ptr<MatchTemplate> > MatchTemplateVector;
	typedef std::map<std::string, boost::shared_ptr<MatchTemplate> > MatchTemplateMap;

	MatchTemplateVector m_vectTemplates;
	MatchTemplateMap m_mapTemplates;

	MsgProcRuleCollection *m_pMsgProcRules;

	Logger *m_pLogger;
};

//======================================================================

#endif // _MATCHTEMPLATECOLLECTION_H
