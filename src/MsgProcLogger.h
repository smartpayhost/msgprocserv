#ifndef _MSGPROCLOGGER_H
#define _MSGPROCLOGGER_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGER_H
#include "Logger.h"
#endif

#ifndef _LOGGERFILE_H
#include "LoggerFile.h"
#endif

#ifndef _LOGGEREVENT_H
#include "LoggerEvent.h"
#endif

#ifndef _LOGGERSTDERR_H
#include "LoggerStderr.h"
#endif

#ifndef _LOGGERDATABASE_H
#include "LoggerDatabase.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <boost/utility.hpp>

//==============================================================

class MsgProcLogger : public Logger, public boost::noncopyable {
public:
	MsgProcLogger(const std::string& strLogName, int iLogLevel, const std::string& strLogPath);

	virtual int vLog(enum LogLevel, const char *, va_list);
	virtual int vLogBitmapMsg(enum LogLevel, const BitmapMsg&, const char *, va_list);

	void SetLogToStderr(BOOL fLogToStderr = TRUE) { m_fLogToStderr = fLogToStderr; }

	virtual void SetLogName(const std::string& strLogName);
	virtual void SetLogLevel(int iLogLevel);
	virtual void SetLogPath(const std::string& strLogPath);

	virtual void SetLogDBDSN(const std::string& strLogDBDSN);
	virtual void SetLogDBUID(const std::string& strLogDBUID);
	virtual void SetLogDBAuthStr(const std::string& strLogDBAuthStr);
	virtual void SetLogDBTraceFile(const std::string& strLogDBTraceFile);
	virtual void SetLogDBTimeout(int iLogDBTimeout);
	virtual void SetLogDBLogTypes(int iLogTypes);
	virtual void SetLogDBLogger(Logger* pLogger);

	virtual void Start();
	virtual void Stop();

private:
	CritSect	m_Lockable;

	BOOL m_fLogToStderr;

	LoggerFile m_loggerFile;
	LoggerEvent m_loggerEvent;
	LoggerStderr m_loggerStderr;
	LoggerDatabase m_loggerDB;
};

//==============================================================

#endif // _MSGPROCLOGGER_H
