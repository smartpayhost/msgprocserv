#ifndef _HOSTDESCRIPTIONCOLLECTION_H
#define _HOSTDESCRIPTIONCOLLECTION_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#include <map>
#include <boost/utility.hpp>
#include <boost/smart_ptr.hpp>

class HostDescription;

class MsgProcRuleCollection;

//======================================================================

class HostDescriptionCollection : public RuleEntityDescriptor, public boost::noncopyable {
public:
	HostDescriptionCollection();
	~HostDescriptionCollection();

	BOOL IsStart(const Line &rLine) const;
	RuleEntity *MakeNew(const Line &rLine);

	void AddHostDescription(const HostDescription &rHost);
	void Register(RuleFile &rRuleFile);
	BOOL Export(std::string &rStrDest) const;
	void MergeFrom(const HostDescriptionCollection& rhs);

	const HostDescription* FindByName(const std::string& strName) const;

	typedef std::map<std::string, boost::shared_ptr<HostDescription> > HostDescriptionMap;

	inline int size() { return (int)m_mapHosts.size(); }
	inline HostDescriptionMap::const_iterator begin() { return m_mapHosts.begin(); }
	inline HostDescriptionMap::const_iterator end() { return m_mapHosts.end(); }

	void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray);
	void SetLogger(Logger *pLogger);

private:
	HostDescriptionMap m_mapHosts;

	MsgProcRuleCollection *m_pMsgProcRules;
	Logger *m_pLogger;
};

//======================================================================

#endif // _HOSTDESCRIPTIONCOLLECTION_H
