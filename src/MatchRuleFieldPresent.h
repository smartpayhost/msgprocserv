#ifndef _MATCHRULEFIELDPRESENT_H
#define _MATCHRULEFIELDPRESENT_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

class Message;

//==============================================================

class MatchRuleFieldPresent : public MatchRule {
public:
	MatchRuleFieldPresent();

	MatchRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	int			m_iFieldNum;
};

//==============================================================

#endif // _MATCHRULEFIELDPRESENT_H
