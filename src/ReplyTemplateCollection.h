#ifndef _REPLYTEMPLATECOLLECTION_H
#define _REPLYTEMPLATECOLLECTION_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _REPLYTEMPLATE_H
#include "ReplyTemplate.h"
#endif

#include <vector>
#include <map>
#include <boost/utility.hpp>

class BitmapMsgStyle;
class SQLReplyQueryCollection;
class Logger;

class MsgProcRuleCollection;

//======================================================================

class ReplyTemplateCollection : public RuleEntityDescriptor, public boost::noncopyable {
public:
	ReplyTemplateCollection();
	~ReplyTemplateCollection();

	BOOL IsStart(const Line &rLine) const;
	RuleEntity *MakeNew(const Line &rLine);

	BOOL Check() const;

	void AddTemplate(ReplyTemplate &rTemplate);
	void Register(RuleFile &rRuleFile);
	BOOL Export(std::string &rStrDest);
	void MergeFrom(const ReplyTemplateCollection& rhs);

	void SetStyle(BitmapMsgStyle *pStyle);
	const ReplyTemplate* FindByName(const std::string& strName) const;

	void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray);
	void SetLogger(Logger *pLogger);

private:
	typedef std::vector<ReplyTemplate*> ReplyTemplateVector;
	typedef std::map<std::string, ReplyTemplate*> ReplyTemplateMap;

	ReplyTemplateVector m_vectTemplates;
	ReplyTemplateMap m_mapTemplates;

	MsgProcRuleCollection *m_pMsgProcRules;

	Logger *m_pLogger;
};

//======================================================================

#endif // _REPLYTEMPLATECOLLECTION_H
