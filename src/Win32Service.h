#ifndef _WIN32SERVICE_H
#define _WIN32SERVICE_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//====================================================================

class Win32Service {
public:
	BOOL Start(DWORD dwServices,
		SERVICE_TABLE_ENTRY Services[],
		int argc, char *argv[]);
};

//====================================================================

class Win32ComponentService {
public:
	static LPHANDLER_FUNCTION g_lpHandler;

	Win32ComponentService(DWORD dwServiceType = SERVICE_WIN32_OWN_PROCESS);

	BOOL RegisterHandler(const char* szServiceName, LPHANDLER_FUNCTION lpHandler);

	BOOL ReportStatusChange(DWORD dwCurrentState,
		DWORD dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE | SERVICE_ACCEPT_SHUTDOWN,
		DWORD dwWaitHint = 5000,
		DWORD dwWin32ExitCode = NO_ERROR,
		DWORD dwProcessSpecificExitCode = 0);

	BOOL SetPendingStatus(DWORD dwCurrentState,
		DWORD dwWaitHint = 5000,
		DWORD dwCheckPoint = 1);

	BOOL UpdateStatus();

	BOOL SetStatus();

protected:
	SERVICE_STATUS_HANDLE	m_hServiceStatus;
	SERVICE_STATUS			m_ServiceStatus;
};

//====================================================================

#endif // _WIN32SERVICE_H
