#include "stdafx.h"
#include "synchro.h"

//=============================================================
//=============================================================

CritSect::CritSect()
{
	::InitializeCriticalSection(&m_critSect);
}

//-------------------------------------------------------------

CritSect::~CritSect()
{
	::DeleteCriticalSection(&m_critSect);
}

//=============================================================

void CritSect::Lock()
{
	::EnterCriticalSection(&m_critSect);
}

//-------------------------------------------------------------

void CritSect::Unlock()
{
	::LeaveCriticalSection(&m_critSect);
}

//=============================================================

Mutex::Mutex(BOOL bInitiallyOwn)
:	m_hmtxMutex(::CreateMutex(NULL, bInitiallyOwn, NULL))
{
}

//-------------------------------------------------------------

Mutex::~Mutex()
{
	if(m_hmtxMutex)
		::CloseHandle(m_hmtxMutex);
}

//-------------------------------------------------------------

void Mutex::Lock()
{
	Wait();
}

//-------------------------------------------------------------

void Mutex::Unlock()
{
	::ReleaseMutex(m_hmtxMutex);
}

//=============================================================
//=============================================================

Lock::Lock(LockableObject *pLockable)
:	m_pLockable(pLockable)
{
	m_pLockable->Lock();
}

//-------------------------------------------------------------

Lock::~Lock()
{
	m_pLockable->Unlock();
}

//====================================================================
//====================================================================

DWORD Waitable::Wait(DWORD dwTimeout)
{
	return ::WaitForSingleObject(GetHandle(), dwTimeout);
}

//====================================================================
//====================================================================

DWORD WaitableCollection::Wait(DWORD dwTimeout, BOOL fWaitAll)
{
	return ::WaitForMultipleObjects((DWORD)m_v.size(), &m_v[0], fWaitAll, dwTimeout);
}

//====================================================================
//====================================================================
