#include "stdafx.h"
#include "WorkBroker.h"

#include <algorithm>

//====================================================================

WorkBroker::WorkBroker(WorkHandlerFactory& whf)
:	m_whf(whf),
	m_iNumHandlers(0),
	m_iMaxHandlers(0),
	m_iMaxWorkLoad(0),
	m_iMinWorkLoad(0),
	m_itCursor(m_collWorkHandlers.begin())
{
}

//--------------------------------------------------------------------

WorkBroker::~WorkBroker()
{
	WHCollection::iterator i;

	Lock localLock(&m_Lockable);

	for(i = m_collWorkHandlers.begin(); i != m_collWorkHandlers.end(); ++i)
		m_whf.DestroyWorkHandler(*i);
}

//====================================================================

void WorkBroker::RemoveWorkHandler(WorkHandler* wh)
{
	WHCollection::iterator i;

	Lock localLock(&m_Lockable);

	i = std::find(m_collWorkHandlers.begin(), m_collWorkHandlers.end(), wh);

	if(i != m_collWorkHandlers.end())
	{
		m_whf.DestroyWorkHandler(*i);
		m_collWorkHandlers.erase(i);
		m_iNumHandlers--;
		m_itCursor = m_collWorkHandlers.begin();
	}
}

//====================================================================

BOOL WorkBroker::AddHandler()
{
	if(m_iMaxHandlers && (m_iNumHandlers >= m_iMaxHandlers))
		return FALSE;

	WorkHandler *wh = m_whf.CreateNewWorkHandler();

	if(!wh)
		return FALSE;

	m_collWorkHandlers.push_back(wh);
	m_itCursor = m_collWorkHandlers.end();
	--m_itCursor;
	m_iNumHandlers++;

	return TRUE;
}

//--------------------------------------------------------------------

BOOL WorkBroker::PreCreateHandlers(int iHandlers)
{
	for(int i = 0; i < iHandlers; i++)
	{
		if(!AddHandler())
			return FALSE;
	}

	return TRUE;
}

//====================================================================

WorkHandler* WorkBroker::FindSuitableHandler()
{
	// find the next one with the lowest workload

	if(m_iNumHandlers)
	{
		WHCollection::iterator itPos;

		itPos = m_itCursor;
		++itPos;

		if(itPos == m_collWorkHandlers.end())
			itPos = m_collWorkHandlers.begin();

		int iLowestLoad = (*itPos)->GetCurrentWorkLoad();
		WHCollection::iterator itLowestLoadPos = itPos;

		for(int i = 0; (i < m_iNumHandlers)&&(iLowestLoad > m_iMinWorkLoad); i++)
		{
			int iLoad = (*itPos)->GetCurrentWorkLoad();

			if(iLoad < iLowestLoad)
			{
				iLowestLoad = iLoad;
				itLowestLoadPos = itPos;
			}

			itPos++;

			if(itPos == m_collWorkHandlers.end())
				itPos = m_collWorkHandlers.begin();
		}

		if(iLowestLoad <= m_iMaxWorkLoad)
		{
			m_itCursor = itLowestLoadPos;
			return *m_itCursor;
		}
	}

	return NULL;
}

//====================================================================

WorkHandler* WorkBroker::GetHandler()
{
	Lock localLock(&m_Lockable);
	
	WorkHandler *wh = NULL;

	wh = FindSuitableHandler();

	if(!wh)
	{
		::Sleep(0);
		wh = FindSuitableHandler();
	}

	if(!wh)
	{
		// none suitable found, create a new one

		if(AddHandler())
			wh = m_collWorkHandlers.back();
	}

	return wh;
}

//====================================================================
