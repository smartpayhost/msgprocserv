#include "stdafx.h"
#include "SQLQuery.h"

#include "MsgProcRuleCollection.h"
#include "DataSource.h"
#include "DataSourceCollection.h"
#include "SQLResultTable.h"

#include <assert.h>

using std::string;

//======================================================================

SQLQuery::SQLQuery()
:	m_pMsgProcRules(NULL)
{
}

//======================================================================

BOOL SQLQuery::ExecQuery(const string &strQuery, SQLResultTable &rResult) const
{
	assert(m_pMsgProcRules);

	const DataSource *pSource = m_pMsgProcRules->GetDataSourceCollection().FindByName(GetDataSource());

	assert(pSource);

	if(!pSource)
	{
		Log(LOG_WARNING, "Query %s couldn't find data source %s", GetName().c_str(), GetDataSource().c_str());
		return FALSE;
	}

	return pSource->ExecQuery(strQuery, rResult);
}

//======================================================================

BOOL SQLQuery::Check() const
{
	assert(m_pMsgProcRules);

	if(m_pMsgProcRules->GetDataSourceCollection().FindByName(GetDataSource()))
		return TRUE;

	Log(LOG_WARNING, "Query %s couldn't find data source %s", GetName().c_str(), GetDataSource().c_str());
	return FALSE;
}

//======================================================================
