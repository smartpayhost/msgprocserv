#ifndef _DATAPIPE_H
#define _DATAPIPE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <list>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

//==============================================================

class DataPipe : public boost::noncopyable, public Waitable {
public:
	DataPipe();
	~DataPipe();

	void Put(int iLen, const BYTE *pbBuf);
	void PutNC(int iLen, BYTE *pbBuf);

	int GetTotalSize();
	int GetNumChunks();
	int GetNextChunkSize();
	int Get(int iMaxLen, BYTE *pbBuf);
	int GetChunk(int iMaxLen, BYTE *pbBuf);
	int GetChunkNC(BYTE **ppbBuf);

	void Flush();

	HANDLE GetHandle() const { return m_Event; }

private:
	class Chunk {
	public:
		Chunk();
		Chunk(int iLen, const BYTE *pbBuf);
		~Chunk();

		void TakeOwnership(int iLen, BYTE *pbBuf);
		int ReleaseOwnership(BYTE *&rpbBuf);
		int Len() const;
		int Get(int iLen, BYTE *pbBuf);

	private:
		int		m_iLen;
		BYTE	*m_pbBuf;
		BYTE	*m_pbPtr;
	};

	mutable CritSect	m_Lockable;

	typedef boost::shared_ptr<Chunk> ChunkPtr;
	std::list<ChunkPtr> m_List;

	Event m_Event;

	int	m_iTotal;
};

//==============================================================

#endif _DATAPIPE_H
