#include "stdafx.h"
#include "MuxDownstreamThread.h"

#include "NetworkHeader.h"
#include "ChinaUnionPayHeader.h"

#include <assert.h>

#define DEF_MUXDOWNSTREAMTHREAD_TRANS	256

//================================================================

MuxDownstreamThread::HdrEntry::HdrEntry()
:	m_wUpThread(0),
	m_wUpConnection(0),
	m_wDnThread(0),
	m_wDnConnection(0)
{
}

//================================================================

MuxDownstreamThread::MuxDownstreamThread()
:	m_HdrTable(NULL),
	m_iHdrTableSize(DEF_MUXDOWNSTREAMTHREAD_TRANS),
	m_bThreadIndex(0),
	m_bThreadInstance(0),
	m_bPktIndex(0),
	m_bPktCycle(0),
	m_pPipeUpDst(NULL),
	m_pPipeDnDst(NULL)
{
}

//================================================================

DataPipe& MuxDownstreamThread::GetPipeUpSrc()
{
	return m_pipeUpSrc;
}

void MuxDownstreamThread::SetPipeUpDst(DataPipe *pPipe)
{
	m_pPipeUpDst = pPipe;
}

DataPipe& MuxDownstreamThread::GetPipeDnSrc()
{
	return m_pipeDnSrc;
}

void MuxDownstreamThread::SetPipeDnDst(DataPipe *pPipe)
{
	m_pPipeDnDst = pPipe;
}

//================================================================

void MuxDownstreamThread::SetNumTrans(int iNumTrans)
{
	m_iHdrTableSize = iNumTrans;
}

void MuxDownstreamThread::SetThreadNumber(BYTE bIndex, BYTE bInstance)
{
	m_bThreadIndex = bIndex;
	m_bThreadInstance = bInstance;
}

//================================================================

DWORD MuxDownstreamThread::ThreadMemberFunc()
{
	SZFN(MuxDownstreamThread);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if(	(m_iHdrTableSize == 0) ||
		(m_pPipeUpDst == NULL) ||
		(m_pPipeDnDst == NULL) ||
		(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) )
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	m_HdrTable.resize(m_iHdrTableSize);

	WaitableCollection wc;
	wc.push_back(m_pipeDnSrc);
	wc.push_back(m_pipeUpSrc);
	wc.push_back(GetShutdownEvent());

	int iLen;

	for(;;)
	{
		// wait for packet on pipe or shutdown

		DWORD e = wc.Wait();

		if(e == (WAIT_OBJECT_0 + 0))	// packet from downstream
		{
			BYTE *pbBuf = NULL;

			// get packet from pipe

			iLen = m_pipeDnSrc.GetChunkNC(&pbBuf);

			Log(LOG_DEBUG, "%s: packet received from downstream, size = %d", szFn, iLen);
			LogRawBitmapMsg(LOG_NOTICE, iLen, pbBuf, "%s: packet received from downstream, size = %d", szFn, iLen);

			if(iLen < 10)
			{
				Log(LOG_WARNING, "%s: packet length invalid", szFn);
				delete[] pbBuf;
				continue;
			}

			// Rewrite header

			NetworkHeader nh;

			if(!nh.CopyFrom(pbBuf))
			{
				Log(LOG_WARNING, "%s: junk packet received from downstream", szFn);
				delete[] pbBuf;
				continue;
			}

			WORD wNewThread = (WORD)((((WORD)m_bThreadIndex) << 8) + m_bThreadInstance);
			WORD wNewConn = (WORD)((((WORD)m_bPktIndex) << 8) + m_bPktCycle);

			m_HdrTable[m_bPktIndex].m_fShortHeader = nh.IsShortHeader();

			m_HdrTable[m_bPktIndex].m_wUpThread = wNewThread;
			m_HdrTable[m_bPktIndex].m_wUpConnection = wNewConn;

			m_HdrTable[m_bPktIndex].m_wDnThread = nh.GetThread();
			m_HdrTable[m_bPktIndex].m_wDnConnection = nh.GetConnection();

			nh.SetThread(wNewThread);
			nh.SetConnection(wNewConn);

			// use normal header for upstream connection
			nh.CopyTo(pbBuf, FALSE, nh.IsShortHeader());

			m_bPktIndex++;
			m_bPktIndex %= m_iHdrTableSize;

			if(m_bPktIndex == 0)
				m_bPktCycle++;

			// Write to upstream pipe

			m_pPipeUpDst->PutNC(iLen, pbBuf);

			// signal activity semaphore

			Log(LOG_DEBUG, "%s: packet sent to upstream, size = %d", szFn, iLen);

			SignalActivity();
		}
		else if(e == (WAIT_OBJECT_0 + 1))	// packet from upstream
		{
			BYTE *pbBuf = NULL;

			// get packet from pipe

			iLen = m_pipeUpSrc.GetChunkNC(&pbBuf);

			Log(LOG_DEBUG, "%s: packet received from upstream, size = %d", szFn, iLen);

			if(iLen == 0)
			{
				Log(LOG_INFO, "%s: packet length invalid", szFn);
				delete[] pbBuf;
				continue;
			}

			std::auto_ptr<int> a(new int(10));


			// Rewrite header

			NetworkHeader nh;

			if(!nh.CopyFrom(pbBuf))
			{
				Log(LOG_WARNING, "%s: packet header invalid", szFn);
				delete[] pbBuf;
				continue;
			}

			WORD wNewThread = nh.GetThread();
			WORD wNewConn = nh.GetConnection();

			BYTE bIdx = (BYTE)(wNewConn >> 8);

			if(bIdx >= m_iHdrTableSize)
			{
				Log(LOG_WARNING, "%s: packet header invalid", szFn);
				delete[] pbBuf;
				continue;
			}

			if( (m_HdrTable[bIdx].m_wUpThread	  != wNewThread)|| 
			    (m_HdrTable[bIdx].m_wUpConnection != wNewConn  )  )
			{
				Log(LOG_WARNING, "%s: packet header doesn't match", szFn);
				delete[] pbBuf;
				continue;
			}

			nh.SetHeaderType(m_HdrTable[bIdx].m_fShortHeader);
			nh.SetThread(m_HdrTable[bIdx].m_wDnThread);
			nh.SetConnection(m_HdrTable[bIdx].m_wDnConnection);

			if (nh.IsShortHeader())
			{
				// need to change back from 10 byte nh1 hdr to 2 byte length hdr
				nh.CopyTo(pbBuf, TRUE);

				// message length is reduced
				iLen -= (nh.GetNormalHeaderLength() - nh.GetShortHeaderLength());
				assert(iLen > 0);
				
				// move data up the buffer
				memmove(pbBuf + nh.GetShortHeaderLength(), pbBuf + nh.GetNormalHeaderLength(), iLen);
			}
			else
			{
				nh.CopyTo(pbBuf);
			}

			// Write to downstream pipe

			LogRawBitmapMsg(LOG_NOTICE, iLen, pbBuf, "%s: sending packet to downstream, size = %d", szFn, iLen);
			m_pPipeDnDst->PutNC(iLen, pbBuf);

			// signal activity semaphore

			Log(LOG_DEBUG, "%s: packet sent to downstream, size = %d", szFn, iLen);

			SignalActivity();
		}
		else if(e == (WAIT_OBJECT_0 + 2))
		{
			// shutdown thread

			Log(LOG_DEBUG, "%s: shutdown requested, ending", szFn);
			return 0;
		}
		else
		{
			Log(LOG_ERROR, "%s: ending, error 2", szFn);
			return 2;
		}
	}
}

//================================================================

