#ifndef _SQLACTIONQUERYRULEENTITY_H
#define _SQLACTIONQUERYRULEENTITY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _SQLACTIONQUERY_H
#include "SQLActionQuery.h"
#endif

class SQLActionQueryCollection;

//======================================================================

class SQLActionQueryRuleEntity : public RuleEntity {
public:
	SQLActionQueryRuleEntity();

	BOOL ProcessLine(const Line &rLine);
	BOOL IsComplete() const;

	void SetName(const std::string &strName);
	void SetSQLActionQueryCollection(SQLActionQueryCollection *pCollection);

private:
	BOOL ProcessDataSource(const Line &rLine);
	BOOL ProcessQuery(const Line &rLine);
	BOOL ProcessMatchQueryEnd(const Line &rLine);

private:
	BOOL m_fComplete;
	SQLActionQuery m_dsSQLActionQuery;
	SQLActionQueryCollection *m_pCollection;
};

//======================================================================

#endif // _SQLACTIONQUERYRULEENTITY_H
