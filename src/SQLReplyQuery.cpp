#include "stdafx.h"
#include "SQLReplyQuery.h"

#include "StrUtil.h"
#include "SQLResultTable.h"

using std::string;

//======================================================================

BOOL SQLReplyQuery::Export(string &rStrDest) const
{
	string strTmp;

	rStrDest = "REPLY_QUERY ";
	StrUtil::PurifyString(GetName(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "\tDATA_SOURCE ";
	StrUtil::PurifyString(GetDataSource(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "\tQUERY ";
	StrUtil::PurifyString(GetQuery(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "REPLY_QUERY_END\n";

	return TRUE;
}

//======================================================================

BOOL SQLReplyQuery::ExecQuery(SQLResultTable &rResult, const BitmapMsg &rOrigMsg, const BitmapMsg &rNewMsg) const
{
	BOOL fResult = SQLMessageQuery::ExecQuery(rResult, &rOrigMsg, &rNewMsg);

	if(rResult.GetNumRows() == 0)
	{
		Log(LOG_WARNING, "Reply query %s: no rows in results", GetName().c_str());
	}
	else if(rResult.GetNumRows() > 1)
	{
		Log(LOG_WARNING, "Reply query %s: more than one row in results", GetName().c_str());
	}

	if(rResult.GetNumCols() > 1)
	{
		Log(LOG_WARNING, "Reply query %s: more than one column in results", GetName().c_str());
	}

	return fResult;
}

//======================================================================
