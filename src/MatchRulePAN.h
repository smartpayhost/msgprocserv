#ifndef _MATCHRULEPAN_H
#define _MATCHRULEPAN_H

//=========================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

#include <string>

class Message;
class BitmapMsg;

//=========================================================

class MatchRulePAN : public MatchRule {
public:
	MatchRulePAN();

	MatchRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	BOOL DoesMatch(const std::string& strTestData, const BitmapMsg *pMsg) const;
	BOOL DoesMatchSQL(const std::string& strQuery, const std::string& strTestData, const BitmapMsg *pMsg) const;
	BOOL DoesMatchFile(const std::string& strFile, const std::string& strTestData) const;

private:
	enum MATCH_WHAT {
		MRP_WHOLE_FIELD,
		MRP_FIRST_N,
		MRP_LAST_N,
		MRP_FROM_M_TO_N
	};

	enum MATCH_WITH {
		MRP_PRESENCE,
		MRP_FIXED_DATA,
		MRP_REGEXP,
		MRP_FILE,
		MRP_SQL
	};

	MATCH_WHAT m_eWhat;
	MATCH_WITH m_eWith;

	int m_iFirst;
	int m_iLast;
	int m_iFrom;
	int m_iTo;

	std::string		m_strMatchData;
};

//=========================================================

#endif // _MATCHRULEPAN_H
