#ifndef _TCPHANDLER_H
#define _TCPHANDLER_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _HANDLERTHREAD_H
#include "HandlerThread.h"
#endif

#ifndef _STATSPACKAGE_H
#include "StatsPackage.h"
#endif

#ifndef _DATAPIPE_H
#include "DataPipe.h"
#endif

class TcpSocket;
class TcpHandler;

//====================================================================

class TcpHandler : public HandlerThread {
public:
	TcpHandler();

	void SetSocket(TcpSocket *s);

	void SetPipeDst(DataPipe *pPipe);
	DataPipe& GetPipeSrc();
	
	void SetAutoConnect(BOOL fAuto);
	void SetDontQueue(BOOL fDontQueue);

	StatsPackage m_sStats;

protected:
    DWORD ThreadMemberFunc();

private:
	class TcpThread : public StoppableThread {
	public:
		TcpThread() : m_s(NULL), m_pPipe(NULL) {}

		void SetSocket(TcpSocket *s) { m_s = s; }
		void SetPipe(DataPipe *pPipe) { m_pPipe = pPipe; }
		void SetTcpHandler(TcpHandler *pHandler) { m_pHandler = pHandler; }

	protected:
		TcpSocket *m_s;
		DataPipe *m_pPipe;
		TcpHandler *m_pHandler;
	};

	class TcpInThread : public TcpThread {
	protected:
		DWORD ThreadMemberFunc();
	};

	class TcpOutThread : public TcpThread {
	protected:
		DWORD ThreadMemberFunc();
	};

	TcpInThread m_tIn;
	TcpOutThread m_tOut;

    DWORD DoOnline();

	BOOL m_fAutoConnect;
	BOOL m_fDontQueue;
	TcpSocket *m_s;
	DataPipe m_pipeSrc;
};

//====================================================================

#endif // _TCPHANDLER_H
