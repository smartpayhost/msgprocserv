#include "stdafx.h"
#include "MatchTemplate.h"

#include "MsgProcRuleCollection.h"
#include "MatchTemplateCollection.h"
#include "ReplyTemplateCollection.h"
#include "MatchRule.h"
#include "Message.h"

#include <assert.h>
#include <algorithm>

using std::string;
using std::for_each;

//=========================================================

MatchTemplate::MatchTemplate()
:	m_pMsgProcRules(NULL),
	m_eAction(Action::NONE)
{
}

//=========================================================

void MatchTemplate::AddRule(MatchRule* pRule)
{
	m_vectRules.push_back(boost::shared_ptr<MatchRule>(pRule));
}

//=========================================================

BOOL MatchTemplate::Export(string &strDesc)
{
	string strTmp;

	strDesc = "MATCH_TEMPLATE ";
	StrUtil::PurifyString(m_strTemplateName, strTmp);
	strDesc += strTmp;
	strDesc += '\n';

	RuleVect::const_iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
	{
		if(!(*i)->Export(strTmp))
			return FALSE;

		strDesc += '\t';
		strDesc += strTmp;
		strDesc += '\n';
	}

	if(!m_strReplyTemplateName.empty())
	{
		strDesc += "\tREPLY_WITH ";
		StrUtil::PurifyString(m_strReplyTemplateName, strTmp);
		strDesc += strTmp;
		strDesc += '\n';
	}

	if(!m_strDestinationHost.empty())
	{
		strDesc += "\tSEND_TO ";
		StrUtil::PurifyString(m_strDestinationHost, strTmp);
		strDesc += strTmp;
		strDesc += '\n';
	}

	strDesc += "MATCH_TEMPLATE_END\n";

	return TRUE;
}

//=========================================================

void MatchTemplate::SetMsgProcRuleCollection(MsgProcRuleCollection *pArray)
{
	m_pMsgProcRules = pArray;

	RuleVect::iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
		(*i)->SetMsgProcRuleCollection(pArray);
}

//=========================================================

void MatchTemplate::SetLogger(Logger* pLogger)
{
	LoggingObject::SetLogger(pLogger);

	RuleVect::iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
		(*i)->SetLogger(pLogger);
}

//=========================================================

BOOL MatchTemplate::DoesMatch(Message &rMsg) const
{
	Log(LOG_DUMP, "Checking against match template %s", GetName().c_str());

	RuleVect::const_iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
	{
		if(!(*i)->DoesMatch(rMsg))
			return FALSE;
	}

	return TRUE;
}

//=========================================================

BOOL MatchTemplate::Check() const
{
	assert(m_pMsgProcRules);

	BOOL fRetval = TRUE;

	RuleVect::const_iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
	{
		if(!(*i)->Check())
			fRetval = FALSE;
	}

	if(m_eAction == Action::RESPONSE)
	{
		if(m_strReplyTemplateName.empty())
		{
			Log(LOG_WARNING, "Match template %s: reply template not specified", m_strTemplateName.c_str());
			fRetval = FALSE;
		}
		else
		{
			if(!m_pMsgProcRules->GetReplyTemplateCollection().FindByName(m_strReplyTemplateName))
			{
				Log(LOG_WARNING, "Reply template %s not defined", m_strReplyTemplateName.c_str());
				fRetval = FALSE;
			}
		}

		if(!m_strDestinationHost.empty())
		{
			Log(LOG_WARNING, "Match template %s: both response and upstream actions specified, using response", m_strTemplateName.c_str());
			fRetval = FALSE;
		}
	}

	if(m_eAction == Action::UPSTREAM)
	{
		if(m_strDestinationHost.empty())
		{
			Log(LOG_WARNING, "Match template %s: upstream host not specified", m_strTemplateName.c_str());
			fRetval = FALSE;
		}
		else
		{
			if(!m_pMsgProcRules->GetHostDescriptionCollection().FindByName(m_strDestinationHost))
			{
				Log(LOG_WARNING, "Upstream host %s not defined", m_strDestinationHost.c_str());
				fRetval = FALSE;
			}
		}

		if(!m_strReplyTemplateName.empty())
		{
			Log(LOG_WARNING, "Match template %s: both response and upstream actions specified, using upstream", m_strTemplateName.c_str());
			fRetval = FALSE;
		}
	}

	return fRetval;
}

//=========================================================
