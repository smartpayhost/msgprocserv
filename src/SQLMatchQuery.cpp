#include "stdafx.h"
#include "SQLMatchQuery.h"

#include "StrUtil.h"
#include "SQLResultTable.h"

using std::string;

//======================================================================

BOOL SQLMatchQuery::Export(string &rStrDest) const
{
	string strTmp;

	rStrDest = "MATCH_QUERY ";
	StrUtil::PurifyString(GetName(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "\tDATA_SOURCE ";
	StrUtil::PurifyString(GetDataSource(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "\tQUERY ";
	StrUtil::PurifyString(GetQuery(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "MATCH_QUERY_END\n";

	return TRUE;
}

//======================================================================

BOOL SQLMatchQuery::ExecQuery(SQLResultTable &rResult, const BitmapMsg &rOrigMsg) const
{
	BOOL fResult = SQLMessageQuery::ExecQuery(rResult, &rOrigMsg, NULL);

	Log(LOG_INFO, "Match query %s: %d results", GetName().c_str(), rResult.GetNumRows());

	if(rResult.GetNumCols() > 1)
	{
		Log(LOG_WARNING, "Match query %s: more than one column in results", GetName().c_str());
	}

	return fResult;
}

//======================================================================
