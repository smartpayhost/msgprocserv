#ifndef _MSGPROCAPP_H
#define _MSGPROCAPP_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SERVICEAPP_H
#include "ServiceApp.h"
#endif

#ifndef _MSGPROCHANDLER_H
#include "MsgProcHandler.h"
#endif

#ifndef _MSGPROCCFG_H
#include "MsgProcCfg.h"
#endif

#ifndef _MSGPROCLOGGER_H
#include "MsgProcLogger.h"
#endif

#include <sql.h>
#include <sqlext.h>

//==============================================================

class MsgProcApp : public ServiceApp {
public:
	MsgProcApp();
	virtual ~MsgProcApp();

	virtual int InitialSetup(int argc, char **argv);

protected:
	virtual BOOL Setup();
	virtual BOOL Main();
	virtual void Cleanup();

private:
	MsgProcHandler m_tMsgProc;
	MsgProcCfg m_cfgConfig;
	MsgProcLogger m_mainLogger;	// logs to Windows event log, file, stderr (if not service), and database (if configured)
	MsgProcLogger m_fileLogger;	// same but doesn't log to database
};

//==============================================================

#endif // _MSGPROCAPP_H
