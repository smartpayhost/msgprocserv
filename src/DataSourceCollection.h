#ifndef _DATASOURCECOLLECTION_H
#define _DATASOURCECOLLECTION_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _DATASOURCE_H
#include "DataSource.h"
#endif

#include <map>
#include <boost/utility.hpp>
#include <boost/smart_ptr.hpp>

class RuleEntity;
class Logger;
class MsgProcRuleCollection;

//======================================================================

class DataSourceCollection : public RuleEntityDescriptor, boost::noncopyable {
public:
	DataSourceCollection();

	BOOL IsStart(const Line &rLine) const;
	RuleEntity *MakeNew(const Line &rLine);

	void AddDataSource(const DataSource &rDataSource);
	void Register(RuleFile &rRuleFile);
	BOOL Export(std::string &rStrDest);
	void MergeFrom(const DataSourceCollection& rhs);

	const DataSource* FindByName(const std::string& strName) const;

	void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray);
	void SetLogger(Logger *pLogger);

	BOOL ConnectAll();

private:
	typedef std::map<std::string, boost::shared_ptr<DataSource> > DataSourceMap;
	DataSourceMap m_mapDataSources;

	MsgProcRuleCollection *m_pMsgProcRules;
	Logger *m_pLogger;
};

//======================================================================

#endif // _DATASOURCECOLLECTION_H
