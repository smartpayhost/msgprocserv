#include "stdafx.h"
#include "MatchRuleActionQuery.h"

#include "MsgProcRuleCollection.h"
#include "SQLActionQuery.h"
#include "SQLActionQueryCollection.h"
#include "SQLResultTable.h"
#include "Message.h"

#include <assert.h>

using std::string;

//======================================================================

MatchRule* MatchRuleActionQuery::Clone() const
{
	return new MatchRuleActionQuery(*this);
}

//======================================================================
//
//	[ACTION] QUERY [ NOT ] <queryname>
//

BOOL MatchRuleActionQuery::Import(const Line &rLine)
{
	m_bInverted = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) == "ACTION")
	{
		if(++i == rLine.end()) return FALSE;
	}

	if((*i) != "QUERY") return FALSE;

	if(++i == rLine.end()) return FALSE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	m_strQuery = (*i);

	return TRUE;
}

//======================================================================
//
//	[ACTION] QUERY [ NOT ] <queryname>
//

BOOL MatchRuleActionQuery::Export(string &strDesc)
{
	string strTmp;

	strDesc = "QUERY";

	if(m_bInverted)
		strDesc += " NOT";

	StrUtil::PurifyString(m_strQuery, strTmp);
	strDesc += ' ';
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL MatchRuleActionQuery::DoesMatch(Message &rMsg) const
{
	assert(m_pMsgProcRules);

	BOOL bRetVal = FALSE;
	const SQLActionQuery *pQuery;

	pQuery = m_pMsgProcRules->GetSQLActionQueryCollection().FindByName(m_strQuery);

	if(pQuery)
	{
		SQLResultTable rResult;

		if(pQuery->ExecQuery(rResult, &rMsg.m_sBMsg, NULL) && rResult.GetNumRows())
			bRetVal = TRUE;
	}

	if(m_bInverted)
		return !bRetVal;
	else
		return bRetVal;
}

//======================================================================

BOOL MatchRuleActionQuery::Check() const
{
	assert(m_pMsgProcRules);

	if(m_pMsgProcRules->GetSQLActionQueryCollection().FindByName(m_strQuery))
		return TRUE;

	Log(LOG_WARNING, "Action query %s not defined", m_strQuery.c_str());

	return FALSE;
}

