#ifndef _REPLYRULEFIELD_H
#define _REPLYRULEFIELD_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULE_H
#include "ReplyRule.h"
#endif

#ifndef _BITMAPMSG_H
#include "BitmapMsg.h"
#endif

#ifndef _STRUTIL_H
#include "StrUtil.h"
#endif

//======================================================================

class ReplyRuleField : public ReplyRule {
public:
	ReplyRuleField(int iFieldNum, const BitmapMsgStyle *pStyle);

	static ReplyRuleField* MakeNew(const BitmapMsgStyle *pStyle, const Line &rLine);

	virtual BOOL Import(const Line &rLine) = 0;
	virtual BOOL Export(std::string &strDesc) = 0;

	inline int GetFieldNum() const { return m_iFieldNum; };
	inline void SetFieldNum(int iNum) { m_iFieldNum = iNum; };

private:
	int	m_iFieldNum;
};

//======================================================================

#endif // _REPLYRULEFIELD_H
