#ifndef _MATCHRULE_H
#define _MATCHRULE_H

//=========================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

class MsgProcRuleCollection;
class Message;
class Line;

//=========================================================

class MatchRule : public LoggingObject {
public:
	MatchRule();
	virtual ~MatchRule() = 0 {}

	static MatchRule* MakeNew(const Line &rLine);

	virtual MatchRule* Clone() const = 0;

	virtual BOOL Check() const;

	BOOL Import(const std::string &strDesc);
	virtual BOOL Import(const Line &rLine) = 0;
	virtual BOOL Export(std::string &strDesc) = 0;
	
	virtual BOOL DoesMatch(Message& rMsg) const = 0;

	inline const MsgProcRuleCollection *GetMsgProcRuleCollection() const { return m_pMsgProcRules; }
	inline void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray) { m_pMsgProcRules = pArray; }

protected:
	BOOL m_bInverted;

	MsgProcRuleCollection *m_pMsgProcRules;
};

//=========================================================

#endif // _MATCHRULE_H
