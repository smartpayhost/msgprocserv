#include "stdafx.h"
#include "MatchRuleFieldPresent.h"

#include "Message.h"
#include "StrUtil.h"

using std::string;

//======================================================================

MatchRuleFieldPresent::MatchRuleFieldPresent()
:	m_iFieldNum(0)
{
}

//---------------------------------------------------------

MatchRule* MatchRuleFieldPresent::Clone() const
{
	return new MatchRuleFieldPresent(*this);
}

//======================================================================
//
//	FIELD <number>
//	FIELD <number> [ NOT ] PRESENT

BOOL MatchRuleFieldPresent::Import(const Line &rLine)
{
	m_bInverted = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	m_iFieldNum = atoi((*i).c_str());

	if((m_iFieldNum < 1)||(m_iFieldNum > 128))
		return FALSE;

	if(++i == rLine.end())
		return TRUE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	if((*i) != "PRESENT")
		return FALSE;

	return TRUE;
}

//======================================================================
//
//	FIELD <number> [ NOT ] PRESENT

BOOL MatchRuleFieldPresent::Export(string &strDesc)
{
	StrUtil::Format(strDesc, "FIELD %d", m_iFieldNum);

	if(m_bInverted)
		strDesc += " NOT";

	strDesc += " PRESENT";

	return TRUE;
}

//======================================================================

BOOL MatchRuleFieldPresent::DoesMatch(Message &rMsg) const
{
	BOOL bRetVal = TRUE;

	if((m_iFieldNum < 1)||(m_iFieldNum > 128))
		bRetVal = FALSE;

	if(rMsg.m_sBMsg.GetField(m_iFieldNum) == NULL)
		bRetVal = FALSE;

	if(m_bInverted)
		return !bRetVal;
	else
		return bRetVal;
}

//======================================================================
