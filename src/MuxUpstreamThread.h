#ifndef _MUXUPSTREAMTHREAD_H
#define _MUXUPSTREAMTHREAD_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "StoppableThread.h"
#include "DataPipe.h"

#include "MsgProcCfg.h"

//==============================================================

class MuxUpstreamThread : public StoppableThread {
public:
	MuxUpstreamThread(MsgProcCfg &config);

	DataPipe& GetPipeUpSrc();
	void SetPipeUpDst(DataPipe *pPipe);

	BOOL AttachDownstream(class TcpMuxDownstreamHandler *tmdHandler);
	void DetachDownstream(BYTE bIndex);

	BOOL SetMaxThreads(int iMaxThreads);

	MsgProcCfg &GetConfig() const { return m_config; };

protected:
    DWORD ThreadMemberFunc();

private:
	enum { MAX_MUXDOWNSTREAMTHREADS = 256 };

	DataPipe m_pipeUpSrc;
	DataPipe *m_pPipeUpDst;
	DataPipe m_pipeDnSrc;

	int m_iMaxThreads;
	int m_iTableFree;
	BYTE m_bIndex;
	BYTE m_bInstance;

	CritSect m_lockPipeDnDstTable;
	DataPipe *m_pPipeDnDstTable[MAX_MUXDOWNSTREAMTHREADS];

	void LogReceivedUpstreamPacket(BYTE *pBuf, int iLen);
	BOOL SetPipeDnDst(DataPipe *pPipe, BYTE &bIndex, BYTE &bInstance);

	MsgProcCfg &m_config;
};

//==============================================================

#endif // _MUXUPSTREAMTHREAD_H
