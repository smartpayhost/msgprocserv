#include "stdafx.h"
#include "DataSource.h"

#include "bcd.h"
#include "StrUtil.h"
#include "Timer.h"

#include "OdbcStatement.h"
#include "OdbcConnection.h"

#include <assert.h>

using std::string;

//======================================================================
//======================================================================

DataSource::DataSource()
:	m_dwTimeout(0),
	m_pMsgProcRules(NULL),
	m_pConnectionManager(new OdbcConnectionManager)
{
}

//======================================================================

BOOL DataSource::Export(string &rStrDest)
{
	string strTmp;

	rStrDest = "DATA_SOURCE ";
	StrUtil::PurifyString(GetName(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "\tDSN ";
	StrUtil::PurifyString(GetDSN(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	if(!m_strUID.empty())
	{
		rStrDest += "\tUSER ";
		StrUtil::PurifyString(GetUID(), strTmp);
		rStrDest += strTmp;
		rStrDest += '\n';

		if(!m_strAuthStr.empty())
		{
			rStrDest += "\tPASSWORD ";
			StrUtil::PurifyString(GetAuthStr(), strTmp);
			rStrDest += strTmp;
			rStrDest += '\n';
		}
	}

	if(!m_strTraceFile.empty())
	{
		rStrDest += "\tTRACEFILE ";
		StrUtil::PurifyString(GetTraceFile(), strTmp);
		rStrDest += strTmp;
		rStrDest += '\n';
	}

	if(m_dwTimeout)
	{
		StrUtil::Format(strTmp, "\tTIMEOUT %ul\n", m_dwTimeout);
		rStrDest += strTmp;
	}

	rStrDest += "DATA_SOURCE_END\n";

	return TRUE;
}

//======================================================================

void DataSource::SetName(const string &strName)
{
	m_strName = strName;
}

void DataSource::SetDSN(const string &strDSN)
{
	m_strDSN = strDSN;
	m_pConnectionManager->SetDSN(strDSN.c_str());
}

void DataSource::SetUID(const string &strUID)
{
	m_strUID = strUID;
	m_pConnectionManager->SetUID(strUID.c_str());
}

void DataSource::SetAuthStr(const string &strAuthStr)
{
	m_strAuthStr = strAuthStr;
	m_pConnectionManager->SetAuthStr(strAuthStr.c_str());
}

void DataSource::SetTimeout(DWORD dwTimeout)
{
	m_dwTimeout = dwTimeout;
}

void DataSource::SetTraceFile(const string &strTraceFile)
{
	m_strTraceFile = strTraceFile;
	m_pConnectionManager->SetTraceFile(strTraceFile.c_str());
}

//======================================================================
//======================================================================

BOOL DataSource::ExecQuery(const string &strQuery, SQLResultTable &rResult) const
{
	OdbcConnection *pConnection = m_pConnectionManager->GetConnection();

	if(!pConnection)
	{
		return FALSE;
	}

	pConnection->SetLogger(GetLogger());

	OdbcStatement stmt(pConnection);

	stmt.SetLogger(GetLogger());

	if(!stmt.ExecQuery(strQuery, m_dwTimeout))
	{
		return FALSE;
	}

	if(!stmt.FetchAllResults(rResult))
	{
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL DataSource::Connect()
{
	return m_pConnectionManager->GetConnection()?TRUE:FALSE;
}

//======================================================================

void DataSource::SetLogger(Logger* pLogger)
{
	SQLLoggingObject::SetLogger(pLogger);
	m_pConnectionManager->SetLogger(pLogger);
}

//======================================================================
