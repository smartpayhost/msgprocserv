#ifndef _LOGGERSTDIO_H
#define _LOGGERSTDIO_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGER_H
#include "Logger.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

//==============================================================

class LoggerStdio : public Logger {
public:
	LoggerStdio(const std::string& m_strLogName, int m_iLogLevel, FILE *fp = NULL);
	LoggerStdio(const LoggerStdio& rhs);
	LoggerStdio& operator=(const LoggerStdio& rhs);
	virtual ~LoggerStdio() = 0 {}

	virtual int vLog(enum LogLevel, const char *, va_list);

protected:
	void SetOutputStream(FILE* fp) { m_fp = fp; }

private:
	CritSect	m_Lockable;

	FILE *m_fp;
};

//==============================================================

#endif // _LOGGERSTDIO_H
