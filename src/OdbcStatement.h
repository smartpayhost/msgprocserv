#ifndef _ODBCSTATEMENT_H
#define _ODBCSTATEMENT_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SQLLOGGINGOBJECT_H
#include "SQLLoggingObject.h"
#endif

#include <boost/smart_ptr.hpp>
#include <boost/utility.hpp>

class OdbcConnection;
class OdbcStatementImpl;
class SQLResultTable;

//======================================================================

class OdbcStatement : public SQLLoggingObject, public boost::noncopyable {
public:
	explicit OdbcStatement(OdbcConnection* pDbc);
	~OdbcStatement();

	BOOL ExecQuery(const std::string &strQuery, DWORD dwTimeout);

	BOOL Prepare(const std::string &strQuery, DWORD dwTimeout);
	BOOL BindParam(SQLUSMALLINT paramNum, SQLSMALLINT ioType, SQLSMALLINT valueType, SQLSMALLINT paramType,
		SQLULEN colSize, SQLSMALLINT decDigits, SQLPOINTER pvaluePtr, SQLLEN buflen, SQLLEN* strlen_or_indptr);

	BOOL SetNumRows(WORD wRows);

	BOOL Execute(DWORD dwTimeout);

	BOOL FetchAllResults(SQLResultTable &rTable);

	void Release();

	void SetLogger(Logger* pLogger);

private:
	OdbcStatementImpl* m_pImpl;
	OdbcConnection* m_pDbc;
};

//======================================================================

#endif // _ODBCSTATEMENT_H
