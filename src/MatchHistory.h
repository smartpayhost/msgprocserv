#ifndef _MATCHHISTORY_H
#define _MATCHHISTORY_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <map>
#include <string>
#include <boost/utility.hpp>

//==============================================================

class MatchHistory : public boost::noncopyable {
public:
	BOOL MatchSeen(const std::string& strMatchName) const;
	BOOL MatchResult(const std::string& strMatchName) const;

	void SaveResult(const std::string& strMatchName, BOOL fResult);

private:
	typedef std::map<std::string, BOOL> MatchMap;
	MatchMap m_mapMatches;
};

//==============================================================

#endif // _MATCHHISTORY_H
