#include "stdafx.h"
#include "ReplyRuleFieldProg.h"

#include "ReplyRuleFieldFile.h"
#include "ReplyRuleFieldAscFile.h"
#include "TmpFileCollection.h"

#include "SQLReplyQuery.h"
#include "SQLResultTable.h"
#include "MsgProcRuleCollection.h"

#include <assert.h>

#include <fstream>
#include <sstream>
#include <iomanip>

#include <process.h>

using std::string;
using std::ofstream;
using std::ios_base;
using std::transform;
using std::vector;
using std::stringstream;
using std::pair;

//======================================================================

ReplyRuleFieldProg::ReplyRuleFieldProg(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldProg::Clone() const
{
	return new ReplyRuleFieldProg(*this);
}

//======================================================================
//
//	FIELD <field_num> PROG <cmdline>

BOOL ReplyRuleFieldProg::Import(const Line &rLine)
{
	m_strCmdline.erase();

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "PROG")
	{
		if(++i == rLine.end()) return FALSE;

		m_strCmdline = (*i);
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldProg::Export(string &strDesc)
{
	string strTmp;

	StrUtil::Format(strDesc, "FIELD %d PROG ", GetFieldNum());
	StrUtil::PurifyString(m_strCmdline, strTmp);
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldProg::CreateSourceFile(const string& strSrcName, int iField, const BitmapMsg *pMsg) const
{
	ofstream of;

	of.open(strSrcName.c_str(), ios_base::out | ios_base::trunc | ios_base::binary);

	if(of.bad())
	{
		char szErr[100];

		strerror_s(szErr, sizeof(szErr), errno);

		Log(LOG_ERROR, "Failed to create temp file %s: %s", strSrcName.c_str(), szErr);
		return FALSE;
	}

	if((iField >= 1) && (iField <= 128))
	{
		const BitmapMsgField *pOrigField = pMsg->GetField(iField);

		if(pOrigField != NULL)
		{
			if(pOrigField->GetFieldLen() != 0)
			{
				of.write((const char *)(pOrigField->GetField()), pOrigField->GetFieldLen());

				if(of.bad())
				{
					char szErr[100];

					strerror_s(szErr, sizeof(szErr), errno);

					Log(LOG_ERROR, "Error writing to temp file %s: %s", strSrcName.c_str(), szErr);
					of.close();
					return FALSE;
				}
			}
		}
	}
	else if(iField == 200)
	{
		of << std::setw(4) << std::setfill('0') << pMsg->GetMsgType();

		if(of.bad())
		{
			char szErr[100];

			strerror_s(szErr, sizeof(szErr), errno);

			Log(LOG_ERROR, "Error writing to temp file %s: %s", strSrcName.c_str(), szErr);
			return FALSE;
		}
	}
	else if(iField == 201)
	{
		of << pMsg->GetPAN();
	}
	else if(iField == 202)
	{
		of << pMsg->GetExpiryYYMM();
	}

	of.close();

	return TRUE;
}

//======================================================================

static BOOL ParseArg(const string& strArg, string& strType, string& strParam)
{
	string::size_type iSep = strArg.find_first_of(":}", 1);

	if((iSep == string::npos) || (*(strArg.end() - 1) != '}'))
	{
		return FALSE;
	}

	strType = strArg.substr(1, iSep - 1);

	if((strArg[iSep] == ':') && (strArg[iSep + 1] != '}'))
	{
		strParam = strArg.substr(iSep + 1, strArg.size() - iSep - 2);
	}

	transform(strType.begin(), strType.end(), strType.begin(), toupper);

	return TRUE;
}

//======================================================================

static int InterpretFieldName(const string& strParam)
{
	if(!strParam.empty())
	{
		string strP(strParam);

		transform(strP.begin(), strP.end(), strP.begin(), toupper);

		if(strP == "MSGTYPE")
			return 200;
		else if(strP == "PAN")
			return 201;
		else if(strP == "EXPDATE")
			return 202;
		else
		{
			if(strP[0] == 'F')
			{
				int iField = atoi(strP.c_str() + 1);

				if((iField >= 1) && (iField <= 128))
					return iField;
			}
		}
	}

	return 0;
}

//======================================================================

BOOL ReplyRuleFieldProg::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	int i;
	BOOL fResult;
	int iResult;
	BOOL fDstFileSeen = FALSE;
	BOOL fDstFileAsc = FALSE;
	
	SZFN(ReplyRuleFieldProg);
	Log(LOG_DEBUG, "%s: %s", szFn, m_strCmdline.c_str());

	char tname[MAX_PATH];
	tmpnam_s(tname, sizeof(tname));
	string strDstName(tname);

	//-----------------------
	// Create source file(s)

	string strExpandedCmdLine;
	Line lineArgs;
	TmpFileCollection vectTmpFiles;
	vector<pair<int, string> > vectDstFiles;
	vector<pair<int, string> > vectDstAscFiles;

	vectTmpFiles.push_back(strDstName);

	StrUtil::StringToLine(m_strCmdline, lineArgs);

	// look for %, $, & arguments and replace with filenames

	int iNumArgs = (int)lineArgs.size();

	for(i = 0; i < iNumArgs; i++)
	{
		string strArg = lineArgs[i];

		if((strArg[0] == '%')||(strArg[0] == '$'))
		{
			const BitmapMsg *pMsg;

			if(strArg[0] == '%')
				pMsg = &rNewMsg;
			else
				pMsg = &rOrigMsg;

			int iField = atoi(strArg.c_str() + 1);

			if(iField == 0)
			{
				lineArgs[i] = strDstName;
				fDstFileSeen = TRUE;
			}
			else if(((iField >= 1) && (iField <= 128)) || ((iField >= 200) && (iField <= 202)))
			{
				char tname[MAX_PATH];
				tmpnam_s(tname, sizeof(tname));
				string strSrcName(tname);

				if(!CreateSourceFile(strSrcName, iField, pMsg))
					return FALSE;

				vectTmpFiles.push_back(strSrcName);
				lineArgs[i] = strSrcName;
			}
			else
			{
				Log(LOG_ERROR, "Invalid field number (%d) in cmdline", iField);
				return FALSE;
			}
		}
		else if(strArg[0] == '&')
		{
			int iField = atoi(strArg.c_str() + 1);

			if(iField == GetFieldNum())
			{
				lineArgs[i] = strDstName;
				fDstFileSeen = TRUE;
			}
			else if((iField >= 1) && (iField <= 128))
			{
				char tname[MAX_PATH];
				tmpnam_s(tname, sizeof(tname));
				string strName(tname);
				vectTmpFiles.push_back(strName);
				vectDstFiles.push_back(pair<int, string>(iField, strName));
				lineArgs[i] = strName;
			}
			else
			{
				Log(LOG_ERROR, "Invalid field number (%d) in cmdline", iField);
				return FALSE;
			}
		}
		else if(strArg[0] == '{')
		{
			// split {type:param} form into type and (optional) param strings.

			string strType;
			string strParam;

			if(!ParseArg(strArg, strType, strParam))
			{
				Log(LOG_ERROR, "Invalid argument (%s) in cmdline", strArg.c_str());
				return FALSE;
			}

			int iField = InterpretFieldName(strParam);

			// interpret type & param

			if((strType == "REQ") || (strType == "RSP"))
			{
				const BitmapMsg *pMsg;
				BOOL fGetFromNew;

				if(strType == "REQ")
				{
					pMsg = &rOrigMsg;
					fGetFromNew = FALSE;
				}
				else
				{
					pMsg = &rNewMsg;
					fGetFromNew = TRUE;
				}

				if((iField >= 1) && (iField <= 128))
				{
					const BitmapMsgField *pField = pMsg->GetField(iField);

					if(pField && pField->GetFieldLen())
					{
						lineArgs[i] = pField->GetAsciiRep();
					}
					else
					{
						Log(LOG_WARNING, "Field (%d) in %s message empty or missing", 
							iField, fGetFromNew?"response":"request");
						return FALSE;
					}
				}
				else if(iField == 200)
				{
					stringstream ss;

					ss << std::setw(4) << std::setfill('0') << pMsg->GetMsgType();

					lineArgs[i] = ss.str();
				}
				else if(iField == 201)
				{
					lineArgs[i] = pMsg->GetPAN();
				}
				else if(iField == 202)
				{
					lineArgs[i] = pMsg->GetExpiryYYMM();
				}
				else
				{
					Log(LOG_ERROR, "Invalid argument (%s) in cmdline", strArg.c_str());
					return FALSE;
				}
			}
			else if((strType == "REQF") || (strType == "RSPF"))
			{
				if(iField)
				{
					char tname[MAX_PATH];
					tmpnam_s(tname, sizeof(tname));
					string strSrcName(tname);

					if(!CreateSourceFile(strSrcName, iField, (strType == "REQF")?&rOrigMsg:&rNewMsg))
						return FALSE;

					vectTmpFiles.push_back(strSrcName);
					lineArgs[i] = strSrcName;
				}
				else
				{
					Log(LOG_ERROR, "Invalid argument (%s) in cmdline", strArg.c_str());
					return FALSE;
				}
			}
			else if(strType == "RESULT")
			{
				if((strParam.empty()) || (iField == GetFieldNum()))
				{
					lineArgs[i] = strDstName;
					fDstFileSeen = TRUE;
				}
				else if((iField >= 1) && (iField <= 128))
				{
					char tname[MAX_PATH];
					tmpnam_s(tname, sizeof(tname));
					string strName(tname);
					vectTmpFiles.push_back(strName);
					vectDstFiles.push_back(pair<int, string>(iField, strName));
					lineArgs[i] = strName;
				}
				else
				{
					Log(LOG_ERROR, "Invalid argument (%s) in cmdline", strArg.c_str());
					return FALSE;
				}
			}
			else if(strType == "ASCRESULT")
			{
				if((strParam.empty()) || (iField == GetFieldNum()))
				{
					lineArgs[i] = strDstName;
					fDstFileSeen = TRUE;
					fDstFileAsc = TRUE;
				}
				else if((iField >= 1) && (iField <= 128))
				{
					char tname[MAX_PATH];
					tmpnam_s(tname, sizeof(tname));
					string strName(tname);
					vectTmpFiles.push_back(strName);
					vectDstAscFiles.push_back(pair<int, string>(iField, strName));
					lineArgs[i] = strName;
				}
				else
				{
					Log(LOG_ERROR, "Invalid argument (%s) in cmdline", strArg.c_str());
					return FALSE;
				}
			}
			else if(strType == "QUERY")
			{
				assert(GetMsgProcRuleCollection());

				const SQLReplyQuery *pQuery = GetMsgProcRuleCollection()->GetSQLReplyQueryCollection().FindByName(strParam);

				if(!pQuery)
				{
					Log(LOG_ERROR, "Invalid argument (%s) in cmdline (query not found)", strArg.c_str());
					return FALSE;
				}

				SQLResultTable tblResult;

				if(!pQuery->ExecQuery(tblResult, rOrigMsg, rNewMsg))
				{
					Log(LOG_ERROR, "Query failed: %s", strArg.c_str());
					return FALSE;
				}

				string strResult;

				if(tblResult.GetData(strResult))
				{
					lineArgs[i] = strResult;
				}
				else
				{
					Log(LOG_ERROR, "Query failed, no data returned: %s", strArg.c_str());
					return FALSE;
				}
			}
			else
			{
				Log(LOG_ERROR, "Invalid argument (%s) in cmdline", strArg.c_str());
				return FALSE;
			}
		}
	}
	
	if(!fDstFileSeen)
	{
		Log(LOG_ERROR, "destination file not specified");
		return FALSE;
	}

	const char **pArgs = new const char *[iNumArgs + 2];

	if (pArgs == NULL)
	{
		Log(LOG_ERROR, "%s: Insufficient Heap; iNumArgs = %d", szFn, iNumArgs);
		return FALSE;
	}

	for(i = 0; i < iNumArgs; i++)
	{
		pArgs[i] = lineArgs[i].c_str();
		Log(LOG_DEBUG, "%s: Argument %d = %s", szFn, i, pArgs[i]);
	}

	pArgs[i] = NULL;

	//------------
	// Run program
	Log(LOG_INFO, "%s: Starting %s", szFn, pArgs[0]);

//	iResult = _spawnv(_P_WAIT, pArgs[0], pArgs);
	iResult = (int)_spawnvpe(_P_WAIT, pArgs[0], pArgs, _environ);

	Log(LOG_INFO, "%s: Returned from %s, iResult = %d", szFn, pArgs[0], iResult);

	delete [] pArgs;

	//--------------
	// Check results
	
	if(iResult == 0)
	{
		// Get results from mandatory destination file

		if(fDstFileAsc)
			fResult = ReplyRuleFieldAscFile::BuildReplyFieldExtFile(rNewMsg, rOrigMsg, GetFieldNum(), strDstName);
		else
			fResult = ReplyRuleFieldFile::BuildReplyFieldExtFile(rNewMsg, rOrigMsg, GetFieldNum(), strDstName);

		if(!fResult)
		{
			char szErr[100];

			strerror_s(szErr, sizeof(szErr), errno);

			Log(LOG_ERROR, "Failed to open file %s: %s", strDstName.c_str(), szErr);
		}

		// Get results from optional destination files

		for(i = 0; i < (int)vectDstFiles.size(); i++)
		{
			int iField = vectDstFiles[i].first;
			string strDstName = vectDstFiles[i].second;

			if(!ReplyRuleFieldFile::BuildReplyFieldExtFile(rNewMsg, rOrigMsg, iField, strDstName))
			{
				char szErr[100];

				strerror_s(szErr, sizeof(szErr), errno);

				Log(LOG_WARNING, "Failed to open file %s: %s", strDstName.c_str(), szErr);
			}
		}

		for(i = 0; i < (int)vectDstAscFiles.size(); i++)
		{
			int iField = vectDstAscFiles[i].first;
			string strDstName = vectDstAscFiles[i].second;

			if(!ReplyRuleFieldAscFile::BuildReplyFieldExtFile(rNewMsg, rOrigMsg, iField, strDstName))
			{
				char szErr[100];

				strerror_s(szErr, sizeof(szErr), errno);

				Log(LOG_WARNING, "Failed to open file %s: %s", strDstName.c_str(), szErr);
			}
		}
	}
	else
	{
		Log(LOG_ERROR, "Failed to execute %s, return value = %d", m_strCmdline.c_str(), iResult);
		fResult = FALSE;
	}

	Log(LOG_DEBUG, "%s: Exit, fResult = %s", szFn, fResult ? "TRUE" : "FALSE");

	return fResult;
}

//======================================================================
