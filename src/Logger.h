#ifndef _LOGGER_H
#define _LOGGER_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOG_H
#include "Log.h"
#endif

#include <string>

class BitmapMsg;

//======================================================================

class Logger {
public:
	Logger(const std::string& m_strLogName, int m_iLogLevel);
	virtual ~Logger() = 0 {}

	virtual int vLog(LogLevel, const char *, va_list) = 0;
	virtual int Log(LogLevel eLevel, const char *fmt, ...);
	
	virtual int vLogBitmapMsg(enum LogLevel, const BitmapMsg&, const char *, va_list);
	virtual int LogBitmapMsg(enum LogLevel level, const BitmapMsg& msg, const char *fmt, ...);

	virtual void SetLogName(const std::string& strLogName) { m_strLogName = strLogName; }
	virtual void SetLogLevel(int iLogLevel) { m_iLogLevel = iLogLevel; }

	const char* GetLogName() const { return m_strLogName.c_str(); }
	inline int GetLogLevel() const { return m_iLogLevel; }

	static const char* GetLogLevelString(LogLevel eLevel);

private:
	std::string m_strLogName;
	int m_iLogLevel;
};

//======================================================================

#endif // _LOGGER_H
