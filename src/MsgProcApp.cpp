#include "stdafx.h"
#include "MsgProcApp.h"
#include "version.h"

#include "Registry.h"
#include "MsgProcCfg.h"
#include "MsgProcServ.h"
#include "DataSource.h"
#include "LogDefaults.h"
#include "DataSourceCollection.h"

#include <conio.h>
#include <winsock.h>

//======================================================================

MsgProcApp::MsgProcApp()
:	m_mainLogger(LOGNAME, LOGLEVEL, LOGPATH),
	m_fileLogger(LOGNAME, LOGLEVEL, LOGPATH)
{
	SetLogger(&m_mainLogger);
}

//======================================================================

MsgProcApp::~MsgProcApp()
{
	m_tMsgProc.Shutdown();
}

//======================================================================

int MsgProcApp::InitialSetup(int argc, char **argv)
{
	int iRetval;

	iRetval = ServiceApp::InitialSetup(argc, argv);

	if(iRetval != 0)
		return iRetval;

	ServiceCfg sCfg;

	if(!sCfg.ReadFromFile(GetConfig()))
	{
		Log(LOG_CRITICAL, "Couldn't load config file %s", GetConfig().c_str());
		return -1;
	}

	m_fileLogger.SetLogName(sCfg.GetServiceName());
	m_fileLogger.SetLogPath(sCfg.GetLogFile());
	m_fileLogger.SetLogLevel(sCfg.GetLogLevel());

	m_mainLogger.SetLogName(sCfg.GetServiceName());
	m_mainLogger.SetLogPath(sCfg.GetLogFile());
	m_mainLogger.SetLogLevel(sCfg.GetLogLevel());

	m_mainLogger.SetLogDBDSN(sCfg.GetLogDBDSN());
	m_mainLogger.SetLogDBUID(sCfg.GetLogDBUID());
	m_mainLogger.SetLogDBAuthStr(sCfg.GetLogDBAuthStr());
	m_mainLogger.SetLogDBTraceFile(sCfg.GetLogDBTraceFile());
	m_mainLogger.SetLogDBTimeout(sCfg.GetLogDBTimeout());
	m_mainLogger.SetLogDBLogTypes(sCfg.GetLogDBLogTypes());

	m_mainLogger.SetLogDBLogger(&m_fileLogger);

	m_mainLogger.Start();

#ifdef _DEBUG
//	remove(sCfg.GetLogFile().c_str());
#endif

	m_cfgConfig.SetLogger(GetLogger());
	m_cfgConfig.SetEncryptionType(sCfg.GetEncryptionType());

	return 0;
}

//======================================================================

BOOL MsgProcApp::Setup()
{
	if(!IsRunningAsService())
	{
		m_mainLogger.SetLogToStderr(TRUE);
		m_fileLogger.SetLogToStderr(TRUE);
	}

	Log(LOG_NOTICE, "MsgProcServ %s %s", VERSION, COPYRIGHT);
	Log(LOG_NOTICE, "Running as %s (%s)", GetName().c_str(), GetDisplayName().c_str());
	Log(LOG_NOTICE, "Using config file %s", GetConfig().c_str());
	Log(LOG_NOTICE, "Using registry key %s", GetRegKey().c_str());

	// setup networking

	WSADATA wsaData;

	if(::WSAStartup(MAKEWORD(1, 1), &wsaData) != 0)
	{
		Log(LOG_ALERT, "WSAStartup()");
		return FALSE;
	}

	// setup application

	if(!m_cfgConfig.ReadFromFile(GetConfig()))
	{
		Log(LOG_ALERT, "Error reading config file");
		return FALSE;
	}

	m_tMsgProc.SetLogger(GetLogger());
	m_tMsgProc.SetConfig(&m_cfgConfig);

	// connected to database(s)

	if(!m_cfgConfig.GetDataSourceCollection().ConnectAll())
	{
		Log(LOG_ERROR, "Couldn't connect to all databases");
		return FALSE;
	}
	else
	{
		Log(LOG_INFO, "Connected to all databases");
	}

	// startup

	if(!m_tMsgProc.StartThread())
	{
		Log(LOG_ALERT, "StartThread()");
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL MsgProcApp::Main()
{
	if(!IsRunningAsService())
	{
		while(m_tMsgProc.Wait(1000L) != WAIT_OBJECT_0)
		{
			if(_kbhit())
			{
				GetStopEvent().Set();
				break;
			}
		}
	}

	WaitableCollection wc;

	wc.push_back(GetStopEvent());
	wc.push_back(m_tMsgProc);

	wc.Wait();

	m_tMsgProc.Shutdown();
	m_tMsgProc.WaitForExit();

	m_mainLogger.Stop();

	return TRUE;
}

//======================================================================

void MsgProcApp::Cleanup()
{
	::WSACleanup();
}

//======================================================================
