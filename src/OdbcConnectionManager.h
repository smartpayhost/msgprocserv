#ifndef _ODBCCONNECTIONMANAGER_H
#define _ODBCCONNECTIONMANAGER_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SQLLOGGINGOBJECT_H
#include "SQLLoggingObject.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <boost/utility.hpp>
#include <boost/smart_ptr.hpp>
#include <map>

class OdbcConnection;

//======================================================================

class OdbcConnectionManager : public SQLLoggingObject, public boost::noncopyable {
public:
	OdbcConnectionManager();

	void SetDSN(const char *szDSN);
	void SetUID(const char *szUID);
	void SetAuthStr(const char *szAuthStr);
	void SetTraceFile(const char *szTraceFile);

	OdbcConnection* GetConnection();

private:
	CritSect m_crit;

	std::string m_strDSN;
	std::string m_strUID;
	std::string m_strAuthStr;
	std::string m_strTraceFile;

	typedef std::map<DWORD, boost::shared_ptr<OdbcConnection> > ConnectionMap;
	ConnectionMap m_Connections;
};

//======================================================================

#endif // _ODBCCONNECTIONMANAGER_H
