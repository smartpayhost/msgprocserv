#include "stdafx.h"
#include "StatsPackage.h"

using std::string;

//===============================================================
//===============================================================

StatsPackage::StatsPackage()
:	m_evEvent(FALSE, TRUE)
{
}

//===============================================================

void StatsPackage::SetInt(int iId, int iValue)
{
	Lock localLock(&m_Lockable);

	m_mapIntStats[iId] = iValue;

	m_evEvent.Set();
}

//===============================================================

int StatsPackage::GetInt(int iId)
{
	Lock localLock(&m_Lockable);

	return m_mapIntStats[iId];
}

//===============================================================

int StatsPackage::IncrementInt(int iId)
{
	Lock localLock(&m_Lockable);

	m_evEvent.Set();

	return ++m_mapIntStats[iId];
}

//===============================================================

int StatsPackage::AddInt(int iId, int iAmount)
{
	Lock localLock(&m_Lockable);

	m_evEvent.Set();

	return m_mapIntStats[iId] += iAmount;
}

//===============================================================

void StatsPackage::SetString(int iId, const string& strValue)
{
	Lock localLock(&m_Lockable);

	m_mapStringStats[iId] = strValue;

	m_evEvent.Set();
}

//===============================================================

string StatsPackage::GetString(int iId)
{
	Lock localLock(&m_Lockable);

	return m_mapStringStats[iId];
}

//===============================================================

HANDLE StatsPackage::GetEventHandle()
{
	return m_evEvent;
}

//===============================================================
