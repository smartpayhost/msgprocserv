#include "stdafx.h"
#include "MatchRuleExpDate.h"

#include "MessagePacket.h"
#include "SQLResultTable.h"
#include "SQLMatchQuery.h"
#include "SQLMatchQueryCollection.h"
#include "MsgProcRuleCollection.h"

#include "RegexpMatch.h"

#include <set>
#include <fstream>
#include <time.h>

using std::string;

//======================================================================

MatchRuleExpDate::MatchRuleExpDate()
:	m_eWith(MRED_MATCHES),
	m_iPivot(DEFAULT_PIVOT)
{
}

//---------------------------------------------------------

MatchRule* MatchRuleExpDate::Clone() const
{
	return new MatchRuleExpDate(*this);
}

//======================================================================
//
//	EXPDATE [ [ NOT ] PRESENT ]
//	EXPDATE [ NOT ] EXPIRED [ PIVOT <YYYY> ]
//	EXPDATE [ NOT ] BEFORE <YYMM> [ PIVOT <YYYY> ]
//	EXPDATE [ NOT ] AFTER <YYMM> [ PIVOT <YYYY> ]
//	EXPDATE [ NOT ] MATCHES <YYMM> 
//	EXPDATE [ NOT ] REGEXP <data>
//

BOOL MatchRuleExpDate::Import(const Line &rLine)
{
	m_bInverted = FALSE;

	m_eWith = MRED_PRESENT;
	m_iPivot = DEFAULT_PIVOT;
	m_strMatchData.erase();

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "EXPDATE") return FALSE;

	if(++i == rLine.end()) 
		return TRUE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	// figure out what to test with

	if((*i) == "PRESENT")
	{
		m_eWith = MRED_PRESENT;
	}
	else if((*i) == "EXPIRED")
	{
		m_eWith = MRED_EXPIRED;
	}
	else if((*i) == "BEFORE")
	{
		m_eWith = MRED_BEFORE;
	}
	else if((*i) == "AFTER")
	{
		m_eWith = MRED_AFTER;
	}
	else if((*i) == "MATCHES")
	{
		m_eWith = MRED_MATCHES;
	}
	else if((*i) == "REGEXP")
	{
		m_eWith = MRED_REGEXP;
	}
	else
	{
		return FALSE;
	}

	switch(m_eWith)
	{
	case MRED_BEFORE:
	case MRED_AFTER:
	case MRED_MATCHES:
		if(++i == rLine.end()) return FALSE;
		m_strMatchData = (*i).c_str();

		if(!ValidYYMM(m_strMatchData))		// must be a valid YYMM
			return FALSE;

		break;

	case MRED_REGEXP:
		if(++i == rLine.end()) return FALSE;
		m_strMatchData = (*i).c_str();
		break;
	}

	switch(m_eWith)
	{
	case MRED_BEFORE:
	case MRED_AFTER:
	case MRED_EXPIRED:
		if(++i == rLine.end()) break;

		if((*i) != "PIVOT") return FALSE;

		if(++i == rLine.end()) return FALSE;

		m_iPivot = atoi((*i).c_str());

		break;
	}

	return TRUE;
}

//======================================================================
//
//	EXPDATE [ [ NOT ] PRESENT ]
//	EXPDATE [ NOT ] EXPIRED [ PIVOT <YY> ]
//	EXPDATE [ NOT ] BEFORE <YYMM> [ PIVOT <YY> ]
//	EXPDATE [ NOT ] AFTER <YYMM> [ PIVOT <YY> ]
//	EXPDATE [ NOT ] MATCHES <YYMM> 
//	EXPDATE [ NOT ] REGEXP <data>
//

BOOL MatchRuleExpDate::Export(string &strDesc)
{
	string strTmp;

	strDesc = "EXPDATE";

	if(m_bInverted)
		strDesc += " NOT";

	switch(m_eWith)
	{
	case MRED_PRESENT:	strDesc += " PRESENT";	break;
	case MRED_EXPIRED:	strDesc += " EXPIRED";	break;
	case MRED_BEFORE:	strDesc += " BEFORE";	break;
	case MRED_AFTER:	strDesc += " AFTER";	break;
	case MRED_MATCHES:	strDesc += " MATCHES";	break;
	case MRED_REGEXP:	strDesc += " REGEXP";	break;

	default:
		return FALSE;
	}

	switch(m_eWith)
	{
	case MRED_BEFORE:
	case MRED_AFTER:
	case MRED_MATCHES:
	case MRED_REGEXP:
		StrUtil::PurifyString(m_strMatchData, strTmp);
		strDesc += ' ';
		strDesc += strTmp;
		break;
	}

	switch(m_eWith)
	{
	case MRED_BEFORE:
	case MRED_AFTER:
	case MRED_EXPIRED:
		StrUtil::Format(strTmp, " PIVOT %04d", m_iPivot);
		strDesc += strTmp;
		break;
	}

	return TRUE;
}

//======================================================================

BOOL MatchRuleExpDate::ValidYYMM(const string& strDate, int iPivot, int* piMonthsSince0000) const
{
	if(strDate.length() < 4)
		return FALSE;

	string strYear = strDate.substr(0, 2);
	string strMonth = strDate.substr(2, 2);

	int iYear = atoi(strYear.c_str());
	int iMonth = atoi(strMonth.c_str());

	if((iYear < 0) || (iYear > 99))
		return FALSE;

	if((iMonth < 1) || (iMonth > 12))
		return FALSE;

	if(piMonthsSince0000)
	{
		iMonth--;			// we want months since january

		if(iYear < (iPivot % 100))
			iYear += 100;	// we want years since 0000

		iYear += (iPivot / 100) * 100;

		*piMonthsSince0000 = iYear * 12 + iMonth;
	}

	return TRUE;
}

//======================================================================

int MatchRuleExpDate::CurrentMonthsSince0000() const
{
	time_t t = time(NULL);
	struct tm sCurrentTime;
	localtime_s(&sCurrentTime, &t);

	return sCurrentTime.tm_year * 12 + sCurrentTime.tm_mon + (1900 * 12);
}

//======================================================================

BOOL MatchRuleExpDate::DoesMatch(const string& strTestData, const BitmapMsg * /*pMsg*/) const
{
	BOOL bResult = FALSE;

	int iCardMonth;

	if(!ValidYYMM(strTestData, m_iPivot, &iCardMonth))
	{
		switch(m_eWith)
		{
		case MRED_PRESENT:
		case MRED_MATCHES:
		case MRED_REGEXP:
		case MRED_BEFORE:
		case MRED_AFTER:
			return FALSE;

		case MRED_EXPIRED:
			return TRUE;
		}
	}

	switch(m_eWith)
	{
	case MRED_REGEXP:
		bResult = RegexpMatch(m_strMatchData, strTestData);
		break;

	case MRED_PRESENT:
		if(!strTestData.empty())
			bResult = TRUE;
		break;

	case MRED_EXPIRED:
		if(iCardMonth < CurrentMonthsSince0000())
			bResult = TRUE;
		break;

	case MRED_BEFORE:
	case MRED_AFTER:
		{
			int iTestMonth;

			if(!ValidYYMM(m_strMatchData, m_iPivot, &iTestMonth))
				return TRUE;

			if(m_eWith == MRED_BEFORE)
			{
				if(iCardMonth < iTestMonth)
					bResult = TRUE;
			}
			else
			{
				if(iCardMonth > iTestMonth)
					bResult = TRUE;
			}
		}
		break;

	case MRED_MATCHES:
		if(strTestData == m_strMatchData)
			bResult = TRUE;
		break;

	default:
		return FALSE;
	}

	if(m_bInverted)
		return !bResult;
	else
		return bResult;
}

//======================================================================

BOOL MatchRuleExpDate::DoesMatch(Message& rMsg) const
{
	string strMatchData = rMsg.m_sBMsg.GetExpiryYYMM();

	return DoesMatch(strMatchData, &(rMsg.m_sBMsg));
}

//======================================================================
