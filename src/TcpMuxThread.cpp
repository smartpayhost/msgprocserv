#include "stdafx.h"
#include "TcpMuxThread.h"

#include <sstream>

using std::string;
using std::ostringstream;

//==================================================================

TcpMuxThread::TcpMuxThread(MsgProcCfg &config)
:	m_dwConnectionCounter(0),
	m_evDeadThread(FALSE, FALSE),
	m_evDownstreamShutdown(TRUE, FALSE),
	m_tUpstreamThread(config),
	m_config(config)
{
}

//==================================================================

void TcpMuxThread::SetPipeDst(DataPipe *pPipe)
{
	m_tUpstreamThread.SetPipeUpDst(pPipe);
}

//------------------------------------------------------------------

DataPipe& TcpMuxThread::GetPipeSrc()
{
	return m_tUpstreamThread.GetPipeUpSrc();
}

//------------------------------------------------------------------

void TcpMuxThread::SetDownstreamPort(const string& strPort)
{
	m_strDownstreamPort = strPort;
}

//==================================================================

void TcpMuxThread::DownstreamEnding(TcpMuxDownstreamHandler *pHandler)
{
	m_listEndingThreads.AddTail(pHandler);
	m_listActiveThreads.RemoveElem(pHandler);
	m_evDeadThread.Set();
}

//==================================================================

DWORD TcpMuxThread::ThreadMemberFunc()
{
	SZFN(TcpMuxThread);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if(	(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) ||
		(m_strDownstreamPort.empty()))
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	if(m_sListen.Open() != SOCKRC_OK)
	{
		Log(LOG_ERROR, "%s: ending, error 4: failed to open listen socket", szFn);
		return 4;
	}

	if(m_sListen.SetBlocking(TRUE) != SOCKRC_OK)
	{
		Log(LOG_ERROR, "%s: ending, error 4: failed to set socket option", szFn);
		return 4;
	}

	AddChild(&m_tUpstreamThread);
	AddChild(&m_tListenThread);

	SetupChildren();

	m_tUpstreamThread.SetLoggingName(GetLoggingName() + "/upstream");

	if(!m_tUpstreamThread.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 6: failed to start upstream handler", szFn);
		ShutdownChildren();
		return 6;
	}

	// Listen to downstream port

	if(m_sListen.Listen(m_strDownstreamPort.c_str()) != SOCKRC_OK)
	{
		Log(LOG_ERROR, "%s: ending, error 5: failed to listen on socket", szFn);
		ShutdownChildren();
		return 5;
	}

	m_tListenThread.SetSocket(m_sListen.GetSocketHandle());

	m_tListenThread.SetLoggingName(GetLoggingName() + "/listen");

	if(!m_tListenThread.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 7: failed to start listen thread", szFn);
		ShutdownChildren();
		return 7;
	}

	m_tListenThread.StartListening();

	// Sit back and keep an eye on things.

	WaitableCollection wc;
	wc.push_back(m_tUpstreamThread);
	wc.push_back(m_tListenThread.GetConnectEvent());
	wc.push_back(m_evDeadThread);
	wc.push_back(GetShutdownEvent());

	DWORD dwRetVal = 0;
	BOOL fExit = FALSE;

	while(!fExit)
	{
		DWORD e = wc.Wait();

		switch(e)
		{
		case WAIT_OBJECT_0 + 0:	// upstream dead
			Log(LOG_INFO, "%s: ending, upstream connection lost", szFn);
			fExit = TRUE;
			break;

		case WAIT_OBJECT_0 + 1:	// new downstream connection
			Log(LOG_DEBUG, "%s: starting new downstream handler", szFn);

			if(ProcessNewDownstream())
			{
				Log(LOG_DEBUG, "%s: listening for new connections", szFn);
				m_tListenThread.StartListening();
			}
			else
			{
				Log(LOG_ERROR, "%s: ending, error 9: failed to start new downstream handler", szFn);
				dwRetVal = 9;
				fExit = TRUE;
			}
			break;

		case WAIT_OBJECT_0 + 2:	// dead downstream connection
			Log(LOG_DEBUG, "%s: cleaning up dead downstream handlers", szFn);

			if(!ProcessDeadDownstream())
			{
				Log(LOG_ERROR, "%s: ending, error 10: failed to clean up dead downstream handlers", szFn);
				dwRetVal = 10;
				fExit = TRUE;
			}
			break;

		case WAIT_OBJECT_0 + 3:	// shutdown requested
			Log(LOG_DEBUG, "%s: shutdown requested", szFn);
			fExit = TRUE;
			break;

		default:				// error
			Log(LOG_ERROR, "%s: ending, error 11: unexpected result from WaitForMultipleObjects", szFn);
			dwRetVal = 11;
			fExit = TRUE;
			break;
		}
	}

	Log(LOG_DEBUG, "%s: shutting down downstream threads", szFn);

	ShutdownDownstreamThreads();

	Log(LOG_DEBUG, "%s: shutting down children", szFn);

	ShutdownChildren();

	Log(LOG_DEBUG, "%s: ending", szFn);

	return dwRetVal;
}

//==================================================================

void TcpMuxThread::ClearDeadList()
{
	TcpMuxDownstreamHandler *pHandler;

	while(!m_listEndingThreads.IsEmpty())
	{
		pHandler = (TcpMuxDownstreamHandler *)m_listEndingThreads.RemoveHead();
		pHandler->WaitForExit();
		delete pHandler;
	}
}

//==================================================================

void TcpMuxThread::ShutdownDownstreamThreads()
{
	// Signal to all threads

	m_evDownstreamShutdown.Set();

	// Wait for all downstream threads to die

	while(!m_listActiveThreads.IsEmpty())
	{
		m_evDeadThread.Wait();
		ClearDeadList();
	}

	m_evDownstreamShutdown.Reset();

	ClearDeadList();
}

//==================================================================

BOOL TcpMuxThread::ProcessDeadDownstream()
{
	ClearDeadList();
	return TRUE;
}

//==================================================================

BOOL TcpMuxThread::ProcessNewDownstream()
{
	SOCKET s;

	s = m_sListen.Accept();

	if(s == INVALID_SOCKET)
		return FALSE;

	m_dwConnectionCounter++;

	TcpMuxDownstreamHandler *pHandler;

	pHandler = new TcpMuxDownstreamHandler(this);
	pHandler->SetShutdownEvent(&m_evDownstreamShutdown);
	pHandler->SetActivityEvent(this);
	pHandler->SetLogger(GetLogger());
	pHandler->SetSocket(s);
	pHandler->SetIdleTimeout(m_config.GetDownstreamIdleTimeout());

	ostringstream os;

	os << GetLoggingName() << "/downstream[" << m_dwConnectionCounter << "/" << pHandler->PeerName() << "]";
	pHandler->SetLoggingName(os.str());

	m_listActiveThreads.AddTail(pHandler);

	pHandler->AttachToUpstream(&m_tUpstreamThread);

	return pHandler->StartThread();
}

//==================================================================
