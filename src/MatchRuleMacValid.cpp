#include "stdafx.h"
#include "MatchRuleMacValid.h"

#include "Message.h"
#include "StrUtil.h"

using std::string;

//======================================================================

MatchRuleMacValid::MatchRuleMacValid()
:	m_eValidity(MAC_GOOD)
{
}

//---------------------------------------------------------

MatchRule* MatchRuleMacValid::Clone() const
{
	return new MatchRuleMacValid(*this);
}

//======================================================================
//
//	MAC [ IS ] [ NOT ] < GOOD | BAD | NONE | ABSENT | PRESENT > 
//

BOOL MatchRuleMacValid::Import(const Line &rLine)
{
	m_bInverted = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "MAC") return FALSE;

	if(++i == rLine.end()) return FALSE;

	if((*i) == "IS")
		if(++i == rLine.end()) return FALSE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	if((*i) == "GOOD")
	{
		m_eValidity = MAC_GOOD;
	}
	else if((*i) == "BAD")
	{
		m_eValidity = MAC_BAD;
	}
	else if((*i) == "ABSENT" || (*i) == "NONE")
	{
		m_eValidity = MAC_NONE;
	}
	else if((*i) == "PRESENT")
	{
		m_bInverted = !m_bInverted;
		m_eValidity = MAC_NONE;
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//======================================================================
//
//	MAC [ IS ] [ NOT ] < GOOD | BAD | NONE | ABSENT | PRESENT > 
//

BOOL MatchRuleMacValid::Export(string &strDesc)
{
	strDesc = "MAC IS";

	if(m_bInverted)
		strDesc += " NOT";

	switch(m_eValidity)
	{
	case MAC_GOOD:	strDesc += " GOOD";		break;
	case MAC_BAD:	strDesc += " BAD";		break;
	case MAC_NONE:	strDesc += " ABSENT";	break;
	}

	return TRUE;
}

//======================================================================

BOOL MatchRuleMacValid::DoesMatch(Message &rMsg) const
{
	BOOL fResult;

	rMsg.CheckMac();

	fResult = (m_eValidity == rMsg.m_sBMsg.GetMacValidity());

	if(m_bInverted)
		return !fResult;
	else
		return fResult;
}

//======================================================================
