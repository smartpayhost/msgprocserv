#include "stdafx.h"
#include "ReplyRuleFieldDate.h"

#include <assert.h>

#include <time.h>

using std::string;

//======================================================================

ReplyRuleFieldDate::ReplyRuleFieldDate(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldDate::Clone() const
{
	return new ReplyRuleFieldDate(*this);
}

//======================================================================
//
//	FIELD <field_num> DATE

BOOL ReplyRuleFieldDate::Import(const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "DATE")
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL ReplyRuleFieldDate::Export(string &strDesc)
{
	StrUtil::Format(strDesc, "FIELD %d DATE", GetFieldNum());

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldDate::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	BitmapMsgField *pField = new BitmapMsgField(GetFieldNum(), rOrigMsg.GetMsgStyle());

	assert(pField);

	if(!pField)
		return FALSE;

	char szBuf[5];

	time_t t;
	struct tm now;

	time(&t);
	localtime_s(&now, &t);

	strftime(szBuf, sizeof(szBuf), "%m%d", &now);

	if(!pField->SetFieldFromString(szBuf))
	{
		delete pField;
		return FALSE;
	}

	rNewMsg.SetField(pField);

	return TRUE;
}

//======================================================================
