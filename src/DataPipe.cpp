#include "stdafx.h"
#include "DataPipe.h"

#include <string.h>

//==============================================================

DataPipe::DataPipe()
:	m_Event(FALSE, FALSE),
	m_iTotal(0)
{
}

//--------------------------------------------------------------

DataPipe::~DataPipe()
{
	Flush();
}

//==============================================================

void DataPipe::Put(int iLen, const BYTE *pbBuf)
{
	Lock localLock(&m_Lockable);

	if(iLen)
	{
		ChunkPtr pChunk(new Chunk(iLen, pbBuf));

		m_List.push_back(pChunk);
		m_iTotal += iLen;
	}

	m_Event.Set();
}

//==============================================================

void DataPipe::PutNC(int iLen, BYTE *pbBuf)
{
	Lock localLock(&m_Lockable);

	if(!iLen)
		return;

	ChunkPtr pChunk(new Chunk());

	pChunk->TakeOwnership(iLen, pbBuf);

	m_List.push_back(pChunk);
	m_iTotal += iLen;

	m_Event.Set();
}

//==============================================================

int DataPipe::Get(int iMaxLen, BYTE *pbBuf)
{
	Lock localLock(&m_Lockable);

	int iLen = 0;

	while(!m_List.empty() && (iLen < iMaxLen))
	{
		ChunkPtr pChunk = m_List.front();

		int iRemaining = iMaxLen - iLen;

		int iGot = pChunk->Get(iRemaining, pbBuf);
		iLen += iGot;
		pbBuf += iGot;
		m_iTotal -= iGot;

		if(pChunk->Len() == 0)
		{
			m_List.pop_front();
		}
	}

	if(!m_List.empty())
		m_Event.Set();

	return iLen;
}

//==============================================================

int DataPipe::GetChunk(int iMaxLen, BYTE *pbBuf)
{
	Lock localLock(&m_Lockable);

	if(m_List.empty())
		return 0;

	ChunkPtr pChunk = m_List.front();

	int iLen = pChunk->Len();

	if(iLen > iMaxLen)
		return -1;

	iLen = pChunk->Get(iMaxLen, pbBuf);

	m_iTotal -= iLen;
	m_List.pop_front();

	if(!m_List.empty())
		m_Event.Set();

	return iLen;
}

//==============================================================

int DataPipe::GetChunkNC(BYTE **ppbBuf)
{
	Lock localLock(&m_Lockable);

	if(m_List.empty())
		return 0;

	ChunkPtr pChunk = m_List.front();
	m_List.pop_front();

	int iLen = pChunk->ReleaseOwnership(*ppbBuf);

	m_iTotal -= iLen;

	if(!m_List.empty())
		m_Event.Set();

	return iLen;
}

//==============================================================

void DataPipe::Flush()
{
	Lock localLock(&m_Lockable);

	m_List.clear();

	m_Event.Reset();
}

//==============================================================

int DataPipe::GetTotalSize()
{
	Lock localLock(&m_Lockable);

	return m_iTotal;
}

//==============================================================

int DataPipe::GetNumChunks()
{
	Lock localLock(&m_Lockable);

	return (int)m_List.size();
}

//==============================================================

int DataPipe::GetNextChunkSize()
{
	Lock localLock(&m_Lockable);

	if(m_List.empty())
		return 0;

	ChunkPtr pChunk = m_List.front();

	return pChunk->Len();
}

//==============================================================
//==============================================================

DataPipe::Chunk::Chunk()
:	m_iLen(0),
	m_pbBuf(NULL),
	m_pbPtr(NULL)
{
}

//--------------------------------------------------------------

DataPipe::Chunk::Chunk(int iLen, const BYTE *pbBuf)
:	m_iLen(iLen),
	m_pbBuf(new BYTE[iLen]),
	m_pbPtr(m_pbBuf)
{
	::memcpy(m_pbBuf, pbBuf, iLen);
}

//--------------------------------------------------------------

DataPipe::Chunk::~Chunk()
{
	if(m_pbBuf)
		delete [] m_pbBuf;
}

//--------------------------------------------------------------

void DataPipe::Chunk::TakeOwnership(int iLen, BYTE *pbBuf)
{
	m_iLen = iLen;
	m_pbBuf = pbBuf;
	m_pbPtr = pbBuf;
}

//--------------------------------------------------------------

int DataPipe::Chunk::ReleaseOwnership(BYTE *&rpbBuf)
{
	int iLen = m_iLen;

	if(m_pbPtr != m_pbBuf)
	{
		rpbBuf = new BYTE[iLen];
		::memcpy(rpbBuf, m_pbPtr, iLen);
	}
	else
	{
		rpbBuf = m_pbBuf;
	}

	m_pbBuf = NULL;
	m_iLen = 0;

	return iLen;
}

//--------------------------------------------------------------

int DataPipe::Chunk::Len() const
{
	return m_iLen;
}

//--------------------------------------------------------------

int DataPipe::Chunk::Get(int iLen, BYTE *pbBuf)
{
	if(iLen > m_iLen)
		iLen = m_iLen;

	::memcpy(pbBuf, m_pbPtr, iLen);
	m_pbPtr += iLen;
	m_iLen -= iLen;

	return iLen;
}

//==============================================================
//==============================================================
