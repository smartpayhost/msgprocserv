#ifndef _TCPPIPEHANDLER_H
#define _TCPPIPEHANDLER_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _TCPHANDLER_H
#include "TcpHandler.h"
#endif

#ifndef _TCPSOCKET_H
#include "TcpSocket.h"
#endif

#ifndef _NH1BLKTHREAD_H
#include "NH1BlkThread.h"
#endif

#ifndef _DATAPIPE_H
#include "DataPipe.h"
#endif

#ifndef _NETWORKHEADERTYPE_H
#include "NetworkHeaderType.h"
#endif

#ifndef _NETWORKHEADER_H
#include "NetworkHeader.h"
#endif

#include <string>

//====================================================================

#define MAX_NH_POOL	256

class TcpPipeHandler : public HandlerThread {
public:
	TcpPipeHandler();
	~TcpPipeHandler();

	void SetHost(const std::string& strHostname, const std::string& strPort, const std::string& strName, NetworkHeaderType eHdrType=NetworkHeaderType::NHT_10BYTE);

	DataPipe& GetPipeSrc()	{ return m_pipeUpstream; }
	void SetPipeDst(DataPipe* ppipeDst)	{ m_ppipeDest = ppipeDst; }

protected:
    DWORD ThreadMemberFunc();

private:
	TcpSocket		m_sSocket;
	TcpHandler		m_tTcp;
	NH1BlkThread	m_tNh1;

	DataPipe		m_pipeUpstream;		// requests pass through here from MsgProcThread on the way to TcpOutThread, allowing examination & logging
	DataPipe		m_pipeDownstream;	// responses pass through here from NH1BlkThread on the way to MsgProcThread, allowing examination & logging
	DataPipe*		m_ppipeDest;		// destination pipe (MsgProcThreadMgr)

	std::string		m_strHost;
	std::string		m_strPort;
	std::string		m_strName;
	NetworkHeaderType m_eHdrType;

	WORD			m_wNextHdr;
	NetworkHeader	m_hdrs[MAX_NH_POOL];
	WORD			m_niis[MAX_NH_POOL];
};

//====================================================================

#endif // _TCPPIPEHANDLER_H
