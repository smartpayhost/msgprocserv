#ifndef _LOGGINGOBJECT_H
#define _LOGGINGOBJECT_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOG_H
#include "Log.h"
#endif

#include <string>

class Logger;
class BitmapMsg;

//======================================================================

#define SZFN(x) const std::string strFn( #x "(" + GetLoggingName() + ")" );
#define szFn strFn.c_str()

//======================================================================

class LoggingObject {
public:
	LoggingObject();
    virtual ~LoggingObject() = 0 {}

	virtual void SetLoggingName(const std::string& strName) { m_strName = strName; }
	inline const std::string& GetLoggingName() const { return m_strName; }

	virtual void SetLogger(Logger* pLogger) { m_pLogger = pLogger; }
	inline Logger* GetLogger() const { return m_pLogger; }

protected:
	virtual int Log(enum LogLevel level, const char *fmt, ...) const;
	virtual void LogHexDump(enum LogLevel level, const char *szPrefix, int iLen, const unsigned char *pbData);
	virtual int LogBitmapMsg(enum LogLevel level, const BitmapMsg& msg, const char *fmt, ...) const;
	virtual int LogRawBitmapMsg(enum LogLevel level, int iLen, const BYTE *pData, const char *fmt, ...) const;

private:
	std::string m_strName;
	Logger *m_pLogger;
};

//======================================================================

#endif // _LOGGINGOBJECT_H
