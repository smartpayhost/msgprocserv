#include "stdafx.h"
#include "ReplyTemplateCollection.h"

#include "ReplyTemplateRuleEntity.h"
#include "ReplyRuleMsgType.h"
#include "ReplyRuleField.h"

#include <assert.h>

using std::string;

//======================================================================

ReplyTemplateCollection::ReplyTemplateCollection()
:	m_pMsgProcRules(NULL),
	m_pLogger(NULL)
{
}

//----------------------------------------------------------------------

ReplyTemplateCollection::~ReplyTemplateCollection()
{
	ReplyTemplateVector::iterator i;

	for(i = m_vectTemplates.begin(); i != m_vectTemplates.end(); i++)
		delete (*i);
}

//======================================================================

void ReplyTemplateCollection::Register(RuleFile &rRuleFile)
{
	rRuleFile.RegisterEntityType(this);
}

//======================================================================

void ReplyTemplateCollection::SetLogger(Logger *pLogger)
{
	m_pLogger = pLogger;

	ReplyTemplateVector::iterator i;

	for(i = m_vectTemplates.begin(); i != m_vectTemplates.end(); i++)
		(*i)->SetLogger(m_pLogger);
}

//======================================================================

void ReplyTemplateCollection::SetMsgProcRuleCollection(MsgProcRuleCollection *pArray)
{
	m_pMsgProcRules = pArray;

	ReplyTemplateVector::const_iterator i;

	for(i = m_vectTemplates.begin(); i != m_vectTemplates.end(); i++)
		(*i)->SetMsgProcRuleCollection(m_pMsgProcRules);
}

//======================================================================

void ReplyTemplateCollection::AddTemplate(ReplyTemplate &rTemplate)
{
	ReplyTemplate* p = new ReplyTemplate(rTemplate);
	
	assert(p);

	if(p)
	{
		p->SetMsgProcRuleCollection(m_pMsgProcRules);
		p->SetLogger(m_pLogger);
		
		m_vectTemplates.push_back(p);
		m_mapTemplates[rTemplate.GetName()] = p;
	}
}

//======================================================================

BOOL ReplyTemplateCollection::Export(string &rStrDest)
{
	ReplyTemplateVector::iterator i;

	for(i = m_vectTemplates.begin();
		i != m_vectTemplates.end();
		i++)
	{
		string strTmp;

		BOOL fRetval = (*i)->Export(strTmp);

		if(!fRetval)
			return FALSE;

		rStrDest += strTmp;
		rStrDest += '\n';
	}

	rStrDest += '\n';

	return TRUE;
}

//======================================================================

void ReplyTemplateCollection::MergeFrom(const ReplyTemplateCollection& rhs)
{
	for(ReplyTemplateVector::const_iterator i = rhs.m_vectTemplates.begin(); i != rhs.m_vectTemplates.end(); i++)
	{
		AddTemplate(*(*i));
	}
}

//======================================================================

BOOL ReplyTemplateCollection::IsStart(const Line &rLine) const
{
	if(rLine.size() != 2)
		return FALSE;

	if(rLine[0] == "REPLY_TEMPLATE")
		return TRUE;

	return FALSE;
}

//======================================================================

RuleEntity *ReplyTemplateCollection::MakeNew(const Line &rLine)
{
	ReplyTemplateRuleEntity *pResult;

	pResult = new ReplyTemplateRuleEntity;

	if(!pResult)
		return NULL;

	pResult->SetName(rLine[1].c_str());

	pResult->SetReplyTemplateCollection(this);

	return pResult;
}

//======================================================================

void ReplyTemplateCollection::SetStyle(BitmapMsgStyle *pStyle)
{
	ReplyTemplateVector::iterator i;

	for(i = m_vectTemplates.begin(); i != m_vectTemplates.end(); i++)
	{
		(*i)->SetBitmapMsgStyle(pStyle);
	}
}

//======================================================================
	
const ReplyTemplate* ReplyTemplateCollection::FindByName(const string& strName) const
{
	ReplyTemplateMap::const_iterator i;

	i = m_mapTemplates.find(strName);

	if(i == m_mapTemplates.end())
		return NULL;
	else
		return (*i).second;
}

//======================================================================

BOOL ReplyTemplateCollection::Check() const
{
	BOOL fRetval = TRUE;

	ReplyTemplateVector::const_iterator i;

	for(i = m_vectTemplates.begin(); i != m_vectTemplates.end(); i++)
	{
		if(!(*i)->Check())
			fRetval = FALSE;
	}

	return fRetval;
}

//======================================================================
