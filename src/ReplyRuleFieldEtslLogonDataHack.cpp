#include "stdafx.h"
#include "ReplyRuleFieldEtslLogonDataHack.h"
#include "DesBlock.h"
#include "bcd.h"

#include <assert.h>

#include <time.h>

using std::string;

//======================================================================

ReplyRuleFieldEtslLogonDataHack::ReplyRuleFieldEtslLogonDataHack(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldEtslLogonDataHack::Clone() const
{
	return new ReplyRuleFieldEtslLogonDataHack(*this);
}

//======================================================================
//
//	FIELD <field_num> ETSL_LOGON_DATA_HACK

BOOL ReplyRuleFieldEtslLogonDataHack::Import(const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "ETSL_LOGON_DATA_HACK")
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL ReplyRuleFieldEtslLogonDataHack::Export(string &strDesc)
{
	StrUtil::Format(strDesc, "FIELD %d ETSL_LOGON_DATA_HACK", GetFieldNum());

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldEtslLogonDataHack::BuildReplyField(MessagePacket &rPacket) const
{
	BitmapMsgField *pField;

	pField = rPacket.m_pResponse.m_sBMsg.GetField(GetFieldNum());

	assert(pField);

	if(!pField)
		return FALSE;

	if(pField->GetLen() < 59)
		return FALSE;

	string strField = pField->GetAsciiRep();
	DesBlock db;
	BYTE bIn[8], bOut[8];
	char szKey[17];

	// we really need to know the master key before we can continue.
	// it should be set by now, but might not if we are not checking
	// incoming MACs.

	if(rPacket.m_pRequest.GetMacKey().empty())
	{
		rPacket.m_pRequest.CheckMac();
	}

	string strKey(rPacket.m_pRequest.GetMacKey());

	if(strKey.empty())
	{
		// just use a default if the terminal has maccing turned off.

		strKey = "3131313131313131";
	}

	// for this hack always use the master key for both the working keys

	db.SetKeyFromString(strKey.c_str(), DesBlock::CM_ENCRYPT);

	BCD::Asc2PackedBcd(strKey.c_str(), bIn, 8);
	db.DoDes(bIn, bOut);
	BCD::Hex2Asc(bOut, szKey, 8);
	strField.replace(27, 16, szKey, 16);
	strField.replace(43, 16, szKey, 16);

	if(!pField->SetFieldFromString(strField.c_str()))
		return FALSE;

	return TRUE;
}

