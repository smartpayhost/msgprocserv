#ifndef _SQLQUERY_H
#define _SQLQUERY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

class MsgProcRuleCollection;

//======================================================================

class SQLQuery : public LoggingObject {
public:
	SQLQuery();

	virtual BOOL Export(std::string &rStrDest) const = 0;

	virtual BOOL Check() const;

	const std::string GetName()	const		{ return m_strName; }
	const std::string GetDataSource() const	{ return m_strDataSource; }
	const std::string GetQuery() const		{ return m_strOrigQuery; }

	void SetName(const std::string &strName)	{ m_strName = strName; }
	void SetDataSource(const std::string &strDataSource)	{ m_strDataSource = strDataSource; }
	void SetQuery(const std::string &strQuery)	{ m_strOrigQuery = strQuery; }

	inline const MsgProcRuleCollection *GetMsgProcRuleCollection() const { return m_pMsgProcRules; }
	inline void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray) { m_pMsgProcRules = pArray; }

	virtual BOOL ExecQuery(const std::string &strQuery, class SQLResultTable &rResult) const;

private:
	std::string m_strName;

	std::string m_strDataSource;
	std::string m_strOrigQuery;

	MsgProcRuleCollection *m_pMsgProcRules;
};

//======================================================================

#endif // _SQLQUERY_H
