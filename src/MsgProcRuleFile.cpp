#include "stdafx.h"
#include "MsgProcRuleFile.h"
#include "MatchTemplateCollection.h"
#include "ReplyTemplateCollection.h"
#include "DataSourceCollection.h"
#include "SQLMatchQueryCollection.h"
#include "SQLActionQueryCollection.h"
#include "SQLReplyQueryCollection.h"
#include "HostDescriptionCollection.h"

#include <assert.h>

class Logger;

using std::string;

//======================================================================

MsgProcRuleFile::MsgProcRuleFile()
{
	GetMatchTemplateCollection().Register(*this);
	GetReplyTemplateCollection().Register(*this);
	GetHostDescriptionCollection().Register(*this);
	GetDataSourceCollection().Register(*this);
	GetSQLMatchQueryCollection().Register(*this);
	GetSQLActionQueryCollection().Register(*this);
	GetSQLReplyQueryCollection().Register(*this);
}

//======================================================================

RuleFile* MsgProcRuleFile::Clone() const
{
	MsgProcRuleFile *p = new MsgProcRuleFile;

	p->SetLogger(GetLogger());
	return p;
}

//======================================================================

void MsgProcRuleFile::MergeFrom(const RuleFile* prfFrom)
{
	assert(prfFrom);
	const MsgProcRuleFile *rf = static_cast<const MsgProcRuleFile*>(prfFrom);

	m_rc.MergeFrom(&(rf->m_rc));
}

//======================================================================

void MsgProcRuleFile::Export(string &rStrDest)
{
	m_rc.Export(rStrDest);
}

//======================================================================

void MsgProcRuleFile::SetLogger(Logger *pLogger)
{
	RuleFile::SetLogger(pLogger);
	m_rc.SetLogger(pLogger);
}

//======================================================================

BOOL MsgProcRuleFile::Check()
{
	BOOL fRetval = TRUE;

	if(!m_rc.Check()) fRetval = FALSE;

	return fRetval;
}

//======================================================================
