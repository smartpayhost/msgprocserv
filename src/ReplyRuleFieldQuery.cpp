#include "stdafx.h"
#include "ReplyRuleFieldQuery.h"

#include "MsgProcRuleCollection.h"

#include <assert.h>

using std::string;

//======================================================================

ReplyRuleFieldQuery::ReplyRuleFieldQuery(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle),
	m_fCooked(FALSE)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldQuery::Clone() const
{
	return new ReplyRuleFieldQuery(*this);
}

//======================================================================
//
//	FIELD <field_num> QUERY <queryname> [COOKED]

BOOL ReplyRuleFieldQuery::Import(const Line &rLine)
{
	m_strQuery.erase();

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "QUERY")
	{
		if(++i == rLine.end()) return FALSE;

		m_strQuery = (*i);

		if(++i != rLine.end())
		{
			if((*i) == "COOKED")
				m_fCooked = TRUE;
		}
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldQuery::Export(string &strDesc)
{
	string strTmp;

	StrUtil::Format(strDesc, "FIELD %d QUERY ", GetFieldNum());
	StrUtil::PurifyString(m_strQuery, strTmp);
	strDesc += strTmp;

	if(m_fCooked)
		strDesc += " COOKED";

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldQuery::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	assert(GetMsgProcRuleCollection());

	const SQLReplyQuery *pQuery = GetMsgProcRuleCollection()->GetSQLReplyQueryCollection().FindByName(m_strQuery);

	assert(pQuery);

	if(!pQuery)
		return FALSE;

	SQLResultTable tblResult;

	if(!pQuery->ExecQuery(tblResult, rOrigMsg, rNewMsg))
		return FALSE;

	string strResult;

	if(tblResult.GetData(strResult))
	{
		BitmapMsgField *pField = new BitmapMsgField(GetFieldNum(), rOrigMsg.GetMsgStyle());

		assert(pField);

		if(!pField)
			return FALSE;

		if(m_fCooked)
		{
			Line line;

			StrUtil::StringToLine(strResult, line);

			if(line.size())
				strResult = line[0];
		}

		if(!pField->SetFieldFromString(strResult))
		{
			delete pField;
			return FALSE;
		}

		rNewMsg.SetField(pField);

		return TRUE;
	}

	return FALSE;
}

//======================================================================

BOOL ReplyRuleFieldQuery::Check() const
{
	assert(GetMsgProcRuleCollection());

	if(GetMsgProcRuleCollection()->GetSQLReplyQueryCollection().FindByName(m_strQuery))
		return TRUE;

	Log(LOG_WARNING, "Reply query %s not defined", m_strQuery.c_str());

	return FALSE;
}

//======================================================================
