#include "stdafx.h"
#include "SQLReplyQueryCollection.h"

#include "SQLReplyQueryRuleEntity.h"
#include "SQLMessageQuery.h"

#include <assert.h>

using std::string;

//======================================================================

SQLReplyQueryCollection::SQLReplyQueryCollection()
:	m_pMsgProcRules(NULL)
{
}

//----------------------------------------------------------------------

SQLReplyQueryCollection::~SQLReplyQueryCollection()
{
	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLReplyQuerys.begin(); i != m_mapSQLReplyQuerys.end(); i++)
		delete((*i).second);
}

//======================================================================

void SQLReplyQueryCollection::SetMsgProcRuleCollection(MsgProcRuleCollection *pArray)
{
	m_pMsgProcRules = pArray;

	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLReplyQuerys.begin(); i != m_mapSQLReplyQuerys.end(); i++)
		(*i).second->SetMsgProcRuleCollection(m_pMsgProcRules);
}

//======================================================================

void SQLReplyQueryCollection::SetLogger(Logger *pLogger)
{
	m_pLogger = pLogger;

	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLReplyQuerys.begin(); i != m_mapSQLReplyQuerys.end(); i++)
		(*i).second->SetLogger(m_pLogger);
}

//======================================================================

void SQLReplyQueryCollection::Register(RuleFile &rRuleFile)
{
	rRuleFile.RegisterEntityType(this);
}

//======================================================================

void SQLReplyQueryCollection::AddSQLReplyQuery(SQLReplyQuery &rSQLReplyQuery)
{
	SQLReplyQuery *p = new SQLReplyQuery(rSQLReplyQuery);

	assert(p);

	if(p)
	{
		p->SetMsgProcRuleCollection(m_pMsgProcRules);
		p->SetLoggingName(rSQLReplyQuery.GetName());
		p->SetLogger(m_pLogger);
		m_mapSQLReplyQuerys[rSQLReplyQuery.GetName()] = p;
	}
}

//======================================================================

BOOL SQLReplyQueryCollection::Export(string &rStrDest)
{
	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLReplyQuerys.begin(); i != m_mapSQLReplyQuerys.end(); i++)
	{
		string strTmp;

		BOOL fRetval = (*i).second->Export(strTmp);

		if(!fRetval)
			return FALSE;

		rStrDest += strTmp;
		rStrDest += '\n';
	}

	rStrDest += '\n';

	return TRUE;
}

//======================================================================

void SQLReplyQueryCollection::MergeFrom(const SQLReplyQueryCollection& rhs)
{
	for(SQLMessageQuery::SQLMessageQueryMap::const_iterator i = rhs.m_mapSQLReplyQuerys.begin(); i != rhs.m_mapSQLReplyQuerys.end(); i++)
	{
		AddSQLReplyQuery(*dynamic_cast<SQLReplyQuery *>((*i).second));
	}
}

//======================================================================

BOOL SQLReplyQueryCollection::IsStart(const Line &rLine) const
{
	if(rLine.size() != 2)
		return FALSE;

	if(rLine[0] == "REPLY_QUERY")
		return TRUE;

	return FALSE;
}

//======================================================================

RuleEntity *SQLReplyQueryCollection::MakeNew(const Line &rLine)
{
	SQLReplyQueryRuleEntity *pResult;

	pResult = new SQLReplyQueryRuleEntity;

	if(!pResult)
		return NULL;

	pResult->SetName(rLine[1]);

	pResult->SetSQLReplyQueryCollection(this);

	return pResult;
}

//======================================================================

const SQLReplyQuery* SQLReplyQueryCollection::FindByName(const string& strName) const
{
	SQLMessageQuery::SQLMessageQueryMap::const_iterator i;

	i = m_mapSQLReplyQuerys.find(strName);

	if(i == m_mapSQLReplyQuerys.end())
		return NULL;
	else
		return dynamic_cast<SQLReplyQuery *>((*i).second);
}

//======================================================================

BOOL SQLReplyQueryCollection::Check() const
{
	BOOL fRetval = TRUE;

	SQLMessageQuery::SQLMessageQueryMap::const_iterator i;

	for(i = m_mapSQLReplyQuerys.begin(); i != m_mapSQLReplyQuerys.end(); i++)
	{
		if(!(*i).second->Check())
			fRetval = FALSE;
	}

	return fRetval;
}

//======================================================================
