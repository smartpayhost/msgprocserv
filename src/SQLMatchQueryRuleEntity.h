#ifndef _SQLMATCHQUERYRULEENTITY_H
#define _SQLMATCHQUERYRULEENTITY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _SQLMATCHQUERY_H
#include "SQLMatchQuery.h"
#endif

class SQLMatchQueryCollection;

//======================================================================

class SQLMatchQueryRuleEntity : public RuleEntity {
public:
	SQLMatchQueryRuleEntity();

	BOOL ProcessLine(const Line &rLine);
	BOOL IsComplete() const;

	void SetName(const std::string &strName);
	void SetSQLMatchQueryCollection(SQLMatchQueryCollection *pCollection);

private:
	BOOL ProcessDataSource(const Line &rLine);
	BOOL ProcessQuery(const Line &rLine);
	BOOL ProcessMatchQueryEnd(const Line &rLine);

private:
	BOOL m_fComplete;
	SQLMatchQuery m_dsSQLMatchQuery;
	SQLMatchQueryCollection *m_pCollection;
};

//======================================================================

#endif // _SQLMATCHQUERYRULEENTITY_H
