#include "stdafx.h"
#include "ReplyRuleFieldExpDate.h"

#include <assert.h>

using std::string;

//======================================================================

ReplyRuleFieldExpDate::ReplyRuleFieldExpDate(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldExpDate::Clone() const
{
	return new ReplyRuleFieldExpDate(*this);
}

//======================================================================
//
//	FIELD <field_num> EXPDATE

BOOL ReplyRuleFieldExpDate::Import(const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "EXPDATE")
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL ReplyRuleFieldExpDate::Export(string &strDesc)
{
	StrUtil::Format(strDesc, "FIELD %d EXPDATE", GetFieldNum());

	return TRUE;
}

//======================================================================
//
//	Build expiry date field if available.
//

BOOL ReplyRuleFieldExpDate::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	string strExpDate(rOrigMsg.GetExpiryYYMM());

	BitmapMsgField *pField;

	if(!strExpDate.empty())
	{
		pField = new BitmapMsgField(GetFieldNum(), rOrigMsg.GetMsgStyle());

		assert(pField);

		if(!pField)
			return FALSE;

		if(!pField->SetFieldFromString(strExpDate))
		{
			delete pField;
			return FALSE;
		}

		rNewMsg.SetField(pField);
	}

	return TRUE;
}

