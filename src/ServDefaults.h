#ifndef _SERVDEFAULTS_H
#define _SERVDEFAULTS_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//======================================================================

#define REGKEY_ROOT				"SOFTWARE\\Cadmus\\MsgProcServ\\"
#define REGKEY_EVENTLOGROOT		"SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\"

#define REGVAL_CONFIGPATH		"ConfigPath"

#define DEFAULT_SERVICE_DISPLAY_NAME	"Generic AS2805 Message Processor"

#define MSG_DLL_PATH			"%SystemRoot%\\System32\\JPMsgDll.dll"

#define DEFAULT_WORK_DIR		"work"

//======================================================================

#endif // _SERVDEFAULTS_H
