#ifndef _REPLYRULETEMPLATE_H
#define _REPLYRULETEMPLATE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULE_H
#include "ReplyRule.h"
#endif

#ifndef _STRUTIL_H
#include "StrUtil.h"
#endif

//==============================================================

class ReplyRuleTemplate : public ReplyRule {
public:
	ReplyRuleTemplate();
	explicit ReplyRuleTemplate(const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Check() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(MessagePacket &rPacket) const;

private:
	std::string m_strTemplateName;
};

//==============================================================

#endif // _REPLYRULETEMPLATE_H
