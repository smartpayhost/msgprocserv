#include "stdafx.h"
#include "DataSourceRuleEntity.h"

#include "DataSourceCollection.h"

#include <assert.h>

using std::string;

//======================================================================

DataSourceRuleEntity::DataSourceRuleEntity()
:	m_fComplete(FALSE),
	m_pCollection(NULL)
{
}

//======================================================================

void DataSourceRuleEntity::SetName(const string &strName)
{
	m_dsDataSource.SetName(strName);
}

//======================================================================

void DataSourceRuleEntity::SetDataSourceCollection(DataSourceCollection *pCollection)
{
	m_pCollection = pCollection;
}

//----------------------------------------------------------------------

BOOL DataSourceRuleEntity::ProcessDSN(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsDataSource.SetDSN(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL DataSourceRuleEntity::ProcessUID(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsDataSource.SetUID(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL DataSourceRuleEntity::ProcessAuthStr(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsDataSource.SetAuthStr(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL DataSourceRuleEntity::ProcessTimeout(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsDataSource.SetTimeout(atol(rLine[1].c_str()));

	return TRUE;
}

//----------------------------------------------------------------------

BOOL DataSourceRuleEntity::ProcessTraceFile(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsDataSource.SetTraceFile(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL DataSourceRuleEntity::ProcessDataSourceEnd(const Line & /*rLine*/)
{
	m_fComplete = TRUE;

	assert(m_pCollection);
	m_pCollection->AddDataSource(m_dsDataSource);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL DataSourceRuleEntity::ProcessLine(const Line &rLine)
{
	if(rLine[0] == "DSN")
	{
		return ProcessDSN(rLine);
	}
	else if(rLine[0] == "USER")
	{
		return ProcessUID(rLine);
	}
	else if(rLine[0] == "PASSWORD")
	{
		return ProcessAuthStr(rLine);
	}
	else if(rLine[0] == "TIMEOUT")
	{
		return ProcessTimeout(rLine);
	}
	else if(rLine[0] == "TRACEFILE")
	{
		return ProcessTraceFile(rLine);
	}
	else if(rLine[0] == "DATA_SOURCE_END")
	{
		return ProcessDataSourceEnd(rLine);
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL DataSourceRuleEntity::IsComplete() const
{
	return m_fComplete;
}

//======================================================================
