#ifndef _ACTION_H
#define _ACTION_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//==============================================================

class Action {
public:
	enum Type { NONE, RESPONSE, UPSTREAM };
};

//==============================================================

#endif // _ACTION_H
