#include "stdafx.h"
#include "SQLMessageQuery.h"

#include "Message.h"
#include "StrUtil.h"
#include "DataSource.h"
#include "DataSourceCollection.h"
#include "SQLResultTable.h"
#include "bcd.h"

#include <iomanip>
#include <sstream>
#include <algorithm>

using std::string;
using std::transform;
using std::ostringstream;

//======================================================================

static BOOL ParseArg(const string& strArg, string& strType, string& strParam)
{
	string::size_type iSep = strArg.find_first_of(":}", 1);

	if((iSep == string::npos) || (*(strArg.end() - 1) != '}'))
	{
		return FALSE;
	}

	strType = strArg.substr(1, iSep - 1);

	if((strArg[iSep] == ':') && (strArg[iSep + 1] != '}'))
	{
		strParam = strArg.substr(iSep + 1, strArg.size() - iSep - 2);
	}

	transform(strType.begin(), strType.end(), strType.begin(), toupper);

	return TRUE;
}

//======================================================================

static int InterpretFieldName(const string& strParam)
{
	if(!strParam.empty())
	{
		string strP(strParam);

		transform(strP.begin(), strP.end(), strP.begin(), toupper);

		if(strP == "MSGTYPE")
			return 200;
		else if(strP == "PAN")
			return 201;
		else if(strP == "EXPDATE")
			return 202;
		else
		{
			if(strP[0] == 'F')
			{
				int iField = atoi(strP.c_str() + 1);

				if((iField >= 1) && (iField <= 128))
					return iField;
			}
		}
	}

	return 0;
}

//======================================================================

BOOL SQLMessageQuery::CookQuery(string &strCookedQuery, const string &strQuery, 
								const BitmapMsg *pOrigMsg, const BitmapMsg *pNewMsg) const
{
	strCookedQuery.erase();

	BOOL fGetNumber = FALSE;
	BOOL fGetFromNew = FALSE;

	string::size_type i = 0;

	while(i < strQuery.size())
	{
		if(fGetNumber)
		{
			if(!fGetFromNew && strQuery[i] == '%')
			{
				strCookedQuery += strQuery[i++];
			}
			else if(fGetFromNew && strQuery[i] == '$')
			{
				strCookedQuery += strQuery[i++];
			}
			else
			{
				const BitmapMsg *pMsg;

				if(fGetFromNew)
					pMsg = pNewMsg;
				else
					pMsg = pOrigMsg;

				int iFieldNum = atoi(strQuery.c_str() + i);

				if((iFieldNum >= 1) && (iFieldNum <= 128))
				{
					const BitmapMsgField *pField;

					pField = pMsg->GetField(iFieldNum);

					if(pField && pField->GetFieldLen())
					{
						strCookedQuery += pField->GetAsciiRep().c_str();
					}
					else
					{
						Log(LOG_WARNING, "Could not execute query (%s), field (%d) in %s message empty or missing", 
							GetName().c_str(), iFieldNum, fGetFromNew?"response":"request");
						return FALSE;
					}
				}
				else if(iFieldNum == 200)	// msg type
				{
					ostringstream os;
					
					os << std::setw(4) << std::setfill('0') << pMsg->GetMsgType();

					strCookedQuery += os.str();
				}
				else if(iFieldNum == 201)	// PAN
				{
					strCookedQuery += pMsg->GetPAN();
				}
				else if(iFieldNum == 202)	// Expiry date YYMM
				{
					strCookedQuery += pMsg->GetExpiryYYMM();
				}
				else
				{
					Log(LOG_WARNING, "Invalid field number (%d) in query", iFieldNum);
					return FALSE;
				}

				i = strQuery.find_first_not_of("0123456789", i);
			}

			fGetNumber = FALSE;
		}
		else
		{
			if(pOrigMsg && strQuery[i] == '%')
			{
				fGetNumber = TRUE;
				fGetFromNew = FALSE;
				i++;
			}
			else if(pNewMsg && strQuery[i] == '$')
			{
				fGetNumber = TRUE;
				fGetFromNew = TRUE;
				i++;
			}
			else if(pNewMsg && strQuery[i] == '{')
			{
				string strArg;
				string strType;
				string strParam;

				// get {type:param} into strArg

				string::size_type iArgEnd = strQuery.find('}', i);

				if(iArgEnd == string::npos)
				{
					Log(LOG_ERROR, "Unmatched brace in query", strQuery.substr(i).c_str());
					return FALSE;
				}

				size_t iArgLen = iArgEnd - i + 1;

				strArg = strQuery.substr(i, iArgLen);
				i += iArgLen;

				// split {type:param} form into type and (optional) param strings.

				if(!ParseArg(strArg, strType, strParam))
				{
					Log(LOG_ERROR, "Invalid argument (%s) in query", strArg.c_str());
					return FALSE;
				}

				int iFieldNum = InterpretFieldName(strParam);

				// interpret type & param

				if((strType == "REQ") || (strType == "RSP"))
				{
					const BitmapMsg *pMsg;
					BOOL fGetFromNew;

					if(strType == "REQ")
					{
						pMsg = pOrigMsg;
						fGetFromNew = FALSE;
					}
					else
					{
						pMsg = pNewMsg;
						fGetFromNew = TRUE;
					}

					if(!pMsg)
					{
						Log(LOG_WARNING, "Could not execute query (%s), wrong query type, no %s message", 
							GetName().c_str(), iFieldNum, fGetFromNew?"response":"request");
						return FALSE;
					}

					if((iFieldNum >= 1) && (iFieldNum <= 128))
					{
						const BitmapMsgField *pField;

						pField = pMsg->GetField(iFieldNum);

						if(pField && pField->GetFieldLen())
						{
							strCookedQuery += pField->GetAsciiRep().c_str();
						}
						else
						{
							Log(LOG_WARNING, "Could not execute query (%s), field (%d) in %s message empty or missing", 
								GetName().c_str(), iFieldNum, fGetFromNew?"response":"request");
							return FALSE;
						}
					}
					else if(iFieldNum == 200)	// msg type
					{
						ostringstream os;
						
						os << std::setw(4) << std::setfill('0') << pMsg->GetMsgType();

						strCookedQuery += os.str();
					}
					else if(iFieldNum == 201)	// PAN
					{
						strCookedQuery += pMsg->GetPAN();
					}
					else if(iFieldNum == 202)	// Expiry date YYMM
					{
						strCookedQuery += pMsg->GetExpiryYYMM();
					}
					else
					{
						Log(LOG_WARNING, "Invalid field number (%d) in query", iFieldNum);
						return FALSE;
					}
				}
				else if((strType == "REQB") || (strType == "RSPB"))
				{
					const BitmapMsg *pMsg;
					BOOL fGetFromNew;

					if(strType == "REQB")
					{
						pMsg = pOrigMsg;
						fGetFromNew = FALSE;
					}
					else
					{
						pMsg = pNewMsg;
						fGetFromNew = TRUE;
					}

					if(!pMsg)
					{
						Log(LOG_WARNING, "Could not execute query (%s), wrong query type, no %s message", 
							GetName().c_str(), iFieldNum, fGetFromNew?"response":"request");
						return FALSE;
					}

					if((iFieldNum >= 1) && (iFieldNum <= 128))
					{
						const BitmapMsgField *pField;

						pField = pMsg->GetField(iFieldNum);

						if(pField && pField->GetFieldLen())
						{
							int iDataLen = pField->GetDataLen();

							if(iDataLen)
							{
								char szAsc[3];
								szAsc[2] = '\0';

								const BYTE *pData = pField->GetData();
								strCookedQuery += "0x";

								while(iDataLen--)
								{
									BCD::Hex2Asc(pData++, szAsc, 1);
									strCookedQuery += szAsc;
								}
							}
						}
						else
						{
							Log(LOG_WARNING, "Could not execute query (%s), field (%d) in %s message empty or missing", 
								GetName().c_str(), iFieldNum, fGetFromNew?"response":"request");
							return FALSE;
						}
					}
					else
					{
						Log(LOG_ERROR, "Invalid argument (%s) in query", strArg.c_str());
						return FALSE;
					}
				}
				else
				{
					Log(LOG_ERROR, "Invalid argument (%s) in query", strArg.c_str());
					return FALSE;
				}
			}
			else
			{
				strCookedQuery += strQuery[i++];
			}
		}
	}

	return TRUE;
}

//======================================================================

BOOL SQLMessageQuery::ExecQuery(SQLResultTable &rResult, const BitmapMsg *pOrigMsg, const BitmapMsg *pNewMsg) const
{
	if(pOrigMsg)
	{
		string strCookedQuery;

		if(!CookQuery(strCookedQuery, GetQuery(), pOrigMsg, pNewMsg))
			return FALSE;

		return SQLQuery::ExecQuery(strCookedQuery, rResult);
	}
	else
	{
		return SQLQuery::ExecQuery(GetQuery(), rResult);
	}
}

//======================================================================
