#include "stdafx.h"
#include "MatchRule.h"

#include "MatchRuleNII.h"
#include "MatchRuleMsgType.h"
#include "MatchRulePAN.h"
#include "MatchRuleExpDate.h"
#include "MatchRuleFieldLen.h"
#include "MatchRuleFieldContents.h"
#include "MatchRuleFieldPresent.h"
#include "MatchRuleMacValid.h"
#include "MatchRuleTemplate.h"
#include "MatchRuleActionQuery.h"
#include "MatchRuleActionProg.h"
#include "MatchRuleMacKey.h"
#include "StrUtil.h"

using std::string;

//=========================================================

MatchRule::MatchRule()
:	m_bInverted(FALSE),
	m_pMsgProcRules(NULL)
{
}

//======================================================================
//
//	NII ...
//	MSG_TYPE ...
//	MAC ...
//	IS ...
//	ACTION ...
//	PAN ...
//	EXPDATE ...
//	FIELD ...
//	QUERY ...
//	PROG ...
//

BOOL MatchRule::Import(const string &strDesc)
{
	Line lineThis;

	if(!StrUtil::StringToLine(strDesc, lineThis))
		return FALSE;

	return Import(lineThis);
}

//---------------------------------------------------------

MatchRule* MatchRule::MakeNew(const Line &rLine)
{
	if(!rLine.size())
		return NULL;

	MatchRule *p = NULL;

	if(rLine[0] == "MSG_TYPE")		p = new MatchRuleMsgType;
	else if(rLine[0] == "NII")		p = new MatchRuleNII;
	else if(rLine[0] == "MAC")		p = new MatchRuleMacValid;
	else if(rLine[0] == "IS")		p = new MatchRuleTemplate;
	else if(rLine[0] == "PAN")		p = new MatchRulePAN;
	else if(rLine[0] == "EXPDATE")	p = new MatchRuleExpDate;
	else if(rLine[0] == "MACKEY")	p = new MatchRuleMacKey;
	else if(rLine[0] == "QUERY" || (rLine.size() >= 2 && rLine[0] == "ACTION" && rLine[1] == "QUERY"))
									p = new MatchRuleActionQuery;
	else if(rLine[0] == "PROG" || (rLine.size() >= 2 && rLine[0] == "ACTION" && rLine[1] == "PROG"))
									p = new MatchRuleActionProg;
	else if(rLine[0] == "FIELD")
	{
		if(rLine.size() < 2)
			return NULL;

		int iFieldNum = atoi(rLine[1].c_str());

		if((iFieldNum < 1) || (iFieldNum > 128))
			return NULL;

		if(rLine.size() == 2)
		{
			p = new MatchRuleFieldPresent;
		}
		else
		{
			string strType = rLine[2];

			if(strType == "NOT")
			{
				if(rLine.size() < 4)
					return NULL;

				strType = rLine[3];
			}

			if(strType == "PRESENT")
			{
				p = new MatchRuleFieldPresent;
			}
			else if(strType == "LENGTH")
			{
				p = new MatchRuleFieldLen;
			}
			else
			{
				p = new MatchRuleFieldContents;
			}
		}
	}

	if(p && p->Import(rLine))
	{
		return p;
	}
	else
	{
		delete p;

		return NULL;
	}
}

//======================================================================

BOOL MatchRule::Check() const
{
	return TRUE;
}

//======================================================================
