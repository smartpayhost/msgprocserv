#include "stdafx.h"
#include "Registry.h"

using std::string;
using std::vector;

//======================================================================

BOOL Registry::CreateKey(HKEY hTopLevelKey, const string& strNewKeyName)
{
	HKEY hKey;
	DWORD dwDisposition;

	if(::RegCreateKeyEx(hTopLevelKey,
		strNewKeyName.c_str(),
		0,
		"REG_SZ",
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,
		NULL,
		&hKey,
		&dwDisposition) == ERROR_SUCCESS)
	{
		::RegCloseKey(hKey);
		return TRUE;
	}
	
	return FALSE;
}

//======================================================================

BOOL Registry::DeleteKey(HKEY hTopLevelKey, const string& strKeyName)
{
	return (ERROR_SUCCESS == ::RegDeleteKey(hTopLevelKey, strKeyName.c_str()));
}

//======================================================================

BOOL Registry::DeleteValue(HKEY hTopKey, const string& strKey, const string& strValueName)
{
	RegistryKey sKey(hTopKey, strKey);

	return (ERROR_SUCCESS == sKey.DeleteValue(strValueName));
}

//======================================================================

BOOL Registry::SetString(HKEY hTopKey, const string& strKey, const string& strValueName,
						 const string& strValueData, DWORD dwType)
{
	RegistryKey sKey(hTopKey, strKey);

	return sKey.SetString(strValueName, strValueData, dwType);
}

//----------------------------------------------------------------------

BOOL Registry::ReadString(HKEY hTopKey, const string& strKey, const string& strValueName, string &strResult)
{
	RegistryKey sKey(hTopKey, strKey, KEY_READ);

	return sKey.ReadString(strValueName, strResult);
}

//----------------------------------------------------------------------

BOOL Registry::ReadString(HKEY hTopKey, const string& strKey, const string& strValueName,
						  DWORD dwMaxLen, char *pbBuf, DWORD *pdwResultLen)
{
	RegistryKey sKey(hTopKey, strKey, KEY_READ);

	return sKey.ReadString(strValueName, dwMaxLen, pbBuf, pdwResultLen);
}

//----------------------------------------------------------------------

BOOL Registry::SetDWORD(HKEY hTopKey, const string& strKey, const string& strValueName, 
						DWORD dwData)
{
	RegistryKey sKey(hTopKey, strKey);

	return sKey.SetDWORD(strValueName, dwData);
}

//----------------------------------------------------------------------

BOOL Registry::ReadDWORD(HKEY hTopKey, const string& strKey, const string& strValueName, 
						DWORD *pdwData)
{
	RegistryKey sKey(hTopKey, strKey, KEY_READ);

	return sKey.ReadDWORD(strValueName, pdwData);
}

//----------------------------------------------------------------------

BOOL Registry::EnumKeys(HKEY hTopKey, const string& strKey, vector<string>& vResult)
{
	RegistryKey sKey(hTopKey, strKey, KEY_READ);

	return sKey.EnumKeys(vResult);
}

//======================================================================
//======================================================================

RegistryKey::RegistryKey(HKEY hParent, const string& strKeyPath, REGSAM samDesired)
{
	Open(hParent, strKeyPath, samDesired);
}

//----------------------------------------------------------------------

RegistryKey::RegistryKey()
: m_fValid(FALSE)
{
}

//----------------------------------------------------------------------

RegistryKey::~RegistryKey()
{
	Close();
}

//======================================================================

BOOL RegistryKey::Open(HKEY hParent, const string& strKeyPath, REGSAM samDesired)
{
	if(ERROR_SUCCESS == ::RegOpenKeyEx(hParent, strKeyPath.c_str(), 0, samDesired, &m_hKey))
		m_fValid = TRUE;
	else
		m_fValid = FALSE;

	return m_fValid;
}

//----------------------------------------------------------------------

void RegistryKey::Close()
{
	if(m_fValid)
		::RegCloseKey(m_hKey);

	m_fValid = FALSE;
}

//======================================================================

BOOL RegistryKey::DeleteValue(const string& strValueName)
{
	if(!IsValid())
		return FALSE;

	return ::RegDeleteValue(m_hKey, strValueName.c_str());
}

//======================================================================

BOOL RegistryKey::SetDWORD(const string& strValueName, DWORD dwData)
{
	return SetBinary(strValueName, (const BYTE *)&dwData, sizeof(DWORD), REG_DWORD);
}

//----------------------------------------------------------------------

BOOL RegistryKey::ReadDWORD(const string& strValueName, DWORD *pdwData)
{
	return ReadBinary(strValueName, sizeof(DWORD), (BYTE *)pdwData);
}

//----------------------------------------------------------------------

BOOL RegistryKey::SetString(const string& strValueName, const string& strValueData, DWORD dwType)
{
	return SetBinary(strValueName, 
					(const BYTE *)strValueData.c_str(), 
					(DWORD)strlen(strValueData.c_str()) + 1, 
					dwType);
}

//----------------------------------------------------------------------

BOOL RegistryKey::ReadString(const string& strValueName, DWORD dwMaxLen, 
							   char *pbBuf, DWORD *pdwResultLen)
{
	return ReadBinary(strValueName, dwMaxLen, (BYTE *)pbBuf, pdwResultLen);
}

//----------------------------------------------------------------------

BOOL RegistryKey::ReadString(const string& strValueName, string &strResult)
{
	DWORD dwSize;

	if(!GetValSize(strValueName, &dwSize))
		return FALSE;

	strResult.erase();

	if(dwSize == 0)
		return TRUE;

	char *szBuf = new char[dwSize];

	DWORD dwResultLen;

	BOOL fRetval = ReadString(strValueName, dwSize,  szBuf, &dwResultLen);

	if(fRetval)
		strResult = szBuf;

	delete[] szBuf;

	return fRetval;
}

//----------------------------------------------------------------------

BOOL RegistryKey::SetBinary(const string& strValueName, const BYTE *pbValueData, 
							  DWORD dwLen, DWORD dwType)
{
	if(!IsValid())
		return FALSE;

	return (ERROR_SUCCESS == ::RegSetValueEx(m_hKey,
		strValueName.c_str(),
		0,
		dwType,
		pbValueData,
		dwLen));
}

//----------------------------------------------------------------------

BOOL RegistryKey::ReadBinary(const string& strValueName, DWORD dwMaxLen, 
								BYTE *pbBuf, DWORD *pdwResultLen)
{
	if(!IsValid())
		return FALSE;

	DWORD dwSize = dwMaxLen;

	if(ERROR_SUCCESS == ::RegQueryValueEx(m_hKey,
		strValueName.c_str(),
		0,
		NULL,
		pbBuf,
		&dwSize))
	{
		if(pdwResultLen)
			*pdwResultLen = dwSize;

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//----------------------------------------------------------------------

BOOL RegistryKey::GetValSize(const string& strValueName, DWORD *pdwSize)
{
	return ReadBinary(strValueName, 0, NULL, pdwSize);
}

//----------------------------------------------------------------------

BOOL RegistryKey::EnumKeys(vector<string>& vResult)
{
	DWORD dwIndex = 0;
	char szSubKey[256];
	DWORD dwSize;
	FILETIME ftLastWriteTime;

	for(;;)
	{
		dwSize = sizeof(szSubKey);

		switch(::RegEnumKeyEx(m_hKey, dwIndex, szSubKey, &dwSize, NULL, NULL, NULL, &ftLastWriteTime))
		{
		case ERROR_SUCCESS:
			vResult.push_back(string(szSubKey));
			dwIndex++;
			break;

		case ERROR_NO_MORE_ITEMS:
			return TRUE;

		default:
			return FALSE;
		}
	}

	return FALSE;
}

//======================================================================
