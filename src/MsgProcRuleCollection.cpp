#include "stdafx.h"
#include "MsgProcRuleCollection.h"

#include "MatchTemplateCollection.h"
#include "ReplyTemplateCollection.h"
#include "DataSourceCollection.h"
#include "SQLMatchQueryCollection.h"
#include "SQLActionQueryCollection.h"
#include "SQLReplyQueryCollection.h"
#include "HostDescriptionCollection.h"

#include <assert.h>

class Logger;

using std::string;

//======================================================================

MsgProcRuleCollection::MsgProcRuleCollection()
{
	m_mtcMatchTemplates.SetMsgProcRuleCollection(this);
	m_rtcReplyTemplates.SetMsgProcRuleCollection(this);
	m_hdcHostDescriptions.SetMsgProcRuleCollection(this);
	m_dscDataSources.SetMsgProcRuleCollection(this);
	m_sqlMatchQueries.SetMsgProcRuleCollection(this);
	m_sqlActionQueries.SetMsgProcRuleCollection(this);
	m_sqlReplyQueries.SetMsgProcRuleCollection(this);
}

//======================================================================

void MsgProcRuleCollection::MergeFrom(const MsgProcRuleCollection* prfFrom)
{
	assert(prfFrom);

	m_mtcMatchTemplates.MergeFrom	(prfFrom->m_mtcMatchTemplates);
	m_rtcReplyTemplates.MergeFrom	(prfFrom->m_rtcReplyTemplates);
	m_hdcHostDescriptions.MergeFrom	(prfFrom->m_hdcHostDescriptions);
	m_dscDataSources.MergeFrom		(prfFrom->m_dscDataSources);
	m_sqlMatchQueries.MergeFrom		(prfFrom->m_sqlMatchQueries);
	m_sqlActionQueries.MergeFrom	(prfFrom->m_sqlActionQueries);
	m_sqlReplyQueries.MergeFrom		(prfFrom->m_sqlReplyQueries);
}

//======================================================================

void MsgProcRuleCollection::Export(string &rStrDest)
{
	m_mtcMatchTemplates.Export(rStrDest);
	m_rtcReplyTemplates.Export(rStrDest);
	m_hdcHostDescriptions.Export(rStrDest);
	m_dscDataSources.Export(rStrDest);
	m_sqlMatchQueries.Export(rStrDest);
	m_sqlActionQueries.Export(rStrDest);
	m_sqlReplyQueries.Export(rStrDest);
}

//======================================================================

void MsgProcRuleCollection::SetLogger(Logger *pLogger)
{
//	RuleFile::SetLogger(pLogger);
	m_mtcMatchTemplates.SetLogger(pLogger);
	m_rtcReplyTemplates.SetLogger(pLogger);
	m_dscDataSources.SetLogger(pLogger);
	m_sqlMatchQueries.SetLogger(pLogger);
	m_sqlActionQueries.SetLogger(pLogger);
	m_sqlReplyQueries.SetLogger(pLogger);
}

//======================================================================

BOOL MsgProcRuleCollection::Check()
{
	BOOL fRetval = TRUE;

	if(!m_mtcMatchTemplates.Check()) fRetval = FALSE;
	if(!m_rtcReplyTemplates.Check()) fRetval = FALSE;
	if(!m_sqlMatchQueries.Check()) fRetval = FALSE;
	if(!m_sqlActionQueries.Check()) fRetval = FALSE;
	if(!m_sqlReplyQueries.Check()) fRetval = FALSE;

	return fRetval;
}

//======================================================================
