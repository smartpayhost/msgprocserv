#include "stdafx.h"

#include "BitmapMsg.h"

typedef BitmapMsgFieldStyle BMTE;

static const BMTE F1 ( BMFF_FIXED   , BMFA_B   , 64, "Pri bitmap", "Primary bit map" );
static const BMTE F2 ( BMFF_LLVARNZ , BMFA_N   , 19, "PAN",        "Primary account number (PAN)" );
static const BMTE F3 ( BMFF_FIXED   , BMFA_N   ,  6, "ProcCode",   "Processing code" );
static const BMTE F4 ( BMFF_FIXED   , BMFA_N   , 12, "TranAmount", "Amount, transaction" );
static const BMTE F11( BMFF_FIXED   , BMFA_N   ,  6, "STAN",       "Systems trace audit number" );
static const BMTE F12( BMFF_FIXED   , BMFA_N   ,  6, "Time",       "Time, local transaction" );
static const BMTE F13( BMFF_FIXED   , BMFA_N   ,  4, "Date",       "Date, local transaction" );
static const BMTE F14( BMFF_FIXED   , BMFA_N   ,  4, "Exp date",   "Date, expiry" );
static const BMTE F15( BMFF_FIXED   , BMFA_N   ,  4, "SettleDate", "Date, settlement" );
static const BMTE F22( BMFF_FIXED   , BMFA_N   ,  3, "POSEntMode", "Point of service entry mode" );
static const BMTE F24( BMFF_FIXED   , BMFA_N   ,  3, "NII",        "Network internation identifier" );
static const BMTE F25( BMFF_FIXED   , BMFA_N   ,  2, "POS CC",     "Point of service condition code" );
static const BMTE F35( BMFF_LLVARNZ , BMFA_Z   , 37, "Track 2",    "Track 2 data" );
static const BMTE F37( BMFF_FIXED   , BMFA_AN  , 12, "RRN",        "Retrieval reference number" );
static const BMTE F38( BMFF_FIXED   , BMFA_AN  ,  6, "AuthID Rsp", "Authorization identification response" );
static const BMTE F39( BMFF_FIXED   , BMFA_AN  ,  2, "Rsp Code",   "Response code" );
static const BMTE F41( BMFF_FIXED   , BMFA_ANS ,  8, "CATID",      "Card acceptor terminal identification" );
static const BMTE F42( BMFF_FIXED   , BMFA_ANS , 15, "CAID",       "Card acceptor identification code" );
static const BMTE F43( BMFF_FIXED   , BMFA_ANS , 40, "CA NameLoc", "Card acceptor name/location" );
static const BMTE F44( BMFF_LLVARNZ , BMFA_ANS , 99, "AddRspData", "Additional response data" );
static const BMTE F47( BMFF_LLVARNZ , BMFA_Z   , 37, "Merch Trk2", "Merchant card track 2 data" );
static const BMTE F48( BMFF_LLLVARNZ, BMFA_ANS ,999, "PurchData",  "Purchase analysis data" );
static const BMTE F52( BMFF_FIXED   , BMFA_B   , 64, "PIN data",   "Personal identification number (PIN) data" );
static const BMTE F53( BMFF_FIXED   , BMFA_B   , 64, "Merch PIN",  "Merchant PIN data" );
static const BMTE F57( BMFF_FIXED   , BMFA_N   , 12, "Cash Amt",   "Amount, cash" );
static const BMTE F60( BMFF_LLLVARNZ, BMFA_ANS ,999, "ETSL data",  "Special ETSL data" );
static const BMTE F61( BMFF_LLLVARNZ, BMFA_ANS ,999, "Last/Logon", "Last completed transaction / Logon required" );
static const BMTE F62( BMFF_LLLVARNZ, BMFA_ANS ,999, "LgonRspDat", "Logon response data" );
static const BMTE F63( BMFF_LLLVARNZ, BMFA_ANS ,999, "RvRsnStInf", "Reversal reason / Settlement information" );
static const BMTE F64( BMFF_FIXED   , BMFA_B   , 64, "MAC",        "Message authentication code" );

static const BMTE &FNS = BMTE::DefaultFieldStyle;

static const BMTE* const pTemplate[128] = {
&F1 ,&F2 ,&F3 ,&F4 ,&FNS,&FNS,&FNS,&FNS,
&FNS,&FNS,&F11,&F12,&F13,&F14,&F15,&FNS,
&FNS,&FNS,&FNS,&FNS,&FNS,&F22,&FNS,&F24,
&F25,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
&FNS,&FNS,&F35,&FNS,&F37,&F38,&F39,&FNS,
&F41,&F42,&F43,&F44,&FNS,&FNS,&F47,&F48,
&FNS,&FNS,&FNS,&F52,&F53,&FNS,&FNS,&FNS,
&F57,&FNS,&FNS,&F60,&F61,&F62,&F63,&F64,
&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS
};

BitmapMsgStyle TemplateETSL("ETSL 4.4", pTemplate);
