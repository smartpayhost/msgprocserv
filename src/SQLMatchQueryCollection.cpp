#include "stdafx.h"
#include "SQLMatchQueryCollection.h"

#include "SQLMatchQueryRuleEntity.h"
#include "SQLMessageQuery.h"

#include <assert.h>

using std::string;

//======================================================================

SQLMatchQueryCollection::SQLMatchQueryCollection()
:	m_pMsgProcRules(NULL)
{
}

//----------------------------------------------------------------------

SQLMatchQueryCollection::~SQLMatchQueryCollection()
{
	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLMatchQuerys.begin(); i != m_mapSQLMatchQuerys.end(); i++)
		delete((*i).second);
}

//======================================================================

void SQLMatchQueryCollection::SetMsgProcRuleCollection(MsgProcRuleCollection *pArray)
{
	m_pMsgProcRules = pArray;

	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLMatchQuerys.begin(); i != m_mapSQLMatchQuerys.end(); i++)
		(*i).second->SetMsgProcRuleCollection(m_pMsgProcRules);
}

//======================================================================

void SQLMatchQueryCollection::SetLogger(Logger *pLogger)
{
	m_pLogger = pLogger;

	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLMatchQuerys.begin(); i != m_mapSQLMatchQuerys.end(); i++)
		(*i).second->SetLogger(m_pLogger);
}

//======================================================================

void SQLMatchQueryCollection::Register(RuleFile &rRuleFile)
{
	rRuleFile.RegisterEntityType(this);
}

//======================================================================

void SQLMatchQueryCollection::AddSQLMatchQuery(SQLMatchQuery &rSQLMatchQuery)
{
	SQLMatchQuery *p = new SQLMatchQuery(rSQLMatchQuery);

	assert(p);

	if(p)
	{
		p->SetMsgProcRuleCollection(m_pMsgProcRules);
		p->SetLoggingName(rSQLMatchQuery.GetName());
		p->SetLogger(m_pLogger);
		m_mapSQLMatchQuerys[rSQLMatchQuery.GetName()] = p;
	}
}

//======================================================================

BOOL SQLMatchQueryCollection::Export(string &rStrDest)
{
	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLMatchQuerys.begin();
		i != m_mapSQLMatchQuerys.end();
		i++)
	{
		string strTmp;

		BOOL fRetval = (*i).second->Export(strTmp);

		if(!fRetval)
			return FALSE;

		rStrDest += strTmp;
		rStrDest += '\n';
	}

	rStrDest += '\n';

	return TRUE;
}

//======================================================================

void SQLMatchQueryCollection::MergeFrom(const SQLMatchQueryCollection& rhs)
{
	for(SQLMessageQuery::SQLMessageQueryMap::const_iterator i = rhs.m_mapSQLMatchQuerys.begin(); i != rhs.m_mapSQLMatchQuerys.end(); i++)
	{
		AddSQLMatchQuery(*dynamic_cast<SQLMatchQuery *>((*i).second));
	}
}

//======================================================================

BOOL SQLMatchQueryCollection::IsStart(const Line &rLine) const
{
	if(rLine.size() != 2)
		return FALSE;

	if(rLine[0] == "MATCH_QUERY")
		return TRUE;

	return FALSE;
}

//======================================================================

RuleEntity *SQLMatchQueryCollection::MakeNew(const Line &rLine)
{
	SQLMatchQueryRuleEntity *pResult;

	pResult = new SQLMatchQueryRuleEntity;

	if(!pResult)
		return NULL;

	pResult->SetName(rLine[1]);

	pResult->SetSQLMatchQueryCollection(this);

	return pResult;
}

//======================================================================

const SQLMatchQuery* SQLMatchQueryCollection::FindByName(const string& strName) const
{
	SQLMessageQuery::SQLMessageQueryMap::const_iterator i;

	i = m_mapSQLMatchQuerys.find(strName);

	if(i == m_mapSQLMatchQuerys.end())
		return NULL;
	else
		return dynamic_cast<SQLMatchQuery *>((*i).second);
}

//======================================================================

BOOL SQLMatchQueryCollection::Check() const
{
	BOOL fRetval = TRUE;

	SQLMessageQuery::SQLMessageQueryMap::const_iterator i;

	for(i = m_mapSQLMatchQuerys.begin(); i != m_mapSQLMatchQuerys.end(); i++)
	{
		if(!(*i).second->Check())
			fRetval = FALSE;
	}

	return fRetval;
}

//======================================================================
