#include "stdafx.h"
#include "ReplyTemplate.h"

#include "ReplyRuleMsgType.h"
#include "ReplyRuleField.h"
#include "SQLActionQueryCollection.h"

using std::string;

//=========================================================

ReplyTemplate::ReplyTemplate()
:	m_pMsgProcRules(NULL)
{
}

//=========================================================

void ReplyTemplate::AddRule(ReplyRule* pRule)
{
	m_vectRules.push_back(boost::shared_ptr<ReplyRule>(pRule));
}

//=========================================================

BOOL ReplyTemplate::Export(string &strDesc) const
{
	string strTmp;

	strDesc = "REPLY_TEMPLATE ";
	StrUtil::PurifyString(m_strName, strTmp);
	strDesc += strTmp;
	strDesc += '\n';

	RuleVect::const_iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
	{
		if(!(*i)->Export(strTmp))
			return FALSE;

		strDesc += '\t';
		strDesc += strTmp;
		strDesc += '\n';
	}

	strDesc += "REPLY_TEMPLATE_END\n";

	return TRUE;
}

//=========================================================

void ReplyTemplate::SetBitmapMsgStyle(BitmapMsgStyle *pStyle)
{
	RuleVect::iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
		(*i)->SetStyle(pStyle);
}

//=========================================================

void ReplyTemplate::SetMsgProcRuleCollection(MsgProcRuleCollection *pArray)
{
	m_pMsgProcRules = pArray;

	RuleVect::iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
		(*i)->SetMsgProcRuleCollection(pArray);
}

//=========================================================

void ReplyTemplate::SetLogger(Logger* pLogger)
{
	LoggingObject::SetLogger(pLogger);

	RuleVect::iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
		(*i)->SetLogger(pLogger);
}

//=========================================================

BOOL ReplyTemplate::BuildReply(MessagePacket &rPacket) const
{
	// do any templates in order

	RuleVect::const_iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
	{
		if(!(*i)->BuildReplyField(rPacket))
			return FALSE;
	}

	return TRUE;
}

//=========================================================

BOOL ReplyTemplate::Check() const
{
	BOOL fRetval = TRUE;

	RuleVect::const_iterator i;

	for(i = m_vectRules.begin(); i != m_vectRules.end(); i++)
	{
		if(!(*i)->Check())
			fRetval = FALSE;
	}

	return fRetval;
}

//=========================================================
