#ifndef _SOCKET_H
#define _SOCKET_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <boost/utility.hpp>

//==============================================================

enum SOCKRC {
	SOCKRC_OK = 0,
	SOCKRC_ERROR = -1,
	SOCKRC_TIMEOUT = -2,
	SOCKRC_CLOSED = -3,
	SOCKRC_CANCELLED = -4,
};

//==============================================================

class Socket : public boost::noncopyable {
public:
	Socket();
	virtual ~Socket();

	virtual SOCKRC Open() = 0;
	virtual SOCKRC Close() = 0;

	virtual BOOL IsConnected() const = 0;

	SOCKRC Recv(char *buf, int bufsize, int *recvd);
	SOCKRC Recv(char *buf, int bufsize, int *recvd, unsigned long timeout);

	SOCKRC Send(const char *buf, int bufsize, int *sent, unsigned long timeout);
	SOCKRC Send(const char *buf, int bufsize, unsigned long timeout);
	SOCKRC Send(const char *buf, int bufsize);

	virtual SOCKRC SetBlocking(BOOL fBlocking) = 0;
	virtual BOOL GetBlocking() const = 0;

	virtual SOCKRC WaitReadable(unsigned long timeout) = 0;
	virtual SOCKRC WaitWritable(unsigned long timeout) = 0;

	virtual int Errno() = 0;

protected:
	virtual SOCKRC DoRecv(char *buf, int bufsize, int *recvd) = 0;
	virtual SOCKRC DoSend(const char *buf, int bufsize, int *sent, unsigned long timeout) = 0;
};

//==============================================================

#endif _SOCKET_H
