#include "stdafx.h"
#include "Format.h"

#include <ctype.h>
#include <string.h>
#include <sstream>
#include <iomanip>

using std::string;
using std::ostringstream;

//---------------------------------------------------------------------
//
//	Strategy for converting buffer to string.
//
//	1. display all unprintables as hex
//	2. Look for clumps of printables
//	3. If clump size >= iMinStringSize, display as string
//

static int LenPrintable(int iLen, const BYTE *pData)
{
	int i;

	for(i = 0; i < iLen; i++)
	{
		if((!isprint(pData[i])) || (pData[i] == '"'))
			break;
	}

	return i;
}

//---------------------------------------------------------------------

void Format::Buf2String(int iLen, const BYTE *pData, string &strDest, int iMinStringSize)
{
	int iLenPrintable;

	int i = 0;

	while(i < iLen)
	{
		iLenPrintable = LenPrintable(iLen - i, pData + i);

		if(iLenPrintable >= iMinStringSize)
		{
			if(i != 0)
				strDest += ',';

			strDest += '"';

			for(int j = 0; j < iLenPrintable; j++)
				strDest += pData[i++];

			strDest += '"';
		}
		else
		{
			if(i != 0)
				strDest += ',';

			ostringstream s;

			s << "0x" << std::setfill('0') << std::hex << std::setw(2) << std::uppercase << (int)(pData[i++]);
			strDest += s.str();
		}
	}
}

//---------------------------------------------------------------------
//
//	Anything inside quotes, strip the quotes and copy verbatim.
//	Anything outside quotes must be comma separated integers, with base prefix if non-decimal.
//

void Format::String2Buf(const string &strSrc, int *piLen, BYTE *pData)
{
	size_t iLen;
	const char *pPtr;

	iLen = 0;

	pPtr = (char *)strSrc.c_str();

	while(*pPtr)
	{
		if(*pPtr == '"')
		{
			pPtr++;
			size_t iStrLen = strcspn(pPtr, "\"");

			memcpy(pData + iLen, pPtr, iStrLen);
			iLen += iStrLen;
			pPtr += iStrLen;

			if(*pPtr)
				pPtr++;
		}
		else if(*pPtr == ',')
		{
			pPtr++;
		}
		else
		{
			size_t iNumLen = strcspn(pPtr, ",");

			string strTmp(pPtr, iNumLen);

			int iNum;

			sscanf_s(strTmp.c_str(), "%i", &iNum);

			pData[iLen++] = (BYTE)iNum;

			pPtr += iNumLen;
		}
	}

	if(piLen != NULL)
		*piLen = (int)iLen;
}

//---------------------------------------------------------------------

void Format::Buf2HexDump(int iLen, const BYTE *pData, string& strDest, int iWidth)
{
	int i, j;

	ostringstream os;
	os << std::setfill('0') << std::hex << std::uppercase;

	int iTotal;

	iTotal = ((iLen + iWidth - 1) / iWidth) * iWidth;

	for(i = 0; i < iTotal; i += iWidth)
	{
		// do address

		os << std::setw(8) << i << "  ";

		// do hex part

		for(j = 0; j < iWidth; j++)
		{
			if(j && ((j % 4) == 0))
				os << " ";

			if((i + j) < iLen)
			{
				os << std::setfill('0') << std::setw(2) << (int)(pData[i + j]) << " ";
			}
			else
			{
				os << "   ";
			}
		}

		// do asc part

		for(j = 0; j < iWidth; j++)
		{
			if((i + j) < iLen)
			{
				if(isprint((unsigned char)pData[i+j]))
					os << (char)(pData[i+j]);
				else
					os << (char)'.';
			}
		}

		os << "\n";
	}

	strDest = os.str();
}

