#ifndef _WORKBROKER_H
#define _WORKBROKER_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <vector>
#include <list>
#include <boost/utility.hpp>

//====================================================================

class WorkHandler {
public:
	virtual int GetCurrentWorkLoad() = 0;
};

//====================================================================

class WorkHandlerFactory {
public:
	virtual WorkHandler* CreateNewWorkHandler() = 0;
	virtual void DestroyWorkHandler(WorkHandler* wh) = 0;
};

//====================================================================

class WorkBroker : public boost::noncopyable {
public:
	WorkBroker(WorkHandlerFactory& whf);
	~WorkBroker();

	BOOL PreCreateHandlers(int iHandlers);

	WorkHandler* GetHandler();

	void RemoveWorkHandler(WorkHandler* wh);

	void SetMaxHandlers(int iMaxHandlers) { m_iMaxHandlers = iMaxHandlers; }
	void SetMaxWorkLoad(int iMaxWorkLoad) { m_iMaxWorkLoad = iMaxWorkLoad; }
	void SetMinWorkLoad(int iMinWorkLoad) { m_iMinWorkLoad = iMinWorkLoad; }

private:
	BOOL AddHandler();
	WorkHandler* FindSuitableHandler();

private:
	WorkHandlerFactory &m_whf;

	CritSect	m_Lockable;

	int m_iNumHandlers;

	int m_iMaxHandlers;	// do not automatically create new handlers
						// if this many already exist

	int m_iMaxWorkLoad;	// create new handler if all existing
						// handlers are higher than this level

	int m_iMinWorkLoad;	// use the first handler with this level
						// or lower

	typedef std::list<WorkHandler *> WHCollection;

	WHCollection m_collWorkHandlers;
	WHCollection::iterator m_itCursor;
};

//====================================================================

#endif // _WORKBROKER_H
