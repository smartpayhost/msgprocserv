#ifndef _MSGPROCHANDLER_H
#define _MSGPROCHANDLER_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _HANDLERTHREAD_H
#include "HandlerThread.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

//==============================================================

class MsgProcCfg;
class TcpBatchHandler;

//==============================================================

class MsgProcHandler : public HandlerThread {
public:
	MsgProcHandler();
	~MsgProcHandler();

	void SetConfig(MsgProcCfg *pConfig) { m_pConfig = pConfig; }

	void Shutdown();

protected:
    DWORD ThreadMemberFunc();

private:
	BOOL Reconfig();
	void ProcessNewConnection();

private:
	MsgProcCfg* m_pConfig;
	TcpBatchHandler *m_ptBatch;

	Event m_evShutdown;
	Event m_evActivity;

	enum MsgProcHandlerState {
		MPHS_STARTUP,
		MPHS_CONFIG_CHANGED,
		MPHS_CONFIG_ERROR,
		MPHS_FATAL_ERROR,
		MPHS_SHUTDOWN,
		MPHS_RESTART,
		MPHS_LISTENING,
		MPHS_RUNNING
	};

	MsgProcHandlerState m_eState;

	CritSect m_critShutdown;
};

//==============================================================

#endif // _MSGPROCHANDLER_H
