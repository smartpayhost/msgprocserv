#ifndef _SQLMATCHQUERYCOLLECTION_H
#define _SQLMATCHQUERYCOLLECTION_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _SQLMATCHQUERY_H
#include "SQLMatchQuery.h"
#endif

#include <map>
#include <boost/utility.hpp>

class MsgProcRuleCollection;
class Logger;
class SQLMessageQuery;

//======================================================================

class SQLMatchQueryCollection : public RuleEntityDescriptor, public boost::noncopyable {
public:
	SQLMatchQueryCollection();
	~SQLMatchQueryCollection();

	BOOL IsStart(const Line &rLine) const;
	RuleEntity *MakeNew(const Line &rLine);

	virtual BOOL Check() const;

	void AddSQLMatchQuery(SQLMatchQuery &rSQLMatchQuery);
	void Register(RuleFile &rRuleFile);
	BOOL Export(std::string &rStrDest);
	void MergeFrom(const SQLMatchQueryCollection& rhs);

	void SetLogger(Logger *pLogger);

	void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray);

	const SQLMatchQuery* FindByName(const std::string& strName) const;

private:
	SQLMessageQuery::SQLMessageQueryMap m_mapSQLMatchQuerys;

	MsgProcRuleCollection *m_pMsgProcRules;
	Logger *m_pLogger;
};

//======================================================================

#endif // _SQLMATCHQUERYCOLLECTION_H
