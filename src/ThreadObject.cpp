#include "stdafx.h"
#include "ThreadObject.h"

#include <process.h>

//==============================================================

ThreadObject::ThreadObject()
:	m_hThread(0),
	m_ThreadId(0),
	m_iPriority(THREAD_PRIORITY_NORMAL),
	m_fIsRunning(FALSE),
	m_pevOnExit(NULL),
	m_dwRetval(0)
{
}

//==============================================================

typedef unsigned (WINAPI *PBEGINTHREADEX_THREADFUNC)(LPVOID lpThreadParameter);
typedef unsigned *PBEGINTHREADEX_THREADID;

BOOL ThreadObject::StartThread()
{
    m_hThread = (HANDLE)::_beginthreadex(NULL,
        0,
        (PBEGINTHREADEX_THREADFUNC)ThreadObject::ThreadFunc,
        (LPVOID)this,
        CREATE_SUSPENDED,
        (PBEGINTHREADEX_THREADID)&m_ThreadId);

    if(m_hThread)
	{
		if(::SetThreadPriority(m_hThread, m_iPriority))
		{
			if(::ResumeThread(m_hThread) != 0xFFFFFFFFL)
			{
				m_fIsRunning = TRUE;
				return TRUE;
			}
		}
	}

	return FALSE;
}

//==============================================================

void ThreadObject::WaitForExit()
{
	if(m_hThread)
	{
		Wait();
		::CloseHandle(m_hThread);
		m_hThread = 0;
	}
}

//==============================================================

DWORD WINAPI ThreadObject::ThreadFunc(LPVOID param)
{
    ThreadObject *pto = (ThreadObject *)param;
    
	pto->m_dwRetval = pto->ThreadMemberFunc();

	pto->m_fIsRunning = FALSE;

	if(pto->m_pevOnExit)
		pto->m_pevOnExit->Set();

    return pto->m_dwRetval;
}

//==============================================================

BOOL ThreadObject::Suspend()
{
	if(0xFFFFFFFFL == ::SuspendThread(m_hThread))
		return FALSE;
	else
		return TRUE;
}

//==============================================================

BOOL ThreadObject::Resume()
{
	if(0xFFFFFFFFL == ::ResumeThread(m_hThread))
		return FALSE;
	else
		return TRUE;
}

//==============================================================
