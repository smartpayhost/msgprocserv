#ifndef _HANDLERTHREAD_H
#define _HANDLERTHREAD_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _STOPPABLETHREAD_H
#include "StoppableThread.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <vector>

//==============================================================

class HandlerThread : public StoppableThread {
public:
	HandlerThread();
	virtual ~HandlerThread() = 0;

    virtual void WaitForExit();

protected:
	void AddChild(StoppableThread *pChild);
	void SetupChildren() {}
	void ShutdownChildren();

private:
	typedef std::vector<StoppableThread *> ChildVector;
	ChildVector m_vectChildren;
	
	CritSect	m_Lockable;

	Event		m_evChildShutdown;
};

//==============================================================

#endif // _HANDLERTHREAD_H
