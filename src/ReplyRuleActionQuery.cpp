#include "stdafx.h"
#include "ReplyRuleActionQuery.h"

#include "ReplyTemplate.h"
#include "MsgProcRuleCollection.h"
#include "SQLActionQueryCollection.h"
#include "SQLResultTable.h"

#include <assert.h>

using std::string;

//======================================================================

ReplyRuleActionQuery::ReplyRuleActionQuery(const BitmapMsgStyle *pStyle)
:	ReplyRule(pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleActionQuery::Clone() const
{
	return new ReplyRuleActionQuery(*this);
}

//======================================================================
//
//	QUERY <query_name>
//

BOOL ReplyRuleActionQuery::Import(const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "QUERY") return FALSE;

	if(++i == rLine.end()) return FALSE;

	m_strQueryName = *i;

	return TRUE;
}

//======================================================================
//
//	TEMPLATE <template_name>
//

BOOL ReplyRuleActionQuery::Export(string &strDesc)
{
	string strTmp;

	strDesc = "QUERY " + m_strQueryName;

	return TRUE;
}

//======================================================================

BOOL ReplyRuleActionQuery::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	assert(GetMsgProcRuleCollection());

	const SQLActionQuery *pQuery = GetMsgProcRuleCollection()->GetSQLActionQueryCollection().FindByName(m_strQueryName);

	if(pQuery == NULL)
	{
		Log(LOG_WARNING, "No reply handling defined for this message type");
		return FALSE;
	}

	SQLResultTable rResult;
	return pQuery->ExecQuery(rResult, &rOrigMsg, &rNewMsg);
}

//======================================================================

BOOL ReplyRuleActionQuery::Check() const
{
	assert(GetMsgProcRuleCollection());

	if(GetMsgProcRuleCollection()->GetSQLActionQueryCollection().FindByName(m_strQueryName))
		return TRUE;

	Log(LOG_WARNING, "Reply template %s not defined", m_strQueryName.c_str());

	return FALSE;
}

//======================================================================
