#include "stdafx.h"
#include "MatchTemplateCollection.h"

#include "MatchTemplateRuleEntity.h"
#include "Message.h"

#include <assert.h>
#include <algorithm>
#include <boost/bind.hpp>

using std::string;

//======================================================================

MatchTemplateCollection::MatchTemplateCollection()
:	m_pMsgProcRules(NULL),
	m_pLogger(NULL)
{
}

//======================================================================

void MatchTemplateCollection::SetLogger(Logger *pLogger)
{
	m_pLogger = pLogger;

	MatchTemplateVector::iterator i;

	for(i = m_vectTemplates.begin(); i != m_vectTemplates.end(); i++)
		(*i)->SetLogger(m_pLogger);
}

//======================================================================

void MatchTemplateCollection::SetMsgProcRuleCollection(MsgProcRuleCollection *pArray)
{
	m_pMsgProcRules = pArray;

	MatchTemplateVector::const_iterator i;

	for(i = m_vectTemplates.begin(); i != m_vectTemplates.end(); i++)
		(*i)->SetMsgProcRuleCollection(m_pMsgProcRules);
}

//======================================================================

void MatchTemplateCollection::Register(RuleFile &rRuleFile)
{
	rRuleFile.RegisterEntityType(this);
}

//======================================================================

void MatchTemplateCollection::AddTemplate(const MatchTemplate &rTemplate)
{
	boost::shared_ptr<MatchTemplate> p(new MatchTemplate(rTemplate));

	p->SetMsgProcRuleCollection(m_pMsgProcRules);
	p->SetLogger(m_pLogger);
	
	m_vectTemplates.push_back(p);
	m_mapTemplates[rTemplate.GetName()] = p;
}

//======================================================================

BOOL MatchTemplateCollection::Export(string &rStrDest)
{
	MatchTemplateVector::iterator i;

	for(i = m_vectTemplates.begin(); i != m_vectTemplates.end(); i++)
	{
		string strTmp;

		BOOL fRetval = (*i)->Export(strTmp);

		if(!fRetval)
			return FALSE;

		rStrDest += strTmp;
		rStrDest += '\n';
	}

	rStrDest += '\n';

	return TRUE;
}

//======================================================================

void MatchTemplateCollection::MergeFrom(const MatchTemplateCollection& rhs)
{
	for(MatchTemplateVector::const_iterator i = rhs.m_vectTemplates.begin(); i != rhs.m_vectTemplates.end(); i++)
	{
		AddTemplate(*(*i));
	}
}

//======================================================================

BOOL MatchTemplateCollection::IsStart(const Line &rLine) const
{
	if(rLine.size() != 2)
		return FALSE;

	if(rLine[0] == "MATCH_TEMPLATE")
		return TRUE;

	return FALSE;
}

//======================================================================

RuleEntity* MatchTemplateCollection::MakeNew(const Line &rLine)
{
	MatchTemplateRuleEntity *pResult;

	pResult = new MatchTemplateRuleEntity;

	if(!pResult)
		return NULL;

	pResult->SetName(rLine[1].c_str());

	pResult->SetMatchTemplateCollection(this);

	return pResult;
}

//======================================================================
	
const MatchTemplate* MatchTemplateCollection::FindByName(const string& strName) const
{
	MatchTemplateMap::const_iterator i;

	i = m_mapTemplates.find(strName);

	if(i == m_mapTemplates.end())
		return NULL;
	else
		return (*i).second.get();
}

//======================================================================

const MatchTemplate* MatchTemplateCollection::FindMatchTemplate(Message& rMsg) const
{
	MatchTemplateVector::const_iterator it;

	it = std::find_if(m_vectTemplates.begin(), m_vectTemplates.end(),
		boost::bind(&MatchTemplate::DoesMatch_p, _1, &rMsg)
		);

	if(it != m_vectTemplates.end())
		return (*it).get();

	return NULL;
}

//======================================================================

BOOL MatchTemplateCollection::Check() const
{
	BOOL fRetval = TRUE;

	MatchTemplateVector::const_iterator it;

	for(it = m_vectTemplates.begin(); it != m_vectTemplates.end(); ++it)
	{
		if(!(*it)->Check())
			fRetval = FALSE;
	}

	return fRetval;
}

//======================================================================
