#ifndef _REPLYRULEACTIONQUERY_H
#define _REPLYRULEACTIONQUERY_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULE_H
#include "ReplyRule.h"
#endif

//==============================================================

class ReplyRuleActionQuery : public ReplyRule {
public:
	explicit ReplyRuleActionQuery(const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Check() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const;

private:
	std::string m_strQueryName;
};

//==============================================================

#endif // _REPLYRULEACTIONQUERY_H
