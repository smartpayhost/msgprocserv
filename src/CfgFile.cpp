#include "stdafx.h"
#include "CfgFile.h"

#include <stdio.h>
#include <io.h>
#include <share.h>
#include <algorithm>

using std::string;
using std::make_pair;
using std::vector;
using std::pair;

//======================================================================

BOOL CfgFile::Load(const string &strFilename, FILETIME *pftConfigLastChange)
{
	FILE *fp;

	fp = _fsopen(strFilename.c_str(), "rt", _SH_DENYWR);

	if(fp == NULL)
		return FALSE;

	if(pftConfigLastChange)
		::GetFileTime((HANDLE)_get_osfhandle(_fileno(fp)), NULL, NULL, pftConfigLastChange);

	char linebuf[1024];
	const char *whitespace = " \t\n";
	const char *line_delimiters = "\n";

	BOOL fRetval = TRUE;

	while(fgets(linebuf, 1024, fp))
	{
		char *ptr = linebuf;

		// Strip comments and join multi-lines.

		while(*ptr != '\0')
		{
			// Comments run to end of line

			if((*ptr == '#') || (*ptr == ';'))
			{
				*ptr = '\0';
				break;
			}

			// Join lines 

			if(*ptr == '\\')
			{
				if(fgets(ptr, (int)(1024 - (ptr - linebuf)), fp) == NULL)
				{
					return FALSE;
				}
			}

			ptr += strcspn(ptr, whitespace);	// Go to end of this token
			ptr += strspn(ptr, whitespace);		// Go to start of next token
		}
		
		// Process line

		char* next_token;

		ptr = strtok_s(linebuf, whitespace, &next_token);

		if(ptr != NULL)
		{
			string strKey(ptr);

			std::transform(strKey.begin(), strKey.end(), 
				strKey.begin(),
				::toupper);	// Convert token to uppercase

			string strValue;

			ptr = strtok_s(NULL, line_delimiters, &next_token);

			if(ptr != NULL)
				strValue = ptr;

			m_mapConfig.insert(make_pair(strKey, strValue));
		}
	}

	fclose(fp);

	return fRetval;
}

//======================================================================

BOOL CfgFile::GetStringValue(const string &strKey, string &strResult)
{
	StringMap::iterator i;

	i = m_mapConfig.find(strKey);

	if(i == m_mapConfig.end())
		return FALSE;

	strResult = (*i).second;

	return TRUE;
}

//----------------------------------------------------------------------

int CfgFile::GetStringValues(const string &strKey, vector<string>& Result)
{
	pair<StringMap::iterator, StringMap::iterator> Range;

	Range = m_mapConfig.equal_range(strKey);

	StringMap::difference_type iCount(0);

	iCount = std::distance(Range.first, Range.second);

	for(StringMap::iterator i = Range.first; i != Range.second; i++)
		Result.push_back((*i).second);

	return (int)iCount;
}

//----------------------------------------------------------------------

BOOL CfgFile::GetIntValue(const string &strKey, int &iResult)
{
	string strResult;

	if(!GetStringValue(strKey, strResult))
		return FALSE;

	iResult = (int)strtol(strResult.c_str(), NULL, 0);

	return TRUE;
}

//======================================================================
