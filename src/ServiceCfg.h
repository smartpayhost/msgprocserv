#ifndef _SERVICECFG_H
#define _SERVICECFG_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Config.h"
#include "LoggingObject.h"
#include "HostDescription.h"
#include "CfgFile.h"

#include <boost/utility.hpp>

//==============================================================

class ServiceCfg : public LoggingObject , public boost::noncopyable {
public:
	ServiceCfg();

	virtual BOOL ReadFromFile(const std::string& strFilename, FILETIME *pftConfigLastChange = NULL);

	std::string GetServiceName() const { return m_strServiceName; }
	std::string GetServiceDisplayName() const { return m_strServiceDisplayName; }
	std::string GetLogFile() const { return m_strLogFile; }
	std::string GetWorkDir() const { return m_strWorkDir; }
	int	GetLogLevel() const { return m_iLogLevel; }

	Config::EncryptionType GetEncryptionType() const { return m_encryptionType; }; 

	std::string GetLogDBDSN() const { return m_strLogDBDSN; }
	std::string GetLogDBUID() const { return m_strLogDBUID; }
	std::string GetLogDBAuthStr() const { return m_strLogDBAuthStr; }
	std::string GetLogDBTraceFile() const { return m_strLogDBTraceFile; }
	int GetLogDBTimeout() const { return m_iLogDBTimeout; }
	int GetLogDBLogTypes() const { return m_iLogDBLogTypes; }
	
protected:
	CfgFile m_sCfg;
	
private:
	std::string m_strServiceName;
	std::string m_strServiceDisplayName;

	int	m_iLogLevel;
	std::string m_strLogFile;
	std::string m_strWorkDir;

	std::string m_strLogDBDSN;
	std::string m_strLogDBUID;
	std::string m_strLogDBAuthStr;
	std::string m_strLogDBTraceFile;
	int m_iLogDBTimeout;
	int m_iLogDBLogTypes;

	Config::EncryptionType m_encryptionType;

	BOOL LoadEncryptionType();
};

//==============================================================

#endif // _SERVICECFG_H
