#ifndef _SQLREPLYQUERY_H
#define _SQLREPLYQUERY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SQLMESSAGEQUERY_H
#include "SQLMessageQuery.h"
#endif

class BitmapMsg;
class SQLResultTable;

//======================================================================

class SQLReplyQuery : public SQLMessageQuery {
public:
	BOOL Export(std::string &rStrDest) const;

	BOOL ExecQuery(SQLResultTable &rResult, const BitmapMsg &rOrigMsg, const BitmapMsg &rNewMsg) const;
};

//======================================================================

#endif // _SQLREPLYQUERY_H
