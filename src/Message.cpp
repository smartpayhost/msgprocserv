#include "stdafx.h"
#include "MessagePacket.h"

#include "NetworkHeader.h"
#include "DesBlock.h"
#include "MAC.h"
#include "ETSL6Header.h"
#include "ChinaUnionPayHeader.h"

#include <vector>

using std::string;
using std::set;
using std::vector;

extern BitmapMsgStyle TemplateISO8583;

//===================================================================

const int Message::NETWORK_HDR_LEN_NH1 = 10;
const int Message::NETWORK_HDR_LEN_NH2 = 2;
const int Message::COMMS_HDR_LEN_TPDU = 5;

//===================================================================

Message::Message()
:	m_iRawLen(0),
	m_iLenRemaining(0),
	m_eNetworkHdrType(Message::NHT_NH1),
	m_iNetworkHdrLen(0),
	m_eCommsHdrType(CHT_AUTOMATIC),
	m_iCommsHdrLen(0),
	m_pRawCommsHdr(NULL),
	m_pRawBMsg(NULL),
	m_pStyle(&TemplateISO8583),
	m_f64Bit(FALSE),
	m_fAnsi(TRUE),
	m_bEtsl6Encrypted(FALSE),
	m_bChinaUnionPay(FALSE)
{
	memset(m_abRawBuf, 0, sizeof(m_abRawBuf));
	memset(m_abNetworkHeader, 0, sizeof(m_abNetworkHeader));
	memset(m_abCommsHeader, 0, sizeof(m_abCommsHeader));
}

//===================================================================

BOOL Message::SetRawMsg(int iLen, const BYTE *pBuf)
{
	if((iLen > MAX_RAW_MSG_LEN) || (iLen < 0))
		return FALSE;

	m_iRawLen = iLen;
	m_iLenRemaining = m_iRawLen;

	memcpy(m_abRawBuf, pBuf, iLen);

	return TRUE;
}

//===================================================================

void Message::BuildRawMsg()
{
	BYTE *pPtr;

	pPtr = m_abRawBuf;
	m_iRawLen = 0;

	if(m_iNetworkHdrLen)
	{
		memcpy(pPtr, m_abNetworkHeader, m_iNetworkHdrLen);
		m_iRawLen += m_iNetworkHdrLen;
		pPtr += m_iNetworkHdrLen;
	}

	if(m_iCommsHdrLen)
	{
		m_pRawCommsHdr = pPtr;
		memcpy(pPtr, m_abCommsHeader, m_iCommsHdrLen);
		m_iRawLen += m_iCommsHdrLen;
		pPtr += m_iCommsHdrLen;
	}

	if(m_sBMsg.GetMsgLen())
	{
		m_pRawBMsg = pPtr;
		memcpy(pPtr, m_sBMsg.GetRawMsg(), m_sBMsg.GetMsgLen());
		m_iRawLen += m_sBMsg.GetMsgLen();
		pPtr += m_sBMsg.GetMsgLen();
	}
}

//===================================================================

BOOL Message::DecodeNetworkHeaderNone()
{
	m_eNetworkHdrType = Message::NHT_NONE;
	m_iNetworkHdrLen = 0;
	memset(m_abNetworkHeader, 0, sizeof(m_abNetworkHeader));

	return TRUE;
}

//-------------------------------------------------------------------

BOOL Message::DecodeNetworkHeaderNH1()
{
	if(m_iRawLen < NETWORK_HDR_LEN_NH1)
		return FALSE;

	if(m_abRawBuf[0] != 0x00 || m_abRawBuf[1] != 0x01 ||
		m_abRawBuf[2] != 0x00 || m_abRawBuf[3] != 0x0A)
		return FALSE;

	m_eNetworkHdrType = Message::NHT_NH1;
	m_iNetworkHdrLen = NETWORK_HDR_LEN_NH1;
	memset(m_abNetworkHeader, 0, sizeof(m_abNetworkHeader));
	memcpy(m_abNetworkHeader, m_abRawBuf, m_iNetworkHdrLen);

	return TRUE;
}

//-------------------------------------------------------------------

BOOL Message::DecodeNetworkHeaderNH2()
{
	if((m_iRawLen < NETWORK_HDR_LEN_NH2) ||
		((m_abRawBuf[0] << 8) + m_abRawBuf[1]) != (m_iRawLen - NETWORK_HDR_LEN_NH2))
		return FALSE;

	m_eNetworkHdrType = Message::NHT_NH2;
	m_iNetworkHdrLen = NETWORK_HDR_LEN_NH2;
	memset(m_abNetworkHeader, 0, sizeof(m_abNetworkHeader));
	memcpy(m_abNetworkHeader, m_abRawBuf, m_iNetworkHdrLen);

	return TRUE;
}

//-------------------------------------------------------------------

BOOL Message::DecodeNetworkHeaderAutomatic()
{
	if(DecodeNetworkHeaderNH1())
		return TRUE;

	if(DecodeNetworkHeaderNH2())
		return TRUE;

	return FALSE;
}

//-------------------------------------------------------------------

BOOL Message::DecodeNetworkHeader(MNetworkHdrType eNetworkHdrType)
{
	BOOL fRetVal;

	switch(eNetworkHdrType)
	{
	case Message::NHT_NONE:
		fRetVal = DecodeNetworkHeaderNone();
		break;

	case Message::NHT_NH1:
		fRetVal = DecodeNetworkHeaderNH1();
		break;

	case Message::NHT_NH2:
		fRetVal = DecodeNetworkHeaderNH2();
		break;

	default:
		fRetVal = DecodeNetworkHeaderAutomatic();
		break;
	}

	if(fRetVal)
	{
		m_pRawCommsHdr = m_abRawBuf + m_iNetworkHdrLen;
		m_iLenRemaining -= m_iNetworkHdrLen;
	}

	return fRetVal;
}

//===================================================================

BOOL Message::DecodeCommsHeaderNone()
{
	m_eCommsHdrType = CHT_NONE;
	m_iCommsHdrLen = 0;
	memset(m_abCommsHeader, 0, sizeof(m_abCommsHeader));

	return TRUE;
}

//-------------------------------------------------------------------

BOOL Message::DecodeCommsHeaderTPDU()
{
	if(m_pRawCommsHdr[0] != 0x60 && m_pRawCommsHdr[0] != 0x68)
		return FALSE;

	m_eCommsHdrType = CHT_TPDU;
	m_iCommsHdrLen = COMMS_HDR_LEN_TPDU;
	memset(m_abCommsHeader, 0, sizeof(m_abCommsHeader));
	memcpy(m_abCommsHeader, m_pRawCommsHdr, COMMS_HDR_LEN_TPDU);

	return TRUE;
}

//-------------------------------------------------------------------

BOOL Message::DecodeCommsHeaderAutomatic()
{
	if(DecodeCommsHeaderTPDU())
		return TRUE;

	if(DecodeCommsHeaderNone())
		return TRUE;

	return FALSE;
}

//-------------------------------------------------------------------

BOOL Message::DecodeCommsHeader(MCommsHdrType eCommsHdrType)
{
	BOOL fRetVal;

	switch(eCommsHdrType)
	{
	case CHT_NONE:
		fRetVal = DecodeCommsHeaderNone();
		break;

	case CHT_TPDU:
		fRetVal = DecodeCommsHeaderTPDU();
		break;

	default:
		fRetVal = DecodeCommsHeaderAutomatic();
		break;
	}

	if(fRetVal)
	{
		m_pRawBMsg = m_pRawCommsHdr + m_iCommsHdrLen;
		m_iLenRemaining -= m_iCommsHdrLen;
	}

	return fRetVal;
}

//===================================================================

BOOL Message::DecodeBMsg(int iLen, const BYTE *pBuf)
{
	const BYTE *pNext;

	m_sBMsg.SetMsgStyle(m_pStyle);

	if(!m_sBMsg.SetMsg(iLen, pBuf, &pNext))
		return FALSE;

	if(pNext != (pBuf + iLen))
		return FALSE;

	return TRUE;
}

//-------------------------------------------------------------------

BOOL Message::DecodeBMsg()
{
	// Comms header has been processed.
	//	m_pRawBMsg is valid
	//	m_pStyle is valid

	return DecodeBMsg(m_iLenRemaining, m_pRawBMsg);
}

//-------------------------------------------------------------------

BOOL Message::DecodeAuto()
{
	if(!DecodeNetworkHeader(NHT_AUTOMATIC))
		return FALSE;

	if(!DecodeCommsHeader(CHT_AUTOMATIC))
		return FALSE;

	if(GetRawBMsg()[0] == ENCRYPTION_INDICATOR)
	{	
		SetEtsl6Encrypted(TRUE);
		Etsl6BuildFakePacket();
	}

	if(ChinaUnionPayHeader().IsChinaUnionPayHeader(GetRawBMsg()) && !GetEtsl6Encrypted())
	{
		SetChinaUnionPay(TRUE);
		ChinaUnionPayBuildFakePacket();
	}

	if(!DecodeBMsg())
		return FALSE;

	return TRUE;
}

//===================================================================

void Message::TurnaroundCommsHeaderNone(const Message & /*rMsg*/)
{
	m_eCommsHdrType = CHT_NONE;
	m_iCommsHdrLen = 0;
}

//-------------------------------------------------------------------

void Message::TurnaroundCommsHeaderTPDU(const Message &rMsg)
{
	m_eCommsHdrType = CHT_TPDU;
	m_iCommsHdrLen = rMsg.m_iCommsHdrLen;
	m_abCommsHeader[0] = rMsg.m_abCommsHeader[0];
	m_abCommsHeader[1] = rMsg.m_abCommsHeader[3];
	m_abCommsHeader[2] = rMsg.m_abCommsHeader[4];
	m_abCommsHeader[3] = rMsg.m_abCommsHeader[1];
	m_abCommsHeader[4] = rMsg.m_abCommsHeader[2];
}

//-------------------------------------------------------------------

void Message::TurnaroundCommsHeader(const Message &rMsg)
{
	switch(rMsg.m_eCommsHdrType)
	{
	case CHT_TPDU:
		TurnaroundCommsHeaderTPDU(rMsg);
		break;

	default:
		TurnaroundCommsHeaderNone(rMsg);
		break;
	}
}

//-------------------------------------------------------------------

void Message::TurnaroundNetworkHeaderNone(const Message & /*rMsg*/)
{
}

//-------------------------------------------------------------------

void Message::TurnaroundNetworkHeaderNH1(const Message &rMsg)
{
	NetworkHeader sHdr;

	sHdr.CopyFrom(rMsg.m_abNetworkHeader);

	sHdr.SetDataLen((WORD)(m_iCommsHdrLen + m_sBMsg.GetMsgLen()));

	sHdr.CopyTo(m_abNetworkHeader);
	m_iNetworkHdrLen = NETWORK_HDR_LEN_NH1;
	m_eNetworkHdrType = Message::NHT_NH1;
}

//-------------------------------------------------------------------

void Message::TurnaroundNetworkHeaderNH2()
{
	NetworkHeader sHdr;

	sHdr.SetHeaderType(TRUE);
	sHdr.SetDataLen((WORD)(m_iCommsHdrLen + m_sBMsg.GetMsgLen()));

	sHdr.CopyTo(m_abNetworkHeader, TRUE);
	m_iNetworkHdrLen = NETWORK_HDR_LEN_NH2;
	m_eNetworkHdrType = Message::NHT_NH2;
}

//-------------------------------------------------------------------

void Message::TurnaroundNetworkHeader(const Message &rMsg)
{
	switch(rMsg.m_eNetworkHdrType)
	{
	case Message::NHT_NH1:
		TurnaroundNetworkHeaderNH1(rMsg);
		break;
	
	case Message::NHT_NH2:
		TurnaroundNetworkHeaderNH2();
		break;
	
	default:
		TurnaroundNetworkHeaderNone(rMsg);
		break;
	}
}

//====================================================================

BOOL Message::CheckMacKey(const string &sKey)
{
	const BitmapMsgField *pField;
	DesBlock s;

	if(sKey.empty())
		return FALSE;

	if(!DesBlock::CheckValidKey(sKey.c_str()))
	{
		return FALSE;
	}

	if((pField = m_sBMsg.GetField(128)) == NULL)		// MAC
		if((pField = m_sBMsg.GetField(64)) == NULL)	// MAC
			return FALSE;

	s.SetKeyFromString(sKey.c_str(), DesBlock::CM_ENCRYPT);

	BYTE bMab[8];

	const BYTE *pBuf = m_sBMsg.GetRawMsg();
	int iLen = m_sBMsg.GetMsgLen() - 8;

	MAC::ANSI(&s, iLen, pBuf, bMab);

	if(memcmp(bMab, pField->GetData(), 8) == 0)
	{
		m_fAnsi = TRUE;
		m_f64Bit = TRUE;
		return TRUE;
	}

	if(memcmp(bMab, pField->GetData(), 4) == 0)
	{
		m_fAnsi = TRUE;
		m_f64Bit = FALSE;
		return TRUE;
	}

	MAC::NETS(&s, iLen, pBuf, bMab);

	if(memcmp(bMab, pField->GetData(), 8) == 0)
	{
		m_fAnsi = FALSE;
		m_f64Bit = TRUE;
		return TRUE;
	}

	if(memcmp(bMab, pField->GetData(), 4) == 0)
	{
		m_fAnsi = FALSE;
		m_f64Bit = FALSE;
		return TRUE;
	}

	return FALSE;
}

//-------------------------------------------------------------------

void Message::SetBitmapMsg(const vector<BYTE> &bMsg)
{
	memcpy( m_pRawBMsg, &bMsg[0], bMsg.size() );
	m_iLenRemaining = (int)bMsg.size();
}

//-------------------------------------------------------------------

void Message::Etsl6BuildFakePacket()
{
	ETSL60Header header(GetRawBMsg());

	m_oldEtsl6Message = vector<BYTE>(GetRawBMsg(), GetRawBMsg() + GetLenRemaining());

	SetBitmapMsg(header.BuildAS2805());
}

//-------------------------------------------------------------------

void Message::ChinaUnionPayBuildFakePacket()
{
	ChinaUnionPayHeader header(GetRawBMsg());

	m_ChinaUnionPayMessage = vector<BYTE>(GetRawBMsg(), GetRawBMsg() + GetLenRemaining());

	SetBitmapMsg(header.BuildAS2805());
}

//-------------------------------------------------------------------

void Message::CheckMac()
{
	if((m_sBMsg.GetField(128) == NULL) &&
		(m_sBMsg.GetField(64) == NULL))	// MAC
	{
		m_sBMsg.SetMacValidity(MAC_NONE);
		return;
	}

	std::set<std::string>::const_iterator i;

	for(i = m_PossibleMacKeys.begin(); i != m_PossibleMacKeys.end(); i++)
	{
		if(CheckMacKey(*i))
		{
			SetMacKey(*i);
			m_sBMsg.SetMacValidity(MAC_GOOD);
			return;
		}
	}

	m_sBMsg.SetMacValidity(MAC_BAD);
}

//-------------------------------------------------------------------

BOOL Message::GenResponseMac(const string& strKey, BOOL fAnsi, BOOL f64Bit)
{
	BitmapMsgField *pField;
	DesBlock s;

	if((pField = m_sBMsg.GetField(128)) == NULL)		// MAC
		if((pField = m_sBMsg.GetField(64)) == NULL)
			return FALSE;

	BYTE bMab[8];

	if(!DesBlock::CheckValidKey(strKey.c_str()))
	{
		memset(bMab, 0xFF, 8);

		if(!f64Bit)
			memset(bMab + 4, 0, 4);

		pField->SetFieldContents(bMab, 8);
		return FALSE;
	}

	s.SetKeyFromString(strKey.c_str(), DesBlock::CM_ENCRYPT);

	const BYTE *pBuf = m_sBMsg.GetRawMsg();
	int iLen = m_sBMsg.GetMsgLen() - 8;

	if(fAnsi)
		MAC::ANSI(&s, iLen, pBuf, bMab);
	else
		MAC::NETS(&s, iLen, pBuf, bMab);

	if(!f64Bit)
		memset(bMab + 4, 0, 4);

	return pField->SetFieldContents(bMab, 8);
}

//===================================================================
