#include "stdafx.h"
#include "SQLReplyQueryRuleEntity.h"

#include "SQLReplyQueryCollection.h"
#include <assert.h>

using std::string;

//======================================================================

SQLReplyQueryRuleEntity::SQLReplyQueryRuleEntity()
:	m_fComplete(FALSE),
	m_pCollection(NULL)
{
}

//======================================================================

void SQLReplyQueryRuleEntity::SetName(const string &strName)
{
	m_dsSQLReplyQuery.SetName(strName);
}

//======================================================================

void SQLReplyQueryRuleEntity::SetSQLReplyQueryCollection(SQLReplyQueryCollection *pCollection)
{
	m_pCollection = pCollection;
}

//----------------------------------------------------------------------

BOOL SQLReplyQueryRuleEntity::ProcessDataSource(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsSQLReplyQuery.SetDataSource(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL SQLReplyQueryRuleEntity::ProcessQuery(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsSQLReplyQuery.SetQuery(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL SQLReplyQueryRuleEntity::ProcessReplyQueryEnd(const Line & /*rLine*/)
{
	m_fComplete = TRUE;

	assert(m_pCollection);
	m_pCollection->AddSQLReplyQuery(m_dsSQLReplyQuery);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL SQLReplyQueryRuleEntity::ProcessLine(const Line &rLine)
{
	if(rLine[0] == "DATA_SOURCE")
	{
		return ProcessDataSource(rLine);
	}
	else if(rLine[0] == "QUERY")
	{
		return ProcessQuery(rLine);
	}
	else if(rLine[0] == "REPLY_QUERY_END")
	{
		return ProcessReplyQueryEnd(rLine);
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL SQLReplyQueryRuleEntity::IsComplete() const
{
	return m_fComplete;
}

//======================================================================
