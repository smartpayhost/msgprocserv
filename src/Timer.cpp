#include "stdafx.h"
#include "Timer.h"

//===========================================================

Timer::Timer()
:	m_dwDuration(0L),
	m_dwExpireTime(::GetTickCount())
{
}

//-----------------------------------------------------------

Timer::Timer(DWORD dwMilliseconds)
{
	Start(dwMilliseconds);
}

//===========================================================
	
void Timer::Start(DWORD dwMilliseconds)
{
	m_dwDuration = dwMilliseconds;
	m_dwExpireTime = ::GetTickCount() + m_dwDuration;
}

//-----------------------------------------------------------
	
void Timer::Restart()
{
	m_dwExpireTime = ::GetTickCount() + m_dwDuration;
}

//===========================================================

int Timer::IsExpired()
{
	if(Remaining() == 0L)
		return 1;
	else
		return 0;
}

//-----------------------------------------------------------

DWORD Timer::Remaining()
{
	DWORD d;

	d = m_dwExpireTime - ::GetTickCount();

	if(d <= m_dwDuration)
		return d;
	else
		return 0L;
}

