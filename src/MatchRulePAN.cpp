#include "stdafx.h"
#include "MatchRulePAN.h"

#include "MessagePacket.h"
#include "SQLResultTable.h"
#include "SQLMatchQuery.h"
#include "SQLMatchQueryCollection.h"
#include "MsgProcRuleCollection.h"

#include "RegexpMatch.h"

#include <assert.h>

#include <set>
#include <fstream>

using std::string;
using std::set;
using std::ifstream;

//======================================================================

MatchRulePAN::MatchRulePAN()
:	m_eWhat(MRP_WHOLE_FIELD),
	m_eWith(MRP_FIXED_DATA),
	m_iFirst(1),
	m_iLast(1),
	m_iFrom(0),
	m_iTo(999)
{
}

//---------------------------------------------------------

MatchRule* MatchRulePAN::Clone() const
{
	return new MatchRulePAN(*this);
}

//======================================================================
//
//	PAN [ [ NOT ] PRESENT ]
//	PAN [ NOT ] [ { FIRST <n> | LAST <n> | FROM <m> TO <n> } ] { MATCHES | REGEXP | IN_FILE | QUERY } <data> 
//

BOOL MatchRulePAN::Import(const Line &rLine)
{
	m_bInverted = FALSE;
	int iFirst = -1;
	int iLast = -1;
	int iFrom = -1;
	int iTo = -1;

	m_eWhat = MRP_WHOLE_FIELD;
	m_eWith = MRP_PRESENCE;
	m_strMatchData.erase();

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "PAN") return FALSE;

	if(++i == rLine.end()) 
		return TRUE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	// figure out what to test

	if((*i) == "FIRST")
	{
		m_eWhat = MRP_FIRST_N;
		if(++i == rLine.end()) return FALSE;
		iFirst = atoi((*i).c_str());
		if(++i == rLine.end()) return FALSE;
	}
	else if((*i) == "LAST")
	{
		m_eWhat = MRP_LAST_N;
		if(++i == rLine.end()) return FALSE;
		iLast = atoi((*i).c_str());
		if(++i == rLine.end()) return FALSE;
	}
	else if((*i) == "FROM")
	{
		m_eWhat = MRP_FROM_M_TO_N;
		if(++i == rLine.end()) return FALSE;
		iFrom = atoi((*i).c_str());
		if(++i == rLine.end()) return FALSE;

		if((*i) == "TO")
			if(++i == rLine.end()) return FALSE;

		iTo = atoi((*i).c_str());
		if(++i == rLine.end()) return FALSE;
	}

	// figure out what to test with

	if((*i) == "MATCHES")
	{
		m_eWith = MRP_FIXED_DATA;
	}
	else if((*i) == "PRESENT")
	{
		m_eWith = MRP_PRESENCE;
		return TRUE;
	}
	else if((*i) == "REGEXP")
	{
		m_eWith = MRP_REGEXP;
	}
	else if((*i) == "IN_FILE")
	{
		m_eWith = MRP_FILE;
	}
	else if(((*i) == "IN_QUERY")||((*i) == "QUERY"))
	{
		m_eWith = MRP_SQL;
	}
	else
	{
		return FALSE;
	}

	if(++i == rLine.end()) return FALSE;

	m_strMatchData = (*i).c_str();

	if(iFirst != -1) m_iFirst = iFirst;
	if(iLast != -1) m_iLast = iLast;
	if(iFrom != -1) m_iFrom = iFrom;
	if(iTo != -1) m_iTo = iTo;

	return TRUE;
}

//======================================================================
//
//	PAN [ NOT ] [PRESENT]
//	PAN [ NOT ] [ { FIRST <n> | LAST <n> | FROM <m> TO <n> } ] { MATCHES | REGEXP | IN_FILE | QUERY } <data> 
//

BOOL MatchRulePAN::Export(string &strDesc)
{
	string strTmp;

	strDesc = "PAN";

	if(m_bInverted)
		strDesc += " NOT";

	switch(m_eWhat)
	{
	case MRP_WHOLE_FIELD:
		break;

	case MRP_FIRST_N:
		StrUtil::Format(strTmp, " FIRST %d", m_iFirst);
		strDesc += strTmp;
		break;

	case MRP_LAST_N:
		StrUtil::Format(strTmp, " LAST %d", m_iLast);
		strDesc += strTmp;
		break;

	case MRP_FROM_M_TO_N:
		StrUtil::Format(strTmp, " FROM %d TO %d", m_iFrom, m_iTo);
		strDesc += strTmp;
		break;

	default:
		return FALSE;
	}

	switch(m_eWith)
	{
	case MRP_PRESENCE:		strDesc += " PRESENT";	break;
	case MRP_FIXED_DATA:	strDesc += " MATCHES";	break;
	case MRP_REGEXP:		strDesc += " REGEXP";	break;
	case MRP_FILE:			strDesc += " IN_FILE";	break;
	case MRP_SQL:			strDesc += " QUERY";	break;

	default:
		return FALSE;
	}

	StrUtil::PurifyString(m_strMatchData, strTmp);
	strDesc += ' ';
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL MatchRulePAN::DoesMatchFile(const string& strFile, const string& strTestData) const
{
	set<string> file_lines;

	ifstream ifs;
	ifs.open(strFile.c_str());

	if(ifs.fail())
		return FALSE;

	char buf[256];

	while(ifs.getline(buf, sizeof(buf)))
	{
		file_lines.insert(string(buf));
	}

	if(file_lines.find(strTestData) != file_lines.end())
		return TRUE;

	return FALSE;
}

//======================================================================

BOOL MatchRulePAN::DoesMatchSQL(const string& strQuery, const string& strTestData, const BitmapMsg *pMsg) const
{
	assert(pMsg);
	assert(m_pMsgProcRules);

	const SQLMatchQuery *pQuery = m_pMsgProcRules->GetSQLMatchQueryCollection().FindByName(strQuery);

	assert(pQuery);

	if(!pQuery)
		return FALSE;

	SQLResultTable tblResult;

	if(!pQuery->ExecQuery(tblResult, *pMsg))
		return FALSE;

	if(tblResult.FindIn(strTestData))
		return TRUE;

	return FALSE;
}

//======================================================================

BOOL MatchRulePAN::DoesMatch(const string& strTestData, const BitmapMsg *pMsg) const
{
	assert(pMsg);

	BOOL bResult = FALSE;
	string strTest;

	switch(m_eWhat)
	{
	case MRP_FIRST_N:
		strTest = strTestData.substr(0, m_iFirst);
		break;

	case MRP_LAST_N:
		strTest = strTestData.substr(strTestData.size() - m_iLast, m_iLast);
		break;

	case MRP_FROM_M_TO_N:
		strTest = strTestData.substr(m_iFrom, m_iTo - m_iFrom + 1);
		break;

	case MRP_WHOLE_FIELD:
	default:
		strTest = strTestData;
		break;
	}

	switch(m_eWith)
	{
	case MRP_REGEXP:
		bResult = RegexpMatch(m_strMatchData, strTest);
		break;

	case MRP_FILE:
		bResult = DoesMatchFile(m_strMatchData, strTest);
		break;

	case MRP_SQL:
		bResult = DoesMatchSQL(m_strMatchData, strTest, pMsg);
		break;

	case MRP_PRESENCE:
		if(!strTest.empty())
			bResult = TRUE;
		break;

	case MRP_FIXED_DATA:
	default:
		if(strTest == m_strMatchData)
			bResult = TRUE;
		break;
	}

	if(m_bInverted)
		return !bResult;

	return bResult;
}

//======================================================================

BOOL MatchRulePAN::DoesMatch(Message& rMsg) const
{
	string strMatchData = rMsg.m_sBMsg.GetPAN();

	return DoesMatch(strMatchData, &(rMsg.m_sBMsg));
}

//======================================================================
