#ifndef _REPLYRULEACTIONPROG_H
#define _REPLYRULEACTIONPROG_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULE_H
#include "ReplyRule.h"
#endif

//==============================================================

class ReplyRuleActionProg : public ReplyRule {
public:
	explicit ReplyRuleActionProg(const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const;

private:
	std::string m_strCmdline;
};

//==============================================================

#endif // _REPLYRULEACTIONPROG_H
