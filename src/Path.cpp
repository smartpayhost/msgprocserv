#include "stdafx.h"
#include "Path.h"

#include <direct.h>
#include <sys/stat.h>
#include <string>

using std::string;

//======================================================================

Path::Path()
{
	char szBuf[_MAX_PATH];

	if(_getcwd(szBuf, sizeof(szBuf)))
		SetPath(szBuf);
	else
		SetPath("");
}

//----------------------------------------------------------------------

Path::Path(const string& strPath)
{
	SetPath(strPath);
}

//----------------------------------------------------------------------

Path::Path(const char *szPath)
{
	SetPath(string(szPath));
}

//----------------------------------------------------------------------

Path::Path(const Path& rhs)
{
	(*this) = rhs;
}

//----------------------------------------------------------------------

Path& Path::operator=(const Path& rhs)
{
	if(this == &rhs) return *this;

	SetPath(rhs.m_szPathBuffer);

	return *this;
}

//======================================================================

void Path::SetPath(const string& strPath)
{
	m_szPathBuffer[0] = 0;
	m_szDrive[0] = 0;
	m_szDir[0] = 0;
	m_szFname[0] = 0;
	m_szExt[0] = 0;

	::_fullpath(m_szPathBuffer, strPath.c_str(), sizeof(m_szPathBuffer));
	
	size_t iLen = strlen(m_szPathBuffer);

	if(iLen)
	{
		char cLastChar = m_szPathBuffer[iLen - 1];
		
		if((cLastChar != '/') && (cLastChar != '\\'))
		{
			struct _stat sStatBuf;

			if(0 == _stat(m_szPathBuffer, &sStatBuf))
			{
				if(sStatBuf.st_mode & _S_IFDIR)
				{
					m_szPathBuffer[iLen] = '\\';
					m_szPathBuffer[iLen + 1] = 0;
				}
			}
		}
	}

	::_splitpath_s(m_szPathBuffer, 
		m_szDrive, sizeof(m_szDrive), 
		m_szDir, sizeof(m_szDir), 
		m_szFname, sizeof(m_szFname), 
		m_szExt, sizeof(m_szExt));
}

//======================================================================

BOOL Path::Chdir(const string& strPath)
{
	m_stackHistory.push(m_szPathBuffer);
	SetPath(strPath);

	string strDir(m_szDrive);
	strDir += m_szDir;
	strDir.resize(strDir.size() - 1);

	return (::_chdir(strDir.c_str()) == 0);
}

//----------------------------------------------------------------------

BOOL Path::PopDir()
{
	if(m_stackHistory.size())
	{
		SetPath(m_stackHistory.top());
		m_stackHistory.pop();
		return (::_chdir(m_szPathBuffer) == 0);
	}

	return FALSE;
}

//======================================================================

string Path::GetFilename() const
{
	return string(m_szFname) + m_szExt;
}

//======================================================================

string Path::GetBasename() const
{
	return string(m_szFname);
}

//======================================================================

string Path::GetDirectory() const
{
	return string(m_szDrive) + m_szDir;
}

//======================================================================
