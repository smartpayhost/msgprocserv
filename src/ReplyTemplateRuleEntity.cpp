#include "stdafx.h"
#include "ReplyTemplateRuleEntity.h"

#include "ReplyTemplateCollection.h"

#include <assert.h>

//======================================================================

ReplyTemplateRuleEntity::ReplyTemplateRuleEntity()
:	m_fComplete(FALSE),
	m_pCollection(NULL)
{
}

//======================================================================

void ReplyTemplateRuleEntity::SetName(const char *szName)
{
	m_rtTemplate.SetName(szName);
}

//======================================================================

void ReplyTemplateRuleEntity::SetReplyTemplateCollection(ReplyTemplateCollection *pCollection)
{
	m_pCollection = pCollection;
}

//======================================================================

BOOL ReplyTemplateRuleEntity::ProcessReplyEnd(const Line & /*rLine*/)
{
	m_fComplete = TRUE;

	assert(m_pCollection);
	m_pCollection->AddTemplate(m_rtTemplate);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL ReplyTemplateRuleEntity::ProcessRule(const Line &rLine)
{
	ReplyRule *pRule = ReplyRule::MakeNew(NULL, rLine);

	if(pRule)
	{
		m_rtTemplate.AddRule(pRule);
		return TRUE;
	}

	return FALSE;
}

//----------------------------------------------------------------------

BOOL ReplyTemplateRuleEntity::ProcessLine(const Line &rLine)
{
	if(rLine[0] == "REPLY_TEMPLATE_END")
	{
		return ProcessReplyEnd(rLine);
	}
	else
	{
		return ProcessRule(rLine);
	}
}

//======================================================================

BOOL ReplyTemplateRuleEntity::IsComplete() const
{
	return m_fComplete;
}

//======================================================================
