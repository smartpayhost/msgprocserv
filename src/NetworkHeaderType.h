#ifndef _NETWORKHEADERTYPE_H
#define _NETWORKHEADERTYPE_H

typedef enum {
	NHT_10BYTE,			///< 10 byte NH1 + message (any type)
	NHT_LENTPDU,		///< 2 byte LLLL + 5 byte TPDU + message
} NetworkHeaderType;

#endif // _NETWORKHEADERTYPE_H
