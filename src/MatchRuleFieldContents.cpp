#include "stdafx.h"
#include "MatchRuleFieldContents.h"

#include "MessagePacket.h"
#include "SQLResultTable.h"
#include "SQLMatchQuery.h"
#include "SQLMatchQueryCollection.h"
#include "MsgProcRuleCollection.h"

#include "RegexpMatch.h"

#include <assert.h>

#include <set>
#include <fstream>

using std::string;
using std::set;
using std::ifstream;

//======================================================================

MatchRuleFieldContents::MatchRuleFieldContents()
:	m_iFieldNum(0),
	m_eWhat(MRFC_WHOLE_FIELD),
	m_eWith(MRFC_FIXED_DATA),
	m_iFirst(1),
	m_iLast(1),
	m_iFrom(0),
	m_iTo(999)
{
}

//---------------------------------------------------------

MatchRule* MatchRuleFieldContents::Clone() const
{
	return new MatchRuleFieldContents(*this);
}

//======================================================================
//
//	FIELD <number> [ NOT ] [ { FIRST <n> | LAST <n> | FROM <m> TO <n> | LEADING_DIGITS } ] { MATCHES | REGEXP | IN_FILE | QUERY} <data> 
//

BOOL MatchRuleFieldContents::Import(const Line &rLine)
{
	m_bInverted = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	m_iFieldNum = atoi((*i).c_str());

	if((m_iFieldNum < 1)||(m_iFieldNum > 128))
		return FALSE;

	if(++i == rLine.end()) return FALSE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	// figure out what to test

	int iFirst = -1;
	int iLast = -1;
	int iFrom = -1;
	int iTo = -1;

	m_eWhat = MRFC_WHOLE_FIELD;

	if((*i) == "FIRST")
	{
		m_eWhat = MRFC_FIRST_N;
		if(++i == rLine.end()) return FALSE;
		iFirst = atoi((*i).c_str());
		if(++i == rLine.end()) return FALSE;
	}
	else if((*i) == "LAST")
	{
		m_eWhat = MRFC_LAST_N;
		if(++i == rLine.end()) return FALSE;
		iLast = atoi((*i).c_str());
		if(++i == rLine.end()) return FALSE;
	}
	else if((*i) == "FROM")
	{
		m_eWhat = MRFC_FROM_M_TO_N;
		if(++i == rLine.end()) return FALSE;
		iFrom = atoi((*i).c_str());
		if(++i == rLine.end()) return FALSE;

		if((*i) == "TO")
			if(++i == rLine.end()) return FALSE;

		iTo = atoi((*i).c_str());
		if(++i == rLine.end()) return FALSE;
	}
	else if((*i) == "LEADING_DIGITS")
	{
		m_eWhat = MRFC_LEADING_DIGITS;
		if(++i == rLine.end()) return FALSE;
	}

	// figure out what to test with

	if((*i) == "MATCHES")
	{
		m_eWith = MRFC_FIXED_DATA;
	}
	else if((*i) == "REGEXP")
	{
		m_eWith = MRFC_REGEXP;
	}
	else if((*i) == "IN_FILE")
	{
		m_eWith = MRFC_FILE;
	}
	else if(((*i) == "IN_QUERY")||((*i) == "QUERY"))
	{
		m_eWith = MRFC_SQL;
	}
	else
	{
		return FALSE;
	}

	if(++i == rLine.end()) return FALSE;

	m_strMatchData = (*i);

	if(iFirst != -1) m_iFirst = iFirst;
	if(iLast != -1) m_iLast = iLast;
	if(iFrom != -1) m_iFrom = iFrom;
	if(iTo != -1) m_iTo = iTo;

	return TRUE;
}

//======================================================================
//	FIELD <number> [ NOT ] [ { FIRST <n> | LAST <n> | FROM <m> TO <n> | LEADING_DIGITS } ] { MATCHES | REGEXP | IN_FILE } <data> 

BOOL MatchRuleFieldContents::Export(string &strDesc)
{
	string strTmp;

	StrUtil::Format(strDesc, "FIELD %d", m_iFieldNum);

	if(m_bInverted)
		strDesc += " NOT";

	switch(m_eWhat)
	{
	case MRFC_WHOLE_FIELD:
		break;

	case MRFC_FIRST_N:
		StrUtil::Format(strTmp, " FIRST %d", m_iFirst);
		strDesc += strTmp;
		break;

	case MRFC_LAST_N:
		StrUtil::Format(strTmp, " LAST %d", m_iLast);
		strDesc += strTmp;
		break;

	case MRFC_FROM_M_TO_N:
		StrUtil::Format(strTmp, " FROM %d TO %d", m_iFrom, m_iTo);
		strDesc += strTmp;
		break;

	case MRFC_LEADING_DIGITS:
		strDesc += " LEADING_DIGITS";
		break;

	default:
		return FALSE;
	}

	switch(m_eWith)
	{
	case MRFC_FIXED_DATA:	strDesc += " MATCHES";	break;
	case MRFC_REGEXP:		strDesc += " REGEXP";	break;
	case MRFC_FILE:			strDesc += " IN_FILE";	break;
	case MRFC_SQL:			strDesc += " QUERY";	break;

	default:
		return FALSE;
	}

	StrUtil::PurifyString(m_strMatchData, strTmp);
	strDesc += ' ';
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL MatchRuleFieldContents::DoesMatchFile(const string& strFile, const string& strTestData) const
{
	set<string> file_lines;

	ifstream ifs;
	ifs.open(strFile.c_str());

	if(ifs.fail())
		return FALSE;

	char buf[256];

	while(ifs.getline(buf, sizeof(buf)))
	{
		file_lines.insert(string(buf));
	}

	if(file_lines.find(strTestData) != file_lines.end())
		return TRUE;

	return FALSE;
}

//======================================================================

BOOL MatchRuleFieldContents::DoesMatchSQL(const string& strQuery, const string& strTestData, const BitmapMsg *pMsg) const
{
	assert(pMsg);
	assert(m_pMsgProcRules);

	const SQLMatchQuery *pQuery = m_pMsgProcRules->GetSQLMatchQueryCollection().FindByName(strQuery);

	assert(pQuery);

	if(!pQuery)
		return FALSE;

	SQLResultTable tblResult;

	if(!pQuery->ExecQuery(tblResult, *pMsg))
		return FALSE;

	if(tblResult.FindIn(strTestData))
		return TRUE;

	return FALSE;
}

//======================================================================

BOOL MatchRuleFieldContents::DoesMatch(const string& strTestData, const BitmapMsg *pMsg) const
{
	assert(pMsg);

	BOOL bResult = FALSE;
	string strTest;

	switch(m_eWhat)
	{
	case MRFC_FIRST_N:
		strTest = strTestData.substr(0, m_iFirst);
		break;

	case MRFC_LAST_N:
		strTest = strTestData.substr(strTestData.size() - m_iLast, m_iLast);
		break;

	case MRFC_FROM_M_TO_N:
		strTest = strTestData.substr(m_iFrom, m_iTo - m_iFrom + 1);
		break;

	case MRFC_LEADING_DIGITS:
		strTest = strTestData.substr(0, strTestData.find_first_not_of("0123456789"));
		break;

	case MRFC_WHOLE_FIELD:
	default:
		strTest = strTestData;
		break;
	}

	switch(m_eWith)
	{
	case MRFC_REGEXP:
		bResult = RegexpMatch(m_strMatchData, strTest);
		break;

	case MRFC_FILE:
		bResult = DoesMatchFile(m_strMatchData, strTest);
		break;

	case MRFC_SQL:
		bResult = DoesMatchSQL(m_strMatchData, strTest, pMsg);
		break;

	case MRFC_FIXED_DATA:
	default:
		if(strTest == m_strMatchData)
			bResult = TRUE;
		break;
	}

	if(m_bInverted)
		return !bResult;

	return bResult;
}

//======================================================================

BOOL MatchRuleFieldContents::DoesMatch(Message& rMsg) const
{
	const BitmapMsgField *pField;

	pField = rMsg.m_sBMsg.GetField(m_iFieldNum);

	if(pField == NULL)
		return FALSE;

	return DoesMatch(pField->GetAsciiRep(), &(rMsg.m_sBMsg));
}

//======================================================================

BOOL MatchRuleFieldContents::Check() const
{
	if(m_eWith != MRFC_SQL)
		return TRUE;

	assert(m_pMsgProcRules);

	if(m_pMsgProcRules->GetSQLMatchQueryCollection().FindByName(m_strMatchData))
		return TRUE;

	Log(LOG_WARNING, "Match query %s not defined", m_strMatchData.c_str());

	return FALSE;
}

//======================================================================
