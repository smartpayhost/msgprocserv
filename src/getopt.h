#ifndef _GETOPT_H
#define _GETOPT_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//==============================================================

extern int	opterr;
extern int	optind;
extern int	optopt;
extern char	*optarg;
extern int getopt(int argc, char **argv, char *opts);

//==============================================================

#endif // _GETOPT_H
