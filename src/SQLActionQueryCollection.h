#ifndef _SQLACTIONQUERYCOLLECTION_H
#define _SQLACTIONQUERYCOLLECTION_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _SQLACTIONQUERY_H
#include "SQLActionQuery.h"
#endif

#include <map>
#include <boost/utility.hpp>

class MsgProcRuleCollection;
class Logger;

//======================================================================

class SQLActionQueryCollection : public RuleEntityDescriptor, public boost::noncopyable {
public:
	SQLActionQueryCollection();
	~SQLActionQueryCollection();

	BOOL IsStart(const Line &rLine) const;
	RuleEntity *MakeNew(const Line &rLine);

	virtual BOOL Check() const;

	void AddSQLActionQuery(SQLActionQuery &rSQLActionQuery);
	void Register(RuleFile &rRuleFile);
	BOOL Export(std::string &rStrDest);
	void MergeFrom(const SQLActionQueryCollection& rhs);

	void SetLogger(Logger *pLogger);

	void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray);

	const SQLActionQuery* FindByName(const std::string& strName) const;

private:
	SQLMessageQuery::SQLMessageQueryMap m_mapSQLActionQuerys;

	MsgProcRuleCollection *m_pMsgProcRules;
	Logger *m_pLogger;
};

//======================================================================

#endif // _SQLACTIONQUERYCOLLECTION_H
