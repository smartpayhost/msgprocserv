#include "stdafx.h"
#include "Socket.h"

#include "Timer.h"

//----------------------------------------------------------------------------------

Socket::Socket()
{
}

//----------------------------------------------------------------------------------

Socket::~Socket()
{
}

//----------------------------------------------------------------------------------
//
//	Returns:
//
//	SOCKRC_OK:			data read
//	SOCKRC_TIMEOUT:		timeout
//	SOCKRC_ERROR:		error
//	SOCKRC_CLOSED:		connection closed by remote end
//

SOCKRC Socket::Recv(char *buf, int bufsize, int *recvd, unsigned long timeout)
{
	SOCKRC result;

	*recvd = 0;

	result = WaitReadable(timeout);

	if(result != SOCKRC_OK)
		return result;

	return Recv(buf, bufsize, recvd);
}

//----------------------------------------------------------------------------------
//
//	Returns:
//
//	SOCKRC_OK:			data read
//	SOCKRC_ERROR:		error
//	SOCKRC_CLOSED:		connection closed by remote end
//

SOCKRC Socket::Recv(char *buf, int bufsize, int *recvd)
{
	return DoRecv(buf, bufsize, recvd);
}

//----------------------------------------------------------------------------------
//
//	returns:
//
//	SOCKRC_OK
//	SOCKRC_ERROR
//	SOCKRC_TIMEOUT
//

SOCKRC Socket::Send(const char *buf, int bufsize, int *sent, unsigned long timeout)
{
	return DoSend(buf, bufsize, sent, timeout);
}

//----------------------------------------------------------------------------------
//
//	returns:
//
//	SOCKRC_OK
//	SOCKRC_ERROR
//	SOCKRC_TIMEOUT
//

SOCKRC Socket::Send(const char *buf, int bufsize, unsigned long timeout)
{
	return Send(buf, bufsize, NULL, timeout);
}

//----------------------------------------------------------------------------------
//
//	returns:
//
//	SOCKRC_OK
//	SOCKRC_ERROR
//

SOCKRC Socket::Send(const char *buf, int bufsize)
{
	return Send(buf, bufsize, 0L);
}

