#include "stdafx.h"
#include "ReplyRuleFieldFile.h"

#include <assert.h>

#include <stdio.h>
#include <io.h>

using std::string;

//======================================================================

ReplyRuleFieldFile::ReplyRuleFieldFile(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldFile::Clone() const
{
	return new ReplyRuleFieldFile(*this);
}

//======================================================================
//
//	FIELD <field_num> FILE <filename>

BOOL ReplyRuleFieldFile::Import(const Line &rLine)
{
	m_strFilename.erase();

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "FILE")
	{
		if(++i == rLine.end()) return FALSE;

		m_strFilename = (*i);
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldFile::Export(string &strDesc)
{
	string strTmp;

	StrUtil::Format(strDesc, "FIELD %d FILE ", GetFieldNum());
	StrUtil::PurifyString(m_strFilename, strTmp);
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldFile::BuildReplyFieldExtFile(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg, int iFieldNum, const string& strFilename)
{
	FILE *fp;
	errno_t err = 0;

	err = fopen_s(&fp, strFilename.c_str(), "rb");

	if(err)
		return FALSE;

	if(fp)
	{
		int iLen = _filelength(_fileno(fp));

		BYTE pBuffer[1002];

		if(iLen > sizeof(pBuffer))
			iLen = sizeof(pBuffer);

		size_t iResult = 0;

		if(iLen)
		{
			iResult = fread(pBuffer, iLen, 1, fp);
		}

		fclose(fp);

		if(iResult)
		{
			BitmapMsgField *pField = new BitmapMsgField(iFieldNum, rOrigMsg.GetMsgStyle());

			assert(pField);

			if(!pField)
				return FALSE;

			if(!pField->SetFieldContents(pBuffer, (DWORD)iLen))
			{
				delete pField;
				return FALSE;
			}
		
			rNewMsg.SetField(pField);
		}
	
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL ReplyRuleFieldFile::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	if(!BuildReplyFieldExtFile(rNewMsg, rOrigMsg, GetFieldNum(), m_strFilename))
	{
		char szErr[100];

		strerror_s(szErr, sizeof(szErr), errno);

		Log(LOG_ERROR, "Failed to open file %s: %s", m_strFilename.c_str(), szErr);
		return FALSE;
	}

	return TRUE;
}

