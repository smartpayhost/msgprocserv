#include "stdafx.h"
#include "BitmapMsg.h"

#include "bcd.h"
#include "MAC.h"
#include "Format.h"

#include <sstream>
#include <iomanip>
#include <ios>

#include <assert.h>
#include <boost/smart_ptr.hpp>

using std::string;
using std::min;
using std::ostringstream;

//====================================================================

const BitmapMsgFieldStyle BitmapMsgFieldStyle::DefaultFieldStyle;
const string BitmapMsgFieldStyle::DefaultShortDesc("Invalid");
const string BitmapMsgFieldStyle::DefaultDesc("Invalid field");

//====================================================================
//====================================================================

BitmapMsgFieldStyle::BitmapMsgFieldStyle()
:	m_eFormat(BMFF_INVALID),
	m_eAttr(BMFA_INVALID),
	m_iMaxLen(0),
	m_strShortDesc(BitmapMsgFieldStyle::DefaultShortDesc),
	m_strDesc(BitmapMsgFieldStyle::DefaultDesc)
{
}

//--------------------------------------------------------------------

BitmapMsgFieldStyle::BitmapMsgFieldStyle(BMFieldFormat eFormat, 
		BMFieldAttr eAttr, 
		int iMaxLen,
		const string& strShortDesc, 
		const string& strDesc)
:	m_eFormat(eFormat), 
	m_eAttr(eAttr),
	m_iMaxLen(iMaxLen),
	m_strShortDesc(strShortDesc),
	m_strDesc(strDesc)
{
}

//====================================================================
//====================================================================

static const BitmapMsgFieldStyle &FNS = BitmapMsgFieldStyle::DefaultFieldStyle;

static const BitmapMsgFieldStyle* const l_apsTemplate[128] = {
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,
	&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS,&FNS
};

//====================================================================

BitmapMsgStyle::BitmapMsgStyle()
:	m_strName("Invalid style"),
	m_pField(l_apsTemplate)
{
}

//--------------------------------------------------------------------

BitmapMsgStyle::BitmapMsgStyle(const string& strName, const BitmapMsgFieldStyle* const *pField)
:	m_strName(strName),
	m_pField(pField)
{
}

//--------------------------------------------------------------------

const BitmapMsgFieldStyle* BitmapMsgStyle::GetField(int iFieldNum) const
{
	if((iFieldNum < 1)||(iFieldNum > 128))
	{
		assert(0);
		return NULL;
	}

	return m_pField[iFieldNum - 1];
}

//--------------------------------------------------------------------

BOOL BitmapMsgStyle::IsValidField(int iFieldNum) const
{
	const BitmapMsgFieldStyle* pFieldStyle;

	pFieldStyle = GetField(iFieldNum);

	if(!pFieldStyle)
		return FALSE;

	if(pFieldStyle->GetFormat() == BMFF_INVALID)
		return FALSE;

	if(pFieldStyle->GetAttr() == BMFA_INVALID)
		return FALSE;

	return TRUE;
}

//====================================================================
//====================================================================

BitmapMsgField::BitmapMsgField(int iFieldNumber, const BitmapMsgStyle *pMsgStyle)
:	m_iFieldNumber(iFieldNumber),
	m_pfsFieldStyle(pMsgStyle->GetField(iFieldNumber)),
	m_iUnitLen(0),
	m_iFieldLen(0),
	m_pField(NULL),
	m_iLenLen(0),
	m_iDataLen(0),
	m_pData(NULL)
{
	::memset(m_abLocalCopy, 0, sizeof(m_abLocalCopy));
}

//--------------------------------------------------------------------
//
//	Set up a field based on a raw representation of its data.
//	Message attributes have already been set up.
//

BOOL BitmapMsgField::SetFieldContents(BYTE *pData,
							   int iBufLen,
							   BYTE **ppNext)		// optional
{
	assert(pData);

	if(!pData)
		return FALSE;

	m_strAsciiRep.erase();
	m_pField = pData;

	// Figure out length of length.

	if(-1 == (m_iLenLen = CalcLenLen()))
		return FALSE;

	// Figure out length of data,
	// adjust data pointer to point to point to actual data
	// Figure out where next field begins

	if((iBufLen -= m_iLenLen) < 0)
		return FALSE;

	if(-1 == (m_iUnitLen = DecodeLen()))
		return FALSE;

	m_pData = m_pField + m_iLenLen;
	
	if(-1 == (m_iDataLen = CalcDataLen()))
		return FALSE;

	if((iBufLen -= m_iDataLen) < 0)
		return FALSE;

	// convert data to string for display/manipulation

	m_strAsciiRep = GenAsciiRep();

	// tidy up

	if(m_pField != m_abLocalCopy)
	{
		if((m_iLenLen + m_iDataLen) > sizeof(m_abLocalCopy))
		{
			assert(0);
			return FALSE;
		}

		memmove(m_abLocalCopy, m_pField, m_iLenLen + m_iDataLen);	// make local copy
	}

	m_pField = m_abLocalCopy;					// point ptrs at local copy
	m_pData = m_pField + m_iLenLen;

	if(ppNext != NULL)
		*ppNext = pData + m_iLenLen + m_iDataLen;

	return TRUE;
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------
//
//	returns length of data field in units, or -1 on error.
//

int BitmapMsgField::CalcUnitLen(const BitmapMsgFieldStyle& fsFieldStyle, const string& strAsciiRep)
{
	size_t iUnitLen;

	if(fsFieldStyle.GetFormat() == BMFF_FIXED)
	{
		iUnitLen = fsFieldStyle.GetMaxLen();
	}
	else
	{
		iUnitLen = strAsciiRep.size();

		if(fsFieldStyle.GetAttr() == BMFA_XN)
			iUnitLen--;
	}

	if(iUnitLen < 0)
	{
		assert(0);
		return -1;
	}

	return (int)iUnitLen;
}

//--------------------------------------------------------------------
//
//	returns length of data field, or -1 on error.
//

int	BitmapMsgField::CalcDataLen()
{
	return CalcDataLen(*m_pfsFieldStyle, m_iUnitLen);
}

//--------------------------------------------------------------------
//
//	returns length of data field in bytes, or -1 on error.
//

int BitmapMsgField::CalcDataLen(const BitmapMsgFieldStyle& fsFieldStyle, int iUnitLen)
{
	switch(fsFieldStyle.GetAttr())
	{
	case BMFA_B:
		return (iUnitLen + 7) / 8;

	case BMFA_N:
	case BMFA_Z:
	case BMFA_NS:
		return (iUnitLen + 1) / 2;

	case BMFA_XN:
		return ((iUnitLen + 1) / 2) + 1;

	case BMFA_A:
	case BMFA_AN:
	case BMFA_ANS:
	case BMFA_AORN:
		return iUnitLen;

	default:
		assert(0);
		return -1;
	}
}

//--------------------------------------------------------------------
//
//	returns length of len field, or -1 on error.
//

int	BitmapMsgField::CalcLenLen()
{
	return CalcLenLen(*m_pfsFieldStyle);
}

//--------------------------------------------------------------------
//
//	returns length of len field, or -1 on error.
//

int BitmapMsgField::CalcLenLen(const BitmapMsgFieldStyle& fsFieldStyle)
{
	switch(fsFieldStyle.GetFormat())
	{
	case BMFF_FIXED:		return 0;

	case BMFF_LLVAROZ:
		switch(fsFieldStyle.GetAttr())
		{
		case BMFA_AN:		return 2;
		case BMFA_ANS:		return 2;
		default:			return 1;
		}
		break;

	case BMFF_LLVARNZ:		return 1;

	case BMFF_LLLVAROZ:
		switch(fsFieldStyle.GetAttr())
		{
		case BMFA_AN:		return 3;
		case BMFA_ANS:		return 3;
		default:			return 2;
		}
		break;

	case BMFF_LLLVARNZ:		return 2;

	case BMFF_INVALID:
	default:
		assert(0);
		return -1;
	}
}

//--------------------------------------------------------------------

void BitmapMsgField::EncodeLen(const BitmapMsgFieldStyle& fsFieldStyle, int iUnitLen, BYTE *pDest)
{
	switch(fsFieldStyle.GetFormat())
	{
	// NZ always in packed BCD format

	case BMFF_LLVARNZ:
		BCD::Bin2LLBCD(iUnitLen, pDest);
		break;

	case BMFF_LLLVARNZ:
		BCD::Bin2LLLBCD(iUnitLen, pDest);
		break;

	// OZ follows field format

	case BMFF_LLVAROZ:
		if((fsFieldStyle.GetAttr() == BMFA_N)||(fsFieldStyle.GetAttr() == BMFA_Z))
			BCD::Bin2LLBCD(iUnitLen, pDest);
		else
			BCD::Bin2LLAsc(iUnitLen, pDest);
		break;

	case BMFF_LLLVAROZ:
		if((fsFieldStyle.GetAttr() == BMFA_N)||(fsFieldStyle.GetAttr() == BMFA_Z))
			BCD::Bin2LLLBCD(iUnitLen, pDest);
		else
			BCD::Bin2LLLAsc(iUnitLen, pDest);
		break;

	case BMFF_FIXED:
	default:
		break;
	}
}

//--------------------------------------------------------------------

void BitmapMsgField::EncodeNumeric(const string& strAscii, BYTE *pDest, int iDestLen, string& strFormatted, BOOL fTrailingF)
{
	size_t iAscLen = strAscii.size();

	if(iAscLen > (size_t)(iDestLen * 2))
		iAscLen = (iDestLen * 2);

	boost::scoped_array<char> pBuf(new char[iDestLen * 2 + 1]);
	pBuf[iDestLen * 2] = 0;
	
	if(fTrailingF)	// trailing F
	{
		memset(&pBuf[0], 'F', iDestLen * 2);
		memcpy(&pBuf[0], strAscii.c_str(), iAscLen);
	}
	else			// leading 0
	{
		memset(&pBuf[0], '0', iDestLen * 2);
		memcpy(&pBuf[0] + (iDestLen * 2) - iAscLen, strAscii.c_str(), iAscLen);
	}

	// convert to final output data

	BCD::Asc2PackedBcd(&pBuf[0], pDest, iDestLen);

	strFormatted = &pBuf[0];
}

//--------------------------------------------------------------------
//
//	Setup a field based on an ascii representation of its data.
//	Builds the field data in m_abLocalCopy and points m_pField at it.
//
//	returns -1 on error, 0 otherwise
//

BOOL BitmapMsgField::SetFieldFromString(const string& strAsciiRep)
{
	assert(m_pfsFieldStyle);

	// Copy parameters into field ----------------------------------

	m_strAsciiRep = strAsciiRep;

	// Calculate data lengths --------------------------------------

	m_iUnitLen = CalcUnitLen(*m_pfsFieldStyle, m_strAsciiRep);

	if(m_iUnitLen > m_pfsFieldStyle->GetMaxLen())
		return FALSE;

	m_iDataLen = CalcDataLen(*m_pfsFieldStyle, m_iUnitLen);
	m_iLenLen =  CalcLenLen(*m_pfsFieldStyle);

	if((m_iUnitLen < 0) || (m_iDataLen < 0) || (m_iLenLen < 0))
	{
		assert(0);
		return FALSE;
	}

	// Build formatted data --------------------------------------------

	m_pField = m_abLocalCopy;
	m_pData = m_pField + m_iLenLen;

	// first do length part

	EncodeLen(*m_pfsFieldStyle, m_iUnitLen, m_pField);

	// next do data

	string strFormatted;

	switch(m_pfsFieldStyle->GetAttr())
	{
		// hex/bcd digits, no spaces
	case BMFA_B:
	case BMFA_N:
	case BMFA_Z:
		EncodeNumeric(m_strAsciiRep, m_pData, m_iDataLen, strFormatted,
			((m_pfsFieldStyle->GetAttr() == BMFA_Z)||(m_pfsFieldStyle->GetFormat() != BMFF_FIXED))	);

		m_strAsciiRep = strFormatted;
		break;

	case BMFA_XN:
		m_pData[0] = m_strAsciiRep[0];
		
		EncodeNumeric(m_strAsciiRep.c_str() + 1, m_pData + 1, m_iDataLen - 1, strFormatted, FALSE);

		m_strAsciiRep = m_strAsciiRep.substr(0, 1);
		m_strAsciiRep += strFormatted;
		break;

	case BMFA_A:
	case BMFA_AN:
	case BMFA_ANS:
		{
			int iCopyLen = min(m_iDataLen, (int)m_strAsciiRep.size());
			int iPadLen = m_iDataLen - iCopyLen;

			if(iCopyLen)
				memcpy(m_pData, m_strAsciiRep.c_str(), iCopyLen);

			if(iPadLen)
				memset(m_pData + iCopyLen, ' ', iPadLen);
		}
		break;

	case BMFA_NS:
	case BMFA_AORN:
	case BMFA_INVALID:
	default:
		assert(0);
		return FALSE;
	}

	return SetFieldContents(m_pField, m_iLenLen + m_iDataLen);
}

//--------------------------------------------------------------------
//
//	returns length, or -1 on error.
//

int	BitmapMsgField::DecodeLen()
{
	switch(m_pfsFieldStyle->GetFormat())
	{
	case BMFF_FIXED:		return m_pfsFieldStyle->GetMaxLen();

	case BMFF_LLVAROZ:
		switch(m_pfsFieldStyle->GetAttr())
		{
		case BMFA_AN:		return (int)BCD::LLAsc2Bin(m_pField);
		case BMFA_ANS:		return (int)BCD::LLAsc2Bin(m_pField);
		default:			return (int)BCD::LLBCD2Bin(m_pField);
		}
		break;

	case BMFF_LLVARNZ:		return (int)BCD::LLBCD2Bin(m_pField);

	case BMFF_LLLVAROZ:
		switch(m_pfsFieldStyle->GetAttr())
		{
		case BMFA_AN:		return (int)BCD::LLLAsc2Bin(m_pField);
		case BMFA_ANS:		return (int)BCD::LLLAsc2Bin(m_pField);

		default:			return (int)BCD::LLLBCD2Bin(m_pField);
		}
		break;

	case BMFF_LLLVARNZ:		return (int)BCD::LLLBCD2Bin(m_pField);

	case BMFF_INVALID:
	default:
		assert(0);
		return -1;
	}
}

//--------------------------------------------------------------------

string BitmapMsgField::GenAsciiRep()
{
	string strResult;
	int i;
	char szTmpBuf[2500];

	switch(m_pfsFieldStyle->GetAttr())
	{
	case BMFA_B:
		BCD::Hex2Asc(m_pData, szTmpBuf, m_iUnitLen / 8);
		szTmpBuf[(m_iUnitLen / 8) * 2] = 0;
		strResult += szTmpBuf;
		break;

	case BMFA_N:
		{
			BYTE *pInPtr = m_pData;
			char *pOutPtr = szTmpBuf;
			int iLen = (m_iUnitLen + 1)/2;

			if(m_pfsFieldStyle->GetFormat() == BMFF_FIXED)	// right justified, 0 filled
			{
				if(m_iUnitLen & 0x01)
				{
					*pOutPtr = BCD::HexDigit2Asc(*pInPtr);
					pOutPtr++;
					pInPtr++;
					iLen--;
				}

				if(iLen)
					BCD::Hex2Asc(pInPtr, pOutPtr, iLen);

				pOutPtr += (iLen * 2);
			}
			else						// left justified, F filled
			{
				if(iLen)
					BCD::Hex2Asc(pInPtr, pOutPtr, iLen);

				pOutPtr += (iLen * 2);

				if(m_iUnitLen & 0x01)
					pOutPtr--;
			}

			*pOutPtr = 0;
			strResult += szTmpBuf;
		}

		break;

	case BMFA_XN:
		strResult += (char)(m_pData[0]);

		for(i = 0; i < (m_iUnitLen + 1)/2; i++)
		{
			ostringstream ss;

			if(m_pfsFieldStyle->GetFormat() == BMFF_FIXED)	// right justified, 0 filled
			{
				if((i == 0) && (m_iUnitLen & 0x01))
				{
					ss << std::setw(1) << std::hex << (int)(m_pData[i+1]);
				}
				else
				{
					ss << std::setfill('0') << std::setw(2) << std::hex << (int)(m_pData[i+1]);
				}
			}
			else						// left justified, F filled
			{
				if((i == (m_iUnitLen / 2)) && (m_iUnitLen & 0x01))
				{
					ss << std::setw(1) << std::hex << (int)(m_pData[i+1] >> 4);
				}
				else
				{
					ss << std::setfill('0') << std::setw(2) << std::hex << (int)(m_pData[i+1]);
				}
			}

			strResult += ss.str();
		}

		break;

	case BMFA_Z:
		for(i = 0; i < m_iUnitLen; i++)
		{
			char dc;

			dc = (char)(((m_pData[i/2] >> 4) & 0x0F) + '0');
			strResult += dc;
			
			i++;

			if(i < m_iUnitLen)
			{
				dc = (char)(((m_pData[i/2]) & 0x0F) + '0');
				strResult += dc;
			}
		}

		break;

	case BMFA_A:
	case BMFA_AN:
	case BMFA_ANS:
		strResult += string((char *)m_pData, m_iUnitLen);
		break;

	default:
		strResult = "Unable to decode";
		break;
	}

	return strResult;
}

//--------------------------------------------------------------------
//
//	Generate ascii description of contents of field in format
//	suitable for display.
//

void BitmapMsgField::Print(string &strDest)
{
	assert(m_pfsFieldStyle);
	assert((m_iFieldNumber >= 1)&&(m_iFieldNumber <= 128));

	if((m_iFieldNumber < 1)||(m_iFieldNumber > 128))
	{
		strDest = "invalid field\n";
		return;
	}

	ostringstream os;

	os << std::setw(3) << std::dec << m_iFieldNumber;

	strDest += os.str();

	strDest += " ";
	strDest += m_pfsFieldStyle->GetShortDesc();

	size_t iLen = 11 - m_pfsFieldStyle->GetShortDesc().size();
	strDest += string(iLen, ' ');

	string ss;

	switch(m_pfsFieldStyle->GetAttr())
	{
	case BMFA_A:
	case BMFA_AN:
		strDest += '"';
		strDest += m_strAsciiRep;
		strDest += '"';
		break;

	case BMFA_ANS:
		Format::Buf2String(m_iDataLen, m_pData, ss, 4);
		strDest += ss;
		break;

	default:
		strDest += m_strAsciiRep;
	}

	strDest += "\n";
}

//====================================================================
//====================================================================

BitmapMsg::BitmapMsg()
:	m_pStyle(NULL),
	m_iMsgType(0),
	m_pMessage(NULL),
	m_eMacValid(MAC_NONE),
	m_iLen(0)
{
	::memset(m_abPrimaryBitmap, 0, 8);
	::memset(m_abSecondaryBitmap, 0, 8);
	::memset(m_apFields, 0, sizeof(m_apFields));
	::memset(m_awFieldOffsets, 0, sizeof(m_awFieldOffsets));
	::memset(m_abRawMsg, 0, sizeof(m_abRawMsg));
}

//--------------------------------------------------------------------

BitmapMsg::~BitmapMsg()
{
	for(int i = 0; i < 128; i++)
	{
		if(m_apFields[i] != NULL)
			delete m_apFields[i];
	}
}

//--------------------------------------------------------------------

void BitmapMsg::Print(string &strDest)
{
	Print(strDest,BMPS_FULL);
}

//--------------------------------------------------------------------
void BitmapMsg::Print(string &strDest, BMPrintOption printOption)
{
	int i;
	ostringstream os;

	os << "    Msg type   " << std::setfill('0') << std::setw(4) << std::dec << m_iMsgType << "\n";

	if (printOption == BMPrintOption::BMPS_FULL)
	{
		os << "    Bitmap     ";
		os << std::setfill('0') << std::hex << std::uppercase;

		for(i = 0; i < 7; i++)
			os << "0x" << std::setw(2) << (int)m_abPrimaryBitmap[i] << ',';

		os << "0x" << std::setw(2) << (int)m_abPrimaryBitmap[7] << '\n';
	}

	strDest += os.str();

	for(i = 0; i < 128; i++)
	{
		if(m_apFields[i] != NULL)
		{
			m_apFields[i]->Print(strDest);
		}
	}
}

//--------------------------------------------------------------------
//
//	returns TRUE if field present in bitmap, FALSE otherwise.
//

BOOL BitmapMsg::GetBit(int iBitNum)
{
	BYTE *pBM;

	if(iBitNum < 1)
	{
		assert(0);
		return FALSE;
	}

	if(iBitNum > 128)
	{
		assert(0);
		return FALSE;
	}

	if(iBitNum <= 64)
	{
		pBM = m_abPrimaryBitmap;
		iBitNum -= 1;
	}
	else
	{
		pBM = m_abSecondaryBitmap;
		iBitNum -= 65;
	}

	if(pBM[iBitNum/8] & (0x80 >> (iBitNum%8)))
		return TRUE;
	else
		return FALSE;
}

//--------------------------------------------------------------------

void BitmapMsg::SetBit(int iBitNum, BOOL bSet)
{
	BYTE *pBM;

	if((iBitNum < 1)||(iBitNum > 128))
	{
		assert(0);
		return;
	}

	if(iBitNum <= 64)
	{
		pBM = m_abPrimaryBitmap;
		iBitNum -= 1;
	}
	else
	{
		pBM = m_abSecondaryBitmap;
		iBitNum -= 65;
	}

	if(bSet)
		pBM[iBitNum/8] |= (0x80 >> (iBitNum%8));
	else
		pBM[iBitNum/8] &= ~(0x80 >> (iBitNum%8));
}

//--------------------------------------------------------------------

BOOL BitmapMsg::SetField(int iFieldNum, BYTE **ppPtr, BYTE *pEndPtr)
{
	if(!m_pStyle->IsValidField(iFieldNum))
		return FALSE;

	if(m_apFields[iFieldNum - 1])
		delete m_apFields[iFieldNum - 1];

	m_awFieldOffsets[iFieldNum - 1] = (WORD)((*ppPtr) - m_pMessage);

	m_apFields[iFieldNum - 1] = new BitmapMsgField(iFieldNum, m_pStyle);

	if(!m_apFields[iFieldNum - 1]->SetFieldContents((*ppPtr), (int)(pEndPtr - (*ppPtr)), ppPtr))
		return FALSE;

	return TRUE;
}

//--------------------------------------------------------------------

BOOL BitmapMsg::SetMsg(int iLen, const BYTE *pData, const BYTE **ppNext)
{
	int i;
	BYTE *pPtr;
	BYTE *pEndPtr;

	if((!pData)||(iLen <= 0)||(iLen > sizeof(m_abRawMsg)))
	{
		assert(0);
		return FALSE;
	}

	memcpy(m_abRawMsg, pData, iLen);
	m_iLen = iLen;

	pPtr = m_abRawMsg;
	pEndPtr = pPtr + iLen;

	if(iLen < 10)
		return FALSE;

	m_pMessage = pPtr;

	for(i = 0; i < 128; i++)
		m_awFieldOffsets[i] = 0;

	// Do message type

	m_iMsgType = BCD::PackedBcd2Bin((pPtr[0] << 8L) + pPtr[1]);
	pPtr += 2;

	// Do primary bitmap

	memcpy(m_abPrimaryBitmap, pPtr, 8);
	pPtr += 8;

	for(i = 1; i <= 64; i++)
	{
		if(GetBit(i))
		{
			if(!SetField(i, &pPtr, pEndPtr))
				return FALSE;
		}
	}

	// Do secondary bitmap

	memset(m_abSecondaryBitmap, 0, 8);

	if(m_apFields[0] != NULL)
	{
		memcpy(m_abSecondaryBitmap, m_apFields[0]->GetField(), 8);

		for(i = 65; i <= 128; i++)
		{
			if(GetBit(i))
			{
				if(!SetField(i, &pPtr, pEndPtr))
					return FALSE;
			}
		}
	}

	// Check if message was correct size

	if(pPtr != pEndPtr)
		return FALSE;

	// Finish up

	if(ppNext != NULL)
		*ppNext = pData + iLen;

	return TRUE;
}

//--------------------------------------------------------------------
//
//	returns -1 on error, 0 otherwise.
//

BOOL BitmapMsg::Build()
{
	int i;
	int iLen;
	BYTE *pPtr;

	// rebuild bitmaps

	for(i = 1; i <= 64; i++)
	{
		if(m_apFields[i-1] != NULL)
			SetBit(i);
	}

	for(i = 65; i <= 128; i++)
	{
		if(m_apFields[i-1] != NULL)
		{
			SetBit(1);
			SetBit(i);
		}
	}

	for(i = 0; i < 128; i++)
		m_awFieldOffsets[i] = 0;

	// Calculate length of new message

	iLen = 0;
	iLen += 2;	// message type
	iLen += 8;	// primary bitmap
	
	if(GetBit(1))
		iLen += 8;	// secondary bitmap
	
	for(i = 1; i < 128; i++)
	{
		if(m_apFields[i] != NULL)
			iLen += m_apFields[i]->GetFieldLen();
	}

	// build the message

	if(iLen > sizeof(m_abRawMsg))
		return FALSE;

	m_iLen = iLen;

	pPtr = m_abRawMsg;
	m_pMessage = pPtr;

	// message type

	WORD wMT = (WORD)BCD::Bin2PackedBcd(m_iMsgType);
	*pPtr++ = (BYTE)(wMT >> 8);
	*pPtr++ = (BYTE)(wMT & 0x00FF);

	// primary bitmap

	memcpy(pPtr, m_abPrimaryBitmap, 8);
	pPtr += 8;

	// secondary bitmap

	if(GetBit(1))
	{
		// use the newly calculated bitmap, not the one in field 1
		memcpy(pPtr, m_abSecondaryBitmap, 8);
		pPtr += 8;
	}

	// fields

	for(i = 1; i < 128; i++)
	{
		if(m_apFields[i] != NULL)
		{
			m_awFieldOffsets[i] = (WORD)(pPtr - m_abRawMsg);

			memcpy(pPtr, m_apFields[i]->GetField(), m_apFields[i]->GetFieldLen());
			pPtr += m_apFields[i]->GetFieldLen();
		}
	}

	return TRUE;
}

//======================================================================

string BitmapMsg::GetPAN() const
{
	const BitmapMsgField *pField2 = GetField(2);
	const BitmapMsgField *pField35 = GetField(35);

	string strPAN;

	if(pField35)
	{
		strPAN = pField35->GetAsciiRep();
	}
	else if(pField2)
	{
		strPAN = pField2->GetAsciiRep();
	}

	return strPAN.substr(0, strPAN.find_first_not_of("0123456789"));
}

//======================================================================

string BitmapMsg::GetExpiryYYMM() const
{
	const BitmapMsgField *pField14 = GetField(14);
	const BitmapMsgField *pField35 = GetField(35);

	string strExpDate;

	if(pField35)
	{
		string strT2 = pField35->GetAsciiRep();

		string::size_type iLen = strT2.length();
		string::size_type iPos = strT2.find('=');

		if((iPos != strT2.npos) && ((iPos + 1 + 4) <= iLen))
		{
			strExpDate = strT2.substr(iPos + 1, 4);
		}
	}
	else if(pField14)
	{
		strExpDate = pField14->GetAsciiRep().substr(0, 4);
	}

	return strExpDate;
}

//====================================================================

BitmapMsgField * BitmapMsg::GetField(int iFieldNum)
{
	if((iFieldNum < 1) || (iFieldNum > 128))
	{
		assert(0);
		return NULL;
	}

	return m_apFields[iFieldNum-1];
}

//--------------------------------------------------------------------

const BitmapMsgField * BitmapMsg::GetField(int iFieldNum) const
{
	if((iFieldNum < 1) || (iFieldNum > 128))
	{
		assert(0);
		return NULL;
	}

	return m_apFields[iFieldNum-1];
}

//--------------------------------------------------------------------

void BitmapMsg::SetField(BitmapMsgField *pField)
{
	int iFieldNum = pField->GetFieldNumber();

	assert(iFieldNum >= 1);
	assert(iFieldNum <= 128);

	if((iFieldNum < 1) || (iFieldNum > 128))
	{
		assert(0);
		return;
	}

	m_apFields[iFieldNum-1] = pField;
}

//--------------------------------------------------------------------

WORD BitmapMsg::GetFieldOffset(int iFieldNum) const
{
	return m_awFieldOffsets[iFieldNum - 1];
}

//====================================================================
