#ifndef _LAYERNETWORK_H
#define _LAYERNETWORK_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

#ifndef _MESSAGE_H
#include "Message.h"
#endif

class LayerMsg;
class MessagePacket;

//==============================================================

class LayerNetwork : public LoggingObject {
public:
	LayerNetwork();

	void Setup(LayerMsg *pML) { m_pML = pML; }

	BOOL Process(MessagePacket &rPacket, Message::MNetworkHdrType eNHType);

private:
	LayerMsg		*m_pML;
};

//==============================================================

#endif _LAYERNETWORK_H
