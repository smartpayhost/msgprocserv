#ifndef _MESSAGEPACKET_H
#define _MESSAGEPACKET_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _ACTION_H
#include "Action.h"
#endif

#ifndef _MESSAGE_H
#include "Message.h"
#endif

#include <boost/utility.hpp>

//======================================================================

class MessagePacket : public boost::noncopyable {
public:
	MessagePacket();

	Message	m_pRequest;
	Message	m_pResponse;

	void SetStyle(BitmapMsgStyle *pStyle);

	Action::Type GetAction() { return m_eAction; }
	void SetAction(Action::Type eAction) { m_eAction = eAction; }

	std::string GetUpstreamHost() { return m_strUpstreamHost; }
	void SetUpstreamHost(const std::string& strHost) { m_strUpstreamHost = strHost; }

	BOOL CalcResponseMac();

private:
	Action::Type	m_eAction;
	std::string		m_strUpstreamHost;
};

//======================================================================

#endif _MESSAGEPACKET_H
