#include "stdafx.h"
#include "MatchRuleMacKey.h"

#include "MessagePacket.h"
#include "SQLResultTable.h"
#include "SQLMatchQuery.h"
#include "SQLMatchQueryCollection.h"
#include "MsgProcRuleCollection.h"
#include "DesBlock.h"

#include <assert.h>

#include <set>
#include <fstream>

using std::set;
using std::string;
using std::ifstream;

//======================================================================

MatchRuleMacKey::MatchRuleMacKey()
:	m_eWith(MRMK_FIXED_DATA)
{
}

//---------------------------------------------------------

MatchRule* MatchRuleMacKey::Clone() const
{
	return new MatchRuleMacKey(*this);
}

//======================================================================
//
//	MACKEY { MATCHES | IN_FILE | QUERY } <data> 
//

BOOL MatchRuleMacKey::Import(const Line &rLine)
{
	m_bInverted = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "MACKEY") return FALSE;

	if(++i == rLine.end()) return FALSE;

	// figure out what to test with

	if((*i) == "MATCHES")
	{
		m_eWith = MRMK_FIXED_DATA;
	}
	else if((*i) == "IN_FILE")
	{
		m_eWith = MRMK_FILE;
	}
	else if((*i) == "QUERY")
	{
		m_eWith = MRMK_SQL;
	}
	else
	{
		return FALSE;
	}

	if(++i == rLine.end()) return FALSE;

	m_strMatchData = (*i);

	return TRUE;
}

//======================================================================
//
//	MACKEY { MATCHES | IN_FILE | QUERY } <data> 

BOOL MatchRuleMacKey::Export(string &strDesc)
{
	string strTmp;

	strDesc = "MACKEY";

	switch(m_eWith)
	{
	case MRMK_FIXED_DATA:	strDesc += " MATCHES";	break;
	case MRMK_FILE:			strDesc += " IN_FILE";	break;
	case MRMK_SQL:			strDesc += " QUERY";	break;

	default:
		return FALSE;
	}

	StrUtil::PurifyString(m_strMatchData, strTmp);
	strDesc += ' ';
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL MatchRuleMacKey::GetKeysFromFile(const string& strFile, set<string>& Keys) const
{
	set<string> file_lines;

	ifstream ifs(strFile.c_str());

	if(!ifs)
		return FALSE;

	string s;

	while(ifs >> s)
	{
		Keys.insert(s);
	}

	return TRUE;
}

//======================================================================

BOOL MatchRuleMacKey::GetKeysFromSQL(const string& strQuery, const BitmapMsg *pMsg, set<string>& Keys) const
{
	assert(pMsg);
	assert(m_pMsgProcRules);

	const SQLMatchQuery *pQuery = m_pMsgProcRules->GetSQLMatchQueryCollection().FindByName(strQuery);

	assert(pQuery);

	if(!pQuery)
		return FALSE;

	SQLResultTable tblResult;

	if(!pQuery->ExecQuery(tblResult, *pMsg))
		return FALSE;

	for(int i = 0; i < tblResult.GetNumRows(); i++)
	{
		string s;

		if(tblResult.GetData(s, i, 0))
			Keys.insert(s);
	}

	return TRUE;
}

//======================================================================

BOOL MatchRuleMacKey::DoesMatch(Message& rMsg) const
{
	set<string> keys;

	switch(m_eWith)
	{
	case MRMK_FILE:
		GetKeysFromFile(m_strMatchData, keys);
		break;

	case MRMK_SQL:
		GetKeysFromSQL(m_strMatchData, &rMsg.m_sBMsg, keys);
		break;

	case MRMK_FIXED_DATA:
	default:
		keys.insert(m_strMatchData);
		break;
	}

	for(std::set<std::string>::iterator i = keys.begin(); i != keys.end(); i++)
	{
		if((*i).empty())
			continue;

		if(DesBlock::CheckValidKey((*i).c_str()))
		{
			rMsg.AddPossibleMacKey(*i);

			if(rMsg.GetMacKey().empty())
			{
				rMsg.SetMacKey(*i);
			}
		}
		else
		{
			Log(LOG_WARNING, "Invalid DES key: %s", (*i).c_str());
			return FALSE;
		}
	}

	return TRUE;
}

//======================================================================

BOOL MatchRuleMacKey::Check() const
{
	if(m_eWith != MRMK_SQL)
		return TRUE;

	assert(m_pMsgProcRules);

	if(m_pMsgProcRules->GetSQLMatchQueryCollection().FindByName(m_strMatchData))
		return TRUE;

	Log(LOG_WARNING, "Match query %s not defined", m_strMatchData.c_str());

	return FALSE;
}

//======================================================================
