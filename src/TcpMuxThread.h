#ifndef _TCPMUXTHREAD_H
#define _TCPMUXTHREAD_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _TCPMUXDOWNSTREAMHANDLER_H
#include "TcpMuxDownstreamHandler.h"
#endif

#ifndef _MUXUPSTREAMTHREAD_H
#include "MuxUpstreamThread.h"
#endif

#ifndef _TCPLISTENTHREAD_H
#include "TcpListenThread.h"
#endif

#ifndef _HANDLERTHREAD_H
#include "HandlerThread.h"
#endif

#ifndef _SAFELIST_H
#include "SafeList.h"
#endif

#include <string>

//====================================================================

class TcpMuxThread : public HandlerThread {
public:
	TcpMuxThread( MsgProcCfg &config );

	void DownstreamEnding(TcpMuxDownstreamHandler *pHandler);

	void SetPipeDst(DataPipe *pPipe);
	DataPipe& GetPipeSrc();

	void SetDownstreamPort(const std::string& strPort);

protected:
    DWORD ThreadMemberFunc();

private:
	std::string m_strDownstreamPort;
	
	SafeList<TcpMuxDownstreamHandler> m_listActiveThreads;
	SafeList<TcpMuxDownstreamHandler> m_listEndingThreads;
	Event m_evDeadThread;
	Event m_evDownstreamShutdown;

	DWORD m_dwConnectionCounter;

	TcpSocket m_sListen;

	MuxUpstreamThread m_tUpstreamThread;
	TcpListenThread m_tListenThread;

	MsgProcCfg& m_config;

	BOOL ProcessNewDownstream();
	BOOL ProcessDeadDownstream();

	void ClearDeadList();
	void ShutdownDownstreamThreads();
};

//====================================================================

#endif // _TCPMUXTHREAD_H
