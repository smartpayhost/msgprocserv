#include "stdafx.h"
#include "LayerHost.h"

#include "MsgProcCfg.h"

#include "MatchTemplateCollection.h"
#include "ReplyTemplateCollection.h"
#include "MessagePacket.h"
#include "MatchTemplate.h"

#include <assert.h>

using std::string;

//======================================================================

LayerHost::LayerHost()
:	m_pConfig(NULL)
{
}

//======================================================================

BOOL LayerHost::Process(MessagePacket &rPacket)
{
	SZFN(LayerHost);

	assert(m_pConfig);

	const MatchTemplate *pTemplate;

	// match it to template

	pTemplate = m_pConfig->GetMatchTemplateCollection().FindMatchTemplate(rPacket.m_pRequest);

	if(pTemplate == NULL)
	{
		string strTmp;
		rPacket.m_pRequest.m_sBMsg.Print(strTmp);
		Log(LOG_WARNING, "%s: No template matches message:\n\n%s", szFn, strTmp.c_str());
		return FALSE;
	}

	Log(LOG_NOTICE, "%s: Message received: %s", szFn, pTemplate->GetName().c_str());

	rPacket.SetAction(pTemplate->GetAction());

	switch(pTemplate->GetAction())
	{
	case Action::NONE:
		break;

	case Action::UPSTREAM:
		Log(LOG_NOTICE, "%s: Message forwarded to %s", szFn, pTemplate->GetDestinationHost().c_str());
		rPacket.SetUpstreamHost(pTemplate->GetDestinationHost());
		break;

	case Action::RESPONSE:
		{
		// figure out how to reply

		string strReplyTemplateName = pTemplate->GetReplyTemplateName();

		const ReplyTemplate *pReplyTemplate = m_pConfig->GetReplyTemplateCollection().FindByName(strReplyTemplateName);

		if(pReplyTemplate == NULL)
		{
			Log(LOG_WARNING, "%s: No reply handling defined for this message type", szFn);
			return FALSE;
		}

		// build reply message

		rPacket.m_pResponse.m_sBMsg.SetMsgLen(0);

		if(!pReplyTemplate->BuildReply(rPacket))
		{
			Log(LOG_WARNING, "%s: Could not build reply message", szFn);
			return FALSE;
		}

		// figure out what sort of message reply turned out to be

		Log(LOG_NOTICE, "%s: Message sent: %s", szFn, strReplyTemplateName.c_str());
		}
		break;

	default:
		rPacket.SetAction(Action::NONE);
		Log(LOG_WARNING, "%s: Unsupported action specified", szFn);
		return FALSE;
	}

	return TRUE;
}

//======================================================================
