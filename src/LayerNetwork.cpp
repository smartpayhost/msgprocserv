#include "StdAfx.h"
#include "LayerNetwork.h"
#include "LayerMsg.h"
#include "MessagePacket.h"
#include "Format.h"

#include <assert.h>

using std::string;

//====================================================================

LayerNetwork::LayerNetwork()
:	m_pML(NULL)
{
}

//====================================================================

BOOL LayerNetwork::Process(MessagePacket &rPacket, Message::MNetworkHdrType eNHType)
{
	SZFN(LayerNetwork);

	assert(m_pML);

	// do network header

	if(!rPacket.m_pRequest.DecodeNetworkHeader(eNHType))
	{
		Log(LOG_WARNING, "%s: Failed to decode network header", szFn);
		return FALSE;
	}

	// log received data

	Log(LOG_INFO, "%s: Message received: Total len = %d, Network header len = %d, Body len = %d",
		szFn,
		rPacket.m_pRequest.GetRawLen(),
		rPacket.m_pRequest.GetNetworkHdrLen(),
		rPacket.m_pRequest.GetLenRemaining());

	string strLog;
	string strTmp;
	
	strLog = "Header:\n";

	Format::Buf2HexDump(rPacket.m_pRequest.GetNetworkHdrLen(),
		rPacket.m_pRequest.GetRawBuf(), strTmp, 16);

	strLog += strTmp;
	strLog += "Data:\n";

	Format::Buf2HexDump(rPacket.m_pRequest.GetLenRemaining(),
		rPacket.m_pRequest.GetRawCommsHdr(), strTmp, 16);

	strLog += strTmp;

	Log(LOG_DUMP, "%s: Received packet:\n\n%s", szFn, strLog.c_str());

	// pass request to message layer

	if(!m_pML->Process(rPacket))
	{
		Log(LOG_WARNING, "%s: No response message to send", szFn);

		return FALSE;
	}

	if(rPacket.GetAction() == Action::RESPONSE)
	{
		// add network header to response

		rPacket.m_pResponse.TurnaroundNetworkHeader(rPacket.m_pRequest);
		
		// build raw message for transmission

		rPacket.m_pResponse.BuildRawMsg();

		Log(LOG_INFO, "%s: Sending response: Total len = %d, Network header len = %d, Body len = %d",
				szFn,
				rPacket.m_pResponse.GetRawLen(),
				rPacket.m_pResponse.GetNetworkHdrLen(),
				rPacket.m_pResponse.GetRawLen() - rPacket.m_pResponse.GetNetworkHdrLen());

		// log response data

		strLog = "Header:\n";

		Format::Buf2HexDump(rPacket.m_pResponse.GetNetworkHdrLen(),
			rPacket.m_pResponse.GetRawBuf(), strTmp, 16);

		strLog += strTmp;
		strLog += "Data:\n";

		Format::Buf2HexDump(rPacket.m_pResponse.GetRawLen() - rPacket.m_pResponse.GetNetworkHdrLen(),
			rPacket.m_pResponse.GetRawBuf() + rPacket.m_pResponse.GetNetworkHdrLen(), strTmp, 16);

		strLog += strTmp;

		Log(LOG_DUMP, "%s: sending packet:\n\n%s", szFn, strLog.c_str());
	}

	return TRUE;
}
