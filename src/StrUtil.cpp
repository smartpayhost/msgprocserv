#include "stdafx.h"
#include "StrUtil.h"

#include "BCD.h"

#include <sstream>
#include <iomanip>

using std::string;
using std::vector;

//======================================================================

StrUtil::StrUtil()
{
	Reset();
}

//======================================================================
	
StrUtil::~StrUtil()
{
	Reset();
}

//======================================================================

BOOL StrUtil::StringToLine(const string &rStr, Line &rLine)
{
	StrUtil s;
	return s.IStringToLine(rStr, rLine);
}

//======================================================================

BOOL StrUtil::LineToString(const Line &rLine, string &rStr)
{
	StrUtil s;
	return s.ILineToString(rLine, rStr);
}

//======================================================================

BOOL StrUtil::IStringToLine(const string &rStr, Line &rLine)
{
	Reset();

	for(int i = 0; (!m_fComplete) && (i < (int)rStr.size()); i++)
	{
		if(!TokeniseChar(rStr[i]))
			return FALSE;
	}

	if(!m_fComplete)
		EndLine();

	rLine = m_lineCurrent;

	return TRUE;
}

//======================================================================

BOOL StrUtil::IsStringPure(const string &rStr)
{
	size_t iSize = rStr.size();

	for(size_t i = 0; i < iSize; i++)
	{
		if(!isgraph(rStr[i]))
			return FALSE;
	}

	return TRUE;
}

//======================================================================

void StrUtil::ForcePurifyString(const string &rSrc, string &rDest)
{
	rDest = '"';

	for(int i = 0; i < (int)rSrc.size(); i++)
	{
		char c = rSrc[i];

		switch(c)
		{
			case '"':
				rDest += '\\';
				rDest += '"';
				break;

			case '\n':
				rDest += '\\';
				rDest += 'n';
				break;

			case '\t':
				rDest += '\\';
				rDest += 't';
				break;

			case '\r':
				rDest += '\\';
				rDest += 'r';
				break;

			case '\v':
				rDest += '\\';
				rDest += 'v';
				break;

			case '\f':
				rDest += '\\';
				rDest += 'f';
				break;

			default:
				if((c >= 0x20)&&(c <= 0x7E))
				{
					rDest += c;
				}
				else
				{
					char szBuf[2];
					BCD::Hex2Asc((BYTE *)&c, szBuf, 1);

					rDest += '\\';
					rDest += 'x';
					rDest += szBuf[0];
					rDest += szBuf[1];
				}

				break;
		}
	}

	rDest += '"';
}

//======================================================================

void StrUtil::PurifyString(const string &rSrc, string &rDest)
{
	if(IsStringPure(rSrc))
		rDest = rSrc;
	else
		ForcePurifyString(rSrc, rDest);
}

//======================================================================

BOOL StrUtil::AddTokenToString(const Token &rToken, string &rStr)
{
	if(!rStr.empty())
		rStr += ' ';

	string strTmp;

	PurifyString(rToken, strTmp);
	rStr += strTmp;

	return TRUE;
}

//----------------------------------------------------------------------

BOOL StrUtil::ILineToString(const Line &rLine, string &rStr)
{
	Line::const_iterator i;

	for(i = rLine.begin(); i != rLine.end(); i++)
	{
		if(!AddTokenToString(*i, rStr))
			return FALSE;
	}

	return TRUE;
}

//======================================================================

void StrUtil::ResetToken()
{
	m_tokCurrent.erase();
	m_eParseState = WHITESPACE;
}

//----------------------------------------------------------------------

void StrUtil::ResetLine()
{
	m_lineCurrent.clear();
	ResetToken();
}

//----------------------------------------------------------------------

void StrUtil::Reset()
{
	ResetLine();
	ResetToken();

	m_fComplete = FALSE;
	m_eParseState = WHITESPACE;
}

//======================================================================

void StrUtil::EndToken()
{
	if(!m_tokCurrent.empty())
		m_lineCurrent.push_back(m_tokCurrent);
	
	ResetToken();
}

//----------------------------------------------------------------------

void StrUtil::EndLine()
{
	EndToken();
	m_fComplete = TRUE;
}

//======================================================================

BOOL StrUtil::TokeniseCharWhitespace(int c)
{
	switch(c)
	{
	case '"':
		m_eParseState = DOUBLEQUOTE;
		return TRUE;

	case '\'':
		m_eParseState = SINGLEQUOTE;
		return TRUE;

	case '\n':
		EndLine();
		return TRUE;

	case ';':
	case '#':
		m_eParseState = COMMENT;
		return TRUE;

	case ' ':
	case '\t':
	case '\r':
	case '\v':
	case '\f':
		return TRUE;

	default:
		m_eParseState = FREETOKEN;
		m_tokCurrent += (char)c;
		return TRUE;
	}
}

//----------------------------------------------------------------------

BOOL StrUtil::TokeniseCharFreeToken(int c)
{
	switch(c)
	{
	case '\n':
		EndLine();
		return TRUE;

	case ';':
	case '#':
		EndToken();
		m_eParseState = COMMENT;
		return TRUE;

	case ' ':
	case '\t':
	case '\r':
	case '\v':
	case '\f':
		EndToken();
		return TRUE;

	default:
		m_tokCurrent += (char)c;
		return TRUE;
	}
}

//----------------------------------------------------------------------

BOOL StrUtil::TokeniseCharDoubleQuote(int c)
{
	switch(c)
	{
	case '"':
		EndToken();
		return TRUE;

	case '\\':
		m_eParseState = DOUBLEQUOTE_ESC;
		return TRUE;

	case '\n':
		return FALSE;

	default:
		m_tokCurrent += (char)c;
		return TRUE;
	}
}

//----------------------------------------------------------------------

BOOL StrUtil::TokeniseCharDoubleQuoteEsc(int c)
{
	m_eParseState = DOUBLEQUOTE;

	switch(c)
	{
	case 'n':
		m_tokCurrent += '\n';
		return TRUE;

	case 'r':
		m_tokCurrent += '\r';
		return TRUE;

	case 'v':
		m_tokCurrent += '\v';
		return TRUE;

	case 't':
		m_tokCurrent += '\t';
		return TRUE;

	case 'f':
		m_tokCurrent += '\f';
		return TRUE;

	case 'x':
		m_eParseState = DOUBLEQUOTE_HEX1;
		return TRUE;

	case '\n':
		return FALSE;

	default:
		m_tokCurrent += (char)c;
		return TRUE;
	}
}

//----------------------------------------------------------------------

BOOL StrUtil::TokeniseCharDoubleQuoteHex1(int c)
{
	m_bHexByte = 0;

	if(!isxdigit(c))
	{
		return FALSE;
	}

	if(isdigit(c))
	{
		m_bHexByte = (BYTE)(m_bHexByte + c - '0');
	}
	else
	{
		if(isupper(c))
			m_bHexByte = (BYTE)(m_bHexByte + c - 'A' + 0x0A);
		else
			m_bHexByte = (BYTE)(m_bHexByte + c - 'a' + 0x0A);
	}

	m_eParseState = DOUBLEQUOTE_HEX2;
	
	return TRUE;
}

//----------------------------------------------------------------------

BOOL StrUtil::TokeniseCharDoubleQuoteHex2(int c)
{
	m_bHexByte <<= 4;

	if(!isxdigit(c))
	{
		return FALSE;
	}

	if(isdigit(c))
	{
		m_bHexByte = (BYTE)(m_bHexByte + c - '0');
	}
	else
	{
		if(isupper(c))
			m_bHexByte = (BYTE)(m_bHexByte + c - 'A' + 0x0A);
		else
			m_bHexByte = (BYTE)(m_bHexByte + c - 'a' + 0x0A);
	}

	m_tokCurrent += (char)m_bHexByte;

	m_eParseState = DOUBLEQUOTE;
	
	return TRUE;
}

//----------------------------------------------------------------------

BOOL StrUtil::TokeniseCharSingleQuote(int c)
{
	switch(c)
	{
	case '\'':
		EndToken();
		return TRUE;

	case '\\':
		m_eParseState = SINGLEQUOTE_ESC;
		return TRUE;

	case '\n':
		return FALSE;

	default:
		m_tokCurrent += (char)c;
		return TRUE;
	}
}

//----------------------------------------------------------------------

BOOL StrUtil::TokeniseCharSingleQuoteEsc(int c)
{
	m_eParseState = SINGLEQUOTE;

	switch(c)
	{
	case '\'':
	case '\\':
		m_tokCurrent += (char)c;
		return TRUE;

	case '\n':
		return FALSE;

	default:
		m_tokCurrent += '\\';
		m_tokCurrent += (char)c;
		return TRUE;
	}
}

//----------------------------------------------------------------------

BOOL StrUtil::TokeniseCharComment(int c)
{
	switch(c)
	{
	case '\n':
		EndLine();
		return TRUE;

	default:
		return TRUE;
	}
}

//----------------------------------------------------------------------

BOOL StrUtil::TokeniseChar(int c)
{
	switch(m_eParseState)
	{
	case WHITESPACE:		return TokeniseCharWhitespace(c);
	case FREETOKEN:			return TokeniseCharFreeToken(c);
	case DOUBLEQUOTE:		return TokeniseCharDoubleQuote(c);
	case DOUBLEQUOTE_ESC:	return TokeniseCharDoubleQuoteEsc(c);
	case DOUBLEQUOTE_HEX1:	return TokeniseCharDoubleQuoteHex1(c);
	case DOUBLEQUOTE_HEX2:	return TokeniseCharDoubleQuoteHex2(c);
	case SINGLEQUOTE:		return TokeniseCharSingleQuote(c);
	case SINGLEQUOTE_ESC:	return TokeniseCharSingleQuoteEsc(c);
	case COMMENT:			return TokeniseCharComment(c);
	}

	return FALSE;
}

//----------------------------------------------------------------------

BOOL StrUtil::Format(string &rStr, const char *szFmt, ...)
{
	char szBuf[4000];

	va_list ap;
	va_start(ap, szFmt);

	int iRetVal = _vsnprintf_s(szBuf, sizeof(szBuf), _TRUNCATE, szFmt, ap);

	va_end(ap);

	if(iRetVal == -1)
		return FALSE;

	rStr = szBuf;

	return TRUE;
}

//====================================================================

string StrUtil::FieldToString( const BitmapMsgField &msgField )
{
	vector<BYTE> fieldData(msgField.GetData(),msgField.GetData() + msgField.GetDataLen());

	string result = VectorToString(fieldData);

	return result;
}

//====================================================================

string StrUtil::VectorToString( const vector<BYTE> &source )
{
	vector<BYTE> nullTerminated(source);

	nullTerminated.push_back(0);

	return (char *)&nullTerminated[0];
}

//====================================================================

void StrUtil::trim( vector<string> &strings )
{
	for (UINT i=0; i<strings.size(); i++)
		trim(strings[i]);
}

//====================================================================

string StrUtil::trim( string &s )
{
	while (s.size())
		if (s[0] == ' ' || s[0] == '\t')
			s.erase(0,1);
		else
			break;

	while (s.size())
	{
		char c = s[s.size()-1];

		if (c == ' ' || c == '\t')
			s.erase(s.size()-1,1);
		else
			break;
	}

	return s;
}