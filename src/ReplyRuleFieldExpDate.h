#ifndef _REPLYRULEFIELDEXPDATE_H
#define _REPLYRULEFIELDEXPDATE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULEFIELD_H
#include "ReplyRuleField.h"
#endif

//==============================================================

class ReplyRuleFieldExpDate : public ReplyRuleField {
public:
	ReplyRuleFieldExpDate(int iFieldNum, const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const;
};

//==============================================================

#endif // _REPLYRULEFIELDEXPDATE_H
