#include "stdafx.h"
#include "TcpPipeHandler.h"
#include "NetworkHeader.h"
#include "Message.h"

using std::string;

//===================================================================

TcpPipeHandler::TcpPipeHandler()
:	m_ppipeDest(NULL),
	m_wNextHdr(0)
{	
}

//-------------------------------------------------------------------

TcpPipeHandler::~TcpPipeHandler()
{
}

//===================================================================

void TcpPipeHandler::SetHost(const string& strHostname, const string& strPort, const string& strName, NetworkHeaderType eHdrType)
{
	m_strHost = strHostname;
	m_strPort = strPort;
	m_strName = strName;
	m_eHdrType = eHdrType;
}

//===================================================================

DWORD TcpPipeHandler::ThreadMemberFunc()
{
	SZFN(TcpPipeHandler);

	Log(LOG_DEBUG, "%s: starting", szFn);

	assert(m_ppipeDest);

	if(	(!m_ppipeDest) ||
		(m_strHost.empty()) ||
		(m_strPort.empty()) ||
		(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) )
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	if(m_sSocket.Open() != SOCKRC_OK)
	{
		return 2;
	}

	m_sSocket.SetHost(m_strHost.c_str(), m_strPort.c_str());

	m_tTcp.SetAutoConnect(TRUE);
	m_tTcp.SetDontQueue(TRUE);
	m_tTcp.SetSocket(&m_sSocket);

	m_tTcp.SetPipeDst(&m_tNh1.GetPipeSrc());
	m_tNh1.SetPipeDst(&m_pipeDownstream);

	AddChild(&m_tNh1);
	AddChild(&m_tTcp);

	SetupChildren();

	if(!m_tTcp.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 3: failed to start TCP handler", szFn);
		ShutdownChildren();
		return 3;
	}

	if(!m_tNh1.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 4: failed to start NH1 handler", szFn);
		ShutdownChildren();
		return 4;
	}

	WaitableCollection wc;

	wc.push_back(GetShutdownEvent());
	wc.push_back(m_tTcp);
	wc.push_back(m_pipeUpstream);
	wc.push_back(m_pipeDownstream);
	wc.push_back(m_tNh1);

	DWORD dwRetVal = 0;
	BOOL fExit = FALSE;

	while(!fExit)
	{
		// wait for packet on pipe or shutdown

		DWORD e = wc.Wait();

		switch(e)
		{
		case WAIT_OBJECT_0 + 2:
			{
				// message to be sent to upstream host

				BYTE *pbBuf;
				Message msg;
				BOOL msg_decoded;

				int iLen = m_pipeUpstream.GetChunkNC(&pbBuf);

				if(m_eHdrType == NetworkHeaderType::NHT_LENTPDU)
				{
					// Translate (NH1 + TPDU) to (LL + TPDU)
					NetworkHeader nh;

					nh.CopyFrom(pbBuf);

					// save original header so response can be built later

					WORD wSrcNii = m_wNextHdr;

					m_hdrs[m_wNextHdr] = nh;
					m_niis[m_wNextHdr] = (pbBuf[13] << 8) + pbBuf[14];	// save old src NII

					m_wNextHdr++;

					if(m_wNextHdr > MAX_NH_POOL)
						m_wNextHdr = 0;

					// rewrite TPDU and LL

					pbBuf[8] = pbBuf[4];
					pbBuf[9] = pbBuf[5];	// copy LL to just in front of TPDU

					wSrcNii |= 0x8000;
					pbBuf[13] = (BYTE)(wSrcNii >> 8);	// replace source NII in TPDU with index into table
					pbBuf[14] = (BYTE)(wSrcNii & 0xFF);

					iLen -= 8;
					m_tTcp.GetPipeSrc().Put(iLen, pbBuf + 8);		// put a copy starting with LL

					msg_decoded = msg.SetRawMsg(iLen, pbBuf + 8);

					delete[] pbBuf;
				}
				else
				{
					msg_decoded = msg.SetRawMsg(iLen, pbBuf);
					m_tTcp.GetPipeSrc().PutNC(iLen, pbBuf);			// just pass through
				}

				Log(LOG_NOTICE, "%s: sending packet (%d bytes) to %s (%s:%s)", szFn, iLen, m_strName.c_str(), m_strHost.c_str(), m_strPort.c_str());

				if(msg_decoded)
				{
					if(msg.DecodeAuto())
						LogBitmapMsg(LOG_NOTICE, msg.m_sBMsg, "%s: sending packet (%d bytes) to %s (%s:%s)", szFn, iLen, m_strName.c_str(), m_strHost.c_str(), m_strPort.c_str());
				}
			}
			break;

		case WAIT_OBJECT_0 + 3:
			{
				// message received from upstream host

				BYTE *pbBuf;

				int iLen = m_pipeDownstream.GetChunkNC(&pbBuf);

				if(m_eHdrType == NetworkHeaderType::NHT_LENTPDU)
				{
					// Translate (NH2 + TPDU) to (NH1 + TPDU)

					WORD wNII = ((pbBuf[11] << 8) + pbBuf[12]) & 0x7FFF;	// look up NII

					if(wNII < MAX_NH_POOL)
					{
						NetworkHeader nh = m_hdrs[wNII];	// retrieve old NH1 header
						nh.SetDataLen((WORD)(iLen - 10));	// update data len to response length
						nh.CopyTo(pbBuf);					// convert from NH2 to NH1

						pbBuf[11] = (BYTE)(m_niis[wNII] >> 8);
						pbBuf[12] = (BYTE)(m_niis[wNII] & 0xFF);	// restore old NII
					}
				}

				LogRawBitmapMsg(LOG_NOTICE, iLen, pbBuf, "%s: received packet (%d bytes) from %s (%s:%s)", szFn, iLen, m_strName.c_str(), m_strHost.c_str(), m_strPort.c_str());

				m_ppipeDest->PutNC(iLen, pbBuf);			// just pass through

				Log(LOG_NOTICE, "%s: received packet (%d bytes) from %s (%s:%s)", szFn, iLen, m_strName.c_str(), m_strHost.c_str(), m_strPort.c_str());
			}
			break;

		case WAIT_OBJECT_0 + 1:
			Log(LOG_DEBUG, "%s: TCP handler child died", szFn);
			fExit = TRUE;
			break;

		case WAIT_OBJECT_0 + 4:
			Log(LOG_DEBUG, "%s: NH1 handler child died", szFn);
			fExit = TRUE;
			break;

		case WAIT_OBJECT_0 + 0:
			Log(LOG_DEBUG, "%s: shutdown requested", szFn);
			fExit = TRUE;
			break;

		default:
			Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result", szFn);
			fExit = TRUE;
			break;
		}
	}

	Log(LOG_DEBUG, "%s: shutting down children", szFn);

	ShutdownChildren();

	Log(LOG_DEBUG, "%s: ending", szFn);

	return dwRetVal;
}

//===================================================================
