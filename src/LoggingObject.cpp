#include "stdafx.h"
#include "LoggingObject.h"

#include "Logger.h"
#include "Message.h"

#include <stdio.h>
#include <assert.h>

//==============================================================

LoggingObject::LoggingObject()
:	m_pLogger(NULL)
{
}

//==============================================================

int LoggingObject::Log(enum LogLevel level, const char *fmt, ...) const
{
	assert(m_pLogger);

	if(!m_pLogger)
		return 0;

	va_list ap;
	va_start(ap, fmt);

	int iRetVal = m_pLogger->vLog(level, fmt, ap);

	va_end(ap);

	return iRetVal;
}

//==============================================================

int LoggingObject::LogBitmapMsg(enum LogLevel level, const BitmapMsg& msg, const char *fmt, ...) const
{
	assert(m_pLogger);

	if(!m_pLogger)
		return 0;

	va_list ap;
	va_start(ap, fmt);

	int iRetVal = m_pLogger->vLogBitmapMsg(level, msg, fmt, ap);

	va_end(ap);

	return iRetVal;
}

//==============================================================

int LoggingObject::LogRawBitmapMsg(enum LogLevel level, int iLen, const BYTE *pData, const char *fmt, ...) const
{
	assert(m_pLogger);

	if(!m_pLogger)
		return 0;

	Message msg;
	
	if(!msg.SetRawMsg(iLen, pData))
		return 0;

	if(!msg.DecodeAuto())
		return 0;

	va_list ap;
	va_start(ap, fmt);

	int iRetVal = m_pLogger->vLogBitmapMsg(level, msg.m_sBMsg, fmt, ap);

	va_end(ap);

	return iRetVal;
}

//==============================================================

void LoggingObject::LogHexDump(enum LogLevel level, const char *szPrefix, int iLen, const unsigned char *pbData)
{
	char szLineBuf[256];
	char szHexBuf[4];
	char szAscBuf[9];
	int i;

	if(iLen == 0)
		return;

	for(i = 0; i < iLen; i++)
	{
		if((i % 8) == 0)
		{
			::sprintf_s(szLineBuf, sizeof(szLineBuf), "%.32s ", szPrefix);
			::strcpy_s(szAscBuf, sizeof(szAscBuf), "        ");
		}

		::sprintf_s(szHexBuf, sizeof(szHexBuf), "%02X ", (int)(pbData[i]));
		::strcat_s(szLineBuf, sizeof(szLineBuf), szHexBuf);

		if(::isprint(pbData[i]))
			szAscBuf[i % 8] = pbData[i];
		else
			szAscBuf[i % 8] = '.';

		if((i % 8) == 7)
		{
			Log(level, "%s %s", szLineBuf, szAscBuf);
			szLineBuf[0] = '\0';
		}
	}


	if(::strlen(szLineBuf))
	{
		while((i++ % 8) != 0)
			::strcat_s(szLineBuf, sizeof(szLineBuf), "   ");

		Log(level, "%s %s", szLineBuf, szAscBuf);
	}
}

//==============================================================
