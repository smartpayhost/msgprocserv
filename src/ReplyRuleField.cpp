#include "stdafx.h"
#include "ReplyRuleField.h"

#include "ReplyRuleFieldCopy.h"
#include "ReplyRuleFieldFixed.h"
#include "ReplyRuleFieldPAN.h"
#include "ReplyRuleFieldExpDate.h"
#include "ReplyRuleFieldDate.h"
#include "ReplyRuleFieldTime.h"
#include "ReplyRuleFieldAscFile.h"
#include "ReplyRuleFieldFile.h"
#include "ReplyRuleFieldProg.h"
#include "ReplyRuleFieldQuery.h"
#include "ReplyRuleFieldEtslLogonDataHack.h"

//======================================================================

ReplyRuleField::ReplyRuleField(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRule(pStyle),
	m_iFieldNum(iFieldNum)
{
}

//======================================================================
//
//	FIELD <field_num>
//	FIELD <field_num> COPY
//	FIELD <field_num> FIXED <data>
//	FIELD <field_num> FILE <filename>
//	FIELD <field_num> ASCFILE <filename>
//	FIELD <field_num> PROG <cmdline>
//	FIELD <field_num> QUERY <queryname>
//	FIELD <field_num> PAN
//	FIELD <field_num> EXPDATE
//	FIELD <field_num> TIME
//	FIELD <field_num> DATE
//	FIELD <field_num> ETSL_LOGON_DATA_HACK

////	FIELD <field_num> PROMPT
////	FIELD <field_num> CENTS

//---------------------------------------------------------

ReplyRuleField* ReplyRuleField::MakeNew(const BitmapMsgStyle *pStyle, const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return NULL;

	ReplyRuleField *p = NULL;

	i++;

	if((i == rLine.end()) || ((*i) == "COPY"))
	{
		p = new ReplyRuleFieldCopy(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "FIXED")
	{
		p = new ReplyRuleFieldFixed(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "PAN")
	{
		p = new ReplyRuleFieldPAN(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "EXPDATE")
	{
		p = new ReplyRuleFieldExpDate(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "DATE")
	{
		p = new ReplyRuleFieldDate(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "TIME")
	{
		p = new ReplyRuleFieldTime(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "FILE")
	{
		p = new ReplyRuleFieldFile(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "ASCFILE")
	{
		p = new ReplyRuleFieldAscFile(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "PROG")
	{
		p = new ReplyRuleFieldProg(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "QUERY")
	{
		p = new ReplyRuleFieldQuery(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}
	else if((*i) == "ETSL_LOGON_DATA_HACK")
	{
		p = new ReplyRuleFieldEtslLogonDataHack(iFieldNum, pStyle);

		if(p->Import(rLine))
			return p;
	}

	delete p;
	return NULL;
}

//======================================================================
