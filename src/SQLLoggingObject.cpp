#include "stdafx.h"
#include "SQLLoggingObject.h"
#include "OdbcEnvironment.h"

using std::string;

//======================================================================

const string SQLLoggingObject::DEFAULT_PREFIX;

//======================================================================

void SQLLoggingObject::LogSQLInfo(enum LogLevel level, HDBC hdbc, HSTMT hstmt, const string& strPrefix) const
{
	UCHAR szSqlState[SQL_MAX_MESSAGE_LENGTH];
	UCHAR szErrorMsg[SQL_MAX_MESSAGE_LENGTH];
	SDWORD sdwNativeError;
	SWORD swErrorMsgSize;

	RETCODE rc = ::SQLError(OdbcEnvironment::HEnv(), hdbc, hstmt, szSqlState, &sdwNativeError, szErrorMsg, sizeof(szErrorMsg) - 1, &swErrorMsgSize);

	switch(rc)
	{
	case SQL_SUCCESS:
		szErrorMsg[swErrorMsgSize] = 0;
		Log(level, "%s SQLState: %s: Code = 0x%08lx: %s", strPrefix.c_str(), szSqlState, sdwNativeError, szErrorMsg);
		break;

	case SQL_NO_DATA_FOUND:
	case SQL_SUCCESS_WITH_INFO:
		Log(level, "%s Code = 0x%08lx", strPrefix.c_str(), sdwNativeError);
		break;

	default:
		Log(level, "%s SQLError (no info available)", strPrefix.c_str());
		break;
	}
}

//======================================================================

void SQLLoggingObject::LogSQLError(enum LogLevel level, HDBC hdbc, HSTMT hstmt, const string& strPrefix) const
{
	UCHAR szSqlState[SQL_MAX_MESSAGE_LENGTH];
	UCHAR szErrorMsg[SQL_MAX_MESSAGE_LENGTH];
	SDWORD sdwNativeError;
	SWORD swErrorMsgSize;

	RETCODE rc = ::SQLError(OdbcEnvironment::HEnv(), hdbc, hstmt, szSqlState, &sdwNativeError, szErrorMsg, sizeof(szErrorMsg) - 1, &swErrorMsgSize);

	switch(rc)
	{
	case SQL_SUCCESS:
		szErrorMsg[swErrorMsgSize] = 0;
		Log(level, "%s SQLState: %s: Error = 0x%08lx: %s", strPrefix.c_str(), szSqlState, sdwNativeError, szErrorMsg);
		break;

	case SQL_NO_DATA_FOUND:
	case SQL_SUCCESS_WITH_INFO:
		Log(level, "%s SQLError = 0x%08lx", strPrefix.c_str(), sdwNativeError);
		break;

	default:
		Log(level, "%s SQLError (no error data available)", strPrefix.c_str());
		break;
	}
}

//======================================================================
