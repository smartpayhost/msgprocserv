#ifndef _LOGGERSTDERR_H
#define _LOGGERSTDERR_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGERSTDIO_H
#include "LoggerStdio.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

//==============================================================

class LoggerStderr : public LoggerStdio {
public:
	LoggerStderr(const std::string& strLogName, int iLogLevel);
	virtual ~LoggerStderr();

	virtual int vLog(enum LogLevel, const char *, va_list);

private:
	CritSect	m_Lockable;
};

//==============================================================

#endif // _LOGGERSTDERR_H
