#include "stdafx.h"

#include "BCD.h"
#include "ETSL6Header.h"
#include "StrUtil.h"

using std::vector;
using std::string;

extern BitmapMsgStyle TemplateISO8583;

static const int TERMINALID_INDEX = 41;
static const int ACCEPTORID_INDEX = 42;
	
//====================================================================

ETSL60Header::ETSL60Header()
{
}

ETSL60Header::ETSL60Header(const BYTE *pBuffer)
{
	Make(pBuffer);
}

void ETSL60Header::Make(const BYTE *pBuffer)
{
	m_rawBytes = vector<BYTE>(pBuffer, pBuffer + ETSL60HEADER_SIZE);

	BitmapMsgField sTerminalID(TERMINALID_INDEX, &TemplateISO8583);
	BitmapMsgField sAcceptorID(ACCEPTORID_INDEX, &TemplateISO8583);

	m_pbTerminalID = &m_rawBytes[1];
	m_pbAcceptorID = &m_rawBytes[1 + TERMINALID_WID];
	m_pbSTAN = &m_rawBytes[1 + TERMINALID_WID + ACCEPTORID_WID];
	m_pbType = &m_rawBytes[1 + TERMINALID_WID + ACCEPTORID_WID + STAN_WID];

	sTerminalID.SetFieldContents(m_pbTerminalID, TERMINALID_WID);
	sAcceptorID.SetFieldContents(m_pbAcceptorID, ACCEPTORID_WID);
	
	m_Type = vector<BYTE>(m_pbType, m_pbType + TYPE_WID);
	m_Stan = vector<BYTE>(m_pbSTAN, m_pbSTAN + STAN_WID);

	m_TerminalID = StrUtil::FieldToString(sTerminalID);
	m_AcceptorID = StrUtil::FieldToString(sAcceptorID);

	StrUtil::trim(m_TerminalID);
	StrUtil::trim(m_AcceptorID);
}

//====================================================================

vector<BYTE> ETSL60Header::BuildAS2805()
{
	BitmapMsgStyle *pMsgStyle = &TemplateISO8583;

	BitmapMsg message;

	BitmapMsgField *pField11 = new BitmapMsgField(STAN_INDEX,pMsgStyle);
	BitmapMsgField *pField41 = new BitmapMsgField(TERMINALID_INDEX,pMsgStyle);
	BitmapMsgField *pField42 = new BitmapMsgField(ACCEPTORID_INDEX,pMsgStyle);

	pField11->SetFieldContents(m_pbSTAN,STAN_WID);
	pField41->SetFieldContents(m_pbTerminalID,TERMINALID_WID);
	pField42->SetFieldContents(m_pbAcceptorID,ACCEPTORID_WID);

	message.SetMsgStyle(pMsgStyle);
	message.SetMsgType(BCD::PackedBcd2Bin((m_pbType[0] << 8L) + m_pbType[1]));
	message.SetField(pField11);
	message.SetField(pField41);
	message.SetField(pField42);
	message.Build();

	return vector<BYTE>(message.GetRawMsg(), message.GetRawMsg() + message.GetMsgLen());
}

//====================================================================

