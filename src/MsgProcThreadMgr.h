#ifndef _MSGPROCTHREADMGR_H
#define _MSGPROCTHREADMGR_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _STOPPABLETHREAD_H
#include "StoppableThread.h"
#endif

#ifndef _MSGPROCTHREAD_H
#include "MsgProcThread.h"
#endif

#ifndef _WORKBROKER_H
#include "WorkBroker.h"
#endif

#include <list>

//====================================================================

class MsgProcThreadMgr : public StoppableThread {
public:
	MsgProcThreadMgr();

	DataPipe& GetPipeSrc();
	void SetPipeDst(DataPipe *pPipe);

	void SetConfig(class MsgProcCfg *pConfig) { m_pConfig = pConfig; }

	void SetLogger(Logger* pLogger);
	void SetLoggingName(const std::string& strName);

protected:
    DWORD ThreadMemberFunc();

private:
	//====================================================================
	
	class MPTMWorkHandler : public WorkHandler {
	public:
		explicit MPTMWorkHandler(MsgProcThread *pThread);
		~MPTMWorkHandler();

		virtual int GetCurrentWorkLoad();

		DataPipe& GetPipe();
		MsgProcThread *GetThread() { return m_pThread; }

	private:
		MsgProcThread *m_pThread;
	};

	//====================================================================

	class MPTMWorkHandlerFactory : public WorkHandlerFactory, public LoggingObject {
	public:
		MPTMWorkHandlerFactory();

		virtual WorkHandler* CreateNewWorkHandler();
		virtual void DestroyWorkHandler(WorkHandler* wh);
		
		void SetMgr(MsgProcThreadMgr* mgr) { m_mgr = mgr; }

		void CleanupDeadHandlers();
		void ShutdownHandlers();

	private:
		MsgProcThreadMgr* m_mgr;

		int m_iNumHandlers;
		int m_iHandlerCounter;
		std::list<MPTMWorkHandler *> m_listHandlers;
	};

	friend MPTMWorkHandlerFactory;

	//====================================================================

private:
	DataPipe m_pipeSrc;
	DataPipe *m_pPipeDst;

	class MsgProcCfg *m_pConfig;

	Event	m_evUpstreamShutdown;
	Event	m_evDeadUpstream;

	MPTMWorkHandlerFactory m_whf;
	WorkBroker m_wb;

	MsgProcThread* CreateNewUpstreamThread();
	void RemoveUpstreamThread(MsgProcThread*);

	BOOL ProcessDataFromDownstream();
};

//====================================================================

#endif // _MSGPROCTHREADMGR_H
