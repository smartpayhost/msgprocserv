#ifndef _LOGGERFILE_H
#define _LOGGERFILE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGERSTDIO_H
#include "LoggerStdio.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

//==============================================================

class LoggerFile : public LoggerStdio {
public:
	LoggerFile(const std::string& strLogName, int iLogLevel, const std::string& strLogPath);

	virtual int vLog(enum LogLevel, const char *, va_list);

	void SetLogPath(const std::string& strLogPath) { m_strLogPath = strLogPath; }

	void SetDefaultToStderr(BOOL fDefaultToStderr) { m_fDefaultToStderr = fDefaultToStderr; }

private:
	CritSect	m_Lockable;

	std::string m_strLogPath;

	BOOL m_fDefaultToStderr;
};

//==============================================================

#endif // _LOGGERFILE_H
