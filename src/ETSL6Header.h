#ifndef _ETSL6MESSAGEPACKET
#define _ETSL6MESSAGEPACKET

#include "message.h"

#include <vector>

#define BMPMSG_OLD_OFFSET		TPDU_SIZE
#define BMPMSG_NEW_OFFSET		(ETSL60HEADER_OFFSET + ETSL60HEADER_SIZE)

#define INDICATOR_WID		1
#define TERMINALID_WID		8
#define ACCEPTORID_WID		15
#define STAN_WID			3
#define TYPE_WID			2

#define ETSL60HEADER_OFFSET		TPDU_SIZE
#define ETSL60HEADER_SIZE		(INDICATOR_WID + TERMINALID_WID + ACCEPTORID_WID + STAN_WID + TYPE_WID)

#define ENCRYPTION_INDICATOR		0xff
#define STAN_INDEX					11

#include "BitmapMsg.h"

//====================================================================


class ETSL60Header 
{
public:
	ETSL60Header();
	ETSL60Header( const BYTE *pBytes );

	void Make( const BYTE *pBuffer );
	std::vector<BYTE> BuildAS2805();

	std::string m_TerminalID, m_AcceptorID;
	std::vector<BYTE> m_Stan;
	std::vector<BYTE> m_Type;
	std::vector<BYTE> m_rawBytes;

private:
	BYTE *m_pbType, *m_pbSTAN, *m_pbTerminalID, *m_pbAcceptorID;
};
#endif