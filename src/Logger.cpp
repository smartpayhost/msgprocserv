#include "stdafx.h"
#include "Logger.h"
#include "BitmapMsg.h"

using std::string;

//======================================================================

Logger::Logger(const string& m_strLogName, int m_iLogLevel)
:	m_strLogName(m_strLogName),
	m_iLogLevel(m_iLogLevel)
{
}

//======================================================================

int Logger::Log(enum LogLevel level, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	int iRetVal = vLog(level, fmt, ap);

	va_end(ap);

	return iRetVal;
}

//======================================================================

int Logger::vLogBitmapMsg(enum LogLevel, const BitmapMsg&, const char *, va_list)
{
	return 0;
}

//======================================================================

int Logger::LogBitmapMsg(enum LogLevel level, const BitmapMsg& msg, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	int iRetVal = vLogBitmapMsg(level, msg, fmt, ap);

	va_end(ap);

	return iRetVal;
}

//======================================================================

const char* Logger::GetLogLevelString(LogLevel eLevel)
{
	switch(eLevel)
	{
	case LOG_EMERGENCY:	return "EMERGENCY";
	case LOG_ALERT:		return "ALERT";
	case LOG_CRITICAL:	return "CRITICAL";
	case LOG_ERROR:		return "ERROR";
	case LOG_WARNING:	return "WARNING";
	case LOG_NOTICE:	return "NOTICE";
	case LOG_INFO:		return "INFO";
	case LOG_DEBUG:		return "DEBUG";
	case LOG_DUMP:		return "DUMP";
	default:			return "UNKNOWN";
	}
}

//======================================================================
