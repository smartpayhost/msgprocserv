#ifndef _STATSPACKAGE_H
#define _STATSPACKAGE_H

//===============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <map>
#include <string>
#include <boost/utility.hpp>

//===============================================================

enum {
	S_STR_STATUS,
	S_INT_BYTES_UP,
	S_INT_BYTES_DOWN,
	S_INT_ERRS_UP,
	S_INT_ERRS_DOWN,
	S_INT_MSGS_UP,
	S_INT_MSGS_DOWN,
};

//===============================================================

class StatsPackage : public boost::noncopyable {
public:
	StatsPackage();
	virtual ~StatsPackage() {}

	virtual void SetInt(int iId, int iValue);
	virtual int GetInt(int iId);
	virtual int IncrementInt(int iId);
	virtual int AddInt(int iId, int iAmount);

	virtual void SetString(int iId, const std::string& strValue);
	virtual std::string GetString(int iId);

	HANDLE GetEventHandle();

private:
	CritSect		m_Lockable;
	Event			m_evEvent;

	std::map<int, int>		m_mapIntStats;
	std::map<int, std::string>	m_mapStringStats;
};

//===============================================================

#endif // _STATSPACKAGE_H
