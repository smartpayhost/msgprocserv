#ifndef _THREADOBJECT_H
#define _THREADOBJECT_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <boost/utility.hpp>

//====================================================================

class ThreadObject : public LoggingObject, public boost::noncopyable, public Waitable {
public:
    ThreadObject();
    virtual ~ThreadObject() = 0 {}

    virtual BOOL StartThread();
    virtual void WaitForExit();

	virtual BOOL Suspend();
	virtual BOOL Resume();

	virtual void SetPriority(int iPriority) { m_iPriority = iPriority; }

	HANDLE GetHandle() const { return m_hThread; }
	inline DWORD GetThreadId() const { return m_ThreadId; }
	inline int GetPriority() const { return m_iPriority; }
	inline BOOL IsRunning() const { return m_fIsRunning; }
	inline DWORD GetRetval() const { return m_dwRetval; }

	virtual void SetOnExitEvent(Event* pevOnExit) { m_pevOnExit = pevOnExit; }

protected:
    virtual DWORD ThreadMemberFunc() = 0;

private:
    static DWORD WINAPI ThreadFunc(LPVOID param);

    HANDLE  m_hThread;
    DWORD   m_ThreadId;
	int		m_iPriority;
	BOOL	m_fIsRunning;
	DWORD	m_dwRetval;

	Event	*m_pevOnExit;
};

//====================================================================

#endif _THREADOBJECT_H
