#include "stdafx.h"
#include "RuleFile.h"

#include "Path.h"

#include <assert.h>
#include <stdio.h>
#include <io.h>
#include <share.h>

using std::string;

//======================================================================
//======================================================================

RuleEntity::~RuleEntity()
{
}

//======================================================================

RuleEntityDescriptor::~RuleEntityDescriptor()
{
}

//======================================================================
//======================================================================

RuleFile::RuleFile()
{
}

//----------------------------------------------------------------------

RuleFile::~RuleFile()
{
}

//======================================================================

void RuleFile::Reset()
{
	if(m_lineCurrent.size())
		m_lineCurrent.clear();

	m_iLineNumber = 0;
}

//======================================================================

BOOL RuleFile::TokeniseChar(int c)
{
	if(c == '\n')
	{
		m_iLineNumber++;

		size_t iSize = m_strCurrent.size();

		if((iSize > 0) && (m_strCurrent[iSize - 1] == '\\'))
		{
			m_strCurrent.resize(m_strCurrent.size() - 1);
			return TRUE;
		}
		else
		{
			if(!StrUtil::StringToLine(m_strCurrent, m_lineCurrent))
				return FALSE;

			if(m_lineCurrent.size())
			{
				m_seqLines.push_back(std::make_pair(m_iLineNumber, m_lineCurrent));
				m_lineCurrent.clear();
			}

			m_strCurrent.erase();
			return TRUE;
		}
	}
	else
	{
		m_strCurrent += (char)c;
		return TRUE;
	}
}

//======================================================================

BOOL RuleFile::Load(const string& strFilename)
{
	Path sPath;

	sPath.Chdir(strFilename);

	string strFile = sPath.GetFilename();

	FILE *fp = _fsopen(strFile.c_str(), "r+t", _SH_DENYWR);

	assert(fp);

	BOOL fRetVal = TRUE;

	if(fp)
	{
		Reset();

		// tokenise file

		int c;

		while((c = fgetc(fp)) != EOF)
		{
			if(!TokeniseChar(c))
			{
				Log(LOG_CRITICAL, "Error tokenising rule file %s at line %d, after:\n\n%s\n\n", 
					strFilename.c_str(), m_iLineNumber, m_strCurrent.c_str());

				assert(0);
				fRetVal = FALSE;
				break;
			}
		}

		fclose(fp);
	}
	else
	{
		char szErr[100];

		strerror_s(szErr, sizeof(szErr), errno);

		Log(LOG_CRITICAL, "Unable to open rule file %s: %s", 
			strFilename.c_str(), szErr);

		fRetVal = FALSE;
	}

	// process tokens to generate rules, hosts etc

	RuleEntity *entityCurrent = NULL;
	LineSequence::iterator i;

	for(i = m_seqLines.begin(); fRetVal && i != m_seqLines.end(); i++)
	{
		string strLine;
		StrUtil::LineToString((*i).second, strLine);

		Log(LOG_DEBUG, "%s",strLine.c_str());

		if(entityCurrent)
		{
			if(!entityCurrent->ProcessLine((*i).second))
			{
				Log(LOG_CRITICAL, "Error processing rule file %s at line %d:\n\n%s\n", 
					strFilename.c_str(), (*i).first, strLine.c_str());

				fRetVal = FALSE;
			}

			if(entityCurrent->IsComplete())
			{
				delete entityCurrent;
				entityCurrent = NULL;
			}
		}
		else if(((*i).second.size() > 1) && (*i).second[0] == "INCLUDE")
		{
			RuleFile *pIncludeFile = Clone();

			assert(pIncludeFile);

			if(pIncludeFile && pIncludeFile->Load((*i).second[1]))
				MergeFrom(pIncludeFile);
			else
				fRetVal = FALSE;

			delete pIncludeFile;
		}
		else
		{
			entityCurrent = MakeNewEntity((*i).second);

			if(entityCurrent == NULL)
			{
				Log(LOG_CRITICAL, "Error processing rule file %s at line %d:\n\n%s\n", 
					strFilename.c_str(), (*i).first, strLine.c_str());

				fRetVal = FALSE;
			}
		}
	}

	sPath.PopDir();

	return fRetVal;
}

//======================================================================

void RuleFile::RegisterEntityType(RuleEntityDescriptor *pDescriptor)
{
	m_seqTypes.push_back(pDescriptor);
}

//======================================================================

RuleEntity *RuleFile::MakeNewEntity(const Line &rLine)
{
	EntityDescriptorSequence::iterator i;

	for(i = m_seqTypes.begin(); i != m_seqTypes.end(); i++)
	{
		RuleEntityDescriptor *pr;
		pr = *i;

		if(pr->IsStart(rLine))
			return pr->MakeNew(rLine);
	}

	return NULL;
}

//======================================================================
