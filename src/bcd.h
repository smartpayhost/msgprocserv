#ifndef _BCD_H
#define _BCD_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//==============================================================

class BCD {
public:
	static DWORD PackedBcd2Bin(DWORD dwBCD);
	static DWORD Bin2PackedBcd(DWORD dwBin);
	static DWORD AscDec2Bin(const char *pcAsc, int iLen);
	static DWORD AscHex2Bin(const char *pcAsc, int iLen);
	static BYTE AscDigit2Bin(const char cChar);
	static void Asc2PackedBcd(const char *pcAsc, BYTE *pBcd, int iLen);
	static char HexDigit2Asc(BYTE bHex);
	static void Hex2Asc(const BYTE *pHex, char *pcAsc, int iLen);

	static void Bin2LLBCD(DWORD dwBin, BYTE *pcMsg);
	static void Bin2LLLBCD(DWORD dwBin, BYTE *pcMsg);
	static void Bin2LLAsc(DWORD dwBin, BYTE *pcMsg);
	static void Bin2LLLAsc(DWORD dwBin, BYTE *pcMsg);
	static DWORD LLBCD2Bin(const BYTE *pcMsg);
	static DWORD LLLBCD2Bin(const BYTE *pcMsg);
	static DWORD LLAsc2Bin(const BYTE *pcMsg);
	static DWORD LLLAsc2Bin(const BYTE *pcMsg);
};

//==============================================================

#endif //_BCD_H
