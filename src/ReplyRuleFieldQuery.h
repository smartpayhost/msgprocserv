#ifndef _REPLYRULEFIELDQUERY_H
#define _REPLYRULEFIELDQUERY_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULEFIELD_H
#include "ReplyRuleField.h"
#endif

//==============================================================

class ReplyRuleFieldQuery : public ReplyRuleField {
public:
	ReplyRuleFieldQuery(int iFieldNum, const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Check() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const;

private:
	std::string	m_strQuery;
	BOOL	m_fCooked;
};

//==============================================================

#endif // _REPLYRULEFIELDQUERY_H
