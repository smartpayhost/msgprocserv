#include "stdafx.h"
#include "ReplyRuleFieldTime.h"

#include <assert.h>

#include <time.h>

using std::string;

//======================================================================

ReplyRuleFieldTime::ReplyRuleFieldTime(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldTime::Clone() const
{
	return new ReplyRuleFieldTime(*this);
}

//======================================================================
//
//	FIELD <field_num> TIME

BOOL ReplyRuleFieldTime::Import(const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "TIME")
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL ReplyRuleFieldTime::Export(string &strDesc)
{
	StrUtil::Format(strDesc, "FIELD %d TIME", GetFieldNum());

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldTime::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	BitmapMsgField *pField = new BitmapMsgField(GetFieldNum(), rOrigMsg.GetMsgStyle());

	assert(pField);

	if(!pField)
		return FALSE;

	char szBuf[7];

	time_t t;
	struct tm now;

	time(&t);
	localtime_s(&now, &t);

	strftime(szBuf, sizeof(szBuf), "%H%M%S", &now);

	if(!pField->SetFieldFromString(szBuf))
	{
		delete pField;
		return FALSE;
	}

	rNewMsg.SetField(pField);

	return TRUE;
}

//======================================================================
