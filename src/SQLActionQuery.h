#ifndef _SQLACTIONQUERY_H
#define _SQLACTIONQUERY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SQLMESSAGEQUERY_H
#include "SQLMessageQuery.h"
#endif

class SQLResultTable;
class BitmapMsg;

//======================================================================

class SQLActionQuery : public SQLMessageQuery {
public:
	BOOL Export(std::string &rStrDest) const;

	BOOL ExecQuery(SQLResultTable &rResult, const BitmapMsg *pOrigMsg = NULL, const BitmapMsg *pNewMsg = NULL) const;
};

//======================================================================

#endif // _SQLACTIONQUERY_H
