#include "stdafx.h"
#include "ReplyRuleMsgType.h"

#include <stdio.h>

using std::string;

//======================================================================

ReplyRuleMsgType::ReplyRuleMsgType(const BitmapMsgStyle *pStyle)
:	ReplyRule(pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleMsgType::Clone() const
{
	return new ReplyRuleMsgType(*this);
}

//======================================================================
//
//	MSG_TYPE COPY_ORIG [ MASKED_WITH <mask> ] [ ADD <offset> ]
//	MSG_TYPE <msg_type>
//

BOOL ReplyRuleMsgType::Import(const Line &rLine)
{
	m_eSource = RRMTS_FROMORIG;
	m_wFixedData = 0;
	m_bUseMask = FALSE;
	m_wMask = 0x0FF0;
	m_bUseOffset = FALSE;
	m_wOffset = 10;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "MSG_TYPE") return FALSE;

	if(++i == rLine.end()) return FALSE;

	if((*i) == "COPY_ORIG")
	{
		if(++i == rLine.end()) return TRUE;

		if((*i) == "MASKED_WITH")
		{
			if(++i == rLine.end()) return FALSE;

			m_wMask = (WORD)strtol((*i).c_str(), NULL, 16);

			if(m_wMask == 0)
				return FALSE;

			m_bUseMask = TRUE;

			if(++i == rLine.end()) return TRUE;
		}

		if((*i) == "ADD")
		{
			if(++i == rLine.end()) return FALSE;

			m_wOffset = (WORD)strtol((*i).c_str(), NULL, 10);

			if(m_wOffset  == 0)
				return FALSE;

			m_bUseOffset = TRUE;
		}
	}
	else
	{
		m_eSource = RRMTS_FIXED;

		m_wFixedData = (WORD)strtol((*i).c_str(), NULL, 10);
	}

	return TRUE;
}

//======================================================================
//
//	MSG_TYPE COPY_ORIG [ MASKED_WITH <mask> ] [ ADD <offset> ]
//	MSG_TYPE <msg_type>
//

BOOL ReplyRuleMsgType::Export(string &strDesc)
{
	string strTmp;

	strDesc = "MSG_TYPE";

	if(m_eSource == RRMTS_FROMORIG)
	{
		strDesc += " COPY_ORIG";

		if(m_bUseMask)
		{
			StrUtil::Format(strTmp, " MASKED_WITH %04X", (int)m_wMask);
			strDesc += strTmp;
		}

		if(m_bUseOffset)
		{
			StrUtil::Format(strTmp, " ADD %d", (int)m_wOffset);
			strDesc += strTmp;
		}
	}
	else
	{
		StrUtil::Format(strTmp, " %04d", (int)m_wFixedData);
		strDesc += strTmp;
	}

	return TRUE;
}

//======================================================================

BOOL ReplyRuleMsgType::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	switch(m_eSource)
	{
	case RRMTS_FIXED:
		rNewMsg.SetMsgType(m_wFixedData);
		break;

	case RRMTS_FROMORIG:
		rNewMsg.SetMsgType(rOrigMsg.GetMsgType());

		if(m_bUseMask)
		{
			int iNewMsgType;
			char szTmp[5];

			sprintf_s(szTmp, sizeof(szTmp), "%04d", rNewMsg.GetMsgType());
			iNewMsgType = strtol(szTmp, NULL, 16);	// convert to hex
			iNewMsgType &= m_wMask;					// mask
			sprintf_s(szTmp, sizeof(szTmp), "%04X", iNewMsgType);
			iNewMsgType = strtol(szTmp, NULL, 10);	// convert back to dec
			rNewMsg.SetMsgType(iNewMsgType);
		}

		if(m_bUseOffset)
		{
			rNewMsg.SetMsgType(rNewMsg.GetMsgType() + m_wOffset);
		}

		break;

	default:
		return FALSE;
	}

	return TRUE;
}

//======================================================================
