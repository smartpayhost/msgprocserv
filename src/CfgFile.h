#ifndef _CFGFILE_H
#define _CFGFILE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <map>
#include <vector>
#include <string>

//==============================================================

class CfgFile {
public:
	virtual ~CfgFile() {}

	BOOL Load(const std::string &strFilename, FILETIME *pftConfigLastChange = NULL);

	BOOL GetStringValue(const std::string &strKey, std::string &strResult);
	int GetStringValues(const std::string &strKey, std::vector<std::string>& Result);
	BOOL GetIntValue(const std::string &strKey, int &iResult);

private:
	typedef std::multimap<std::string, std::string> StringMap;

	StringMap m_mapConfig;

	std::string m_strFilename;
};

//==============================================================

#endif // _CFGFILE_H
