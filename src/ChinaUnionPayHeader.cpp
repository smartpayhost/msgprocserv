#include "stdafx.h"

#include "BCD.h"
#include "ChinaUnionPayHeader.h"
#include "StrUtil.h"

using std::vector;
using std::string;

extern BitmapMsgStyle TemplateISO8583;

//static const int TERMINALID_INDEX = 41;
//static const int ACCEPTORID_INDEX = 42;
	
//====================================================================

ChinaUnionPayHeader::ChinaUnionPayHeader()
{
}

ChinaUnionPayHeader::ChinaUnionPayHeader(const BYTE *pBuffer)
{
	Make(pBuffer);
}

void ChinaUnionPayHeader::Make(const BYTE *pBuffer)
{
	m_rawBytes = vector<BYTE>(pBuffer, pBuffer + CUP_HEADER_SIZE);
	m_pbCupHdr = &m_rawBytes[0];
	m_pbType = &m_rawBytes[0 + CUP_HEADER_WID];
	m_Type = vector<BYTE>(m_pbType, m_pbType + CUP_TYPE_WID);
}

//====================================================================

BOOL ChinaUnionPayHeader::IsChinaUnionPayHeader(const BYTE *pBuffer)
{
	const BYTE abCupHdr[] = { 0x60, 0x22, 0x00, 0x00, 0x00, 0x00 };
	return memcmp(abCupHdr, pBuffer, sizeof(abCupHdr)) == 0;
}

//====================================================================

vector<BYTE> ChinaUnionPayHeader::BuildAS2805()
{
	BitmapMsgStyle *pMsgStyle = &TemplateISO8583;

	BitmapMsg message;

	message.SetMsgStyle(pMsgStyle);
	message.SetMsgType(BCD::PackedBcd2Bin((m_pbType[0] << 8L) + m_pbType[1]));
	message.Build();

	return vector<BYTE>(message.GetRawMsg(), message.GetRawMsg() + message.GetMsgLen());
}

//====================================================================

