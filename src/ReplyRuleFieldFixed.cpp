#include "stdafx.h"
#include "ReplyRuleFieldFixed.h"

#include <assert.h>

using std::string;

//======================================================================

ReplyRuleFieldFixed::ReplyRuleFieldFixed(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldFixed::Clone() const
{
	return new ReplyRuleFieldFixed(*this);
}

//======================================================================
//
//	FIELD <field_num> FIXED <data>

BOOL ReplyRuleFieldFixed::Import(const Line &rLine)
{
	m_strFixedData.erase();

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "FIXED")
	{
		if(++i == rLine.end()) return FALSE;

		m_strFixedData = (*i);
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldFixed::Export(string &strDesc)
{
	string strTmp;

	StrUtil::Format(strDesc, "FIELD %d FIXED ", GetFieldNum());
	StrUtil::PurifyString(m_strFixedData, strTmp);
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldFixed::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	BitmapMsgField *pField;

	pField = new BitmapMsgField(GetFieldNum(), rOrigMsg.GetMsgStyle());

	assert(pField);

	if(!pField)
		return FALSE;

	if(!pField->SetFieldFromString(m_strFixedData))
	{
		delete pField;
		return FALSE;
	}

	rNewMsg.SetField(pField);

	return TRUE;
}

//======================================================================
