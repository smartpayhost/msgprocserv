#include "stdafx.h"
#include "NetworkHeader.h"

//--------------------------------------------------------------------

const int NetworkHeader::SHORT_NETWORK_HDR = 2;
const int NetworkHeader::NORMAL_NETWORK_HDR = 1;

//--------------------------------------------------------------------

NetworkHeader::NetworkHeader()
:	m_wHdrType(1),
	m_wHdrLen(10),
	m_wDataLen(0),
	m_wThread(0),
	m_wConnection(0)
{	
}

//--------------------------------------------------------------------

BOOL NetworkHeader::CopyFrom(const BYTE *pbBuf, BOOL fShortHdr)
{
	if(fShortHdr)
	{
		m_wHdrType		= (WORD)SHORT_NETWORK_HDR;
		m_wHdrLen		= 10;
		m_wDataLen		= (WORD)((pbBuf[0] << 8) + pbBuf[1]);
		m_wThread		= 0;
		m_wConnection	= 0;
	}
	else
	{
		m_wHdrType		= (WORD)((pbBuf[0] << 8) + pbBuf[1]);
		m_wHdrLen		= (WORD)((pbBuf[2] << 8) + pbBuf[3]);
		m_wDataLen		= (WORD)((pbBuf[4] << 8) + pbBuf[5]);
		m_wThread		= (WORD)((pbBuf[6] << 8) + pbBuf[7]);
		m_wConnection	= (WORD)((pbBuf[8] << 8) + pbBuf[9]);
	}

	if(m_wHdrType != (WORD)NORMAL_NETWORK_HDR && m_wHdrType != (WORD)SHORT_NETWORK_HDR)
		return FALSE;

	if(m_wHdrLen != 10)
		return FALSE;

	return TRUE;
}

//--------------------------------------------------------------------

void NetworkHeader::CopyTo(BYTE *pbBuf, BOOL fCheckForShortHdr, BOOL fUseNormalHdr)
{
	if(fCheckForShortHdr && IsShortHeader())
	{
		pbBuf[0] = (BYTE)((m_wDataLen    >> 8) & 0x00FF);
		pbBuf[1] = (BYTE)((m_wDataLen        ) & 0x00FF);
	}
	else
	{
		pbBuf[0] = fUseNormalHdr ? (BYTE)0					: (BYTE)((m_wHdrType >> 8) & 0x00FF);
		pbBuf[1] = fUseNormalHdr ? (BYTE)NORMAL_NETWORK_HDR : (BYTE)((m_wHdrType) & 0x00FF);
		pbBuf[2] = (BYTE)((m_wHdrLen     >> 8) & 0x00FF);
		pbBuf[3] = (BYTE)((m_wHdrLen         ) & 0x00FF);
		pbBuf[4] = (BYTE)((m_wDataLen    >> 8) & 0x00FF);
		pbBuf[5] = (BYTE)((m_wDataLen        ) & 0x00FF);
		pbBuf[6] = (BYTE)((m_wThread     >> 8) & 0x00FF);
		pbBuf[7] = (BYTE)((m_wThread         ) & 0x00FF);
		pbBuf[8] = (BYTE)((m_wConnection >> 8) & 0x00FF);
		pbBuf[9] = (BYTE)((m_wConnection     ) & 0x00FF);
	}
}

//--------------------------------------------------------------------

BOOL NetworkHeader::IsShortHeader()
{
	return m_wHdrType == SHORT_NETWORK_HDR;
}

//--------------------------------------------------------------------

WORD NetworkHeader::GetNormalHeaderLength()
{
	return 10;
}

//--------------------------------------------------------------------

WORD NetworkHeader::GetShortHeaderLength()
{
	return 2;
}

//--------------------------------------------------------------------

void NetworkHeader::SetHeaderType(BOOL fShortHdr)
{
	m_wHdrType = fShortHdr ? (WORD)SHORT_NETWORK_HDR : (WORD)NORMAL_NETWORK_HDR;
}

//--------------------------------------------------------------------