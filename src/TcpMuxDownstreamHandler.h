#ifndef _TCPMUXDOWNSTREAMHANDLER_H
#define _TCPMUXDOWNSTREAMHANDLER_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _TCPHANDLER_H
#include "TcpHandler.h"
#endif

#ifndef _TCPSOCKET_H
#include "TcpSocket.h"
#endif

#ifndef _NH1BLKTHREAD_H
#include "NH1BlkThread.h"
#endif

#ifndef _MUXDOWNSTREAMTHREAD_H
#include "MuxDownstreamThread.h"
#endif

#ifndef _HANDLERTHREAD_H
#include "HandlerThread.h"
#endif

#include <string>

class MuxUpstreamThread;
class TcpMuxThread;

//====================================================================

class TcpMuxDownstreamHandler : public HandlerThread {
public:
	TcpMuxDownstreamHandler(TcpMuxThread *pParent);

	void SetSocket(SOCKET s);
	std::string PeerName() const { return m_peer; }

	void SetPipeDst(DataPipe *pPipe);
	DataPipe& GetPipeSrc();

	void SetThreadNumber(BYTE bIndex, BYTE bInstance);
	BOOL AttachToUpstream(MuxUpstreamThread *tmuThread);

	void SetIdleTimeout(DWORD dwTimeout);

protected:
    DWORD ThreadMemberFunc();

private:
	TcpHandler m_th;
	NH1BlkThread m_nt;
	MuxDownstreamThread m_mdt;
	
	Event m_evDownstreamActivity;

	DataPipe *m_ppipe_mux_up;

	MuxUpstreamThread* m_pUpstream;
	BYTE m_bIndex;
	BYTE m_bInstance;

	TcpMuxThread *m_pParent;
	TcpSocket m_s;
	std::string m_peer;

	DWORD m_dwIdleTimeout;
};

//====================================================================

#endif // _TCPMUXDOWNSTREAMHANDLER_H
