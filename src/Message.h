#ifndef _MESSAGE_H
#define _MESSAGE_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BitmapMsg.h"
#include "MatchHistory.h"

#include <set>
#include <boost/utility.hpp>
#include <vector>

//======================================================================

class Message : public boost::noncopyable {
public:
	Message();

	BOOL SetRawMsg(int iLen, const BYTE *pBuf);
	void BuildRawMsg();

	BOOL DecodeBMsg();

	enum MNetworkHdrType {
		NHT_AUTOMATIC,
		NHT_NONE,
		NHT_NH1,
		NHT_NH2,
	};

	BOOL DecodeNetworkHeader(MNetworkHdrType eNetworkHdrType);

	enum MCommsHdrType {
		CHT_AUTOMATIC,
		CHT_NONE,
		CHT_TPDU,
	};

	BOOL DecodeCommsHeader(MCommsHdrType eCommsHdrType);

	BOOL DecodeAuto();

	void TurnaroundCommsHeader(const Message &rMsg);
	void TurnaroundNetworkHeader(const Message &rMsg);

	const BitmapMsgField* GetField(int iFieldNum) const { return m_sBMsg.GetField(iFieldNum); }
	BitmapMsgField* GetField(int iFieldNum) { return m_sBMsg.GetField(iFieldNum); }

	int GetMsgType() const { return m_sBMsg.GetMsgType(); }

	const BitmapMsg& GetBitmapMsg() const { return m_sBMsg; }
	
	void SetStyle(BitmapMsgStyle *pStyle) { m_pStyle = pStyle; }

	int	GetRawLen() const { return m_iRawLen; }
	const BYTE *GetRawBuf() const { return m_abRawBuf; }

	const BYTE *GetRawBMsg() const { return m_pRawBMsg; }

	int GetLenRemaining() const { return m_iLenRemaining; }

	int GetNetworkHdrLen() const { return m_iNetworkHdrLen; }

	MCommsHdrType GetCommsHdrType() const { return m_eCommsHdrType; }
	const BYTE *GetCommsHeader() const { return m_abCommsHeader; }

	const BYTE *GetRawCommsHdr() const { return m_pRawCommsHdr; }

	MatchHistory& GetMatchHistory() { return m_sMatchHistory; }

	void AddPossibleMacKey(const std::string& strKey) { m_PossibleMacKeys.insert(strKey); }
	const std::set<std::string>& GetPossibleMacKeys() const { return m_PossibleMacKeys; }

	std::string GetMacKey() const { return m_strMacKey; }
	void SetMacKey(const std::string& strMacKey) { m_strMacKey = strMacKey; }

	void CheckMac();
	BOOL GenResponseMac(const std::string& strKey, BOOL fAnsi, BOOL f64Bit);

	BOOL IsMacAnsi() const { return m_fAnsi; }
	BOOL IsMac64Bit() const { return m_f64Bit; }

	void SetBitmapMsg( const std::vector<BYTE> &bMsg );

	void SetEtsl6Encrypted( BOOL bValue ) { m_bEtsl6Encrypted = bValue; }
	BOOL GetEtsl6Encrypted() { return m_bEtsl6Encrypted; }

	void Etsl6BuildFakePacket();

	void SetChinaUnionPay( BOOL bValue ) { m_bChinaUnionPay = bValue; }
	BOOL GetChinaUnionPay() { return m_bChinaUnionPay; }

	void ChinaUnionPayBuildFakePacket();

public:
	BitmapMsg			m_sBMsg;

private:
	BOOL DecodeNetworkHeaderNone();
	BOOL DecodeNetworkHeaderNH1();
	BOOL DecodeNetworkHeaderNH2();
	BOOL DecodeNetworkHeaderAutomatic();

	BOOL DecodeCommsHeaderNone();
	BOOL DecodeCommsHeaderTPDU();
	BOOL DecodeCommsHeaderAutomatic();

	BOOL DecodeBMsg(int iLen, const BYTE *pBuf);

	void TurnaroundCommsHeaderNone(const Message &rMsg);
	void TurnaroundCommsHeaderTPDU(const Message &rMsg);

	void TurnaroundNetworkHeaderNone(const Message &rMsg);
	void TurnaroundNetworkHeaderNH1(const Message &rMsg);
	void TurnaroundNetworkHeaderNH2();

	BOOL CheckMacKey(const std::string &sKey);

private:
	static const int NETWORK_HDR_LEN_NH1;
	static const int NETWORK_HDR_LEN_NH2;
	static const int COMMS_HDR_LEN_TPDU;

	enum {
		MAX_NETWORK_HDR_LEN = 10,
		MAX_COMMS_HDR_LEN = 21,
		MAX_RAW_MSG_LEN = 2048,
	};

private:
	int					m_iRawLen;
	BYTE				m_abRawBuf[MAX_RAW_MSG_LEN];

	int					m_iLenRemaining;	// undecoded

	MNetworkHdrType		m_eNetworkHdrType;
	int					m_iNetworkHdrLen;
	BYTE				m_abNetworkHeader[MAX_NETWORK_HDR_LEN];

	MCommsHdrType		m_eCommsHdrType;
	int					m_iCommsHdrLen;

	BYTE				m_abCommsHeader[MAX_COMMS_HDR_LEN];

	BYTE				*m_pRawCommsHdr;	// pointer into m_pRawBuf

	BYTE				*m_pRawBMsg;		// pointer into m_pRawBuf

	BitmapMsgStyle		*m_pStyle;

	std::set<std::string>	m_PossibleMacKeys;
	BOOL				m_f64Bit;
	BOOL				m_fAnsi;
	std::string			m_strMacKey;

	MatchHistory		m_sMatchHistory;

	BOOL				m_bEtsl6Encrypted;
	std::vector<BYTE>	m_oldEtsl6Message;

	BOOL				m_bChinaUnionPay;
	std::vector<BYTE>	m_ChinaUnionPayMessage;
};

//======================================================================

#endif // _MESSAGE_H
