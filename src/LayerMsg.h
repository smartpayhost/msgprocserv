#ifndef _LAYERMSG_H
#define _LAYERMSG_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LoggingObject.h"
#include "MsgProcCfg.h"

#include <set>

class MessagePacket;
class LayerHost;

//==============================================================

class LayerMsg : public LoggingObject {
public:
	LayerMsg();

	BOOL Process(MessagePacket &rPacket);

	void SetDefaultKeys(const std::set<std::string>& Keys) { m_DefaultKeys = Keys; }

	void Setup(LayerHost *pHL) { m_pHL = pHL; }
	void SetConfig(MsgProcCfg *pConfig) { m_pConfig = pConfig; }
	MsgProcCfg &GetConfig() { return *m_pConfig; }	

private:

	std::set<std::string> m_DefaultKeys;

	LayerHost	*m_pHL;
	MsgProcCfg* m_pConfig;
};

//==============================================================

#endif _LAYERMSG_H
