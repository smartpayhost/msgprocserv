#ifndef _REPLYRULEFIELDETSLLOGONDATAHACK_H
#define _REPLYRULEFIELDETSLLOGONDATAHACK_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULEFIELD_H
#include "ReplyRuleField.h"
#endif

//==============================================================

class ReplyRuleFieldEtslLogonDataHack : public ReplyRuleField {
public:
	ReplyRuleFieldEtslLogonDataHack(int iFieldNum, const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(MessagePacket &rPacket) const;
};

//==============================================================

#endif // _REPLYRULEFIELDETSLLOGONDATAHACK_H
