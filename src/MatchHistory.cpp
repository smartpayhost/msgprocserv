#include "stdafx.h"
#include "MatchHistory.h"

using std::string;

//===================================================================

BOOL MatchHistory::MatchSeen(const string& strMatchName) const
{
	return m_mapMatches.find(strMatchName) != m_mapMatches.end();
}

//===================================================================

BOOL MatchHistory::MatchResult(const string& strMatchName) const
{
	MatchMap::const_iterator i;

	i = m_mapMatches.find(strMatchName);

	if(i != m_mapMatches.end())
	{
		return (*i).second;
	}

	return FALSE;
}

//===================================================================

void MatchHistory::SaveResult(const string& strMatchName, BOOL fResult)
{
	m_mapMatches[strMatchName] = fResult;
}

//===================================================================
