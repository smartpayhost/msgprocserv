#include "stdafx.h"
#include "MatchRuleTemplate.h"

#include "MsgProcRuleCollection.h"
#include "MatchTemplateCollection.h"
#include "MatchHistory.h"
#include "Message.h"
#include "MatchTemplate.h"

#include <assert.h>

using std::string;

//======================================================================

MatchRule* MatchRuleTemplate::Clone() const
{
	return new MatchRuleTemplate(*this);
}

//======================================================================
//
//	IS [ NOT ] A <templatename>
//

BOOL MatchRuleTemplate::Import(const Line &rLine)
{
	m_bInverted = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "IS") return FALSE;

	if(++i == rLine.end()) return FALSE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	if((*i) != "A") return FALSE;

	if(++i == rLine.end()) return FALSE;

	m_strTemplateName = (*i);

	return TRUE;
}

//======================================================================
//
//	IS [ NOT ] A <templatename>
//

BOOL MatchRuleTemplate::Export(string &strDesc)
{
	string strTmp;

	strDesc = "IS";

	if(m_bInverted)
		strDesc += " NOT";

	strDesc += " A";

	StrUtil::PurifyString(m_strTemplateName, strTmp);
	strDesc += ' ';
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL MatchRuleTemplate::DoesMatch(Message &rMsg) const
{
	assert(m_pMsgProcRules);

	BOOL fRetVal;

	if(rMsg.GetMatchHistory().MatchSeen(m_strTemplateName))
	{
		fRetVal = rMsg.GetMatchHistory().MatchResult(m_strTemplateName);
	}
	else
	{
		const MatchTemplate *pTemplate;

		pTemplate = m_pMsgProcRules->GetMatchTemplateCollection().FindByName(m_strTemplateName);

		if(pTemplate == NULL)
		{
			fRetVal = FALSE;
		}
		else
		{
			fRetVal = pTemplate->DoesMatch(rMsg);
		}

		rMsg.GetMatchHistory().SaveResult(m_strTemplateName, fRetVal);
	}

	if(m_bInverted)
		return !fRetVal;
	else
		return fRetVal;
}

//======================================================================

BOOL MatchRuleTemplate::Check() const
{
	assert(m_pMsgProcRules);

	if(m_pMsgProcRules->GetMatchTemplateCollection().FindByName(m_strTemplateName))
		return TRUE;

	Log(LOG_WARNING, "Match template %s not defined", m_strTemplateName.c_str());

	return FALSE;
}

//======================================================================
