#include "stdafx.h"
#include "SQLMatchQueryRuleEntity.h"

#include "SQLMatchQueryCollection.h"
#include <assert.h>

using std::string;

//======================================================================

SQLMatchQueryRuleEntity::SQLMatchQueryRuleEntity()
:	m_fComplete(FALSE),
	m_pCollection(NULL)
{
}

//======================================================================

void SQLMatchQueryRuleEntity::SetName(const string &strName)
{
	m_dsSQLMatchQuery.SetName(strName);
}

//======================================================================

void SQLMatchQueryRuleEntity::SetSQLMatchQueryCollection(SQLMatchQueryCollection *pCollection)
{
	m_pCollection = pCollection;
}

//----------------------------------------------------------------------

BOOL SQLMatchQueryRuleEntity::ProcessDataSource(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsSQLMatchQuery.SetDataSource(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL SQLMatchQueryRuleEntity::ProcessQuery(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsSQLMatchQuery.SetQuery(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL SQLMatchQueryRuleEntity::ProcessMatchQueryEnd(const Line & /*rLine*/)
{
	m_fComplete = TRUE;

	assert(m_pCollection);
	m_pCollection->AddSQLMatchQuery(m_dsSQLMatchQuery);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL SQLMatchQueryRuleEntity::ProcessLine(const Line &rLine)
{
	if(rLine[0] == "DATA_SOURCE")
	{
		return ProcessDataSource(rLine);
	}
	else if(rLine[0] == "QUERY")
	{
		return ProcessQuery(rLine);
	}
	else if(rLine[0] == "MATCH_QUERY_END")
	{
		return ProcessMatchQueryEnd(rLine);
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL SQLMatchQueryRuleEntity::IsComplete() const
{
	return m_fComplete;
}

//======================================================================
