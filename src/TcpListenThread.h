#ifndef _TCPLISTENTHREAD_H
#define _TCPLISTENTHREAD_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _TCPSOCKET_H
#include "TcpSocket.h"
#endif

#ifndef _STOPPABLETHREAD_H
#include "StoppableThread.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

//====================================================================

class TcpListenThread : public StoppableThread {
public:
	TcpListenThread();

	void SetSocket(SOCKET s);
	Event& GetConnectEvent() { return m_evConnectEvent; }

	void SetShutdownPollTime(DWORD dwPollTime);

	void StartListening();

protected:
	DWORD ThreadMemberFunc();

private:
	TcpSocket m_s;
	Event	m_evConnectEvent;
	Event	m_evListenEvent;
	DWORD m_dwShutdownPollTime;
};

//====================================================================

#endif // _TCPLISTENTHREAD_H
