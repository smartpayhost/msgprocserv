#ifndef _IPSOCKET_H
#define _IPSOCKET_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_H
#include "socket.h"
#endif

#include <winsock.h>
#include <string>

//======================================================================

class IpSocket : public Socket {
public:
	IpSocket();
	virtual ~IpSocket();

	virtual SOCKRC Open() = 0;
	virtual SOCKRC Close();

	virtual SOCKRC Connect(const std::string& strHostname, const std::string& strPort) = 0;

	virtual BOOL IsConnected() const { return m_fConnected; }

	virtual SOCKRC SetBlocking(BOOL fBlocking);
	virtual BOOL GetBlocking() const { return m_fBlocking; }

	virtual SOCKRC PendingReadable(unsigned long *pulResult);

	virtual SOCKRC WaitReadable(unsigned long timeout);
	virtual SOCKRC WaitWritable(unsigned long timeout);

	virtual void SetSocketHandle(SOCKET ns) { m_shSocket = ns; }
	virtual inline SOCKET GetSocketHandle() const { return m_shSocket; }

	virtual int Errno();

	static short ParsePort(const std::string& strPort, const std::string& strProto);
	static unsigned long ParseAddr(const std::string& strAddr);

protected:
	virtual SOCKRC DoRecv(char *buf, int bufsize, int *recvd);
	virtual SOCKRC DoSend(const char *buf, int bufsize, int *sent, unsigned long timeout);

	virtual void SetConnected(BOOL fConnected) { m_fConnected = fConnected; }

	SOCKET m_shSocket;		// socket handle

private:
	BOOL m_fConnected;		// connection status, TRUE = connected
	BOOL m_fBlocking;		// TRUE = blocking, FALSE = nonblocking
};

//======================================================================

#endif _IPSOCKET_H
