#ifndef _MATCHRULEMACVALID_H
#define _MATCHRULEMACVALID_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//==============================================================

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

#ifndef _MAC_H
#include "MAC.h"
#endif

class Message;

//==============================================================

class MatchRuleMacValid : public MatchRule {
public:
	MatchRuleMacValid();

	MatchRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	MacValidity m_eValidity;
};

//==============================================================

#endif // _MATCHRULEMACVALID_H
