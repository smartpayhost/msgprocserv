#include "StdAfx.h"
#include "LayerMsg.h"

#include "BitmapMsg.h"
#include "LayerHost.h"
#include "Message.h"
#include "MessagePacket.h"
#include "DesBlock.h"
#include "MAC.h"
#include "Format.h"

#include "ETSL6Header.h"
#include "MsgProcCfg.h"
#include "ChinaUnionPayHeader.h"

#include <assert.h>

using std::string;
using std::set;

//====================================================================

LayerMsg::LayerMsg()
:	m_pHL(NULL)
{
}

//====================================================================

BOOL LayerMsg::Process(MessagePacket &rPacket)
{
	SZFN(LayerMsg);

	assert(m_pHL);

	//	m_pRawBuf is valid
	//	m_pRawCommsHdr is valid

//	extern BitmapMsgStyle TemplateETSL;
	extern BitmapMsgStyle TemplateISO8583;

	rPacket.SetStyle(&TemplateISO8583);
//	rPacket.SetStyle(&TemplateETSL);

	// Do comms header

	if(!rPacket.m_pRequest.DecodeCommsHeader(Message::CHT_AUTOMATIC))
	{
		Log(LOG_WARNING, "%s: Failed to decode comms header", szFn);
		return FALSE;
	}

	if (GetConfig().GetEncryptionType() == Config::ETSL60)
	{
		if (*rPacket.m_pRequest.GetRawBMsg() == ENCRYPTION_INDICATOR)
		{	
			rPacket.m_pRequest.SetEtsl6Encrypted(TRUE);
			rPacket.m_pRequest.Etsl6BuildFakePacket();
		}
	}

	if(ChinaUnionPayHeader().IsChinaUnionPayHeader(rPacket.m_pRequest.GetRawBMsg()))
	{
		string strTmp;

/*		Format::Buf2HexDump(rPacket.m_pRequest.GetLenRemaining(),
			rPacket.m_pRequest.GetRawCommsHdr(), strTmp, 16);
*/
		Log(LOG_WARNING, "%s: Detected China Union Pay message: Len = %d, Data =\n%s", szFn, 
			rPacket.m_pRequest.GetLenRemaining(),
			strTmp.c_str());

		if(!rPacket.m_pRequest.GetEtsl6Encrypted())
		{
			rPacket.m_pRequest.SetChinaUnionPay(TRUE);
			rPacket.m_pRequest.ChinaUnionPayBuildFakePacket();
		}
	}

	// Decode request message and check length

	if(!rPacket.m_pRequest.DecodeBMsg())
	{
		string strTmp;

		Format::Buf2HexDump(rPacket.m_pRequest.GetLenRemaining(),
			rPacket.m_pRequest.GetRawCommsHdr(), strTmp, 16);

		Log(LOG_WARNING, "%s: Failed to decode message: Len = %d, Data =\n%s", szFn, 
			rPacket.m_pRequest.GetLenRemaining(),
			strTmp.c_str());

		if(!rPacket.m_pRequest.GetChinaUnionPay()) // don't fail here for CUP
			return FALSE;
	}

	// Dump to log

	string strTmp, strMessage;

	if (rPacket.m_pRequest.GetEtsl6Encrypted())
		rPacket.m_pRequest.m_sBMsg.Print(strTmp,BMPrintOption::BMPS_ETSL60);
	else
		rPacket.m_pRequest.m_sBMsg.Print(strTmp,BMPrintOption::BMPS_FULL);

	string strEnc = rPacket.m_pRequest.GetEtsl6Encrypted() ? "ETSL 6.0 encrypted " : "";

	string strCUP = rPacket.m_pRequest.GetChinaUnionPay() ? "CUP " : "";

	strMessage = string(szFn) + ": Received " + strEnc + strCUP + "request:\n\n" + strTmp;
	
	Log(LOG_INFO, "%s", strMessage.c_str());

	// Prepare for MAC checking

	std::set<std::string>::const_iterator i;

	for(i = m_DefaultKeys.begin(); i != m_DefaultKeys.end(); i++)
	{
		rPacket.m_pRequest.AddPossibleMacKey(*i);
	}

	// pass to host layer

	if(!m_pHL->Process(rPacket))
	{
		Log(LOG_WARNING, "%s: No response message to send", szFn);
		return FALSE;
	}

	if(rPacket.GetAction() == Action::RESPONSE)
	{
		// build response message

		if(!rPacket.m_pResponse.m_sBMsg.Build())
		{
			Log(LOG_WARNING, "%s: Couldn't build response message", szFn);
			return FALSE;
		}

		// generate response MAC

		if((rPacket.m_pResponse.m_sBMsg.GetField(128) == NULL) &&
			(rPacket.m_pResponse.m_sBMsg.GetField(64) == NULL))	// MAC
		{
			Log(LOG_DEBUG, "%s: No MAC on response", szFn);
		}
		else
		{
			if(!rPacket.CalcResponseMac())
			{
				Log(LOG_WARNING, "%s: Couldn't generate response MAC", szFn);
			}
		}

		if(!rPacket.m_pResponse.m_sBMsg.Build())		// rebuild with MAC
		{
			Log(LOG_WARNING, "%s: Couldn't build response message", szFn);
			return FALSE;
		}

		// Dump to log

		strTmp.erase();
		rPacket.m_pResponse.m_sBMsg.Print(strTmp);

		Log(LOG_INFO, "%s: Sent response:\n\n%s", szFn, strTmp.c_str());

		// add comms header

		rPacket.m_pResponse.TurnaroundCommsHeader(rPacket.m_pRequest);
	}

	// return to network layer

	return TRUE;
}	

//====================================================================
