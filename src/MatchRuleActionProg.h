#ifndef _MATCHRULEACTIONPROG_H
#define _MATCHRULEACTIONPROG_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

class Message;

//==============================================================

class MatchRuleActionProg : public MatchRule {
public:
	MatchRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	std::string m_strCmdline;
};

//==============================================================

#endif // _MATCHRULEACTIONPROG_H
