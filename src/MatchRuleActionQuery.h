#ifndef _MATCHRULEACTIONQUERY_H
#define _MATCHRULEACTIONQUERY_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//==============================================================

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

class Message;

//==============================================================

class MatchRuleActionQuery : public MatchRule {
public:
	MatchRule* Clone() const;

	BOOL Check() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	std::string		m_strQuery;
};

//==============================================================

#endif // _MATCHRULEACTIONQUERY_H
