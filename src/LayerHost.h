#ifndef _LAYERHOST_H
#define _LAYERHOST_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

class MessagePacket;
class MsgProcCfg;

//==============================================================

class LayerHost : public LoggingObject {
public:
	LayerHost();

	void SetConfig(class MsgProcCfg *pConfig) { m_pConfig = pConfig; }

	BOOL Process(MessagePacket &rPacket);

private:
	MsgProcCfg* m_pConfig;
};

//==============================================================

#endif _LAYERHOST_H
