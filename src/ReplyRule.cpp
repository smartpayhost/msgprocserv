#include "stdafx.h"
#include "ReplyRule.h"

#include "ReplyRuleField.h"
#include "ReplyRuleMsgType.h"
#include "ReplyRuleActionQuery.h"
#include "ReplyRuleActionProg.h"
#include "ReplyRuleTemplate.h"

using std::string;

//======================================================================

ReplyRule::ReplyRule(const BitmapMsgStyle *pStyle)
:	m_pStyle(pStyle),
	m_pMsgProcRules(NULL)
{
}

//======================================================================
//
//	MSG_TYPE ...
//	FIELD ...
//	TEMPLATE ...
//	QUERY ...
//

BOOL ReplyRule::Import(const string &strDesc)
{
	Line lineThis;

	if(!StrUtil::StringToLine(strDesc, lineThis))
		return FALSE;

	return Import(lineThis);
}

//---------------------------------------------------------

ReplyRule* ReplyRule::MakeNew(const BitmapMsgStyle *pStyle, const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	ReplyRule *p = NULL;

	if((*i) == "FIELD")
	{
		p = ReplyRuleField::MakeNew(pStyle, rLine);

		if(p)
			return p;
	}
	else if((*i) == "MSG_TYPE")	p = new ReplyRuleMsgType(pStyle);
	else if((*i) == "QUERY")	p = new ReplyRuleActionQuery(pStyle);
	else if((*i) == "PROG")		p = new ReplyRuleActionProg(pStyle);
	else if((*i) == "TEMPLATE")	p = new ReplyRuleTemplate(pStyle);

	if(p && p->Import(rLine))
		return p;

	delete p;
	return NULL;
}

//======================================================================

BOOL ReplyRule::Check() const
{
	return TRUE;
}

//======================================================================

BOOL ReplyRule::BuildReplyField(MessagePacket &rPacket) const
{
	return BuildReplyField(rPacket.m_pResponse.m_sBMsg, rPacket.m_pRequest.m_sBMsg);
}

//======================================================================
