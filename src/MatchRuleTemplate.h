#ifndef _MATCHRULETEMPLATE_H
#define _MATCHRULETEMPLATE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

class Message;

//==============================================================

class MatchRuleTemplate : public MatchRule {
public:
	MatchRule* Clone() const;

	BOOL Check() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	std::string		m_strTemplateName;
};

//==============================================================

#endif // _MATCHRULETEMPLATE_H
