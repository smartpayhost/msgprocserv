#ifndef _SQLLOGGINGOBJECT_H
#define _SQLLOGGINGOBJECT_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//======================================================================

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

#include <sql.h>
#include <sqlext.h>

//======================================================================

class SQLLoggingObject : public LoggingObject {
public:
	virtual ~SQLLoggingObject() {}

protected:
	static const std::string DEFAULT_PREFIX;

	virtual void LogSQLInfo(enum LogLevel level,
							HDBC hdbc=SQL_NULL_HDBC,
							HSTMT hstmt=SQL_NULL_HSTMT,
							const std::string& strPrefix = DEFAULT_PREFIX) const;

	virtual void LogSQLError(enum LogLevel level, 
							HDBC hdbc=SQL_NULL_HDBC,
							HSTMT hstmt=SQL_NULL_HSTMT,
							const std::string& strPrefix = DEFAULT_PREFIX) const;
};

//======================================================================

#endif // _SQLLOGGINGOBJECT_H
