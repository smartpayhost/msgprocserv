#ifndef _REPLYTEMPLATERULEENTITY_H
#define _REPLYTEMPLATERULEENTITY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _REPLYTEMPLATE_H
#include "ReplyTemplate.h"
#endif

class ReplyTemplateCollection;

//======================================================================

class ReplyTemplateRuleEntity : public RuleEntity {
public:
	ReplyTemplateRuleEntity();

	BOOL ProcessLine(const Line &rLine);
	BOOL IsComplete() const;

	void SetName(const char *szName);
	void SetReplyTemplateCollection(ReplyTemplateCollection *pCollection);

private:
	BOOL ProcessRule(const Line &rLine);
	BOOL ProcessReplyEnd(const Line &rLine);

private:
	BOOL m_fComplete;
	ReplyTemplateCollection *m_pCollection;
	ReplyTemplate m_rtTemplate;
};

//======================================================================

#endif // _REPLYTEMPLATERULEENTITY_H
