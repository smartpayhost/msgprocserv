#include "stdafx.h"
#include "HostDescriptionCollection.h"

#include "HostDescriptionRuleEntity.h"
#include "HostDescription.h"

#include <assert.h>
#include <algorithm>

using std::string;
using std::for_each;

//======================================================================

HostDescriptionCollection::HostDescriptionCollection()
:	m_pMsgProcRules(NULL),
	m_pLogger(NULL)
{
}

//----------------------------------------------------------------------

HostDescriptionCollection::~HostDescriptionCollection()
{
//	for_each(m_mapHosts.begin(), m_mapHosts.end(), DeleteSecond());
}

//======================================================================

void HostDescriptionCollection::SetLogger(Logger *pLogger)
{
	m_pLogger = pLogger;

	HostDescriptionMap::iterator it;

	for(it = m_mapHosts.begin(); it != m_mapHosts.end(); ++it)
		(*it).second->SetLogger(m_pLogger);
}

//======================================================================

void HostDescriptionCollection::SetMsgProcRuleCollection(MsgProcRuleCollection *pArray)
{
	m_pMsgProcRules = pArray;

	HostDescriptionMap::iterator it;

	for(it = m_mapHosts.begin(); it != m_mapHosts.end(); ++it)
		(*it).second->SetMsgProcRuleCollection(m_pMsgProcRules);
}

//======================================================================

void HostDescriptionCollection::Register(RuleFile &rRuleFile)
{
	rRuleFile.RegisterEntityType(this);
}

//======================================================================

void HostDescriptionCollection::AddHostDescription(const HostDescription &rHost)
{
	boost::shared_ptr<HostDescription> p(new HostDescription(rHost));

	p->SetMsgProcRuleCollection(m_pMsgProcRules);
	p->SetLoggingName(rHost.GetName());
	p->SetLogger(m_pLogger);

	m_mapHosts[rHost.GetName()] = p;
}

//======================================================================

BOOL HostDescriptionCollection::Export(string &rStrDest) const
{
	HostDescriptionMap::const_iterator it;

	for(it = m_mapHosts.begin(); it != m_mapHosts.end(); ++it)
	{
		string strTmp;

		BOOL fRetval = (*it).second->Export(strTmp);

		if(!fRetval)
			return FALSE;

		rStrDest += strTmp;
		rStrDest += '\n';
	}

	rStrDest += '\n';

	return TRUE;
}

//======================================================================

void HostDescriptionCollection::MergeFrom(const HostDescriptionCollection& rhs)
{
	for(HostDescriptionMap::const_iterator it = rhs.m_mapHosts.begin(); it != rhs.m_mapHosts.end(); ++it)
	{
		AddHostDescription(*((*it).second));
	}
}

//======================================================================

BOOL HostDescriptionCollection::IsStart(const Line &rLine) const
{
	if(rLine.size() != 2)
		return FALSE;

	if(rLine[0] == "UPSTREAM_HOST")
		return TRUE;

	return FALSE;
}

//======================================================================

RuleEntity *HostDescriptionCollection::MakeNew(const Line &rLine)
{
	HostDescriptionRuleEntity *pResult;

	pResult = new HostDescriptionRuleEntity;

	if(!pResult)
		return NULL;

	pResult->SetName(rLine[1]);

	pResult->SetHostDescriptionCollection(this);

	return pResult;
}

//======================================================================
	
const HostDescription* HostDescriptionCollection::FindByName(const string& strName) const
{
	HostDescriptionMap::const_iterator i;

	i = m_mapHosts.find(strName);

	if(i == m_mapHosts.end())
		return NULL;
	else
		return (*i).second.get();
}

//======================================================================
