#ifndef _SQLREPLYQUERYCOLLECTION_H
#define _SQLREPLYQUERYCOLLECTION_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _SQLREPLYQUERY_H
#include "SQLReplyQuery.h"
#endif

#include <map>

class MsgProcRuleCollection;
class Logger;

//==============================================================

class SQLReplyQueryCollection : public RuleEntityDescriptor, public boost::noncopyable {
public:
	SQLReplyQueryCollection();
	~SQLReplyQueryCollection();

	BOOL IsStart(const Line &rLine) const;
	RuleEntity *MakeNew(const Line &rLine);

	virtual BOOL Check() const;

	void AddSQLReplyQuery(SQLReplyQuery &rSQLReplyQuery);
	void Register(RuleFile &rRuleFile);
	BOOL Export(std::string &rStrDest);
	void MergeFrom(const SQLReplyQueryCollection& rhs);

	void SetLogger(Logger *pLogger);

	void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray);

	const SQLReplyQuery* FindByName(const std::string& strName) const;

private:
	SQLMessageQuery::SQLMessageQueryMap m_mapSQLReplyQuerys;

	MsgProcRuleCollection *m_pMsgProcRules;
	Logger *m_pLogger;
};

//==============================================================

#endif // _SQLREPLYQUERYCOLLECTION_H
