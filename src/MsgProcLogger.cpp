#include "stdafx.h"
#include "MsgProcLogger.h"

#include <stdarg.h>

#ifdef UNIT_TESTS
#include <boost/test/unit_test.hpp>
#endif

using std::string;

//======================================================================

MsgProcLogger::MsgProcLogger(const string& strLogName, int iLogLevel, const string& strLogPath)
:	Logger(strLogName, iLogLevel),
	m_fLogToStderr(FALSE),
	m_loggerFile(strLogName, iLogLevel, strLogPath),
	m_loggerEvent(strLogName, iLogLevel),
	m_loggerStderr(strLogName, iLogLevel),
	m_loggerDB(strLogName, iLogLevel)
{
}

//======================================================================

int MsgProcLogger::vLog(enum LogLevel level, const char *fmt, va_list ap)
{
	Lock localLock(&m_Lockable);

	if(level <= LOG_NOTICE)
		m_loggerEvent.vLog(level, fmt, ap);

	if(m_fLogToStderr)
		m_loggerStderr.vLog(level, fmt, ap);

	int iRetval;

	iRetval = m_loggerFile.vLog(level, fmt, ap);

	m_loggerDB.vLog(level, fmt, ap);

	return iRetval;
}

//======================================================================

int MsgProcLogger::vLogBitmapMsg(enum LogLevel level, const BitmapMsg& msg, const char *fmt, va_list ap)
{
	return m_loggerDB.vLogBitmapMsg(level, msg, fmt, ap);
}

//======================================================================

void MsgProcLogger::SetLogName(const string& strLogName)
{
	Logger::SetLogName(strLogName);
	m_loggerFile.SetLogName(strLogName);
	m_loggerEvent.SetLogName(strLogName);
	m_loggerStderr.SetLogName(strLogName);
	m_loggerDB.SetLogName(strLogName);
}

//======================================================================

void MsgProcLogger::SetLogLevel(int iLogLevel)
{
	Logger::SetLogLevel(iLogLevel);
	m_loggerFile.SetLogLevel(iLogLevel);
	m_loggerEvent.SetLogLevel(iLogLevel);
	m_loggerStderr.SetLogLevel(iLogLevel);
	m_loggerDB.SetLogLevel(iLogLevel);
}

//======================================================================

void MsgProcLogger::SetLogPath(const string& strLogPath)
{
	m_loggerFile.SetLogPath(strLogPath);
}

//======================================================================

void MsgProcLogger::SetLogDBDSN(const string& strLogDBDSN)
{
	m_loggerDB.SetDSN(strLogDBDSN.c_str());
}

void MsgProcLogger::SetLogDBUID(const string& strLogDBUID)
{
	m_loggerDB.SetUID(strLogDBUID.c_str());
}

void MsgProcLogger::SetLogDBAuthStr(const string& strLogDBAuthStr)
{
	m_loggerDB.SetAuthStr(strLogDBAuthStr.c_str());
}

void MsgProcLogger::SetLogDBTraceFile(const string& strLogDBTraceFile)
{
	m_loggerDB.SetTraceFile(strLogDBTraceFile.c_str());
}

void MsgProcLogger::SetLogDBTimeout(int iLogDBTimeout)
{
	m_loggerDB.SetTimeout(iLogDBTimeout);
}

void MsgProcLogger::SetLogDBLogTypes(int iLogTypes)
{
	m_loggerDB.SetLogTypes(iLogTypes);
}

void MsgProcLogger::SetLogDBLogger(Logger* pLogger)
{
	m_loggerDB.SetLogger(pLogger);
}

//======================================================================

void MsgProcLogger::Start()
{
	m_loggerDB.Start();
}

void MsgProcLogger::Stop()
{
	m_loggerDB.Stop();
}

//======================================================================

#ifdef UNIT_TESTS
BOOST_AUTO_TEST_CASE(MsgProcLogger__Log)
{
	char name1[L_tmpnam_s];
	tmpnam_s(name1, L_tmpnam_s);
	MsgProcLogger theLogger("test", LOG_DEBUG, name1);

	char buf[2500];
	int i;

	for(i = 0; i < sizeof(buf); i++)
	{
		buf[i] = (char)i;
//		buf[i] = (char)'x';
		if(!buf[i])
			buf[i] = ' ';
	}
	
	buf[sizeof(buf) - 1] = 0;

	((Logger*)&theLogger)->Log(LOG_DEBUG, "%d %s", 1, buf);
	((Logger*)&theLogger)->Log(LOG_NOTICE, "%d %s", 1, buf);	// extra param needed to select correct overload
	BOOST_CHECK(true);

	//theLogger.LogHexDump(LOG_DEBUG, "", sizeof(buf), buf);
}
#endif
