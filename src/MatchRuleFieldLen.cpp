#include "stdafx.h"
#include "MatchRuleFieldLen.h"

#include "Message.h"
#include "StrUtil.h"

using std::string;

//======================================================================

MatchRuleFieldLen::MatchRuleFieldLen()
:	m_iFieldNum(0),
	m_iMatchData(0),
	m_bUseRange(FALSE),
	m_iRangeMin(0),
	m_iRangeMax(999)
{
}

//---------------------------------------------------------

MatchRule* MatchRuleFieldLen::Clone() const
{
	return new MatchRuleFieldLen(*this);
}

//======================================================================
//
//	FIELD <number> [ NOT ] LENGTH <MIN <n> MAX <n>> | <n>
//

BOOL MatchRuleFieldLen::Import(const Line &rLine)
{
	m_bInverted = FALSE;

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	m_iFieldNum = atoi((*i).c_str());

	if((m_iFieldNum < 1)||(m_iFieldNum > 128))
		return FALSE;

	if(++i == rLine.end()) return FALSE;

	if((*i) == "NOT")
	{
		m_bInverted = TRUE;

		if(++i == rLine.end()) return FALSE;
	}

	if((*i) != "LENGTH")
		return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iMin = -1;
	int iMax = -1;
	int iLen = -1;

	if((*i) == "MIN")
	{
		if(++i == rLine.end()) return FALSE;

		iMin = atoi((*i).c_str());

		++i;
	}

	if(i != rLine.end() && (*i) == "MAX")
	{
		if(++i == rLine.end()) return FALSE;

		iMax = atoi((*i).c_str());

		++i;
	}

	if((iMin == -1) && (iMax == -1))
	{
		if(i == rLine.end()) return FALSE;

		iLen = atoi((*i).c_str());
	}

	if((iMin == -1) && (iMax == -1) && (iLen == -1))
		return FALSE;

	if((iMin != -1) || (iMax != -1))
	{
		m_bUseRange = TRUE;
		m_iRangeMin = 0;
		m_iRangeMax = 999;

		if(iMin != -1)
			m_iRangeMin = iMin;

		if(iMax != -1)
			m_iRangeMax = iMax;
	}
	else
	{
		m_bUseRange = FALSE;
		m_iMatchData = iLen;
	}

	return TRUE;
}

//======================================================================
//
//	FIELD <number> [ NOT ] LENGTH <MIN <n> MAX <n>> | <n>
//

BOOL MatchRuleFieldLen::Export(string &strDesc)
{
	string strTmp;

	StrUtil::Format(strDesc, "FIELD %d", m_iFieldNum);

	if(m_bInverted)
		strDesc += " NOT";

	strDesc += " LENGTH";

	if(m_bUseRange)
	{
		StrUtil::Format(strTmp, " MIN %d MAX %d", m_iRangeMin, m_iRangeMax);
	}
	else
	{
		StrUtil::Format(strTmp, " %d", m_iMatchData);
	}

	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL MatchRuleFieldLen::DoesMatch(int iTestData) const
{
	BOOL bResult = FALSE;

	if(m_bUseRange)
	{
		if((iTestData >= m_iRangeMin)&&(iTestData <= m_iRangeMax))
			bResult = TRUE;
	}
	else
	{
		if(iTestData == m_iMatchData)
			bResult = TRUE;
	}

	if(m_bInverted)
		return !bResult;
	else
		return bResult;
}

//======================================================================

BOOL MatchRuleFieldLen::DoesMatch(Message &rMsg) const
{
	const BitmapMsgField *pField;

	pField = rMsg.m_sBMsg.GetField(m_iFieldNum);

	if(pField == NULL)
		return FALSE;

	return DoesMatch(pField->GetLen());
}

