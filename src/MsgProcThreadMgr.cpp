#include "stdafx.h"
#include "MsgProcThreadMgr.h"
#include "MsgProcCfg.h"

#include <assert.h>

#include <sstream>

using std::string;
using std::ostringstream;

//====================================================================

MsgProcThreadMgr::MsgProcThreadMgr()
:	m_pConfig(NULL),
	m_pPipeDst(NULL),
	m_evUpstreamShutdown(TRUE, FALSE),
	m_evDeadUpstream(FALSE, FALSE),
	m_wb(m_whf)
{
	m_whf.SetMgr(this);
}

//====================================================================

DataPipe& MsgProcThreadMgr::GetPipeSrc()
{
	return m_pipeSrc;
}

//--------------------------------------------------------------------

void MsgProcThreadMgr::SetPipeDst(DataPipe *pPipe)
{
	m_pPipeDst = pPipe;
}

//--------------------------------------------------------------------

void MsgProcThreadMgr::SetLogger(Logger* pLogger)
{
	StoppableThread::SetLogger(pLogger);
	m_whf.SetLogger(pLogger);
}

//--------------------------------------------------------------------

void MsgProcThreadMgr::SetLoggingName(const string& strName)
{
	StoppableThread::SetLoggingName(strName);
	m_whf.SetLoggingName(strName);
}

//====================================================================

DWORD MsgProcThreadMgr::ThreadMemberFunc()
{
	SZFN(MsgProcThreadMgr);

	assert(m_pConfig);

	Log(LOG_DEBUG, "%s: starting", szFn);

	WaitableCollection wc;

	wc.push_back(GetShutdownEvent());
	wc.push_back(m_evDeadUpstream);
	wc.push_back(m_pipeSrc);

	m_wb.PreCreateHandlers(m_pConfig->GetMinThreads());
	m_wb.SetMaxHandlers(m_pConfig->GetMaxThreads());
	m_wb.SetMaxWorkLoad(0);
	m_wb.SetMinWorkLoad(0);

	DWORD dwRetVal = 0;
	BOOL fExit = FALSE;

	while(!fExit)
	{
		// wait for packet on pipe or shutdown

		DWORD e = wc.Wait();

		switch(e)
		{
		case WAIT_OBJECT_0 + 0:
			Log(LOG_DEBUG, "%s: shutdown requested", szFn);
			fExit = TRUE;
			break;

		case WAIT_OBJECT_0 + 1:
			Log(LOG_DEBUG, "%s: upstream thread died", szFn);
			m_whf.CleanupDeadHandlers();
			break;

		case WAIT_OBJECT_0 + 2:
			Log(LOG_DEBUG, "%s: data from downstream received", szFn);

			if(!ProcessDataFromDownstream())
			{
				Log(LOG_WARNING, "%s: ProcessDataFromDownstream() failed", szFn);
			}
			break;

		default:
			Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result %lx", szFn, e);
			fExit = TRUE;
			break;
		}
	}

	m_whf.ShutdownHandlers();

	Log(LOG_DEBUG, "%s: ending", szFn);

	return dwRetVal;
}

//====================================================================

BOOL MsgProcThreadMgr::ProcessDataFromDownstream()
{
	MPTMWorkHandler *wh = (MPTMWorkHandler *)(m_wb.GetHandler());

	if(!wh)
		return FALSE;

	// pass data from downstream to selected upstream pipe

	BYTE *pbBuf;

	int iLen = m_pipeSrc.GetChunkNC(&pbBuf);
	assert(iLen);

	wh->GetPipe().PutNC(iLen, pbBuf);

	return TRUE;
}

//====================================================================
//====================================================================

MsgProcThreadMgr::MPTMWorkHandler::MPTMWorkHandler(MsgProcThread *pThread)
:	m_pThread(pThread)
{
}

//--------------------------------------------------------------------

MsgProcThreadMgr::MPTMWorkHandler::~MPTMWorkHandler()
{
	if(m_pThread)
	{
		m_pThread->WaitForExit();
		delete m_pThread;
		m_pThread = NULL;
	}
}

//--------------------------------------------------------------------

int MsgProcThreadMgr::MPTMWorkHandler::GetCurrentWorkLoad()
{
	assert(m_pThread);
	return m_pThread->GetCurrentWorkLoad();
}

//--------------------------------------------------------------------

DataPipe& MsgProcThreadMgr::MPTMWorkHandler::GetPipe()
{
	assert(m_pThread);
	return m_pThread->GetPipeSrc();
}

//====================================================================
//====================================================================

MsgProcThreadMgr::MPTMWorkHandlerFactory::MPTMWorkHandlerFactory()
:	m_mgr(NULL),
	m_iNumHandlers(0),
	m_iHandlerCounter(0)
{
}

//--------------------------------------------------------------------

MsgProcThread* MsgProcThreadMgr::CreateNewUpstreamThread()
{
	SZFN(MsgProcThreadMgr);

	assert(m_pConfig);

	Log(LOG_INFO, "%s: creating new upstream thread", szFn);

	MsgProcThread *pNewThread = new MsgProcThread;

	assert(pNewThread);

	if(!pNewThread)
	{
		Log(LOG_ERROR, "%s: ending, error: failed to create upstream thread", szFn);
		return NULL;
	}

	pNewThread->SetConfig(m_pConfig);
	pNewThread->SetPipeDst(m_pPipeDst);

	pNewThread->SetShutdownEvent(&m_evUpstreamShutdown);
	pNewThread->SetPriority(GetPriority());
	pNewThread->SetActivityEvent(this);
	pNewThread->SetLogger(GetLogger());

	pNewThread->SetOnExitEvent(&m_evDeadUpstream);

	return pNewThread;
}

//====================================================================

WorkHandler* MsgProcThreadMgr::MPTMWorkHandlerFactory::CreateNewWorkHandler()
{
	SZFN(MsgProcThreadMgr);

	assert(m_mgr);

	MsgProcThread *pThread = m_mgr->CreateNewUpstreamThread();

	assert(pThread);

	if(!pThread)
		return NULL;

	MPTMWorkHandler *wh = new MPTMWorkHandler(pThread);

	assert(wh);

	if(!wh)
	{
		delete pThread;
		return NULL;
	}

	m_listHandlers.push_back(wh);
	m_iNumHandlers++;
	m_iHandlerCounter++;

	ostringstream os;

	os << m_mgr->GetLoggingName() << "/msgproc[" << m_iHandlerCounter << "]";
	pThread->SetLoggingName(os.str());

	if(!pThread->StartThread())
	{
		m_mgr->Log(LOG_ERROR, "%s: error: failed to start upstream thread", szFn);
		delete wh;
		return NULL;
	}

	Log(LOG_NOTICE, "%s: new upstream thread [%d] created", szFn, m_iHandlerCounter);
	Log(LOG_INFO, "%s: %d threads active", szFn, m_iNumHandlers);

	return wh;
}

//====================================================================

void MsgProcThreadMgr::MPTMWorkHandlerFactory::ShutdownHandlers()
{
	SZFN(MsgProcThreadMgr);

	Log(LOG_INFO, "%s: shutting down upstream threads", szFn);

	m_mgr->m_evUpstreamShutdown.Set();

	std::list<MPTMWorkHandler *>::iterator i;

	for(i = m_listHandlers.begin(); i != m_listHandlers.end(); i++)
	{
		(*i)->GetThread()->WaitForExit();
	}

	m_mgr->m_evUpstreamShutdown.Reset();

	CleanupDeadHandlers();
}

//====================================================================

void MsgProcThreadMgr::MPTMWorkHandlerFactory::CleanupDeadHandlers()
{
	SZFN(MsgProcThreadMgr);

	MsgProcThread *pThread;
	MPTMWorkHandler *pHandler;
	std::list<MPTMWorkHandler *>::iterator i;

	Log(LOG_NOTICE, "%s: cleaning up dead upstream threads", szFn);

	int iKilledThreads = 0;

	i = m_listHandlers.begin();

	while(i != m_listHandlers.end())
	{
		pHandler = *i;
		pThread = pHandler->GetThread();

		if(pThread->IsRunning())
		{
			i++;
		}
		else
		{
			m_mgr->m_wb.RemoveWorkHandler(pHandler);

			m_listHandlers.erase(i++);

			m_iNumHandlers--;
			iKilledThreads++;
		}
	}

	Log(LOG_NOTICE, "%s: %d dead upstream threads removed", szFn, iKilledThreads);
	Log(LOG_INFO, "%s: %d threads active", szFn, m_iNumHandlers);
}

//--------------------------------------------------------------------

void MsgProcThreadMgr::MPTMWorkHandlerFactory::DestroyWorkHandler(WorkHandler* wh)
{
	delete (MPTMWorkHandler *)wh;
}

//====================================================================
