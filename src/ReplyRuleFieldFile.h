#ifndef _REPLYRULEFIELDFILE_H
#define _REPLYRULEFIELDFILE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULEFIELD_H
#include "ReplyRuleField.h"
#endif

//==============================================================

class ReplyRuleFieldFile : public ReplyRuleField {
public:
	ReplyRuleFieldFile(int iFieldNum, const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const;

	static BOOL BuildReplyFieldExtFile(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg, int iFieldNum, const std::string& strFileName);

private:
	std::string	m_strFilename;
};

//==============================================================

#endif // _REPLYRULEFIELDFILE_H
