#include "stdafx.h"
#include "ReplyRuleTemplate.h"

#include "ReplyTemplate.h"
#include "ReplyTemplateCollection.h"
#include "MsgProcRuleCollection.h"

#include <assert.h>

using std::string;

//======================================================================

ReplyRuleTemplate::ReplyRuleTemplate()
:	ReplyRule(NULL)
{
}

//---------------------------------------------------------

ReplyRuleTemplate::ReplyRuleTemplate(const BitmapMsgStyle *pStyle)
:	ReplyRule(pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleTemplate::Clone() const
{
	return new ReplyRuleTemplate(*this);
}

//======================================================================
//
//	TEMPLATE <template_name>
//

BOOL ReplyRuleTemplate::Import(const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "TEMPLATE") return FALSE;

	if(++i == rLine.end()) return FALSE;

	m_strTemplateName = *i;

	return TRUE;
}

//======================================================================
//
//	TEMPLATE <template_name>
//

BOOL ReplyRuleTemplate::Export(string &strDesc)
{
	string strTmp;

	strDesc = "TEMPLATE " + m_strTemplateName;

	return TRUE;
}

//======================================================================

BOOL ReplyRuleTemplate::BuildReplyField(MessagePacket &rPacket) const
{
	assert(GetMsgProcRuleCollection());

	const ReplyTemplate *pReplyTemplate = GetMsgProcRuleCollection()->GetReplyTemplateCollection().FindByName(m_strTemplateName);

	if(pReplyTemplate == NULL)
	{
		Log(LOG_WARNING, "No reply handling defined for this message type");
		return FALSE;
	}

	// build reply message

	if(!pReplyTemplate->BuildReply(rPacket))
	{
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL ReplyRuleTemplate::Check() const
{
	assert(GetMsgProcRuleCollection());

	if(GetMsgProcRuleCollection()->GetReplyTemplateCollection().FindByName(m_strTemplateName))
		return TRUE;

	Log(LOG_WARNING, "Reply template %s not defined", m_strTemplateName.c_str());

	return FALSE;
}

//======================================================================
