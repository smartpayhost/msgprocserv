#include "stdafx.h"
#include "ReplyRuleFieldAscFile.h"

#include <assert.h>

#include <sstream>
#include <fstream>

using std::string;
using std::ifstream;
using std::stringstream;

//======================================================================

ReplyRuleFieldAscFile::ReplyRuleFieldAscFile(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldAscFile::Clone() const
{
	return new ReplyRuleFieldAscFile(*this);
}

//======================================================================
//
//	FIELD <field_num> ASCFILE <filename>

BOOL ReplyRuleFieldAscFile::Import(const Line &rLine)
{
	m_strFilename.erase();

	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return FALSE;

	if((*i) == "ASCFILE")
	{
		if(++i == rLine.end()) return FALSE;

		m_strFilename = (*i);
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldAscFile::Export(string &strDesc)
{
	string strTmp;

	StrUtil::Format(strDesc, "FIELD %d ASCFILE ", GetFieldNum());
	StrUtil::PurifyString(m_strFilename, strTmp);
	strDesc += strTmp;

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldAscFile::BuildReplyFieldExtFile(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg, int iFieldNum, const string& strFilename)
{
	ifstream ifs;

	ifs.open(strFilename.c_str());

	if(!ifs)
		return FALSE;

	stringstream strData;

	strData << ifs.rdbuf();

	if(strData.str().size())
	{
		BitmapMsgField *pField = new BitmapMsgField(iFieldNum, rOrigMsg.GetMsgStyle());

		assert(pField);

		if(!pField)
			return FALSE;

		if(!pField->SetFieldFromString(strData.str()))
		{
			delete pField;
			return FALSE;
		}
	
		rNewMsg.SetField(pField);
	
		return TRUE;
	}

	return FALSE;
}

//======================================================================

BOOL ReplyRuleFieldAscFile::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	if(!BuildReplyFieldExtFile(rNewMsg, rOrigMsg, GetFieldNum(), m_strFilename))
	{
		char szErr[100];

		strerror_s(szErr, sizeof(szErr), errno);

		Log(LOG_ERROR, "Failed to open file %s: %s", m_strFilename.c_str(), szErr);
		return FALSE;
	}

	return TRUE;
}

