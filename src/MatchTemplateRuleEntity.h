#ifndef _MATCHTEMPLATERULEENTITY_H
#define _MATCHTEMPLATERULEENTITY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _MATCHTEMPLATE_H
#include "MatchTemplate.h"
#endif

class MatchTemplateCollection;
class Line;

//======================================================================

class MatchTemplateRuleEntity : public RuleEntity {
public:
	MatchTemplateRuleEntity();

	BOOL ProcessLine(const Line &rLine);
	BOOL IsComplete() const;

	void SetName(const char *szName);
	void SetMatchTemplateCollection(MatchTemplateCollection *pCollection);

private:
	BOOL ProcessReplyWith(const Line &rLine);
	BOOL ProcessSendTo(const Line &rLine);
	BOOL ProcessMatchEnd(const Line &rLine);
	BOOL ProcessRule(const Line &rLine);

private:
	BOOL m_fComplete;
	MatchTemplateCollection* m_pCollection;
	MatchTemplate m_mtTemplate;
};

//======================================================================

#endif // _MATCHTEMPLATERULEENTITY_H
