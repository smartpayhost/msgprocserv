#ifndef _MSGPROCRULECOLLECTION_H
#define _MSGPROCRULECOLLECTION_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHTEMPLATECOLLECTION_H
#include "MatchTemplateCollection.h"
#endif

#ifndef _REPLYTEMPLATECOLLECTION_H
#include "ReplyTemplateCollection.h"
#endif

#ifndef _DATASOURCECOLLECTION_H
#include "DataSourceCollection.h"
#endif

#ifndef _SQLMATCHQUERYCOLLECTION_H
#include "SQLMatchQueryCollection.h"
#endif

#ifndef _SQLACTIONQUERYCOLLECTION_H
#include "SQLActionQueryCollection.h"
#endif

#ifndef _SQLREPLYQUERYCOLLECTION_H
#include "SQLReplyQueryCollection.h"
#endif

#ifndef _HOSTDESCRIPTIONCOLLECTION_H
#include "HostDescriptionCollection.h"
#endif

#include <boost/utility.hpp>

class Logger;

//==============================================================

class MsgProcRuleCollection : public boost::noncopyable {
public:
	MsgProcRuleCollection();

	virtual void MergeFrom(const MsgProcRuleCollection* rfFrom);

	void Export(std::string &rStrDest);

	const MatchTemplateCollection&		GetMatchTemplateCollection() const		{ return m_mtcMatchTemplates; }
	const ReplyTemplateCollection&		GetReplyTemplateCollection() const		{ return m_rtcReplyTemplates; }
	const HostDescriptionCollection&	GetHostDescriptionCollection() const	{ return m_hdcHostDescriptions; }
	const DataSourceCollection&			GetDataSourceCollection() const			{ return m_dscDataSources; }
	const SQLMatchQueryCollection&		GetSQLMatchQueryCollection() const		{ return m_sqlMatchQueries; }
	const SQLActionQueryCollection&		GetSQLActionQueryCollection() const		{ return m_sqlActionQueries; }
	const SQLReplyQueryCollection&		GetSQLReplyQueryCollection() const		{ return m_sqlReplyQueries; }

	MatchTemplateCollection&	GetMatchTemplateCollection()	{ return m_mtcMatchTemplates; }
	ReplyTemplateCollection&	GetReplyTemplateCollection()	{ return m_rtcReplyTemplates; }
	HostDescriptionCollection&	GetHostDescriptionCollection()	{ return m_hdcHostDescriptions; }
	DataSourceCollection&		GetDataSourceCollection()		{ return m_dscDataSources; }
	SQLMatchQueryCollection&	GetSQLMatchQueryCollection()	{ return m_sqlMatchQueries; }
	SQLActionQueryCollection&	GetSQLActionQueryCollection()	{ return m_sqlActionQueries; }
	SQLReplyQueryCollection&	GetSQLReplyQueryCollection()	{ return m_sqlReplyQueries; }

	virtual void SetLogger(Logger *pLogger);

	virtual BOOL Check();

private:
	MatchTemplateCollection m_mtcMatchTemplates;
	ReplyTemplateCollection m_rtcReplyTemplates;
	HostDescriptionCollection m_hdcHostDescriptions;
	DataSourceCollection m_dscDataSources;
	SQLMatchQueryCollection m_sqlMatchQueries;
	SQLActionQueryCollection m_sqlActionQueries;
	SQLReplyQueryCollection m_sqlReplyQueries;
};

//==============================================================

#endif // _MSGPROCRULECOLLECTION_H
