#include "stdafx.h"
#include "LoggerStdio.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <process.h>
#include <assert.h>

using std::string;

//======================================================================

LoggerStdio::LoggerStdio(const string& m_strLogName, int m_iLogLevel, FILE *fp)
:	Logger(m_strLogName, m_iLogLevel),
	m_fp(fp)
{
}

//----------------------------------------------------------------------

LoggerStdio::LoggerStdio(const LoggerStdio& rhs)
:	Logger(rhs)
{
	(*this) = rhs;
}

//----------------------------------------------------------------------

LoggerStdio& LoggerStdio::operator=(const LoggerStdio& rhs)
{
	if(this == &rhs) return *this;

	Logger::operator=(rhs);

	m_fp = NULL;

	return *this;
}

//----------------------------------------------------------------------------

int LoggerStdio::vLog(enum LogLevel level, const char *fmt, va_list ap)
{
	assert(m_fp);

	if(m_fp == NULL)
		return -1;

	Lock localLock(&m_Lockable);

	char timestr[30];
	time_t t;
	struct tm now;

	if(GetLogLevel() < level)
		return 0;

	time(&t);
	localtime_s(&now, &t);

	strftime(timestr, sizeof(timestr), "%a %b %d %H:%M:%S ", &now);

	fprintf(m_fp, "%s %s[%d/%ld] %d: ", timestr, GetLogName(), _getpid(), GetCurrentThreadId(), level);
	vfprintf(m_fp, fmt, ap);
	fprintf(m_fp, "\n");
	fflush(m_fp);

	return 0;
}

//======================================================================
