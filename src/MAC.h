#ifndef _MAC_H
#define _MAC_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class DesBlock;

//==============================================================

enum MacValidity {
	MAC_BAD, 
	MAC_GOOD,
	MAC_NONE
};

//==============================================================

class MAC {
public:
	static void NETS(DesBlock *pKey, int iDataLen, const BYTE *pData, BYTE *pResult);
	static void ANSI(DesBlock *pKey, int iDataLen, const BYTE *pData, BYTE *pResult);
};

//==============================================================

#endif // _MAC_H
