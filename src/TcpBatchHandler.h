#ifndef _TCPBATCHHANDLER_H
#define _TCPBATCHHANDLER_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _HANDLERTHREAD_H
#include "HandlerThread.h"
#endif

#ifndef _MSGPROCTHREADMGR_H
#include "MsgProcThreadMgr.h"
#endif

#ifndef _TCPMUXTHREAD_H
#include "TcpMuxThread.h"
#endif

#ifndef _DATAPIPE_H
#include "DataPipe.h"
#endif

class MsgProcCfg;

//====================================================================

class TcpBatchHandler : public HandlerThread {
public:
	TcpBatchHandler(MsgProcCfg *pConfig);
	
protected:
    DWORD ThreadMemberFunc();

private:
	MsgProcCfg* m_pConfig;

	TcpMuxThread m_tMux;
	MsgProcThreadMgr m_tUpstream;
};

//====================================================================

#endif // _TCPBATCHHANDLER_H
