#include "stdafx.h"
#include "Win32Service.h"

LPHANDLER_FUNCTION Win32ComponentService::g_lpHandler;

//=========================================================================

BOOL Win32Service::Start(DWORD /*dwServices*/, SERVICE_TABLE_ENTRY Services[], 
						 int /*argc*/, char * /*argv*/[])
{
	return ::StartServiceCtrlDispatcher(Services);
}

//=========================================================================

Win32ComponentService::Win32ComponentService(DWORD dwServiceType)
{
	g_lpHandler = NULL;
	m_hServiceStatus = NULL;
	::ZeroMemory(&m_ServiceStatus, sizeof(SERVICE_STATUS));

	m_ServiceStatus.dwServiceType = dwServiceType;
}

//=========================================================================

BOOL Win32ComponentService::RegisterHandler(const char* szServiceName, 
											LPHANDLER_FUNCTION lpHandler)
{
	m_hServiceStatus = ::RegisterServiceCtrlHandler(szServiceName, lpHandler);

	return m_hServiceStatus != NULL;
}

//=========================================================================

BOOL Win32ComponentService::ReportStatusChange(DWORD dwCurrentState,
	DWORD dwControlsAccepted,
	DWORD dwWaitHint,
	DWORD dwWin32ExitCode,
	DWORD dwProcessSpecificExitCode)
{
	m_ServiceStatus.dwCurrentState = dwCurrentState;
	m_ServiceStatus.dwControlsAccepted = dwControlsAccepted;
	m_ServiceStatus.dwWin32ExitCode = dwWin32ExitCode;
	m_ServiceStatus.dwServiceSpecificExitCode = dwProcessSpecificExitCode;

	switch(dwCurrentState)
	{
	case SERVICE_START_PENDING:
	case SERVICE_STOP_PENDING:
	case SERVICE_PAUSE_PENDING:
	case SERVICE_CONTINUE_PENDING:
		m_ServiceStatus.dwCheckPoint = 1;
		m_ServiceStatus.dwWaitHint = dwWaitHint;
		break;

	default:
		m_ServiceStatus.dwCheckPoint = 0;
		m_ServiceStatus.dwWaitHint = 0;
		break;
	}

	return SetStatus();
}

//=========================================================================

BOOL Win32ComponentService::SetPendingStatus(DWORD dwCurrentState,
	DWORD dwWaitHint,
	DWORD dwCheckPoint)
{
	m_ServiceStatus.dwCurrentState = dwCurrentState;
	m_ServiceStatus.dwControlsAccepted = 0;
	m_ServiceStatus.dwWin32ExitCode = 0;
	m_ServiceStatus.dwServiceSpecificExitCode = 0;
	m_ServiceStatus.dwWaitHint = dwWaitHint;
	m_ServiceStatus.dwCheckPoint = dwCheckPoint;

	return SetStatus();
}

//=========================================================================

BOOL Win32ComponentService::UpdateStatus()
{
	if(m_ServiceStatus.dwCheckPoint > 0)
		m_ServiceStatus.dwCheckPoint++;

	return SetStatus();
}

//=========================================================================

BOOL Win32ComponentService::SetStatus()
{
	return ::SetServiceStatus(m_hServiceStatus, &m_ServiceStatus);
}

//=========================================================================
