#ifndef _MATCHTEMPLATE_H
#define _MATCHTEMPLATE_H

//=========================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

#ifndef _ACTION_H
#include "Action.h"
#endif

#include <vector>
#include <boost/smart_ptr.hpp>

class Message;
class Logger;
class MatchRule;

class MsgProcRuleCollection;

//=========================================================

class MatchTemplate : public LoggingObject {
public:
	MatchTemplate();

	BOOL Export(std::string &strDesc);

	BOOL Check() const;

	void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray);
	void SetLogger(Logger* pLogger);

	const std::string& GetName() const { return m_strTemplateName; }
	void SetName(const std::string& strName) { m_strTemplateName = strName; }

	const std::string& GetReplyTemplateName() const { return m_strReplyTemplateName; }
	void SetReplyTemplateName(const std::string& strName) { m_strReplyTemplateName = strName; }

	const std::string& GetDestinationHost() const { return m_strDestinationHost; }
	void SetDestinationHost(const std::string& strName) { m_strDestinationHost = strName; }

	Action::Type GetAction() const { return m_eAction; }
	void SetAction(Action::Type eAction) { m_eAction = eAction; }

	void AddRule(MatchRule* pRule);

	BOOL DoesMatch(Message &rMsg) const;
	BOOL DoesMatch_p(Message *pMsg) const { return DoesMatch(*pMsg); }

private:
	typedef std::vector<boost::shared_ptr<MatchRule> > RuleVect;
	RuleVect m_vectRules;

	MsgProcRuleCollection *m_pMsgProcRules;

	Action::Type m_eAction;

	std::string	m_strReplyTemplateName;
	std::string	m_strDestinationHost;

	std::string	m_strTemplateName;
};

//=========================================================

#endif // _MATCHTEMPLATE_H
