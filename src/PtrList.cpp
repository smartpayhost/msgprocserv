#include "stdafx.h"
#include "PtrList.h"

//==============================================================

PtrList::PtrList()
:	m_iCount(0),
	pHead(NULL),
	pTail(NULL)
{
}

//--------------------------------------------------------------

PtrList::~PtrList()
{
	PtrListElem *pElem;

	while(pHead)
	{
		pElem = pHead;
		pHead = pElem->pNext;
		delete pElem;
	}
}

//==============================================================

void *PtrList::GetHead()
{
	Lock localLock(&m_Lockable);

	if(!pHead)
		return NULL;
	
	return pHead->pPtr;
}

//--------------------------------------------------------------

void *PtrList::GetTail()
{
	Lock localLock(&m_Lockable);

	if(!pTail)
		return NULL;

	return pTail->pPtr;
}

//==============================================================

void *PtrList::RemoveHead()
{
	Lock localLock(&m_Lockable);

	PtrListElem *pElem;

	if(!pHead)
		return NULL;

	pElem = pHead;
	pHead = pElem->pNext;

	if(pHead)
		pHead->pPrev = NULL;
	else
		pTail = NULL;

	void *pRetVal = pElem->pPtr;

	delete pElem;
	m_iCount--;

	return pRetVal;
}

//--------------------------------------------------------------

void *PtrList::RemoveTail()
{
	Lock localLock(&m_Lockable);

	PtrListElem *pElem;

	if(!pTail)
		return NULL;

	pElem = pTail;
	pTail = pElem->pPrev;

	if(pTail)
		pTail->pNext = NULL;
	else
		pHead = NULL;

	void *pRetVal = pElem->pPtr;

	delete pElem;
	m_iCount--;

	return pRetVal;
}

//==============================================================

void *PtrList::RemoveElem(void *pPtr)
{
	Lock localLock(&m_Lockable);

	PtrListElem *pElem;
	
	pElem = pHead;

	while(pElem)
	{
		if(pElem->pPtr == pPtr)
		{
			if(pElem->pPrev)
			{
				pElem->pPrev->pNext = pElem->pNext;
			}
			else
			{
				pHead = pElem->pNext;
			}

			if(pElem->pNext)
			{
				pElem->pNext->pPrev = pElem->pPrev;
			}
			else
			{
				pTail = pElem->pPrev;
			}

			delete pElem;
			m_iCount--;

			return pPtr;
		}

		pElem = pElem->pNext;
	}

	return NULL;
}

//==============================================================

void PtrList::AddHead(void *pPtr)
{
	Lock localLock(&m_Lockable);

	PtrListElem *pElem = new PtrListElem;

	pElem->pPtr = pPtr;

	if(pHead)
	{
		pElem->pNext = pHead;
		pHead->pPrev = pElem;
	}
	else
	{
		pTail = pElem;
	}

	pHead = pElem;

	m_iCount++;
}

//--------------------------------------------------------------

void PtrList::AddTail(void *pPtr)
{
	Lock localLock(&m_Lockable);

	PtrListElem *pElem = new PtrListElem;

	pElem->pPtr = pPtr;

	if(pTail)
	{
		pElem->pPrev = pTail;
		pTail->pNext = pElem;
	}
	else
	{
		pHead = pElem;
	}

	pTail = pElem;

	m_iCount++;
}

//==============================================================

void PtrList::RemoveAll()
{
	Lock localLock(&m_Lockable);

	PtrListElem *pElem;

	while(pHead)
	{
		pElem = pHead;
		pHead = pElem->pNext;
		delete pElem;
	}

	pHead = NULL;
	pTail = NULL;
	m_iCount = 0;
}

//==============================================================

int PtrList::GetCount()
{
	Lock localLock(&m_Lockable);

	return m_iCount;
}

//--------------------------------------------------------------

BOOL PtrList::IsEmpty()
{
	Lock localLock(&m_Lockable);

	return (m_iCount == 0)?TRUE:FALSE;
}

//==============================================================

PtrList::PtrListElem::PtrListElem()
:	pPrev(NULL),
	pNext(NULL),
	pPtr(NULL)
{
}

//==============================================================
