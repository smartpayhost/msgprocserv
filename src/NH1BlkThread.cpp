#include "stdafx.h"
#include "NH1BlkThread.h"

#include "DataPipe.h"
#include "NetworkHeader.h"

#include "ChinaUnionPayHeader.h"

const int NH1BlkThread::SHORT_NH_SIZE = 2;
const int NH1BlkThread::NH_SIZE = 10;

//====================================================================

NH1BlkThread::NH1BlkThread()
:	m_pPipeDst(NULL)
{
}

//====================================================================

DataPipe& NH1BlkThread::GetPipeSrc()
{
	return m_pipeSrc;
}

//--------------------------------------------------------------------

void NH1BlkThread::SetPipeDst(DataPipe *pPipe)
{
	m_pPipeDst = pPipe;
}

//--------------------------------------------------------------------

DWORD NH1BlkThread::ThreadMemberFunc()
{
	SZFN(NH1BlkThread);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if(	(m_pPipeDst == NULL) ||
		(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) )
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	WaitableCollection wc;
	wc.push_back(m_pipeSrc);
	wc.push_back(GetShutdownEvent());
	
	BOOL fWaitHdrType = TRUE;
	BOOL fHaveHdr = FALSE;
	BOOL fShortHdr = FALSE;
	BYTE abHdr[NH_SIZE];
	int iHdrLen = 0;
	int iDataLen = 0;
	int iTotalSize = 0;

	for(;;)
	{
		// wait for packet on pipe or shutdown

		DWORD e = wc.Wait();

		if(e == (WAIT_OBJECT_0 + 0))	// packet from src
		{
			if(!fHaveHdr)
			{
				// read header if present

				iTotalSize = m_pipeSrc.GetTotalSize();

				if(fWaitHdrType)
				{
					if(iTotalSize >= SHORT_NH_SIZE)
					{
						// get the first 2 bytes to check if this is a normal or short nh1 header
						iHdrLen = m_pipeSrc.Get(SHORT_NH_SIZE, abHdr);						

						if(iHdrLen >= SHORT_NH_SIZE)
						{
							// check for normal nh1 header type
							if(!(abHdr[0] == 0 && abHdr[1] == 1))
							{
								fShortHdr = TRUE;
								fHaveHdr = TRUE;
								iDataLen = (abHdr[0] << 8) + abHdr[1];
							}
							
							fWaitHdrType = FALSE;
						}
					}
				}
				else 
				{
					// need to get the rest of the header, 8 more bytes for nh1
					//iHdrLen += m_pipeSrc.Get(NH_SIZE - SHORT_NH_SIZE, &abHdr[2]);
					if(iTotalSize >= (NH_SIZE - SHORT_NH_SIZE))
						iHdrLen += m_pipeSrc.Get(NH_SIZE - SHORT_NH_SIZE, &abHdr[2]);

					if(iHdrLen >= NH_SIZE)
					{
						if (abHdr[0] == 0x00 && abHdr[1] == 0x01 &&
							abHdr[2] == 0x00 &&	abHdr[3] == 0x0A )
						{
							fHaveHdr = TRUE;
							iDataLen = (abHdr[4] << 8) + abHdr[5];
						}
					}
					else
					{
						// we have enough data for the header but it's not actually an NH1 header,
						// started with 00 01 but didn't follow with 00 0A.

						// shutdown thread & let the parent figure it out & respawn
						// (assume this is just a garbage connection & retrying won't help)

						Log(LOG_WARNING, "%s: malformed packet received, terminating", szFn);
						return 0;
					}
				}

				if(fHaveHdr)
				{
					Log(LOG_DEBUG, "%s: checking header received, size = %d, type = %s", szFn, iHdrLen, (fShortHdr ? "Short" : "Normal"));
					Log(LOG_DEBUG, "%s: data size = %d", szFn, iDataLen);
				}
			}
			else
			{
				// read data if present

				iTotalSize = m_pipeSrc.GetTotalSize();
				
				if(iTotalSize >= iDataLen)
				{
					BYTE *pbBuf = new BYTE[iDataLen + NH_SIZE];	
					
					if(fShortHdr)
					{
						// abHdr contains 2 byte length only
						NetworkHeader n;

						// now we need to convert the 2 byte length header to a normal nh1 header
						n.CopyFrom(abHdr, TRUE);

						// and insert the new nh1 header into the onward message
						n.CopyTo(pbBuf);
					}
					else
					{
						// use the received nh1 header contained in abHdr
						memcpy(pbBuf, abHdr, NH_SIZE);
					}

					// append the rest of the message data to the onward message
					m_pipeSrc.Get(iDataLen, pbBuf + NH_SIZE);
/*
#define TPDU_SIZE 5

					if(ChinaUnionPayHeader().IsChinaUnionPayHeader(pbBuf + NH_SIZE + TPDU_SIZE))
					{
						// change back to short nh1
						NetworkHeader nh;
						nh.SetHeaderType(TRUE);
						nh.CopyFrom(pbBuf);
						nh.CopyTo(pbBuf, TRUE, FALSE);
						memmove(pbBuf + nh.GetShortHeaderLength(), pbBuf + nh.GetNormalHeaderLength(), iDataLen);

						Log(LOG_DEBUG, "%s: data received", szFn);
						// write to destination pipe
						m_pPipeDst->PutNC(iDataLen + SHORT_NH_SIZE, pbBuf);
						Log(LOG_DEBUG, "%s: data sent, size = %d", szFn, iDataLen + SHORT_NH_SIZE);
					}
					else
*/
					{
						Log(LOG_DEBUG, "%s: data received", szFn);

						// write to destination pipe

						m_pPipeDst->PutNC(iDataLen + NH_SIZE, pbBuf);

						Log(LOG_DEBUG, "%s: data sent, size = %d", szFn, iDataLen + NH_SIZE);
					}

					fHaveHdr = FALSE;

					fWaitHdrType = TRUE;
				}
			}

			// signal activity semaphore

			SignalActivity();
		}
		else if(e == (WAIT_OBJECT_0 + 1))
		{
			// shutdown thread

			Log(LOG_DEBUG, "%s: ending", szFn);
			return 0;
		}
		else
		{
			Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result: %lx", szFn, e);
			return 2;
		}
	}
}

