#ifndef _LOGGEREVENT_H
#define _LOGGEREVENT_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGER_H
#include "Logger.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

//==============================================================

class LoggerEvent : public Logger {
public:
	LoggerEvent(const std::string& strLogName, int iLogLevel);

	virtual int vLog(enum LogLevel, const char *, va_list);

private:
	CritSect	m_Lockable;
};

//==============================================================

#endif // _LOGGEREVENT_H
