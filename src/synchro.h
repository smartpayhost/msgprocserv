#ifndef _SYNCHRO_H
#define _SYNCHRO_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include <boost/utility.hpp>
#include <deque>

//==============================================================

class LockableObject : public boost::noncopyable {
public:
	virtual ~LockableObject() {}

	virtual void Lock() = 0;
	virtual void Unlock() = 0;
};

//==============================================================

class CritSect : public LockableObject {
public:
	CritSect();
	virtual ~CritSect();

	virtual void Lock();
	virtual void Unlock();

private:
	CRITICAL_SECTION	m_critSect;
};

//==============================================================

class Waitable {
public:
	inline operator HANDLE() const { return GetHandle(); }
	virtual HANDLE GetHandle() const = 0;

	DWORD Wait(DWORD dwTimeout=INFINITE);
};

//==============================================================

class WaitableCollection {
public:
	void push_back(const Waitable& obj) { m_v.push_back(obj.GetHandle()); }
	DWORD Wait(DWORD dwTimeout=INFINITE, BOOL fWaitAll=FALSE);

private:
	std::vector<HANDLE> m_v;
};

//==============================================================

class Mutex : public LockableObject, public Waitable {
public:
	Mutex(BOOL bInitiallyOwn=FALSE);
	virtual ~Mutex();

	virtual void Lock();
	virtual void Unlock();

	HANDLE GetHandle() const { return m_hmtxMutex; }

private:
	HANDLE m_hmtxMutex;
};

//====================================================================

class Event : public Waitable {
public:
	Event(BOOL bManualReset=FALSE, BOOL bInitialState=FALSE)
		:	m_hevEvent(::CreateEvent(NULL, bManualReset, bInitialState, NULL)) {}

	~Event() { if(m_hevEvent) ::CloseHandle(m_hevEvent); }

	HANDLE GetHandle() const { return m_hevEvent; }

	inline BOOL Set() { return m_hevEvent?::SetEvent(m_hevEvent):FALSE; }
	inline BOOL Reset() { return m_hevEvent?::ResetEvent(m_hevEvent):FALSE; }
	inline BOOL Pulse() { return m_hevEvent?::PulseEvent(m_hevEvent):FALSE; }

private:
	HANDLE m_hevEvent;
};

//==============================================================

class Lock : public boost::noncopyable {
public:
	explicit Lock(LockableObject *pLockable);
	~Lock();

private:
	LockableObject *m_pLockable;
};

//==============================================================

template <class T> 
class Queue {
public:
    Queue()
		: m_Event(FALSE, FALSE)
    { 
    }

    void Add(const T& rVal)
    {
		{
		Lock lock(&m_Crit);
        m_Data.push_back(rVal);
		}

		m_Event.Set();
    }

    operator HANDLE() { return m_Event; }

    BOOL Remove(T* pVal)
    {
        if(pVal)
        {
			Lock lock(&m_Crit);

            if(!m_Data.empty())
            {
                *pVal = m_Data.front();
                m_Data.pop_front();

                return TRUE;
            }
        }

        return FALSE;
    }

private:
    std::deque<T>	m_Data;
	CritSect		m_Crit;
	Event			m_Event;
};

//====================================================================

#endif // _SYNCHRO_H
