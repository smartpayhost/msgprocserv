#include "stdafx.h"
#include "HostDescriptionRuleEntity.h"

#include "HostDescriptionCollection.h"

#include <assert.h>
#include <boost/algorithm/string.hpp>

using std::string;

//======================================================================

HostDescriptionRuleEntity::HostDescriptionRuleEntity()
:	m_fComplete(FALSE),
	m_pCollection(NULL)
{
}

//======================================================================

void HostDescriptionRuleEntity::SetName(const string &strName)
{
	m_hHostDescription.SetName(strName);
}

//======================================================================

void HostDescriptionRuleEntity::SetHostDescriptionCollection(HostDescriptionCollection *pCollection)
{
	m_pCollection = pCollection;
}

//----------------------------------------------------------------------

BOOL HostDescriptionRuleEntity::ProcessAddress(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_hHostDescription.SetAddr(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL HostDescriptionRuleEntity::ProcessPort(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_hHostDescription.SetPort(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL HostDescriptionRuleEntity::ProcessHeaderType(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	if(rLine[1] == "LENTPDU")
	{
		m_hHostDescription.SetHeaderType(NetworkHeaderType::NHT_LENTPDU);
	}
	else if(rLine[1] == "NH1")
	{
		m_hHostDescription.SetHeaderType(NetworkHeaderType::NHT_10BYTE);
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

//----------------------------------------------------------------------

BOOL HostDescriptionRuleEntity::ProcessHostEnd(const Line & /*rLine*/)
{
	m_fComplete = TRUE;

	assert(m_pCollection);
	m_pCollection->AddHostDescription(m_hHostDescription);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL HostDescriptionRuleEntity::ProcessLine(const Line &rLine)
{
	if(rLine[0] == "PORT")
	{
		return ProcessPort(rLine);
	}
	else if(rLine[0] == "ADDRESS")
	{
		return ProcessAddress(rLine);
	}
	else if(rLine[0] == "HEADER_TYPE")
	{
		return ProcessHeaderType(rLine);
	}
	else if(rLine[0] == "UPSTREAM_HOST_END")
	{
		return ProcessHostEnd(rLine);
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL HostDescriptionRuleEntity::IsComplete() const
{
	return m_fComplete;
}

//======================================================================
