#ifndef _SQLREPLYQUERYRULEENTITY_H
#define _SQLREPLYQUERYRULEENTITY_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _SQLREPLYQUERY_H
#include "SQLReplyQuery.h"
#endif

class SQLReplyQueryCollection;

//==============================================================

class SQLReplyQueryRuleEntity : public RuleEntity {
public:
	SQLReplyQueryRuleEntity();

	BOOL ProcessLine(const Line &rLine);
	BOOL IsComplete() const;

	void SetName(const std::string &strName);
	void SetSQLReplyQueryCollection(SQLReplyQueryCollection *pCollection);

private:
	BOOL ProcessDataSource(const Line &rLine);
	BOOL ProcessQuery(const Line &rLine);
	BOOL ProcessReplyQueryEnd(const Line &rLine);

private:
	BOOL m_fComplete;
	SQLReplyQueryCollection *m_pCollection;
	SQLReplyQuery m_dsSQLReplyQuery;
};

//==============================================================

#endif // _SQLREPLYQUERYRULEENTITY_H
