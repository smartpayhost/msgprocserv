#ifndef _PTRLIST_H
#define _PTRLIST_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

//==============================================================

class PtrList {
public:
	PtrList();
	~PtrList();

	void *GetHead();
	void *GetTail();

	void *RemoveHead();
	void *RemoveTail();
	void *RemoveElem(void *pPtr);

	void AddHead(void *pPtr);
	void AddTail(void *pPtr);

	void RemoveAll();

	int GetCount();
	BOOL IsEmpty();

private:
	class PtrListElem {
	public:
		PtrListElem();
		class PtrListElem *pPrev;
		class PtrListElem *pNext;
		void *pPtr;
	};

	PtrListElem *pHead;
	PtrListElem *pTail;

	int m_iCount;

	CritSect	m_Lockable;
};

//==============================================================

#endif _PTRLIST_H
