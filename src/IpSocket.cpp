#include "stdafx.h"
#include "IpSocket.h"

#include "Timer.h"

using std::string;

//----------------------------------------------------------------------------------

IpSocket::IpSocket()
:	m_shSocket(INVALID_SOCKET),
	m_fConnected(FALSE),
	m_fBlocking(FALSE)
{
}

//----------------------------------------------------------------------------------

IpSocket::~IpSocket()
{
	Close();
}

//----------------------------------------------------------------------------------

SOCKRC IpSocket::Close()
{
	SOCKRC result;
	int rc;

	SetConnected(FALSE);

	result = SOCKRC_OK;

	if(m_shSocket != INVALID_SOCKET)
	{
		rc = ::closesocket(m_shSocket);
		m_shSocket = INVALID_SOCKET;

		if(rc != 0)
			result = SOCKRC_ERROR;
	}

	return result;
}

//----------------------------------------------------------------------------------
//
//	Returns:
//
//	SOCKRC_OK:			data read (possibly 0 bytes if socket is non-blocking)
//	SOCKRC_ERROR:		error
//	SOCKRC_CLOSED:		connection closed by remote end
//

SOCKRC IpSocket::DoRecv(char *buf, int bufsize, int *recvd)
{
	int result;

	*recvd = 0;

	result = ::recv(m_shSocket, buf, bufsize, 0);

	if(result == 0)
	{
		SetConnected(FALSE);
		return SOCKRC_CLOSED;
	}

	if(result == SOCKET_ERROR)
	{
		result = ::WSAGetLastError();

		if(result == WSAECONNRESET)
		{
			SetConnected(FALSE);
			return SOCKRC_CLOSED;
		}

		if(result != WSAEWOULDBLOCK)
		{
			SetConnected(FALSE);
			return SOCKRC_ERROR;
		}

		result = 0;
	}

	*recvd = result;

	return SOCKRC_OK;
}

//----------------------------------------------------------------------------------
//
//	returns:
//
//	SOCKRC_OK
//	SOCKRC_ERROR
//	SOCKRC_TIMEOUT
//

SOCKRC IpSocket::DoSend(const char *buf, int len, int *sent, unsigned long timeout)
{
	SOCKRC result;
	int iSent;
	Timer t;

	if(sent != NULL)
		*sent = 0;

	t.Start(timeout);

	do
	{
		if(timeout)
		{
			if(t.IsExpired())
				return SOCKRC_TIMEOUT;

			result = WaitWritable(t.Remaining());

			if(result != SOCKRC_OK)		// Timeout or error
				return result;
		}

		iSent = ::send(m_shSocket, (char *)buf, len, 0);

		if(iSent == SOCKET_ERROR)
		{
			if(::WSAGetLastError() != WSAEWOULDBLOCK)
			{
				SetConnected(FALSE);
				return SOCKRC_ERROR;
			}

			iSent = 0;
		}

		buf += iSent;
		len -= iSent;

		if(sent != NULL)
			*sent += iSent;

	} while(len);

	return SOCKRC_OK;
}

//----------------------------------------------------------------------------------

SOCKRC IpSocket::SetBlocking(BOOL fBlocking)
{
	unsigned long dummy_ulong = fBlocking?0L:1L;

	if(::ioctlsocket(m_shSocket, FIONBIO, &dummy_ulong) == SOCKET_ERROR)
	{
		return SOCKRC_ERROR;
	}

	m_fBlocking = fBlocking;
	return SOCKRC_OK;
}

//----------------------------------------------------------------------------------

SOCKRC IpSocket::PendingReadable(unsigned long *pulResult)
{
	if(::ioctlsocket(m_shSocket, FIONREAD, pulResult) == SOCKET_ERROR)
	{
		return SOCKRC_ERROR;
	}

	return SOCKRC_OK;
}

//----------------------------------------------------------------------------------
//
//	Check if a socket is readable, wait if not.
//	(timeout in milliseconds)
//
//	returns:
//	SOCKRC_OK:			readable
//	SOCKRC_TIMEOUT:		timeout
//	SOCKRC_ERROR:		error
//

SOCKRC IpSocket::WaitReadable(unsigned long timeout)
{
	int result;
	fd_set fds;
	fd_set efds;
	struct timeval t;

	t.tv_sec = timeout / 1000L;
	t.tv_usec = (timeout % 1000L) * 1000;

#pragma warning (disable : 4127)
	FD_ZERO(&fds);
	FD_SET(m_shSocket, &fds);
	FD_ZERO(&efds);
	FD_SET(m_shSocket, &efds);
#pragma warning (default : 4127)
	
	result = ::select(0, &fds, NULL, &efds, &t);

	if(result == SOCKET_ERROR)
		return SOCKRC_ERROR;

	if(result == 0)
		return SOCKRC_TIMEOUT;

	if(FD_ISSET(m_shSocket, &fds))
		return SOCKRC_OK;

	return SOCKRC_ERROR;
}

//----------------------------------------------------------------------------------
//
//	Check if a socket is writable, wait if not.
//	(timeout in milliseconds)
//
//	returns:
//	SOCKRC_OK:			writable
//	SOCKRC_TIMEOUT:		timeout
//	SOCKRC_ERROR:		error
//

SOCKRC IpSocket::WaitWritable(unsigned long timeout)
{
	int result;
	fd_set fds;
	fd_set efds;
	struct timeval t;

	t.tv_sec = timeout / 1000L;
	t.tv_usec = timeout % 1000L;

#pragma warning (disable : 4127)
	FD_ZERO(&fds);
	FD_SET(m_shSocket, &fds);
	FD_ZERO(&efds);
	FD_SET(m_shSocket, &efds);
#pragma warning (default : 4127)
	
	result = ::select(0, NULL, &fds, &efds, &t);

	if(result == SOCKET_ERROR)
		return SOCKRC_ERROR;

	if(result == 0)
		return SOCKRC_TIMEOUT;

	if(FD_ISSET(m_shSocket, &fds))
		return SOCKRC_OK;

	return SOCKRC_ERROR;
}

//----------------------------------------------------------------------------------

short IpSocket::ParsePort(const string& strPort, const string& strProto)
{
	struct servent *servent;

	if(strPort.size() == 0)
		return 0;

	if(strPort.find_first_not_of("0123456789") == strPort.npos)
	{
		return ::htons((WORD)(atoi(strPort.c_str())));
	}
	else
	{
		servent = ::getservbyname(strPort.c_str(), strProto.c_str());
	}

	if(servent == NULL)
		return 0;

	return servent->s_port;
}

//----------------------------------------------------------------------------------

unsigned long IpSocket::ParseAddr(const string& strAddr)
{
	struct hostent *hostent;

	if(strAddr.size() == 0)
		return 0;

	if(strAddr.find_first_not_of("0123456789.") == strAddr.npos)
	{
		return ::inet_addr(strAddr.c_str());
	}
	else
	{
		hostent = ::gethostbyname(strAddr.c_str());

		if(hostent == NULL)
			return INADDR_NONE;

		return *((unsigned long *)(hostent->h_addr_list[0]));
	}
}

//----------------------------------------------------------------------------------

int IpSocket::Errno()
{
	return ::WSAGetLastError();
}

//----------------------------------------------------------------------------------
