#ifndef _SQLRESULTTABLE_H
#define _SQLRESULTTABLE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>
#include <vector>
#include <boost/utility.hpp>

//==============================================================

class SQLResultTable : boost::noncopyable {
public:
	void Erase();

	BOOL FindIn(const std::string& strTest, int iColumn = 0);
	BOOL GetData(std::string &rResult, int iRow = 0, int iCol = 0);

	void AddRow(const std::vector<std::string>& rrRow);

	int GetNumCols() const;
	int GetNumRows() const;

private:
	typedef std::vector<std::string> ResultRow;
	typedef std::vector<ResultRow> ResultTable;

	ResultTable m_tblResult;

};

//==============================================================

#endif // _SQLRESULTTABLE_H
