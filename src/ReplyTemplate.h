#ifndef _REPLYTEMPLATE_H
#define _REPLYTEMPLATE_H

//=========================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

#ifndef _BITMAPMSG_H
#include "BitmapMsg.h"
#endif

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _REPLYRULE_H
#include "ReplyRule.h"
#endif

#include <vector>
#include <boost/smart_ptr.hpp>

class MessagePacket;
class MsgProcRuleCollection;
class Logger;

//=========================================================

class ReplyTemplate : public LoggingObject {
public:
	ReplyTemplate();

	BOOL Export(std::string &strDesc) const;

	BOOL Check() const;

	void SetBitmapMsgStyle(BitmapMsgStyle *pStyle);

	void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray);
	void SetLogger(Logger* pLogger);

	const std::string& GetName() const { return m_strName; }
	void SetName(const std::string& strName) { m_strName = strName; }

	void AddRule(ReplyRule* pRule);

	BOOL BuildReply(MessagePacket &rPacket) const;

private:
	typedef std::vector<boost::shared_ptr<ReplyRule> > RuleVect;
	RuleVect m_vectRules;

	MsgProcRuleCollection *m_pMsgProcRules;

	std::string m_strName;
};

//=========================================================

#endif // _REPLYTEMPLATE_H
