#include "stdafx.h"
#include "MsgProcHandler.h"

#include "MsgProcCfg.h"
#include "MsgProcServ.h"
#include "Registry.h"
#include "TcpBatchHandler.h"

#include <sys/types.h>
#include <sys/stat.h>

extern const char *g_szLogpath;
extern int g_iLogLevel;

using std::string;

//=======================================================================

static string l_strNewLogPath;

//=======================================================================

MsgProcHandler::MsgProcHandler()
:	m_pConfig(NULL),
	m_ptBatch(NULL),
	m_evShutdown(TRUE, FALSE),
	m_evActivity(FALSE, FALSE),
	m_eState(MPHS_STARTUP)
{
	SetShutdownEvent(&m_evShutdown);
	SetActivityEvent(&m_evActivity);
}

//-----------------------------------------------------------------------

MsgProcHandler::~MsgProcHandler()
{
	ShutdownChildren();

	if(m_ptBatch)
	{
		m_ptBatch->WaitForExit();
		delete m_ptBatch;
	}
}

//=======================================================================

void MsgProcHandler::Shutdown()
{
	Lock localLock(&m_critShutdown);

	m_evShutdown.Set();
	ThreadObject::WaitForExit();
}

//=======================================================================

BOOL MsgProcHandler::Reconfig()
{
	SZFN(Reconfig);

	// shut everything down -------------------------------------

	ShutdownChildren();

	if(m_ptBatch)
	{
		m_ptBatch->WaitForExit();
		delete m_ptBatch;
	}
	
	// start everything up -------------------------------------

	m_ptBatch = new TcpBatchHandler(m_pConfig);

	AddChild(m_ptBatch);
	SetupChildren();
		
	if(!m_ptBatch->StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 3: failed to start batch thread", szFn);
		m_eState = MPHS_RESTART;
		ShutdownChildren();
		return FALSE;
	}

	m_eState = MPHS_RUNNING;

	return TRUE;
}

//=======================================================================

DWORD MsgProcHandler::ThreadMemberFunc()
{
	SZFN(MsgProcHandler);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if(	(m_pConfig == NULL) ||
		(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) )
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	m_eState = MPHS_STARTUP;

	BOOL fExit = FALSE;

	while(!fExit)
	{
		switch(m_eState)
		{
		case MPHS_STARTUP:
		case MPHS_RESTART:
		case MPHS_CONFIG_CHANGED:
			Reconfig();
			break;

		case MPHS_CONFIG_ERROR:
		case MPHS_LISTENING:
		case MPHS_RUNNING:
			// do nothing
			break;

		case MPHS_SHUTDOWN:
			fExit = TRUE;
			continue;

		case MPHS_FATAL_ERROR:
			Log(LOG_ERROR, "%s: ending: fatal error", szFn);
			fExit = TRUE;
			continue;

		default:
			Log(LOG_ERROR, "%s: ending: invalid state", szFn);
			fExit = TRUE;
			continue;
		}

		WaitableCollection wc;

		wc.push_back(GetShutdownEvent());

		switch(m_eState)
		{
		case MPHS_RUNNING:
			wc.push_back(*m_ptBatch);
			break;

		default:
			break;
		}

		DWORD e = wc.Wait();

		switch(e)
		{
		case WAIT_OBJECT_0 + 0:		// shutdown
			Log(LOG_DEBUG, "%s: shutdown requested", szFn);
			m_eState = MPHS_SHUTDOWN;
			break;

		case WAIT_OBJECT_0 + 1:		// child thread died
			Log(LOG_NOTICE, "%s: child thread died unexpectedly, restarting", szFn);
			m_eState = MPHS_RESTART;
			break;

		case WAIT_TIMEOUT:
			break;

		default:
			Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result: %lx", szFn, e);
			m_eState = MPHS_RESTART;
			break;
		}
	}

	Log(LOG_DEBUG, "%s: shutting down children", szFn);

	ShutdownChildren();

	Log(LOG_DEBUG, "%s: ending", szFn);

	return 0;
}

//=======================================================================
