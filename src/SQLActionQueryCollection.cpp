#include "stdafx.h"
#include "SQLActionQueryCollection.h"

#include "SQLActionQueryRuleEntity.h"
#include "SQLMessageQuery.h"

#include <assert.h>

using std::string;

//======================================================================

SQLActionQueryCollection::SQLActionQueryCollection()
:	m_pMsgProcRules(NULL)
{
}

//----------------------------------------------------------------------

SQLActionQueryCollection::~SQLActionQueryCollection()
{
	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLActionQuerys.begin(); i != m_mapSQLActionQuerys.end(); i++)
		delete((*i).second);
}

//======================================================================

void SQLActionQueryCollection::SetMsgProcRuleCollection(MsgProcRuleCollection *pArray)
{
	m_pMsgProcRules = pArray;

	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLActionQuerys.begin(); i != m_mapSQLActionQuerys.end(); i++)
		(*i).second->SetMsgProcRuleCollection(m_pMsgProcRules);
}

//======================================================================

void SQLActionQueryCollection::SetLogger(Logger *pLogger)
{
	m_pLogger = pLogger;

	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLActionQuerys.begin(); i != m_mapSQLActionQuerys.end(); i++)
		(*i).second->SetLogger(m_pLogger);
}

//======================================================================

void SQLActionQueryCollection::Register(RuleFile &rRuleFile)
{
	rRuleFile.RegisterEntityType(this);
}

//======================================================================

void SQLActionQueryCollection::AddSQLActionQuery(SQLActionQuery &rSQLActionQuery)
{
	SQLActionQuery *p = new SQLActionQuery(rSQLActionQuery);

	assert(p);

	if(p)
	{
		p->SetMsgProcRuleCollection(m_pMsgProcRules);
		p->SetLoggingName(rSQLActionQuery.GetName());
		p->SetLogger(m_pLogger);
		m_mapSQLActionQuerys[rSQLActionQuery.GetName()] = p;
	}
}

//======================================================================

BOOL SQLActionQueryCollection::Export(string &rStrDest)
{
	SQLMessageQuery::SQLMessageQueryMap::iterator i;

	for(i = m_mapSQLActionQuerys.begin(); i != m_mapSQLActionQuerys.end(); i++)
	{
		string strTmp;

		BOOL fRetval = (*i).second->Export(strTmp);

		if(!fRetval)
			return FALSE;

		rStrDest += strTmp;
		rStrDest += '\n';
	}

	rStrDest += '\n';

	return TRUE;
}

//======================================================================

void SQLActionQueryCollection::MergeFrom(const SQLActionQueryCollection& rhs)
{
	for(SQLMessageQuery::SQLMessageQueryMap::const_iterator i = rhs.m_mapSQLActionQuerys.begin(); i != rhs.m_mapSQLActionQuerys.end(); i++)
	{
		AddSQLActionQuery(*dynamic_cast<SQLActionQuery *>((*i).second));
	}
}

//======================================================================

BOOL SQLActionQueryCollection::IsStart(const Line &rLine) const
{
	if(rLine.size() != 2)
		return FALSE;

	if(rLine[0] == "ACTION_QUERY")
		return TRUE;

	return FALSE;
}

//======================================================================

RuleEntity *SQLActionQueryCollection::MakeNew(const Line &rLine)
{
	SQLActionQueryRuleEntity *pResult;

	pResult = new SQLActionQueryRuleEntity;

	if(!pResult)
		return NULL;

	pResult->SetName(rLine[1]);

	pResult->SetSQLActionQueryCollection(this);

	return pResult;
}

//======================================================================

const SQLActionQuery* SQLActionQueryCollection::FindByName(const string& strName) const
{
	SQLMessageQuery::SQLMessageQueryMap::const_iterator i;

	i = m_mapSQLActionQuerys.find(strName);

	if(i == m_mapSQLActionQuerys.end())
		return NULL;
	else
		return dynamic_cast<SQLActionQuery *>((*i).second);
}

//======================================================================

BOOL SQLActionQueryCollection::Check() const
{
	BOOL fRetval = TRUE;

	SQLMessageQuery::SQLMessageQueryMap::const_iterator i;

	for(i = m_mapSQLActionQuerys.begin(); i != m_mapSQLActionQuerys.end(); i++)
	{
		if(!(*i).second->Check())
			fRetval = FALSE;
	}

	return fRetval;
}

//======================================================================
