#ifndef _MATCHRULEFIELDCONTENTS_H
#define _MATCHRULEFIELDCONTENTS_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

class Message;
class BitmapMsg;

//==============================================================

class MatchRuleFieldContents : public MatchRule {
public:
	MatchRuleFieldContents();

	MatchRule* Clone() const;

	BOOL Check() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	BOOL DoesMatch(const std::string& strTestData, const BitmapMsg *pMsg) const;
	BOOL DoesMatchSQL(const std::string& strQuery, const std::string& strTestData, const BitmapMsg *pMsg) const;
	BOOL DoesMatchFile(const std::string& strFile, const std::string& strTestData) const;

private:
	int m_iFieldNum;

	enum MATCH_WHAT {
		MRFC_WHOLE_FIELD,
		MRFC_FIRST_N,
		MRFC_LAST_N,
		MRFC_FROM_M_TO_N,
		MRFC_LEADING_DIGITS
	};

	enum MATCH_WITH {
		MRFC_FIXED_DATA,
		MRFC_REGEXP,
		MRFC_FILE,
		MRFC_SQL
	};

	MATCH_WHAT m_eWhat;
	MATCH_WITH m_eWith;

	int m_iFirst;
	int m_iLast;
	int m_iFrom;
	int m_iTo;

	std::string		m_strMatchData;
};

//==============================================================

#endif // _MATCHRULEFIELDCONTENTS_H
