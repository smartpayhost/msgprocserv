#ifndef _SQLMATCHQUERY_H
#define _SQLMATCHQUERY_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//======================================================================

#ifndef _SQLMESSAGEQUERY_H
#include "SQLMessageQuery.h"
#endif

class SQLResultTable;
class BitmapMsg;

//======================================================================

class SQLMatchQuery : public SQLMessageQuery {
public:
	BOOL Export(std::string &rStrDest) const;

	BOOL ExecQuery(SQLResultTable &rResult, const BitmapMsg &rOrigMsg) const;
};

//======================================================================

#endif // _SQLMATCHQUERY_H
