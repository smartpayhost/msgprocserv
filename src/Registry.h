#ifndef _REGISTRY_H
#define _REGISTRY_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>
#include <vector>
#include <boost/utility.hpp>

//==============================================================

class RegistryKey : public boost::noncopyable {
public:
	RegistryKey();
	RegistryKey(HKEY hParent, const std::string& strKeyPath, REGSAM samDesired=KEY_ALL_ACCESS);
	~RegistryKey();

	BOOL IsValid() { return m_fValid; }
	HKEY GetHKey() { return m_hKey; }

	BOOL Open(HKEY hParent, const std::string& strKeyPath, REGSAM samDesired=KEY_ALL_ACCESS);
	void Close();

	BOOL DeleteValue(const std::string& strValueName);

	BOOL SetDWORD(const std::string& strValueName, DWORD dwData);
	BOOL ReadDWORD(const std::string& strValueName, DWORD *pdwData);

	BOOL SetString(const std::string& strValueName, 
					const std::string& strValueData, 
					DWORD dwType = REG_SZ);

	BOOL ReadString(const std::string& strValueName, 
					std::string &strResult);

	BOOL ReadString(const std::string& strValueName, 
					DWORD dwMaxLen, 
					char *pbBuf, 
					DWORD *pdwResultLen = NULL);

	BOOL SetBinary(const std::string& strValueName, 
					const BYTE *pbValueData, 
					DWORD dwLen, 
					DWORD dwType = REG_SZ);

	BOOL ReadBinary(const std::string& strValueName, 
					DWORD dwMaxLen, 
					BYTE *pbBuf, 
					DWORD *pdwResultLen = NULL);

	BOOL GetValSize(const std::string& strValueName, DWORD *pdwSize);

	BOOL EnumKeys(std::vector<std::string>& vResult);

private:
	HKEY m_hKey;
	BOOL m_fValid;
};

//==============================================================

class Registry : public boost::noncopyable {
public:
	static BOOL CreateKey(HKEY hTopLevelKey, 
							const std::string& strNewKeyName);

	static BOOL DeleteKey(HKEY hTopLevelKey, 
							const std::string& strKeyName);

	static BOOL DeleteValue(HKEY hTopKey, 
							const std::string& strKey, 
							const std::string& strValueName);

	static BOOL SetString(HKEY hTopKey, 
							const std::string& strKey, 
							const std::string& strValueName,
							const std::string& strValueData, 
							DWORD dwType = REG_SZ);

	static BOOL ReadString(HKEY hTopKey, 
							const std::string& strKey, 
							const std::string& strValueName, 
							std::string &strResult);

	static BOOL ReadString(HKEY hTopKey, 
							const std::string& strKey, 
							const std::string& strValueName,
							DWORD dwMaxLen, 
							char *pbBuf, 
							DWORD *pdwResultLen = NULL);

	static BOOL SetDWORD(HKEY hTopKey, 
							const std::string& strKey, 
							const std::string& strValueName, 
							DWORD dwData);

	static BOOL ReadDWORD(HKEY hTopKey, 
							const std::string& strKey, 
							const std::string& strValueName, 
							DWORD *pdwData);

	static BOOL EnumKeys(HKEY hTopKey, 
							const std::string& strKey, 
							std::vector<std::string>& vResult);
};

//==============================================================

#endif // _REGISTRY_H
