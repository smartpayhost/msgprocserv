#include "stdafx.h"
#include "TcpListenThread.h"

//====================================================================

#define DEFAULT_SHUTDOWN_POLLTIME 1000L

//=============================================================

TcpListenThread::TcpListenThread()
:	m_evConnectEvent(FALSE, FALSE),
	m_evListenEvent(FALSE, FALSE),
	m_dwShutdownPollTime(DEFAULT_SHUTDOWN_POLLTIME)
{
}

//=============================================================

void TcpListenThread::SetSocket(SOCKET s)
{
	m_s.SetSocketHandle(s);
}

//-------------------------------------------------------------

void TcpListenThread::SetShutdownPollTime(DWORD dwPollTime)
{
	m_dwShutdownPollTime = dwPollTime;
}

//=============================================================

void TcpListenThread::StartListening()
{
	m_evListenEvent.Set();
}

//=============================================================

DWORD TcpListenThread::ThreadMemberFunc()
{
	SZFN(TcpListenThread);

	Log(LOG_DEBUG, "%s: starting", szFn);

	for(;;)
	{
		WaitableCollection wc;
		wc.push_back(m_evListenEvent);
		wc.push_back(GetShutdownEvent());

		// wait for signal to start listening or shutdown

		DWORD e = wc.Wait();

		switch(e)
		{
		case WAIT_OBJECT_0 + 0:	// start listening
			Log(LOG_DEBUG, "%s: starting to listen", szFn);
			break;

		case WAIT_OBJECT_0 + 1:	// shutdown
			Log(LOG_DEBUG, "%s: shutdown requested, ending", szFn);
			return 0;

		default:				// error
			Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result: %lx", szFn, e);
			return 1;
		}
		
		for(;;)
		{
			// wait for connection or timeout

			int iResult = m_s.WaitReadable(m_dwShutdownPollTime);

			// if connecting, signal it and drop out to main loop

			if(iResult == SOCKRC_OK)
			{
				Log(LOG_DEBUG, "%s: detected incoming connection", szFn);
				m_evConnectEvent.Set();
				break;
			}

			// if timeout, check for shutdown

			if(iResult == SOCKRC_TIMEOUT)
			{
				if(CheckForShutdown() == WAIT_OBJECT_0)
				{
					Log(LOG_DEBUG, "%s: shutdown requested, ending", szFn);
					return 0;
				}
			}

			if(iResult == SOCKRC_ERROR)
			{
				Log(LOG_ERROR, "%s: error %d on listen socket, ending", szFn, m_s.Errno());
				return 1;
			}
		}
	}
}

//====================================================================
