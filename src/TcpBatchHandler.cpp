#include "stdafx.h"
#include "TcpBatchHandler.h"

#include "MsgProcCfg.h"
#include "Timer.h"

#include <assert.h>

//====================================================================

TcpBatchHandler::TcpBatchHandler(MsgProcCfg *pConfig)
:	m_pConfig(pConfig),
	m_tMux(*pConfig)
{
}

//====================================================================

DWORD TcpBatchHandler::ThreadMemberFunc()
{
	SZFN(TcpBatchHandler);

	assert(m_pConfig);

	Log(LOG_DEBUG, "%s: starting", szFn);

	AddChild(&m_tMux);
	AddChild(&m_tUpstream);

	SetupChildren();

	m_tMux.SetPipeDst(&m_tUpstream.GetPipeSrc());
	m_tUpstream.SetPipeDst(&m_tMux.GetPipeSrc());
	m_tMux.SetDownstreamPort(m_pConfig->GetListenPort());

	m_tUpstream.SetConfig(m_pConfig);

	if(!m_tMux.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 3: failed to start mux thread", szFn);
		ShutdownChildren();
		return 3;
	}

	if(!m_tUpstream.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 4: failed to start upstream thread", szFn);
		ShutdownChildren();
		return 4;
	}

	WaitableCollection wc;
	wc.push_back(GetShutdownEvent());
	wc.push_back(m_tMux);
	wc.push_back(m_tUpstream);

	DWORD e = wc.Wait();

	switch(e)
	{
	case WAIT_OBJECT_0 + 0:
		Log(LOG_DEBUG, "%s: shutdown requested", szFn);
		break;

	case WAIT_OBJECT_0 + 1:
		Log(LOG_DEBUG, "%s: Mux child died", szFn);
		break;

	case WAIT_OBJECT_0 + 2:
		Log(LOG_DEBUG, "%s: Batch child died", szFn);
		break;

	default:
		Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result %lx", szFn, e);
		break;
	}

	Log(LOG_DEBUG, "%s: shutting down children", szFn);

	ShutdownChildren();

	Log(LOG_DEBUG, "%s: ending", szFn);

	return 0;
}
