#include "stdafx.h"
#include "OdbcStatement.h"
#include "OdbcConnection.h"
#include "bcd.h"
#include "SQLResultTable.h"

#include <assert.h>

using std::string;
using std::vector;

//======================================================================

class OdbcResultBuf : boost::noncopyable {
public:
	OdbcResultBuf(HSTMT hstmt, WORD wCol);

	const std::string GetBuf();

private:
	static const int DEFAULT_BUFSIZE;
	static const int MAX_BUFSIZE;

	std::vector<char> m_vBuf;

	SQLLEN m_sdwResultLen;
	SWORD m_wSqlType;

private:
	OdbcResultBuf();	// no valid default constructor possible
};

//======================================================================
//======================================================================

const int OdbcResultBuf::DEFAULT_BUFSIZE = 2100;
const int OdbcResultBuf::MAX_BUFSIZE = 4000;

//----------------------------------------------------------------------

OdbcResultBuf::OdbcResultBuf(HSTMT hstmt, WORD wCol)
{
	SQLLEN dwSize;

	SQLRETURN rc = ::SQLColAttribute(hstmt, wCol,
		SQL_DESC_DISPLAY_SIZE,
		NULL, 0, NULL,
		&dwSize);
	
	if(((rc != SQL_SUCCESS)&&(rc != SQL_SUCCESS_WITH_INFO)) || (dwSize == SQL_NO_TOTAL))
		dwSize = DEFAULT_BUFSIZE;

	if((dwSize < 0) || (dwSize > MAX_BUFSIZE))
		dwSize = MAX_BUFSIZE;

	dwSize++;

	m_vBuf.resize(dwSize);

	UCHAR szColName[32];
	SWORD cbColName;
	SQLULEN cbColDef;
	SWORD ibScale;
	SWORD wNullable;

	// get the sql type and save for later
	::SQLDescribeCol(hstmt, wCol, 
		szColName, (SQLSMALLINT)sizeof(szColName), &cbColName,
		&m_wSqlType,
		&cbColDef, &ibScale, &wNullable);

	::SQLBindCol(hstmt, wCol, SQL_C_CHAR, &m_vBuf[0], dwSize, &m_sdwResultLen);
}

//----------------------------------------------------------------------

const std::string OdbcResultBuf::GetBuf()
{
	if(m_sdwResultLen == SQL_NULL_DATA)
		return string();

	switch(m_wSqlType)
	{
	case SQL_BINARY:
	case SQL_VARBINARY:
	case SQL_LONGVARBINARY:
		{
			SQLLEN iLen = m_sdwResultLen / 2;

			if(!iLen)
				return string();

			vector<BYTE> vBcdBuf(iLen);

			BCD::Asc2PackedBcd(&m_vBuf[0], &vBcdBuf[0], (int)iLen);

			return string((char *)&vBcdBuf[0], iLen);
		}

	default:
		return string(&m_vBuf[0], (int)m_sdwResultLen);
	}
}

//======================================================================
//======================================================================

class OdbcResultRowBuf : boost::noncopyable {
public:
	explicit OdbcResultRowBuf(HSTMT hstmt);

	void AppendResults(SQLResultTable &rTable);

	SWORD Cols() const { return m_swCols; }

private:
	typedef vector<boost::shared_ptr<OdbcResultBuf> > ResultVect;
	ResultVect m_vectOdbcResultBufs;

	SWORD m_swCols;

private:
	OdbcResultRowBuf();	// no valid default constructor possible
};

//======================================================================

OdbcResultRowBuf::OdbcResultRowBuf(HSTMT hstmt)
{
	::SQLFreeStmt(hstmt, SQL_UNBIND);

	SQLRETURN rc = ::SQLNumResultCols(hstmt, &m_swCols);

	if(rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
	{
		m_vectOdbcResultBufs.reserve(m_swCols);

		for(WORD i = 0; i < m_swCols; i++)
		{
			m_vectOdbcResultBufs.push_back(boost::shared_ptr<OdbcResultBuf>(new OdbcResultBuf(hstmt, (WORD)(i+1))));
		}
	}
}

//----------------------------------------------------------------------

void OdbcResultRowBuf::AppendResults(SQLResultTable &rTable)
{
	vector<string> result;

	result.reserve(m_vectOdbcResultBufs.size());

	for(ResultVect::iterator it = m_vectOdbcResultBufs.begin(); it != m_vectOdbcResultBufs.end(); ++it)
	{
		result.push_back((*it)->GetBuf());
	}

	rTable.AddRow(result);
}

//======================================================================
//======================================================================

class OdbcStatementImpl : public SQLLoggingObject, public boost::noncopyable {
public:
	explicit OdbcStatementImpl(OdbcConnection* pDbc);
	~OdbcStatementImpl();

	BOOL ExecQuery(const string &strQuery, DWORD dwTimeout=0);

	BOOL Prepare(const std::string &strQuery, DWORD dwTimeout);
	BOOL BindParam(SQLUSMALLINT paramNum, SQLSMALLINT ioType, SQLSMALLINT valueType, SQLSMALLINT paramType,
		SQLULEN colSize, SQLSMALLINT decDigits, SQLPOINTER pvaluePtr, SQLLEN buflen, SQLLEN* strlen_or_indptr);
	BOOL SetNumRows(WORD wRows);
	BOOL Execute(DWORD dwTimeout);

	BOOL NumResultCols(SWORD &swNumCols);

	BOOL FetchResult(SQLResultTable &rTable, int iMaxRows=0);
	BOOL FetchAllResults(SQLResultTable &rTable);
	BOOL MoreResults();

	void Release();

private:
	BOOL Alloc();
	BOOL HandleResult(SQLRETURN rc);

private:
	HSTMT m_hStmt;
	
	OdbcConnection* m_pDbc;
};

//======================================================================

OdbcStatementImpl::OdbcStatementImpl(OdbcConnection* pDbc)
:	m_hStmt(SQL_NULL_HSTMT),
	m_pDbc(pDbc)
{
}

//======================================================================

OdbcStatementImpl::~OdbcStatementImpl()
{
	Release();
}

//======================================================================

BOOL OdbcStatementImpl::Alloc()
{
	Release();

	assert(m_pDbc);

	if(m_pDbc)
		m_hStmt = m_pDbc->AllocStatement();

	return m_hStmt != SQL_NULL_HSTMT;
}

//======================================================================

void OdbcStatementImpl::Release()
{
	if(m_hStmt != SQL_NULL_HSTMT)
	{
		::SQLFreeHandle(SQL_HANDLE_STMT, m_hStmt);
		m_hStmt = SQL_NULL_HSTMT;
	}
}

//======================================================================

BOOL OdbcStatementImpl::HandleResult(SQLRETURN rc)
{
	if(rc == SQL_SUCCESS_WITH_INFO)
	{
		LogSQLInfo(LOG_INFO, *m_pDbc, m_hStmt);
	}
	else if(rc != SQL_SUCCESS)
	{
		LogSQLError(LOG_ERROR, *m_pDbc, m_hStmt);
		m_pDbc->Error();
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL OdbcStatementImpl::ExecQuery(const string &strQuery, DWORD dwTimeout)
{
	SZFN(OdbcStatementImpl::ExecQuery);

	assert(m_pDbc);

	if(!m_pDbc)
	{
		Log(LOG_ERROR, "%s: no connection", szFn);
		return FALSE;
	}

	if(m_hStmt == SQL_NULL_HSTMT && !Alloc())
	{
		Log(LOG_ERROR, "%s: no statement handle", szFn);
		return FALSE;
	}

	assert(m_hStmt != SQL_NULL_HSTMT);

	SQLRETURN rc;

	if(dwTimeout)
	{
		rc = ::SQLSetStmtAttr(m_hStmt,
			SQL_ATTR_QUERY_TIMEOUT, (SQLPOINTER)dwTimeout, SQL_IS_UINTEGER);

		if(!HandleResult(rc))
		{
			Log(LOG_ERROR, "%s: SQLSetConnectAttr failed (SQL_ATTR_QUERY_TIMEOUT)", szFn);
			return FALSE;
		}
	}

	rc = ::SQLFreeStmt(m_hStmt, SQL_CLOSE);	// close previous cursor

	if(!HandleResult(rc))
	{
		Log(LOG_ERROR, "%s: SQLFreeStmt failed", szFn);
		return FALSE;
	}

	rc = ::SQLExecDirect(m_hStmt, (UCHAR *)strQuery.c_str(), SQL_NTS);

	if(!HandleResult(rc))
	{
		Log(LOG_ERROR, "%s: SQLExecDirect failed", szFn);
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL OdbcStatementImpl::Prepare(const std::string &strQuery, DWORD dwTimeout)
{
	SZFN(OdbcStatementImpl::Prepare);

	assert(m_pDbc);

	if(!m_pDbc)
	{
		Log(LOG_ERROR, "%s: no connection", szFn);
		return FALSE;
	}

	if(m_hStmt == SQL_NULL_HSTMT && !Alloc())
	{
		Log(LOG_ERROR, "%s: no statement handle", szFn);
		return FALSE;
	}

	assert(m_hStmt != SQL_NULL_HSTMT);

	SQLRETURN rc;

	if(dwTimeout)
	{
		rc = ::SQLSetStmtAttr(m_hStmt,
			SQL_ATTR_QUERY_TIMEOUT, (SQLPOINTER)dwTimeout, SQL_IS_UINTEGER);

		if(!HandleResult(rc))
		{
			Log(LOG_ERROR, "%s: SQLSetConnectAttr failed (SQL_ATTR_QUERY_TIMEOUT)", szFn);
			return FALSE;
		}
	}

	rc = ::SQLFreeStmt(m_hStmt, SQL_CLOSE);	// close previous cursor

	if(!HandleResult(rc))
	{
		Log(LOG_ERROR, "%s: SQLFreeStmt failed", szFn);
		return FALSE;
	}

	rc = ::SQLPrepare(m_hStmt, (UCHAR *)strQuery.c_str(), SQL_NTS);

	if(!HandleResult(rc))
	{
		Log(LOG_ERROR, "%s: SQLPrepare failed", szFn);
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL OdbcStatementImpl::BindParam(SQLUSMALLINT paramNum, SQLSMALLINT ioType, SQLSMALLINT valueType, SQLSMALLINT paramType,
	SQLULEN colSize, SQLSMALLINT decDigits, SQLPOINTER pvaluePtr, SQLLEN buflen, SQLLEN* strlen_or_indptr)
{
	SZFN(OdbcStatementImpl::BindParam);

	if(m_hStmt == SQL_NULL_HSTMT)
	{
		Log(LOG_ERROR, "%s: no statement handle", szFn);
		return FALSE;
	}

	assert(m_hStmt != SQL_NULL_HSTMT);

	SQLRETURN rc;

	rc = ::SQLBindParameter(m_hStmt, paramNum, ioType, valueType, paramType, colSize, decDigits, pvaluePtr, buflen, strlen_or_indptr);

	if(!HandleResult(rc))
	{
		Log(LOG_ERROR, "%s: SQLBindParameter failed", szFn);
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL OdbcStatementImpl::SetNumRows(WORD wRows)
{
	SZFN(OdbcStatementImpl::SetNumRows);

	if(m_hStmt == SQL_NULL_HSTMT)
	{
		Log(LOG_ERROR, "%s: no statement handle", szFn);
		return FALSE;
	}

	assert(m_hStmt != SQL_NULL_HSTMT);

	SQLRETURN rc;

	rc = ::SQLSetStmtAttr(m_hStmt, SQL_ATTR_PARAMSET_SIZE, (SQLPOINTER)wRows, SQL_IS_UINTEGER);

	if(!HandleResult(rc))
	{
		Log(LOG_ERROR, "%s: SQLSetStmtAttr failed", szFn);
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL OdbcStatementImpl::Execute(DWORD dwTimeout)
{
	SZFN(OdbcStatementImpl::Execute);

	if(m_hStmt == SQL_NULL_HSTMT)
	{
		Log(LOG_ERROR, "%s: no statement handle", szFn);
		return FALSE;
	}

	assert(m_hStmt != SQL_NULL_HSTMT);

	SQLRETURN rc;

	if(dwTimeout)
	{
		rc = ::SQLSetStmtAttr(m_hStmt,
			SQL_ATTR_QUERY_TIMEOUT, (SQLPOINTER)dwTimeout, SQL_IS_UINTEGER);

		if(!HandleResult(rc))
		{
			Log(LOG_ERROR, "%s: SQLSetConnectAttr failed (SQL_ATTR_QUERY_TIMEOUT)", szFn);
			return FALSE;
		}
	}

	rc = ::SQLExecute(m_hStmt);

	if(!HandleResult(rc))
	{
		Log(LOG_ERROR, "%s: SQLExecute failed", szFn);
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL OdbcStatementImpl::FetchResult(SQLResultTable &rTable, int iMaxRows)
{
	SZFN(OdbcStatementImpl::FetchResult);

	BOOL fCountRows = (iMaxRows > 0);

	OdbcResultRowBuf ResultBuf(m_hStmt);

	if(ResultBuf.Cols())
	{
		while(!fCountRows || iMaxRows--)
		{
			SQLRETURN rc = ::SQLFetch(m_hStmt);

			if(rc == SQL_NO_DATA_FOUND)
			{
				break;
			}
			else if(!HandleResult(rc))
			{
				Log(LOG_ERROR, "%s: SQLFetch failed", szFn);
				return FALSE;
			}

			ResultBuf.AppendResults(rTable);
		}
	}

	return TRUE;
}

//======================================================================

BOOL OdbcStatementImpl::MoreResults()
{
	SZFN(OdbcStatementImpl::MoreResults);

	SQLRETURN rc = ::SQLMoreResults(m_hStmt);

	if(rc == SQL_NO_DATA)
		return FALSE;

	if(!HandleResult(rc))
	{
		Log(LOG_ERROR, "%s: SQLMoreResults failed", szFn);
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL OdbcStatementImpl::FetchAllResults(SQLResultTable &rTable)
{
	int iMaxRows = rTable.GetNumRows();

	BOOL fCountRows = (iMaxRows > 0);

	while(!fCountRows || (rTable.GetNumRows() < iMaxRows))
	{
		if(!FetchResult(rTable, fCountRows?(iMaxRows - rTable.GetNumRows()):0))
			return FALSE;

		if(!MoreResults())
			break;
	}

	return TRUE;
}

//======================================================================
//======================================================================

OdbcStatement::OdbcStatement(OdbcConnection* pDbc)
:	m_pImpl(new OdbcStatementImpl(pDbc)),
	m_pDbc(pDbc)
{
	assert(m_pDbc);
	
	if(m_pDbc)
		m_pDbc->RegisterStatement(this);
}

OdbcStatement::~OdbcStatement()
{
	if(m_pDbc)
		m_pDbc->UnregisterStatement(this);

	delete m_pImpl;
}

//======================================================================

void OdbcStatement::SetLogger(Logger* pLogger)
{
	SQLLoggingObject::SetLogger(pLogger);
	m_pImpl->SetLogger(pLogger);
}

//======================================================================

BOOL OdbcStatement::ExecQuery(const string &strQuery, DWORD dwTimeout)
{
	return m_pImpl->ExecQuery(strQuery, dwTimeout);
}

//======================================================================

BOOL OdbcStatement::Prepare(const string &strQuery, DWORD dwTimeout)
{
	return m_pImpl->Prepare(strQuery, dwTimeout);
}

//======================================================================

BOOL OdbcStatement::BindParam(SQLUSMALLINT paramNum, SQLSMALLINT ioType, SQLSMALLINT valueType, SQLSMALLINT paramType,
		SQLULEN colSize, SQLSMALLINT decDigits, SQLPOINTER pvaluePtr, SQLLEN buflen, SQLLEN* strlen_or_indptr)
{
	return m_pImpl->BindParam(paramNum, ioType, valueType, paramType, colSize, decDigits, pvaluePtr, buflen, strlen_or_indptr);
}

//======================================================================

BOOL OdbcStatement::Execute(DWORD dwTimeout)
{
	return m_pImpl->Execute(dwTimeout);
}

//======================================================================

BOOL OdbcStatement::SetNumRows(WORD wRows)
{
	return m_pImpl->SetNumRows(wRows);
}

//======================================================================

void OdbcStatement::Release()
{
	m_pImpl->Release();
}

//======================================================================

BOOL OdbcStatement::FetchAllResults(SQLResultTable &rTable)
{
	return m_pImpl->FetchAllResults(rTable);
}

//======================================================================
