#include "stdafx.h"
#include "TcpHandler.h"

#include "DataPipe.h"
#include "TcpSocket.h"

//===================================================================

TcpHandler::TcpHandler()
:	m_fAutoConnect(FALSE),
	m_fDontQueue(FALSE),
	m_s(NULL)
{
}

//===================================================================

void TcpHandler::SetSocket(TcpSocket *s)
{
	m_s = s;
	m_tIn.SetSocket(s);
	m_tOut.SetSocket(s);
}

//-------------------------------------------------------------------

void TcpHandler::SetAutoConnect(BOOL fAuto)
{
	m_fAutoConnect = fAuto;
}

//-------------------------------------------------------------------

void TcpHandler::SetDontQueue(BOOL fDontQueue)
{
	m_fDontQueue = fDontQueue;
}

//-------------------------------------------------------------------

DataPipe& TcpHandler::GetPipeSrc()
{
	return m_pipeSrc;
}

//-------------------------------------------------------------------

void TcpHandler::SetPipeDst(DataPipe *pPipe)
{
	m_tIn.SetPipe(pPipe);
}

//===================================================================

DWORD TcpHandler::ThreadMemberFunc()
{
	SZFN(TcpHandler);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if(	(m_s == NULL) ||
		(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) )
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	m_tOut.SetPipe(&m_pipeSrc);

	if(m_fAutoConnect)
	{
		BOOL fExit = FALSE;

		while(!fExit)
		{
			if(m_s->IsConnected())
			{
				DoOnline();

				if(CheckForShutdown() == WAIT_OBJECT_0)
				{
					fExit = TRUE;
				}
			}
			else
			{
				WaitableCollection wc;

				wc.push_back(GetShutdownEvent());
				wc.push_back(m_pipeSrc);

				m_sStats.SetString(S_STR_STATUS, "Offline");

				DWORD e = wc.Wait();

				fExit = TRUE;

				switch(e)
				{
				case WAIT_OBJECT_0 + 1:
					Log(LOG_DEBUG, "%s: data to send, connecting", szFn);
					m_sStats.SetString(S_STR_STATUS, "Connecting");
					fExit = FALSE;
					m_pipeSrc.Put(0, NULL);	// just set the event so we trigger again

					m_s->Close();

					if(m_s->Open() == SOCKRC_OK)
					{
						if(m_s->Connect() == SOCKRC_OK)
						{
							Log(LOG_INFO, "%s: connected", szFn);
							break;
						}
					}

					Log(LOG_WARNING, "%s: could not connect", szFn);

					if(m_fDontQueue)
					{
						m_pipeSrc.Flush();
						Log(LOG_WARNING, "%s: data discarded", szFn);
					}

					if(CheckForShutdown(3000L) == WAIT_OBJECT_0)
						fExit = TRUE;
					break;

				case WAIT_OBJECT_0 + 0:
					Log(LOG_DEBUG, "%s: shutdown requested", szFn);
					break;

				default:
					Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result %lx", szFn, e);
					break;
				}
			}
		}
	}
	else
	{
		DoOnline();
	}

	Log(LOG_DEBUG, "%s: ending", szFn);

	return 0;
}

//===================================================================

DWORD TcpHandler::DoOnline()
{
	SZFN(TcpHandler::DoOnline);

	Log(LOG_DEBUG, "%s: starting", szFn);

	AddChild(&m_tIn);
	AddChild(&m_tOut);

	SetupChildren();

	m_tIn.SetTcpHandler(this);
	m_tOut.SetTcpHandler(this);

	if(!m_tOut.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 2: failed to start TCP output thread", szFn);
		ShutdownChildren();
		return 2;
	}

	if(!m_tIn.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 3: failed to start TCP input thread", szFn);
		ShutdownChildren();
		return 3;
	}

	m_sStats.SetString(S_STR_STATUS, "Connected");

	WaitableCollection wc;
	wc.push_back(GetShutdownEvent());
	wc.push_back(m_tOut);
	wc.push_back(m_tIn);

	DWORD e = wc.Wait();

	switch(e)
	{
	case WAIT_OBJECT_0 + 1:
		Log(LOG_WARNING, "%s: TCP out child died", szFn);
		break;

	case WAIT_OBJECT_0 + 2:
		if(m_tIn.GetRetval())
			Log(LOG_WARNING, "%s: TCP in child died, code 0x%08lx", szFn, m_tIn.GetRetval());
		else
			Log(LOG_INFO, "%s: TCP in child died", szFn);
		break;

	case WAIT_OBJECT_0 + 0:
		Log(LOG_DEBUG, "%s: shutdown requested", szFn);
		break;

	default:
		Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result %lx", szFn, e);
		break;
	}

	Log(LOG_DEBUG, "%s: shutting down children", szFn);

	ShutdownChildren();

	Log(LOG_DEBUG, "%s: ending", szFn);

	m_sStats.SetString(S_STR_STATUS, "Disconnected");

	return 0;
}

// take packets from the socket, send them to the pipe

//====================================================================

#define TCP_IN_BUFSIZE 4000

//===================================================================

DWORD TcpHandler::TcpInThread::ThreadMemberFunc()
{
	SZFN(TcpInThread);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if(	(m_s == NULL) ||
		(m_pPipe == NULL) ||
		(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) )
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	m_s->SetBlocking(TRUE);

	int iLen;
	BYTE bBuffer[TCP_IN_BUFSIZE];
	int rc;

	for(;;)
	{
		// Wait for data or socket closed

		rc = m_s->Recv((char *)bBuffer, TCP_IN_BUFSIZE, &iLen);

		if(rc == SOCKRC_OK)
		{
			Log(LOG_DEBUG, "%s: packet received, size = %d", szFn, iLen);

			// Write to pipe

			m_pPipe->Put(iLen, bBuffer);

			// Signal upstream semaphore

			Log(LOG_DEBUG, "%s: packet sent, size = %d", szFn, iLen);

			m_pHandler->m_sStats.IncrementInt(S_INT_MSGS_DOWN);
			m_pHandler->m_sStats.AddInt(S_INT_BYTES_DOWN, iLen);

			SignalActivity();
		}
		else if(rc == SOCKRC_CLOSED)
		{
			Log(LOG_INFO, "%s: ending, socket closed", szFn);
			return 0;	// Terminate
		}
		else if(rc == SOCKRC_ERROR)
		{
			if(CheckForShutdown() == WAIT_OBJECT_0)
				Log(LOG_DEBUG, "%s: ending", szFn);
			else
				Log(LOG_ERROR, "%s: ending, error 3: %d", szFn, m_s->Errno());

			return 3;	// Terminate
		}

		// If shutdown semaphore

		if(CheckForShutdown() != WAIT_TIMEOUT)
		{
			Log(LOG_DEBUG, "%s: ending", szFn);
			return 0;	// Terminate
		}
	}
}

//===================================================================

// take packets from the pipe, send them to the socket

DWORD TcpHandler::TcpOutThread::ThreadMemberFunc()
{
	SZFN(TcpOutThread);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if(	(m_s == NULL) ||
		(m_pPipe == NULL) ||
		(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) )
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	m_s->SetBlocking(TRUE);

	WaitableCollection wc;
	wc.push_back(*m_pPipe);
	wc.push_back(GetShutdownEvent());

	int iLen;

	int rc;

	for(;;)
	{
		// wait for packet on pipe or shutdown

		DWORD e = wc.Wait();

		if(e == (WAIT_OBJECT_0 + 0))
		{
			BYTE *pbBuf = NULL;

			// get packet from pipe

			iLen = m_pPipe->GetChunkNC(&pbBuf);

			if(iLen == 0)
				continue;

			Log(LOG_DEBUG, "%s: packet received, size = %d", szFn, iLen);

			m_pHandler->m_sStats.IncrementInt(S_INT_MSGS_UP);
			m_pHandler->m_sStats.AddInt(S_INT_BYTES_UP, iLen);

			// Write to socket

			rc = m_s->Send((const char *)pbBuf, iLen, NULL, 0);

			delete[] pbBuf;

			if(rc != SOCKRC_OK)
			{
				Log(LOG_ERROR, "%s: ending, error 3: %d", szFn, m_s->Errno());
				return 3;
			}

			// signal activity semaphore

			Log(LOG_DEBUG, "%s: packet sent, size = %d", szFn, iLen);

			SignalActivity();
		}
		else if(e == (WAIT_OBJECT_0 + 1))
		{
			// shutdown thread

			m_s->Close();
			Log(LOG_DEBUG, "%s: ending", szFn);
			return 0;
		}
		else
		{
			Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result: %lx", szFn, e);
			return 2;
		}
	}
}

//===================================================================
