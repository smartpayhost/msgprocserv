#ifndef _MATCHRULEFIELDLEN_H
#define _MATCHRULEFIELDLEN_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

class Message;

//==============================================================

class MatchRuleFieldLen : public MatchRule {
public:
	MatchRuleFieldLen();

	MatchRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	BOOL DoesMatch(int iTestData) const;

private:
	int			m_iFieldNum;
	int			m_iMatchData;
	BOOL		m_bUseRange;
	int			m_iRangeMin;
	int			m_iRangeMax;
};

//==============================================================

#endif // _MATCHRULEFIELDLEN_H
