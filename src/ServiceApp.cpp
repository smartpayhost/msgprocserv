#include "stdafx.h"
#include "ServiceApp.h"
#include "version.h"

#include "Log.h"
#include "getopt.h"
#include "Registry.h"
#include "ServiceCfg.h"
#include "ServDefaults.h"

#include <stdio.h>
#include <direct.h>

using std::string;

//======================================================================

ServiceApp *ServiceApp::m_pApp = NULL;

//======================================================================

void WINAPI ServiceAppServMain(DWORD dwArgc, LPTSTR *lpszArgv)
{
	if(ServiceApp::m_pApp)
		ServiceApp::m_pApp->ServMain(dwArgc, lpszArgv);
}

//======================================================================

void WINAPI ServiceAppCtrlHandler(DWORD dwCommand)
{
	if(ServiceApp::m_pApp)
		ServiceApp::m_pApp->CtrlHandler(dwCommand);
}

//======================================================================

void ServiceApp::CtrlHandler(DWORD dwCommand)
{
	switch(dwCommand)
	{
	case SERVICE_CONTROL_STOP:
	case SERVICE_CONTROL_SHUTDOWN:
		Log(LOG_NOTICE, "Stopping");
		m_thisComponent.ReportStatusChange(SERVICE_STOP_PENDING, 0, 1000);
		m_evStop.Set();
		break;
	}

	m_thisComponent.UpdateStatus();
}

//======================================================================

void ServiceApp::ServMain(DWORD /*dwArgc*/, LPTSTR * /*lpszArgv*/)
{
	// configure main handler

	if(m_fRunningAsService)
	{
		try
		{
			// register service control handler

			if(!m_thisComponent.RegisterHandler(m_strName.c_str(), ServiceAppCtrlHandler))
			{
				Log(LOG_ALERT, "RegisterHandler()");
				throw "";
			}

			// report status

			if(!m_thisComponent.ReportStatusChange(SERVICE_START_PENDING, 0))
			{
				Log(LOG_ALERT, "ReportStatusChange()");
				throw "";
			}

			if(!Setup())
			{
				Log(LOG_ERROR, "Setup error");
				throw "";
			}

			// tell NT we are up and running

			if(!m_thisComponent.ReportStatusChange(SERVICE_RUNNING,
				SERVICE_ACCEPT_STOP | 
				SERVICE_ACCEPT_SHUTDOWN,
				0))
			{
				Log(LOG_ALERT, "ReportStatusChange()");
				throw "";
			}

			// wait til its all over

			if(!Main())
			{
				Log(LOG_WARNING, "Abnormal exit");
				throw "";
			}
		}
		catch(...)
		{
		}

		Cleanup();

		m_thisComponent.ReportStatusChange(SERVICE_STOPPED, 0, 0);
	}
	else
	{
		if(!Setup())
		{
			Log(LOG_ERROR, "Setup error");
		}
		else if(!Main())
		{
			Log(LOG_WARNING, "Abnormal exit");
		}

		Cleanup();
	}
}

//======================================================================

ServiceApp::ServiceApp()
:	m_evStop(FALSE, FALSE),
	m_fRunningAsService(FALSE)
{
	m_pApp = this;
}

//======================================================================

BOOL ServiceApp::ControlService(DWORD dwCmd, DWORD dwNewState)
{
	SC_HANDLE hService;
	SC_HANDLE hServiceControlManager;

	hServiceControlManager = ::OpenSCManager(NULL, "ServicesActive", SC_MANAGER_ALL_ACCESS);

	if(!hServiceControlManager)
	{
		Log(LOG_CRITICAL, "\nUnable to access Service Control Manager, "
			"GetLastError() = %ld\n", ::GetLastError());

		return FALSE;
	}

	// check whether service already exists

	hService = ::OpenService(hServiceControlManager, m_strName.c_str(), SERVICE_ALL_ACCESS);

	if(hService)
	{
		// stop it

		SERVICE_STATUS ss;
		DWORD dwCheckPoint = 0xFFFFFFFFL;
		DWORD dwStartTime = ::GetTickCount();

		if(::ControlService(hService, dwCmd, &ss))
		{
			while((ss.dwCurrentState != dwNewState) &&
				((::GetTickCount() - dwStartTime) < 10000))
			{
				if(dwCheckPoint == ss.dwCheckPoint)
					break;	// give up, checkpoint hasn't been incremented

				dwCheckPoint = ss.dwCheckPoint;
				::Sleep(ss.dwWaitHint);
				::QueryServiceStatus(hService, &ss);
			}
		}

		::CloseServiceHandle(hService);
	}

	::CloseServiceHandle(hServiceControlManager);

	return TRUE;
}

//======================================================================

BOOL ServiceApp::StopService()
{
	return ControlService(SERVICE_CONTROL_STOP, SERVICE_STOPPED);
}

//======================================================================

BOOL ServiceApp::StartService()
{
	BOOL fRetVal = FALSE;

	SC_HANDLE hService;
	SC_HANDLE hServiceControlManager;

	hServiceControlManager = ::OpenSCManager(NULL, "ServicesActive", SC_MANAGER_ALL_ACCESS);

	if(!hServiceControlManager)
	{
		Log(LOG_CRITICAL, "\nUnable to access Service Control Manager, "
			"GetLastError() = %ld\n", ::GetLastError());

		return FALSE;
	}

	// check whether service already exists

	hService = ::OpenService(hServiceControlManager, m_strName.c_str(), SERVICE_ALL_ACCESS);

	if(hService)
	{
		fRetVal = ::StartService(hService, 0, NULL);
		::CloseServiceHandle(hService);
	}

	::CloseServiceHandle(hServiceControlManager);

	return fRetVal;
}

//======================================================================

BOOL ServiceApp::UninstallService()
{
	// First stop the service if it is running

	StopService();

	// then remove it

	SC_HANDLE hService;
	SC_HANDLE hServiceControlManager;

	hServiceControlManager = ::OpenSCManager(NULL, "ServicesActive", SC_MANAGER_ALL_ACCESS);

	if(!hServiceControlManager)
	{
		Log(LOG_CRITICAL, "\nUnable to access Service Control Manager, "
			"GetLastError() = %ld\n", ::GetLastError());

		return FALSE;
	}

	// check whether service already exists

	hService = ::OpenService(hServiceControlManager, m_strName.c_str(), SERVICE_ALL_ACCESS);

	if(hService)
	{
		::DeleteService(hService);
		::CloseServiceHandle(hService);
	}

	::CloseServiceHandle(hServiceControlManager);

	return TRUE;
}

//======================================================================

BOOL ServiceApp::InstallService(const char *szServiceExecutable)
{
	// first get rid of the existing one, if any

	UninstallService();

	// install the new one

	SC_HANDLE hService;
	SC_HANDLE hServiceControlManager;

	hServiceControlManager = ::OpenSCManager(NULL, "ServicesActive", SC_MANAGER_ALL_ACCESS);

	if(!hServiceControlManager)
	{
		Log(LOG_CRITICAL, "\nUnable to access Service Control Manager, "
			"GetLastError() = %ld\n", ::GetLastError());

		return FALSE;
	}

	hService = ::CreateService(hServiceControlManager,
		m_strName.c_str(),
		m_strDisplayName.c_str(),
		SERVICE_ALL_ACCESS,
		SERVICE_WIN32_OWN_PROCESS,
		SERVICE_AUTO_START,
		SERVICE_ERROR_NORMAL,
		szServiceExecutable,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL);

	if(hService)
	{
		::CloseServiceHandle(hService);

		Log(LOG_CRITICAL, "\n%s successfully installed\n", m_strDisplayName.c_str());
		Log(LOG_CRITICAL, "%s", szServiceExecutable);

		::CloseServiceHandle(hServiceControlManager);
		return TRUE;
	}
	else
	{
		Log(LOG_CRITICAL, "\nUnable to install %s, "
			"GetLastError() = %ld\n", m_strDisplayName.c_str(), ::GetLastError());

		::CloseServiceHandle(hServiceControlManager);
		return FALSE;
	}
}

//======================================================================

int ServiceApp::Run(BOOL fRunningAsService, int argc, char **argv)
{
	m_fRunningAsService = fRunningAsService;

	if(fRunningAsService)
	{
		SERVICE_TABLE_ENTRY ServiceTable[] = {
			{ "DummyServiceName", ServiceAppServMain },
			{ NULL, NULL }
		};

		Win32Service ThisService;

		if(!ThisService.Start(1, ServiceTable, argc, argv))
			return 1;
	}
	else
	{
		printf("%s %s %s\n", argv[0], VERSION, COPYRIGHT);
		ServiceAppServMain(argc, argv);
	}

	return 0;
}

//======================================================================

int ServiceApp::InitialSetup(int argc, char **argv)
{
	string strBaseName;
	string strCmdLineName;
	string strCmdLineConfig;

	char szSearch[_MAX_PATH];
	char *szFile;
	char szExe[_MAX_PATH];
	char szName[_MAX_FNAME];

	::SearchPath(NULL, argv[0], ".exe", _MAX_PATH, szSearch, &szFile);

	_fullpath(szExe, szSearch, _MAX_PATH);
	_splitpath_s(szExe, NULL, 0, NULL, 0, szName, sizeof(szName), NULL, 0);

	m_strExePath = szExe;
	strBaseName = szName;

	// parse command line

	int c;

	while((c = getopt(argc, argv, "C:N:?h")) != EOF)
	{
		switch(c)
		{
		case 'C':
			strCmdLineConfig = optarg;
			break;

		case 'N':
			strCmdLineName = optarg;
			break;

		default:
			return 1;
		}
	}

	// figure out location of config file and load it

	string strConfigFile;

	if(!strCmdLineConfig.empty())			// if config specified on cmd line, use that
	{
		strConfigFile = strCmdLineConfig;
	}
	else if((!strCmdLineName.empty()) &&	// otherwise, if service name specified, try and look up config filename in registry
				Registry::ReadString(HKEY_LOCAL_MACHINE,
									(string(REGKEY_ROOT) + strCmdLineName).c_str(),
									REGVAL_CONFIGPATH,
									strConfigFile))
	{
	}
	else if(Registry::ReadString(HKEY_LOCAL_MACHINE,	// otherwise, guess service name from argv[0] and try and look up registry
									(string(REGKEY_ROOT) + strBaseName).c_str(),
									REGVAL_CONFIGPATH,
									strConfigFile))
	{
	}
	else									// if all that fails, tack ".conf" on the end of argv[0] and use that
	{
		strConfigFile = strBaseName + ".conf";
	}

	ServiceCfg sCfg;

	if(!sCfg.ReadFromFile(strConfigFile))
	{
		Log(LOG_CRITICAL, "Couldn't load config file %s", strConfigFile.c_str());
		return -1;
	}

	// figure out service name

	string strName;
	string strDisplayName;

	if(!strCmdLineName.empty())					// if specified on cmd line, use that
	{
		strName = strCmdLineName;
	}
	else if(!sCfg.GetServiceName().empty())		// otherwise, if specified in config file, use that
	{
		strName = sCfg.GetServiceName();
	}
	else										// otherwise, guess service name from argv[0]
	{
		strName = strBaseName;
	}

	// figure out service display name

	if(!sCfg.GetServiceDisplayName().empty())	// if specified in config file, use that
	{
		strDisplayName = sCfg.GetServiceDisplayName();
	}
	else										// otherwise, use default
	{
		strDisplayName = DEFAULT_SERVICE_DISPLAY_NAME;
	}

	string strRegKeyMain = string(REGKEY_ROOT) + strName;

	m_strName = strName;
	m_strDisplayName = strDisplayName;
	m_strRegKeyMain = strRegKeyMain;
	m_strConfigFile = strConfigFile;

   	_chdir(sCfg.GetWorkDir().c_str());

	return 0;
}

//======================================================================
