#ifndef _REPLYRULE_H
#define _REPLYRULE_H

//=========================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

#ifndef _BITMAPMSG_H
#include "BitmapMsg.h"
#endif

#ifndef _STRUTIL_H
#include "StrUtil.h"
#endif

#ifndef _MESSAGEPACKET_H
#include "MessagePacket.h"
#endif

class MsgProcRuleCollection;

//=========================================================

class ReplyRule : public LoggingObject {
public:
	explicit ReplyRule(const BitmapMsgStyle *pStyle);
	virtual ~ReplyRule() = 0 {}

	static ReplyRule* MakeNew(const BitmapMsgStyle *pStyle, const Line &rLine);

	virtual ReplyRule* Clone() const = 0;

	virtual BOOL Check() const;

	BOOL Import(const std::string &strDesc);
	virtual BOOL Import(const Line &rLine) = 0;
	virtual BOOL Export(std::string &strDesc) = 0;

	virtual BOOL BuildReplyField(MessagePacket &rPacket) const;

	inline const BitmapMsgStyle *GetStyle() const { return m_pStyle; }
	inline void SetStyle(BitmapMsgStyle *pStyle) { m_pStyle = pStyle; }

	inline const MsgProcRuleCollection *GetMsgProcRuleCollection() const { return m_pMsgProcRules; }
	inline void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray) { m_pMsgProcRules = pArray; }

protected:
	virtual BOOL BuildReplyField(BitmapMsg& /*rNewMsg*/, const BitmapMsg& /*rOrigMsg*/) const { return FALSE; }

private:
	const BitmapMsgStyle *m_pStyle;
	MsgProcRuleCollection *m_pMsgProcRules;
};

//=========================================================

#endif // _REPLYRULE_H
