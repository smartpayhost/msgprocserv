#ifndef _ODBCCONNECTION_H
#define _ODBCCONNECTION_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SQLLOGGINGOBJECT_H
#include "SQLLoggingObject.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <boost/smart_ptr.hpp>
#include <boost/utility.hpp>
#include <set>

class OdbcStatement;

//======================================================================

class OdbcConnection : public SQLLoggingObject, public boost::noncopyable {
public:
	OdbcConnection();
	~OdbcConnection();

	inline operator HDBC() { return m_hDbc; }

	inline BOOL Connected() const { return m_fConnected; }

	HSTMT AllocStatement();
	void RegisterStatement(OdbcStatement *pStmt);
	void UnregisterStatement(OdbcStatement *pStmt);

	BOOL Connect(const char *szDSN,
				 const char *szUID=NULL,
				 const char *szAuthStr=NULL,
				 const char *szTraceFile=NULL);

	void Error();

private:
	BOOL Alloc();
	void Release();
	BOOL HandleResult(SQLRETURN rc);

private:
	HDBC	m_hDbc;
	BOOL	m_fConnected;

	CritSect m_Lockable;
	typedef std::set<OdbcStatement* > StatementVect;
	StatementVect m_statements;
};

//======================================================================

#endif // _ODBCCONNECTION_H
