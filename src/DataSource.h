#ifndef _DATASOURCE_H
#define _DATASOURCE_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SQLLOGGINGOBJECT_H
#include "SQLLoggingObject.h"
#endif

#ifndef _SQLRESULTTABLE_H
#include "SQLResultTable.h"
#endif

#include "synchro.h"
#include "OdbcConnectionManager.h"

#include <vector>
#include <map>
#include <boost/utility.hpp>
#include <boost/smart_ptr.hpp>

class MsgProcRuleCollection;

//======================================================================

class DataSource : public SQLLoggingObject {
public:
	DataSource();

	BOOL Export(std::string &rStrDest);

	std::string GetName() const			{ return m_strName; }
	std::string GetDSN() const			{ return m_strDSN; }
	std::string GetUID() const			{ return m_strUID; }
	std::string GetAuthStr() const		{ return m_strAuthStr; }
	DWORD GetTimeout() const			{ return m_dwTimeout; }
	std::string GetTraceFile() const	{ return m_strTraceFile ; }

	void SetName(const std::string &strName);
	void SetDSN(const std::string &strDSN);
	void SetUID(const std::string &strUID);
	void SetAuthStr(const std::string &strAuthStr);
	void SetTimeout(DWORD dwTimeout);
	void SetTraceFile(const std::string &strTraceFile);

	void SetLogger(Logger* pLogger);

	inline const MsgProcRuleCollection *GetMsgProcRuleCollection() const { return m_pMsgProcRules; }
	inline void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray) { m_pMsgProcRules = pArray; }

	BOOL Connect();
	BOOL ExecQuery(const std::string &strQuery, class SQLResultTable &rResult) const;

private:
	std::string m_strName;

	std::string m_strDSN;
	std::string m_strUID;
	std::string m_strAuthStr;
	DWORD m_dwTimeout;
	std::string m_strTraceFile;

	MsgProcRuleCollection *m_pMsgProcRules;

	mutable boost::shared_ptr<OdbcConnectionManager> m_pConnectionManager;
};

//======================================================================

#endif // _DATASOURCE_H
