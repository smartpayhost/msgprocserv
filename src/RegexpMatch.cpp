#include "stdafx.h"

#include "RegexpMatch.h"
#include <boost/cregex.hpp>

using std::string;

BOOL RegexpMatch(const string& strMatchData, const string& strTest)
{
	boost::RegEx regexp(strMatchData);

	if(regexp.Match(strTest))
		return TRUE;

	return FALSE;
}