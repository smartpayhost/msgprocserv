#ifndef _CUPMESSAGEPACKET
#define _CUPMESSAGEPACKET

//====================================================================

#include "message.h"

#include <vector>

//#define BMPMSG_OLD_OFFSET		TPDU_SIZE
//#define BMPMSG_NEW_OFFSET		(CUP_HEADER_OFFSET + CUP_HEADER_SIZE)

#define CUP_HEADER_WID		6
//#define INDICATOR_WID		1
//#define TERMINALID_WID	8
//#define ACCEPTORID_WID	15
//#define STAN_WID			3
#define CUP_TYPE_WID		2

#define CUP_HEADER_OFFSET	TPDU_SIZE
#define CUP_HEADER_SIZE		CUP_HEADER_WID + CUP_TYPE_WID// (INDICATOR_WID + TERMINALID_WID + ACCEPTORID_WID + STAN_WID + TYPE_WID)

//#define ENCRYPTION_INDICATOR		0xff
//#define STAN_INDEX					11

#include "BitmapMsg.h"

//====================================================================

class ChinaUnionPayHeader 
{
public:
	ChinaUnionPayHeader();
	ChinaUnionPayHeader( const BYTE *pBytes );

	BOOL IsChinaUnionPayHeader(const BYTE *pBuffer);

	void Make( const BYTE *pBuffer );
	std::vector<BYTE> BuildAS2805();

	std::vector<BYTE> m_Type;
	std::vector<BYTE> m_rawBytes;

private:
	BYTE *m_pbType, *m_pbCupHdr;
};

//====================================================================

#endif //_CUPMESSAGEPACKET