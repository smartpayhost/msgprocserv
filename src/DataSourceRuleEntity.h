#ifndef _DATASOURCERULEENTITY_H
#define _DATASOURCERULEENTITY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _DATASOURCE_H
#include "DataSource.h"
#endif

#include <string>

class DataSourceCollection;

//======================================================================

class DataSourceRuleEntity : public RuleEntity {
public:
	DataSourceRuleEntity();

	BOOL ProcessLine(const Line &rLine);
	BOOL IsComplete() const;

	void SetName(const std::string &strName);
	void SetDataSourceCollection(DataSourceCollection *pCollection);

private:
	BOOL ProcessDSN(const Line &rLine);
	BOOL ProcessUID(const Line &rLine);
	BOOL ProcessAuthStr(const Line &rLine);
	BOOL ProcessTimeout(const Line &rLine);
	BOOL ProcessTraceFile(const Line &rLine);
	BOOL ProcessDataSourceEnd(const Line &rLine);

private:
	BOOL m_fComplete;
	DataSourceCollection *m_pCollection;
	DataSource m_dsDataSource;
};

//======================================================================

#endif // _DATASOURCERULEENTITY_H
