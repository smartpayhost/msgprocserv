#ifndef _TIMER_H
#define _TIMER_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <boost/utility.hpp>

//==============================================================

class Timer : public boost::noncopyable {
public:
	Timer();
	Timer(DWORD dwMilliseconds);
	
	int IsExpired();
	DWORD Remaining();
	void Start(DWORD dwMilliseconds);
	void Restart();

private:
	DWORD m_dwExpireTime;
	DWORD m_dwDuration;
};

//==============================================================

#endif // _TIMER_H
