#ifndef _PATH_H
#define _PATH_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <stdlib.h>
#include <string>
#include <stack>
#include <list>

//======================================================================

class Path {
public:
	Path();
	explicit Path(const char *szPath);
	explicit Path(const std::string& strPath);

	Path(const Path& rhs);
	Path& operator=(const Path& rhs);

	BOOL Chdir(const std::string& strPath);
	BOOL PopDir();

	std::string GetFilename() const;
	std::string GetBasename() const;
	std::string GetDirectory() const;

	void SetPath(const std::string& strPath);

private:

	char m_szPathBuffer[_MAX_PATH];

	char m_szDrive[_MAX_DRIVE];
	char m_szDir[_MAX_DIR];
	char m_szFname[_MAX_FNAME];
	char m_szExt[_MAX_EXT];

	typedef std::stack<std::string, std::list<std::string> > PathHistory;
	PathHistory m_stackHistory;
};

//======================================================================

#endif // _PATH_H
