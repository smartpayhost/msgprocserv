#ifndef _NH1BLKTHREAD_H
#define _NH1BLKTHREAD_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _STOPPABLETHREAD_H
#include "StoppableThread.h"
#endif

#ifndef _DATAPIPE_H
#include "DataPipe.h"
#endif

//==============================================================

class NH1BlkThread : public StoppableThread {
public:
	NH1BlkThread();

	DataPipe& GetPipeSrc();
	void SetPipeDst(DataPipe *pPipe);	

protected:
    DWORD ThreadMemberFunc();

private:
	static const int NH_SIZE;
	static const int SHORT_NH_SIZE;

	DataPipe m_pipeSrc;
	DataPipe *m_pPipeDst;
};

//==============================================================

#endif // _NH1BLKTHREAD_H
