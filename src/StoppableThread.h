#ifndef _STOPPABLETHREAD_H
#define _STOPPABLETHREAD_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _THREADOBJECT_H
#include "ThreadObject.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

//==============================================================

class StoppableThread : public ThreadObject {
public:
	StoppableThread();

	virtual void SetActivityEvent(Event* pevActivity) { m_pevActivity = pevActivity; }
	virtual void SetActivityEvent(const StoppableThread* ptThread) { m_pevActivity = ptThread->m_pevActivity; }

	virtual void SetShutdownEvent(Event* pevShutdown) {	m_pevShutdown = pevShutdown; }

protected:
	inline Event& GetActivityEvent() { return *m_pevActivity; }
	inline HANDLE GetActivityHandle() const { return m_pevActivity?m_pevActivity->GetHandle():NULL; }
	inline void SignalActivity() { if(m_pevActivity) m_pevActivity->Set(); }

	inline Event& GetShutdownEvent() { return *m_pevShutdown; }
	inline HANDLE GetShutdownHandle() const { return m_pevShutdown?m_pevShutdown->GetHandle():NULL; }
	DWORD CheckForShutdown(DWORD dwTimeout=0) { return m_pevShutdown?m_pevShutdown->Wait(dwTimeout):WAIT_FAILED; }

private:
	Event* m_pevActivity;
	Event* m_pevShutdown;
};

//==============================================================

#endif // _STOPPABLETHREAD_H
