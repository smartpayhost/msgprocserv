#ifndef _SQLMESSAGEQUERY_H
#define _SQLMESSAGEQUERY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SQLQUERY_H
#include "SQLQuery.h"
#endif

#include <map>

class BitmapMsg;
class SQLResultTable;

//======================================================================

class SQLMessageQuery : public SQLQuery {
public:
	typedef std::map<std::string, SQLMessageQuery*> SQLMessageQueryMap;

	virtual BOOL Export(std::string &rStrDest) const = 0;

	virtual BOOL ExecQuery(SQLResultTable &rResult, const BitmapMsg *pOrigMsg = NULL, const BitmapMsg *pNewMsg = NULL) const;

private:
	BOOL CookQuery(std::string &strCookedQuery, const std::string &strQuery, const BitmapMsg *pOrigMsg, const BitmapMsg *pNewMsg) const;
};

//======================================================================

#endif // _SQLMESSAGEQUERY_H
