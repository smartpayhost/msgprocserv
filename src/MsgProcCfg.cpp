#include "stdafx.h"
#include "MsgProcCfg.h"

#include "MsgProcServ.h"
#include "DesBlock.h"

using std::string;
using std::vector;

//======================================================================

MsgProcCfg::MsgProcCfg()
:	m_strListenPort(DEFAULT_LISTEN_PORT),
	m_strRuleFile(DEFAULT_RULE_FILE),
	m_iMinThreads(DEFAULT_MIN_THREADS),
	m_iMaxThreads(DEFAULT_MAX_THREADS),
	m_encryptionType(Config::NO_TYPE),
	m_iDownstreamIdleTimeout(DEFAULT_DOWNSTREAM_IDLE_TIMEOUT)
{
}

//======================================================================

BOOL MsgProcCfg::ReadFromFile(const string& strFilename, FILETIME *pftConfigLastChange)
{
	if(!ServiceCfg::ReadFromFile(strFilename, pftConfigLastChange))
	{
		return FALSE;
	}

	m_sCfg.GetStringValue("PORT",		m_strListenPort);
	m_sCfg.GetStringValue("RULEFILE",	m_strRuleFile);
	m_sCfg.GetIntValue("MINTHREADS",	m_iMinThreads);
	m_sCfg.GetIntValue("MAXTHREADS",	m_iMaxThreads);
	m_sCfg.GetIntValue("DOWNSTREAM_IDLE_TIMEOUT",	m_iDownstreamIdleTimeout);

	vector<string> keys;

	// if nothing is specified, bung these two in for defaults (backwards compatibility)

	if(0 == m_sCfg.GetStringValues("DEFAULTKEY", keys))
	{
		keys.push_back("9245F129C8A720A1");
		keys.push_back("3131313131313131");
	}

	for(std::vector<string>::iterator i = keys.begin(); i != keys.end(); i++)
	{
		if((*i).empty())
			continue;

		if(DesBlock::CheckValidKey((*i).c_str()))
			m_DefaultKeys.insert(*i);
		else
		{
			Log(LOG_ERROR, "Invalid DES key: %s", (*i).c_str());
			return FALSE;
		}
	}

	if(!m_rfRuleFile.Load(m_strRuleFile))
	{
		return FALSE;
	}

	m_rfRuleFile.Check();

	return TRUE;
}

//======================================================================

void MsgProcCfg::SetLogger(Logger *pLogger)
{
	ServiceCfg::SetLogger(pLogger);
	m_rfRuleFile.SetLogger(pLogger);
}

