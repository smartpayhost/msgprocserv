#include "stdafx.h"
#include "HandlerThread.h"

#include <assert.h>

//====================================================================

HandlerThread::HandlerThread()
:	m_evChildShutdown(TRUE, FALSE)
{
}

//--------------------------------------------------------------------

HandlerThread::~HandlerThread()
{
	ShutdownChildren();
}

//====================================================================

void HandlerThread::AddChild(StoppableThread *pChild)
{
	Lock localLock(&m_Lockable);

	pChild->SetLoggingName(GetLoggingName());
	pChild->SetShutdownEvent(&m_evChildShutdown);
	pChild->SetPriority(GetPriority());
	pChild->SetActivityEvent(this);
	pChild->SetLogger(GetLogger());

	m_vectChildren.push_back(pChild);
}

//====================================================================

void HandlerThread::ShutdownChildren()
{
	Lock localLock(&m_Lockable);

	m_evChildShutdown.Set();

	ChildVector::iterator i;

	for(i = m_vectChildren.begin(); i != m_vectChildren.end(); i++)
		(*i)->WaitForExit();

	m_vectChildren.clear();

	m_evChildShutdown.Reset();
}

//====================================================================

void HandlerThread::WaitForExit()
{
	ShutdownChildren();
	StoppableThread::WaitForExit();
}

//====================================================================
