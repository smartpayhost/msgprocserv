#ifndef _TMPFILECOLLECTION_H
#define _TMPFILECOLLECTION_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//====================================================================

#include <vector>
#include <string>

class TmpFileCollection : public std::vector<std::string> {
public:
	virtual ~TmpFileCollection();
};

//====================================================================

#endif // _TMPFILECOLLECTION_H
