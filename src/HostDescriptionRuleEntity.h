#ifndef _HOSTDESCRIPTIONRULEENTITY_H
#define _HOSTDESCRIPTIONRULEENTITY_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _HOSTDESCRIPTION_H
#include "HostDescription.h"
#endif

class HostDescriptionCollection;

//======================================================================

class HostDescriptionRuleEntity : public RuleEntity {
public:
	HostDescriptionRuleEntity();

	BOOL ProcessLine(const Line &rLine);
	BOOL IsComplete() const;

	void SetName(const std::string &strName);
	void SetHostDescriptionCollection(HostDescriptionCollection *pCollection);

private:
	BOOL ProcessAddress(const Line &rLine);
	BOOL ProcessPort(const Line &rLine);
	BOOL ProcessHeaderType(const Line &rLine);
	BOOL ProcessHostEnd(const Line &rLine);

private:
	BOOL m_fComplete;
	HostDescriptionCollection *m_pCollection;
	HostDescription m_hHostDescription;
};

//======================================================================

#endif // _HOSTDESCRIPTIONRULEENTITY_H
