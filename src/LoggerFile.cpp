#include "stdafx.h"
#include "LoggerFile.h"

#include <stdio.h>
#include <stdarg.h>
#include <time.h>

using std::string;

//======================================================================

LoggerFile::LoggerFile(const string& strLogName, int iLogLevel, const string& strLogPath)
:	LoggerStdio(strLogName, iLogLevel),
	m_strLogPath(strLogPath),
	m_fDefaultToStderr(FALSE)
{
}

//======================================================================

int LoggerFile::vLog(enum LogLevel level, const char *fmt, va_list ap)
{
	Lock localLock(&m_Lockable);

	FILE *lfp, *ofp;
	errno_t err = 0;

	lfp = NULL;

	if(!m_strLogPath.empty())
	{
		char szPath[_MAX_PATH];
		time_t ltime;
		struct tm now;

		time(&ltime);
		localtime_s(&now, &ltime);
		strftime(szPath, _MAX_PATH, m_strLogPath.c_str(), &now);

//		err = fopen_s(&lfp, szPath, "atc");	// enable commit flag so data written straight to disk on fflush()
		err = fopen_s(&lfp, szPath, "at");
	}

	if(lfp != NULL)
		ofp = lfp;
	else if(m_fDefaultToStderr)
		ofp = stderr;
	else
		ofp = NULL;

	int iRetVal = 0;

	if(ofp)
	{
		SetOutputStream(ofp);
		iRetVal = LoggerStdio::vLog(level, fmt, ap);
		
		if(lfp)
			fflush(lfp);	// commit to disk ASAP

		SetOutputStream(NULL);
	}

	if(lfp != NULL)
		fclose(lfp);

	return iRetVal;
}

//======================================================================
