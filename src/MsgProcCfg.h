#ifndef _MSGPROCCFG_H
#define _MSGPROCCFG_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Config.h"

#ifndef _SERVICECFG_H
#include "ServiceCfg.h"
#endif

#ifndef _MSGPROCRULEFILE_H
#include "MsgProcRuleFile.h"
#endif

//==============================================================

class MsgProcCfg : public ServiceCfg {
public:
	MsgProcCfg();

	std::string GetListenPort() const { return m_strListenPort; }
	std::string GetRuleFile() const { return m_strRuleFile; }
	int GetMinThreads() const { return m_iMinThreads; }
	int GetMaxThreads() const { return m_iMaxThreads; }
	int GetDownstreamIdleTimeout() const { return m_iDownstreamIdleTimeout; }

	std::set<std::string>	GetDefaultKeys() const { return m_DefaultKeys; }

	const MatchTemplateCollection& GetMatchTemplateCollection() const		{ return m_rfRuleFile.GetMatchTemplateCollection(); }
	const ReplyTemplateCollection& GetReplyTemplateCollection() const		{ return m_rfRuleFile.GetReplyTemplateCollection(); }
	const HostDescriptionCollection& GetHostDescriptionCollection() const	{ return m_rfRuleFile.GetHostDescriptionCollection(); }
	const DataSourceCollection& GetDataSourceCollection() const				{ return m_rfRuleFile.GetDataSourceCollection(); }
	const SQLMatchQueryCollection& GetSQLMatchQueryCollection() const		{ return m_rfRuleFile.GetSQLMatchQueryCollection(); }

	MatchTemplateCollection& GetMatchTemplateCollection()		{ return m_rfRuleFile.GetMatchTemplateCollection(); }
	ReplyTemplateCollection& GetReplyTemplateCollection()		{ return m_rfRuleFile.GetReplyTemplateCollection(); }
	HostDescriptionCollection& GetHostDescriptionCollection()	{ return m_rfRuleFile.GetHostDescriptionCollection(); }
	DataSourceCollection& GetDataSourceCollection()				{ return m_rfRuleFile.GetDataSourceCollection(); }
	SQLMatchQueryCollection& GetSQLMatchQueryCollection()		{ return m_rfRuleFile.GetSQLMatchQueryCollection(); }

	Config::EncryptionType GetEncryptionType() 								{ return m_encryptionType; };
	void SetEncryptionType( Config::EncryptionType encryptionType )			{ m_encryptionType = encryptionType; }

	virtual BOOL ReadFromFile(const std::string& strFilename, FILETIME *pftConfigLastChange = NULL);

	virtual void SetLogger(class Logger *pLogger);

private:
	std::string m_strListenPort;
	std::string m_strRuleFile;

	int m_iMinThreads;
	int m_iMaxThreads;

	int m_iDownstreamIdleTimeout;	// units = milliseconds

	std::set<std::string>	m_DefaultKeys;

	MsgProcRuleFile m_rfRuleFile;

	Config::EncryptionType m_encryptionType;
};

//==============================================================

#endif // _MSGPROCCFG_H
