#include "stdafx.h"
#include "OdbcEnvironment.h"

#include "OdbcConnection.h"

//======================================================================

OdbcEnvironment::OdbcEnvironment()
:	m_hEnv(SQL_NULL_HANDLE)
{
}

//======================================================================

OdbcEnvironment::~OdbcEnvironment()
{
	Release();
}

//======================================================================

OdbcEnvironment& OdbcEnvironment::instance()
{
	static OdbcEnvironment l_Instance;

	return l_Instance;
}

//======================================================================

BOOL OdbcEnvironment::Alloc()
{
	if(::SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &m_hEnv) != SQL_SUCCESS)
	{
		Log(LOG_ALERT, "SQLAllocHandle() failed, ODBC cannot be initialised");
		return FALSE;
	}

	if(::SQLSetEnvAttr(m_hEnv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, SQL_IS_UINTEGER) != SQL_SUCCESS)
	{
		Log(LOG_ALERT, "SQLSetEnvAttr() failed, ODBC cannot be initialised");
		return FALSE;
	}

	return TRUE;
}

//======================================================================

void OdbcEnvironment::Release()
{
	if(m_hEnv != SQL_NULL_HANDLE)
	{
		::SQLFreeEnv(&m_hEnv);
		m_hEnv = SQL_NULL_HANDLE;
	}
}

//======================================================================

HENV OdbcEnvironment::HEnv()
{
	return instance().m_hEnv;
}

//======================================================================

HDBC OdbcEnvironment::AllocConnection()
{
	return instance().impAllocConnection();
}

//======================================================================

HDBC OdbcEnvironment::impAllocConnection()
{
	SZFN(OdbcEnvironment::AllocConnection);

	Lock lock(&m_crit);

	if(m_hEnv == SQL_NULL_HANDLE && !Alloc())
	{
		return NULL;
	}
	
	HDBC hdbc(SQL_NULL_HDBC);

	SQLRETURN rc = ::SQLAllocHandle(SQL_HANDLE_DBC, m_hEnv, &hdbc);

	if(rc == SQL_SUCCESS_WITH_INFO)
	{
		LogSQLInfo(LOG_INFO, hdbc);
	}
	else if(rc != SQL_SUCCESS)
	{
		Log(LOG_ERROR, "%s: SQLAllocConnect failed", szFn);
		LogSQLError(LOG_ERROR, hdbc);
		return NULL;
	}

	return hdbc;
}

//======================================================================

