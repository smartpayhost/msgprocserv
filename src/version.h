#ifndef _VERSION_H
#define _VERSION_H

//====================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//====================================================================

#define VERSION "1.60"
#define COPYRIGHT "(C) Copyright 1998-2015 Smartpay Ltd"

//====================================================================

#endif // _VERSION_H
