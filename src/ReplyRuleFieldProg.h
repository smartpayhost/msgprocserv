#ifndef _REPLYRULEFIELDPROG_H
#define _REPLYRULEFIELDPROG_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULEFIELD_H
#include "ReplyRuleField.h"
#endif

//==============================================================

class ReplyRuleFieldProg : public ReplyRuleField {
public:
	ReplyRuleFieldProg(int iFieldNum, const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const;

private:
	BOOL CreateSourceFile(const std::string& strSrcName, int iField, const BitmapMsg *pMsg) const;

private:
	std::string	m_strCmdline;
};

//==============================================================

#endif // _REPLYRULEFIELDPROG_H
