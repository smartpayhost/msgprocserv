#include "stdafx.h"

#include "BitmapMsg.h"

typedef BitmapMsgFieldStyle BMTE;

static const BMTE F1  ( BMFF_FIXED   , BMFA_B   , 64, "Ext bitmap", "Bit map, extended" );
static const BMTE F2  ( BMFF_LLVARNZ , BMFA_N   , 19, "PAN",        "Primary account number (PAN)" );
static const BMTE F3  ( BMFF_FIXED   , BMFA_N   ,  6, "ProcCode",   "Processing code" );
static const BMTE F4  ( BMFF_FIXED   , BMFA_N   , 12, "TranAmount", "Amount, transaction" );
static const BMTE F5  ( BMFF_FIXED   , BMFA_N   , 12, "Settle Amt", "Amount, settlement" );
static const BMTE F6  ( BMFF_FIXED   , BMFA_N   , 12, "BillingAmt", "Amount, cardholder billing" );
static const BMTE F7  ( BMFF_FIXED   , BMFA_N   , 10, "Date/Time",  "Transmission date and time" );
static const BMTE F8  ( BMFF_FIXED   , BMFA_N   ,  8, "BillingFee", "Amount, cardholder billing fee" );
static const BMTE F9  ( BMFF_FIXED   , BMFA_N   ,  8, "CnvRatSett", "Conversion rate, settlement" );
static const BMTE F10 ( BMFF_FIXED   , BMFA_N   ,  8, "CnvRatBill", "Conversion rate, cardholder billing" );
static const BMTE F11 ( BMFF_FIXED   , BMFA_N   ,  6, "STAN",       "Systems trace audit number" );
static const BMTE F12 ( BMFF_FIXED   , BMFA_N   ,  6, "Time",       "Time, local transaction" );
static const BMTE F13 ( BMFF_FIXED   , BMFA_N   ,  4, "Date",       "Date, local transaction" );
static const BMTE F14 ( BMFF_FIXED   , BMFA_N   ,  4, "Exp Date",   "Date, expiry" );
static const BMTE F15 ( BMFF_FIXED   , BMFA_N   ,  4, "SettleDate", "Date, settlement" );
static const BMTE F16 ( BMFF_FIXED   , BMFA_N   ,  4, "Conv Date",  "Date, conversion" );
static const BMTE F17 ( BMFF_FIXED   , BMFA_N   ,  4, "Capt Date",  "Date, capture" );
static const BMTE F18 ( BMFF_FIXED   , BMFA_N   ,  4, "Merch Type", "Merchant's type" );
static const BMTE F19 ( BMFF_FIXED   , BMFA_N   ,  3, "AICC",       "Acquiring institution country code" );
static const BMTE F20 ( BMFF_FIXED   , BMFA_N   ,  3, "PAN CC",     "Primary account number (PAN) extended, country code" );
static const BMTE F21 ( BMFF_FIXED   , BMFA_N   ,  3, "FICC",       "Forwarding institution country code" );
static const BMTE F22 ( BMFF_FIXED   , BMFA_N   ,  3, "POS Entry",  "Point of service entry mode" );
static const BMTE F23 ( BMFF_FIXED   , BMFA_N   ,  3, "CardSeqNum", "Card sequence number" );
static const BMTE F24 ( BMFF_FIXED   , BMFA_N   ,  3, "NII",        "Network international identifier" );
static const BMTE F25 ( BMFF_FIXED   , BMFA_N   ,  2, "POS CC",     "Point of service condition code" );
static const BMTE F26 ( BMFF_FIXED   , BMFA_N   ,  2, "POS Pin CC", "Point of service PIN capture code" );
static const BMTE F27 ( BMFF_FIXED   , BMFA_N   ,  1, "AIRL",       "Authorization identification response length" );
static const BMTE F28 ( BMFF_FIXED   , BMFA_XN  ,  8, "TransFee",   "Amount, transaction fee" );
static const BMTE F29 ( BMFF_FIXED   , BMFA_XN  ,  8, "SettleFee",  "Amount, settlement fee" );
static const BMTE F30 ( BMFF_FIXED   , BMFA_XN  ,  8, "TranPrcFee", "Amount, transaction processing fee" );
static const BMTE F31 ( BMFF_FIXED   , BMFA_XN  ,  8, "SettPrcFee", "Amount, settlement processing fee" );
static const BMTE F32 ( BMFF_LLVARNZ , BMFA_N   , 11, "AIIC",       "Acquiring institution identification code" );
static const BMTE F33 ( BMFF_LLVARNZ , BMFA_N   , 11, "FIIC",       "Forwarding institution identification code" );
static const BMTE F34 ( BMFF_LLVARNZ , BMFA_NS  , 28, "PAN, ext",   "Primary account number (PAN), extended" );
static const BMTE F35 ( BMFF_LLVARNZ , BMFA_Z   , 37, "Track 2",    "Track 2 data" );
static const BMTE F36 ( BMFF_LLLVARNZ, BMFA_Z   ,104, "Track 3",    "Track 3 data" );
static const BMTE F37 ( BMFF_FIXED   , BMFA_AN  , 12, "RRN",        "Retrieval reference number" );
static const BMTE F38 ( BMFF_FIXED   , BMFA_AN  ,  6, "AuthID Rsp", "Authorization identification response" );
static const BMTE F39 ( BMFF_FIXED   , BMFA_AN  ,  2, "Rsp code",   "Response code" );
static const BMTE F40 ( BMFF_FIXED   , BMFA_AN  ,  3, "SvcRestCod", "Service restriction code" );
static const BMTE F41 ( BMFF_FIXED   , BMFA_ANS ,  8, "CATID",      "Card acceptor terminal identification" );
static const BMTE F42 ( BMFF_FIXED   , BMFA_ANS , 15, "CAID",       "Card acceptor identification code" );
static const BMTE F43 ( BMFF_FIXED   , BMFA_ANS , 40, "CA NameLoc", "Card acceptor name/location" );
static const BMTE F44 ( BMFF_LLVARNZ , BMFA_ANS , 99, "AddRspData", "Additional response data" );
static const BMTE F45 ( BMFF_LLVARNZ , BMFA_ANS , 76, "Track 1",    "Track 1 data" );
static const BMTE F46 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "AddDataISO", "Additional data-ISO" );
static const BMTE F47 ( BMFF_LLVARNZ , BMFA_Z   , 99, "MerchCard",  "Merchant card track 2 data" );
static const BMTE F48 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "AddDataPrv", "Additional data-private" );
static const BMTE F49 ( BMFF_FIXED   , BMFA_N,     3, "CC Trans",   "Currency code, transaction" );
static const BMTE F50 ( BMFF_FIXED   , BMFA_N,     3, "CC Settle",  "Currency code, settlement" );
static const BMTE F51 ( BMFF_FIXED   , BMFA_N,     3, "CC Billing", "Currency code, cardholder billing" );
static const BMTE F52 ( BMFF_FIXED   , BMFA_B   , 64, "PIN data",   "Personal identification number (PIN) data" );
static const BMTE F53 ( BMFF_FIXED   , BMFA_B   , 64, "MerchPIN",   "Merchant PIN data" );
static const BMTE F54 ( BMFF_LLLVARNZ, BMFA_AN  ,120, "Addn amts",  "Additional amounts" );
static const BMTE F55 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "ICC Data",   "Integrated Circuit Card (ICC) Data" );
static const BMTE F56 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd ISO",   "Reserved ISO" );
static const BMTE F57 ( BMFF_FIXED   , BMFA_N   , 12, "Cash Amt",   "Amount, cash (Aus only)" );
static const BMTE F58 ( BMFF_FIXED   , BMFA_N   , 12, "LedgerBal",  "Ledger balance (Aus only)" );
static const BMTE F59 ( BMFF_FIXED   , BMFA_N   , 12, "AccountBal", "Account balance, cleared funds (Aus only)" );
static const BMTE F60 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved private" );
static const BMTE F61 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved private" );
static const BMTE F62 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved private" );
static const BMTE F63 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved private" );
static const BMTE F64 ( BMFF_FIXED   , BMFA_B   , 64, "MAC",        "Message authentication code field" );

static const BMTE F65 ( BMFF_FIXED   , BMFA_B   , 64, "Sec bitmap", "Bit map, extended" );
static const BMTE F66 ( BMFF_FIXED   , BMFA_N   ,  1, "SettleCode", "Settlement code" );
static const BMTE F67 ( BMFF_FIXED   , BMFA_N   ,  2, "ExtPayCode", "Extended payment code" );
static const BMTE F68 ( BMFF_FIXED   , BMFA_N   ,  3, "RICC",       "Receiving institution country code" );
static const BMTE F69 ( BMFF_FIXED   , BMFA_N   ,  3, "SICC",       "Settlement institution country code" );
static const BMTE F70 ( BMFF_FIXED   , BMFA_N   ,  3, "NMIC",       "Network management information code" );
static const BMTE F71 ( BMFF_FIXED   , BMFA_N   ,  4, "MsgNum",     "Message number" );
static const BMTE F72 ( BMFF_FIXED   , BMFA_N   ,  4, "MsgNumLast", "Message number last" );
static const BMTE F73 ( BMFF_FIXED   , BMFA_N   ,  6, "DateAction", "Date, action" );
static const BMTE F74 ( BMFF_FIXED   , BMFA_N   , 10, "CrNum",      "Credits, number" );
static const BMTE F75 ( BMFF_FIXED   , BMFA_N   , 10, "CrRevNum",   "Credit reversals, number" );
static const BMTE F76 ( BMFF_FIXED   , BMFA_N   , 10, "DbNum",      "Debits, number" );
static const BMTE F77 ( BMFF_FIXED   , BMFA_N   , 10, "DbRevNum",   "Debit reversals, number" );
static const BMTE F78 ( BMFF_FIXED   , BMFA_N   , 10, "XferNum",    "Transfers, number" );
static const BMTE F79 ( BMFF_FIXED   , BMFA_N   , 10, "XferRevNum", "Transfer reversals, number" );
static const BMTE F80 ( BMFF_FIXED   , BMFA_N   , 10, "InqNum",     "Inquiries, number" );
static const BMTE F81 ( BMFF_FIXED   , BMFA_N   , 10, "AuthNum",    "Authorizations, number" );
static const BMTE F82 ( BMFF_FIXED   , BMFA_N   , 12, "CrPrFeeAmt", "Credits, processing fee amount" );
static const BMTE F83 ( BMFF_FIXED   , BMFA_N   , 12, "CrTrFeeAmt", "Credits, transaction fee amount" );
static const BMTE F84 ( BMFF_FIXED   , BMFA_N   , 12, "DbPrFeeAmt", "Debits, processing fee amount" );
static const BMTE F85 ( BMFF_FIXED   , BMFA_N   , 12, "DbTrFeeAmt", "Debits, transaction fee amount" );
static const BMTE F86 ( BMFF_FIXED   , BMFA_N   , 16, "CrAmt",      "Credits, amount" );
static const BMTE F87 ( BMFF_FIXED   , BMFA_N   , 16, "CrRevAmt",   "Credit reversals, amount" );
static const BMTE F88 ( BMFF_FIXED   , BMFA_N   , 16, "DbAmt",      "Debits, amount" );
static const BMTE F89 ( BMFF_FIXED   , BMFA_N   , 16, "DbRevAmt",   "Debit reversals, amount" );
static const BMTE F90 ( BMFF_FIXED   , BMFA_N   , 42, "ODE",        "Original data elements" );
static const BMTE F91 ( BMFF_FIXED   , BMFA_AN  ,  1, "FileUpCod",  "File update code" );
static const BMTE F92 ( BMFF_FIXED   , BMFA_AN  ,  2, "FileSecCod", "File security code" );
static const BMTE F93 ( BMFF_FIXED   , BMFA_AN  ,  5, "RespInd",    "Response indicator" );
static const BMTE F94 ( BMFF_FIXED   , BMFA_AN  ,  7, "ServiceInd", "Service indicator" );
static const BMTE F95 ( BMFF_FIXED   , BMFA_AN  , 42, "RplcmtAmts", "Replacement amounts" );
static const BMTE F96 ( BMFF_FIXED   , BMFA_B   , 64, "MsgSecCode", "Message security code" );
static const BMTE F97 ( BMFF_FIXED   , BMFA_XN  , 16, "AmtNetSett", "Amount, net settlement" );
static const BMTE F98 ( BMFF_LLVARNZ , BMFA_ANS , 25, "Payee",      "Payee" );
static const BMTE F99 ( BMFF_LLVARNZ , BMFA_N   , 11, "SIIC",       "Settlement institution identification code" );
static const BMTE F100 ( BMFF_LLVARNZ , BMFA_N   , 11, "RIIC",       "Receiving institution identification code" );
static const BMTE F101 ( BMFF_LLVARNZ , BMFA_ANS , 17, "Filename",   "File name" );
static const BMTE F102 ( BMFF_LLVARNZ , BMFA_ANS , 28, "Acc ID 1",   "Account identification 1" );
static const BMTE F103 ( BMFF_LLVARNZ , BMFA_ANS , 28, "Acc ID 2",   "Account identification 2" );
static const BMTE F104 ( BMFF_LLLVARNZ, BMFA_ANS ,100, "Tran desc",  "Transaction description" );
static const BMTE F105 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd ISO",   "Reserved for ISO use" );
static const BMTE F106 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd ISO",   "Reserved for ISO use" );
static const BMTE F107 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd ISO",   "Reserved for ISO use" );
static const BMTE F108 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd ISO",   "Reserved for ISO use" );
static const BMTE F109 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd ISO",   "Reserved for ISO use" );
static const BMTE F110 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd ISO",   "Reserved for ISO use" );
static const BMTE F111 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd ISO",   "Reserved for ISO use" );
static const BMTE F112 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd nat",   "Reserved for national use" );
static const BMTE F113 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd nat",   "Reserved for national use" );
static const BMTE F114 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd nat",   "Reserved for national use" );
static const BMTE F115 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd nat",   "Reserved for national use" );
static const BMTE F116 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd nat",   "Reserved for national use" );
static const BMTE F117 ( BMFF_FIXED   , BMFA_AN  ,  2, "CSUC",       "Card status update code (Aus only)" );
static const BMTE F118 ( BMFF_FIXED   , BMFA_N   , 10, "CashTotNum", "Cash, total number (Aus only)" );
static const BMTE F119 ( BMFF_FIXED   , BMFA_N   , 16, "CashTotAmt", "Cash, total amount (Aus only)" );
static const BMTE F120 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved for private use" );
static const BMTE F121 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved for private use" );
static const BMTE F122 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved for private use" );
static const BMTE F123 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved for private use" );
static const BMTE F124 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved for private use" );
static const BMTE F125 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved for private use" );
static const BMTE F126 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved for private use" );
static const BMTE F127 ( BMFF_LLLVARNZ, BMFA_ANS ,999, "Rsvd priv",  "Reserved for private use" );
static const BMTE F128 ( BMFF_FIXED   , BMFA_B   , 64, "MAC",        "Message authentication code field" );

static const BMTE * const pTemplate[128] = {
&F1,&F2,&F3,&F4,&F5,&F6,&F7,&F8,&F9,
&F10,&F11,&F12,&F13,&F14,&F15,&F16,&F17,&F18,&F19,
&F20,&F21,&F22,&F23,&F24,&F25,&F26,&F27,&F28,&F29,
&F30,&F31,&F32,&F33,&F34,&F35,&F36,&F37,&F38,&F39,
&F40,&F41,&F42,&F43,&F44,&F45,&F46,&F47,&F48,&F49,
&F50,&F51,&F52,&F53,&F54,&F55,&F56,&F57,&F58,&F59,
&F60,&F61,&F62,&F63,&F64,&F65,&F66,&F67,&F68,&F69,
&F70,&F71,&F72,&F73,&F74,&F75,&F76,&F77,&F78,&F79,
&F80,&F81,&F82,&F83,&F84,&F85,&F86,&F87,&F88,&F89,
&F90,&F91,&F92,&F93,&F94,&F95,&F96,&F97,&F98,&F99,
&F100,&F101,&F102,&F103,&F104,&F105,&F106,&F107,&F108,&F109,
&F110,&F111,&F112,&F113,&F114,&F115,&F116,&F117,&F118,&F119,
&F120,&F121,&F122,&F123,&F124,&F125,&F126,&F127,&F128
};

BitmapMsgStyle TemplateISO8583("ISO8583", pTemplate);
