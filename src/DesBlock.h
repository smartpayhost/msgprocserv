#ifndef _DESBLOCK_H
#define _DESBLOCK_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//==============================================================

class DesBlock {
public:
	DesBlock();

	enum CryptMode {
		CM_ENCRYPT,
		CM_DECRYPT,
	};

	void SetKeyFromString(const char *szKey, CryptMode eMode);

	void SetKey(const BYTE abKey[8], CryptMode eMode);
	
	void DoDes(const BYTE abInBlock[8], BYTE abOutBlock[8]);

	static BOOL CheckValidKey(const char *szKey);

private:
	void scrunch(const BYTE *pbOutOf, DWORD *pdwInto);
	void unscrun(const DWORD *pdwOutOf, BYTE *pbInto);
	void desfunc(DWORD adwBlock[2], DWORD *pdwKeys);
	void cookey(DWORD *raw1);

private:
	DWORD m_adwKnL[32];
};

//==============================================================

#endif // _DESBLOCK_H
