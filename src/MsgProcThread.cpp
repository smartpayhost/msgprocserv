#include "stdafx.h"
#include "MsgProcThread.h"

#include "MessagePacket.h"
#include "MsgProcCfg.h"
#include "HostDescription.h"

#include <assert.h>

using std::string;

//===================================================================

const int MsgProcThread::BATCH_BUFSIZE = 2000;

//===================================================================

MsgProcThread::MsgProcThread()
:	m_pPipeDst(NULL),
	m_pConfig(NULL),
	m_fProcessing(FALSE),
	m_evUpstreamShutdown(TRUE, FALSE)
{
}

//-------------------------------------------------------------------

MsgProcThread::~MsgProcThread()
{
	TcpPipeHandlerMap::iterator i;

	for(i = m_mapTcpHandlers.begin(); i != m_mapTcpHandlers.end(); i++)
		delete (*i).second;
}

//====================================================================

void MsgProcThread::SetPipeDst(DataPipe *pPipe)
{
	m_pPipeDst = pPipe;
}

//--------------------------------------------------------------------

DataPipe& MsgProcThread::GetPipeSrc()
{
	return m_pipeSrc;
}

//===================================================================

int MsgProcThread::GetCurrentWorkLoad()
{
	int iRetVal = 0;

	iRetVal += m_pipeSrc.GetNumChunks();

	if(m_fProcessing)
		iRetVal++;

	return iRetVal;
}

//===================================================================

DWORD MsgProcThread::ThreadMemberFunc()
{
	SZFN(MsgProcThread);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if( (m_pPipeDst == NULL) ||
		(m_pConfig == NULL) ||
		(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) )
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		assert(0);
		return 1;
	}

	m_lNetwork.SetLogger(GetLogger());
	m_lNetwork.SetLoggingName(GetLoggingName());
	m_lNetwork.Setup(&m_lMessage);

	m_lMessage.SetLogger(GetLogger());
	m_lMessage.SetLoggingName(GetLoggingName());
	m_lMessage.Setup(&m_lHost);
	m_lMessage.SetDefaultKeys(m_pConfig->GetDefaultKeys());
	m_lMessage.SetConfig(m_pConfig);

	m_lHost.SetLogger(GetLogger());
	m_lHost.SetLoggingName(GetLoggingName());
	m_lHost.SetConfig(m_pConfig);

	int iNumHandles = 2 + m_pConfig->GetHostDescriptionCollection().size();

	if(iNumHandles > MAXIMUM_WAIT_OBJECTS)
	{
		Log(LOG_ERROR, "%s: ending, error 2: too many upstream hosts", szFn);
		return 2;
	}

	// Create the upstream threads

	HostDescriptionCollection::HostDescriptionMap::const_iterator i;

	for(i = m_pConfig->GetHostDescriptionCollection().begin(); 
					i != m_pConfig->GetHostDescriptionCollection().end(); i++)
	{
		string strName = (*i).first;
		HostDescription *psHD = (*i).second.get();

		TcpPipeHandler *pHandler = new TcpPipeHandler;

		pHandler->SetHost(psHD->GetAddr(), psHD->GetPort(), strName, psHD->GetHeaderType());
		pHandler->SetPipeDst(m_pPipeDst);

		pHandler->SetShutdownEvent(&m_evUpstreamShutdown);
		pHandler->SetActivityEvent(this);
		pHandler->SetLogger(GetLogger());
		pHandler->SetLoggingName(GetLoggingName() + "/upstream[" + strName + "]");

		m_mapTcpHandlers[strName] = pHandler;
	}

	// Start the threads

	TcpPipeHandlerMap::iterator tphmI;

	for(tphmI = m_mapTcpHandlers.begin(); tphmI != m_mapTcpHandlers.end(); ++tphmI)
	{
		TcpPipeHandler *ph = (*tphmI).second;

		if(!ph->StartThread())
		{
			Log(LOG_ERROR, "%s: ending, error 4: failed to start upstream thread", szFn);
			ShutdownUpstreamThreads();
			return 4;
		}

		m_vectActiveHandlers.push_back(ph);
	}

	// setup event array

	WaitableCollection wc;

	wc.push_back(GetShutdownEvent());
	wc.push_back(m_pipeSrc);

	TcpPipeHandlerVector::iterator tphvI;

	for(tphvI = m_vectActiveHandlers.begin(); tphvI != m_vectActiveHandlers.end(); ++tphvI)
	{
		wc.push_back(**tphvI);
	}

	// Main loop

	int iLen;
	BYTE bBuffer[BATCH_BUFSIZE];

	DWORD dwRetVal = 0;
	BOOL fExit = FALSE;

	while(!fExit)
	{
		// wait for packet on pipe or shutdown

		m_fProcessing = FALSE;

		DWORD e = wc.Wait();

		if(e == (WAIT_OBJECT_0 + 1))
		{
			m_fProcessing = TRUE;

			// get packet from pipe

			iLen = m_pipeSrc.GetChunk(BATCH_BUFSIZE, bBuffer);

			if(iLen < 0)
			{
				Log(LOG_DEBUG, "%s: over sized packet received from downstream, size = %d", szFn, iLen);

				// fetch & discard packet
				BYTE* pbBuf;
				iLen = m_pipeSrc.GetChunkNC(&pbBuf);
				
				if(iLen)
				{
					Log(LOG_DEBUG, "%s: over sized packet discarded, size = %d", szFn, iLen);
					delete[] pbBuf;
				}

				continue;
			}
			else
			{
				Log(LOG_DEBUG, "%s: packet received from downstream, size = %d", szFn, iLen);

				if(iLen == 0)
					continue;
			}

			// process message

			MessagePacket rPacket;

			if(!rPacket.m_pRequest.SetRawMsg(iLen, bBuffer))
			{
				Log(LOG_WARNING, "%s: error in SetRawMsg", szFn);
				continue;
			}

			if(!m_lNetwork.Process(rPacket, Message::NHT_NH1))
			{
				Log(LOG_WARNING, "%s: error in Process", szFn);
				continue;
			}

			// Write to pipe

			if(rPacket.GetAction() == Action::UPSTREAM)
			{
				string strHost = rPacket.GetUpstreamHost();

				TcpPipeHandlerMap::iterator tphmI;

				tphmI = m_mapTcpHandlers.find(strHost);

				if(tphmI == m_mapTcpHandlers.end())
				{
					Log(LOG_ERROR, "%s: no handler for host %s", szFn, strHost.c_str());
					continue;
				}

				TcpPipeHandler *ph = (*tphmI).second;

				ph->GetPipeSrc().Put(iLen, bBuffer);

				Log(LOG_DEBUG, "%s: packet sent to upstream host %s, size = %d", szFn, strHost.c_str(), iLen);
				SignalActivity();
			}
			else if(rPacket.GetAction() == Action::RESPONSE)
			{
				iLen = rPacket.m_pResponse.GetRawLen();

				if(iLen)
				{
					m_pPipeDst->Put(iLen, rPacket.m_pResponse.GetRawBuf());
					Log(LOG_DEBUG, "%s: packet sent to downstream, size = %d", szFn, iLen);
					SignalActivity();
				}
				else
				{
					Log(LOG_DEBUG, "%s: zero length downstream packet", szFn);
				}
			}
		}
		else if(e == (WAIT_OBJECT_0 + 0))
		{
			// shutdown thread

			Log(LOG_DEBUG, "%s: ending", szFn);
			fExit = TRUE;
		}
		else if(e < (WAIT_OBJECT_0 + iNumHandles))
		{
			// upstream thread died

			Log(LOG_WARNING, "%s: upstream thread died unexpectedly, shutting down", szFn);
			fExit = TRUE;
		}
		else
		{
			Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result: %lx", szFn, e);
			dwRetVal = 3;
			fExit = TRUE;
		}
	}

	Log(LOG_DEBUG, "%s: shutting down upstream threads", szFn);

	ShutdownUpstreamThreads();

	Log(LOG_DEBUG, "%s: ending", szFn);

	return dwRetVal;
}

//==================================================================

void MsgProcThread::ShutdownUpstreamThreads()
{
	SZFN(ShutdownUpstreamThreads);

	// Signal to all threads

	m_evUpstreamShutdown.Set();

	// Wait for all downstream threads to die

	TcpPipeHandlerVector::iterator i;

	for(i = m_vectActiveHandlers.begin(); i != m_vectActiveHandlers.end(); i++)
	{
		if((*i)->Wait(3000L) != WAIT_OBJECT_0)
		{
			Log(LOG_WARNING, "%s: upstream thread did not shutdown cleanly", szFn);
		}
	}

	m_evUpstreamShutdown.Reset();
}

//==================================================================
