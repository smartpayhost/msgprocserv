#include "stdafx.h"
#include "LoggerStderr.h"

#include <stdio.h>
#include <stdarg.h>

using std::string;

//======================================================================

LoggerStderr::~LoggerStderr()
{
}

LoggerStderr::LoggerStderr(const string& strLogName, int iLogLevel)
:	LoggerStdio(strLogName, iLogLevel, stderr)
{
}

//======================================================================

int LoggerStderr::vLog(enum LogLevel level, const char *fmt, va_list ap)
{
	Lock localLock(&m_Lockable);

	int iRetVal = LoggerStdio::vLog(level, fmt, ap);

	return iRetVal;
}

