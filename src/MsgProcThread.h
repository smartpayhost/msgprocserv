#ifndef _MSGPROCTHREAD_H
#define _MSGPROCTHREAD_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _STOPPABLETHREAD_H
#include "StoppableThread.h"
#endif

#ifndef _DATAPIPE_H
#include "DataPipe.h"
#endif

#ifndef _LAYERNETWORK_H
#include "LayerNetwork.h"
#endif

#ifndef _LAYERMSG_H
#include "LayerMsg.h"
#endif

#ifndef _LAYERHOST_H
#include "LayerHost.h"
#endif

#ifndef _TCPPIPEHANDLER_H
#include "TcpPipeHandler.h"
#endif

#include <vector>
#include <map>
#include <boost/shared_ptr.hpp>

class MsgProcCfg;

//==============================================================

class MsgProcThread : public StoppableThread {
public:
	MsgProcThread();
	~MsgProcThread();

	void SetConfig(MsgProcCfg *pConfig) { m_pConfig = pConfig; }

	DataPipe& GetPipeSrc();
	void SetPipeDst(DataPipe *pPipe);

	int GetCurrentWorkLoad();

protected:
    DWORD ThreadMemberFunc();

private:
	void ShutdownUpstreamThreads();

private:
	static const int BATCH_BUFSIZE;

	BOOL m_fProcessing;
	LayerNetwork	m_lNetwork;
	LayerMsg		m_lMessage;
	LayerHost		m_lHost;

	typedef std::map<std::string, TcpPipeHandler *> TcpPipeHandlerMap;
	TcpPipeHandlerMap	m_mapTcpHandlers;

	typedef std::vector<TcpPipeHandler *> TcpPipeHandlerVector;
	TcpPipeHandlerVector m_vectActiveHandlers;

	DataPipe m_pipeSrc;
	DataPipe *m_pPipeDst;

	MsgProcCfg *m_pConfig;

	Event	m_evUpstreamShutdown;
};

//==============================================================

#endif // _MSGPROCTHREAD_H
