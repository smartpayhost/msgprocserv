#ifndef _LOGGERDATABASE_H
#define _LOGGERDATABASE_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGER_H
#include "Logger.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#ifndef _DATAPIPE_H
#include "DataPipe.h"
#endif

#ifndef _ODBCCONNECTIONMANAGER_H
#include "OdbcConnectionManager.h"
#endif

#ifndef _STOPPABLETHREAD_H
#include "StoppableThread.h"
#endif

#ifndef _SQLLOGGINGOBJECT_H
#include "SQLLoggingObject.h"
#endif

//==============================================================

#define LOGDB_TYPE_NORMAL	0x1L
#define LOGDB_TYPE_MSG		0x2L

//==============================================================

class LoggerDatabase : public Logger, public LoggingObject {
public:
	LoggerDatabase(const std::string& strLogName, int iLogLevel);

	virtual int vLog(enum LogLevel, const char *, va_list);
	virtual int vLogBitmapMsg(enum LogLevel eLevel, const BitmapMsg& msg, const char *fmt, va_list ap);

	void SetDSN(const char *szDSN);
	void SetUID(const char *szUID);
	void SetAuthStr(const char *szAuthStr);
	void SetTimeout(DWORD dwTimeout);
	void SetTraceFile(const char *szTraceFile);

	void SetLogTypes(DWORD dwLogTypes);

	void Start();
	void Stop();

	void SetLogger(Logger* pLogger);

private:
	CritSect	m_Lockable;

	class DBThread : public StoppableThread {
	public:
		DBThread();
		DataPipe& GetPipeLog()	{ return m_pipeLog; }
		DataPipe& GetPipeMsgLog()	{ return m_pipeMsgLog; }

		void SetDSN(const char *szDSN);
		void SetUID(const char *szUID);
		void SetAuthStr(const char *szAuthStr);
		void SetTimeout(DWORD dwTimeout);
		void SetTraceFile(const char *szTraceFile);

		BOOL Connect();

		void SetLogger(Logger* pLogger);

	protected:
		DWORD ThreadMemberFunc();

	private:
		DataPipe m_pipeLog;
		DataPipe m_pipeMsgLog;

		DWORD m_dwTimeout;

		mutable boost::shared_ptr<OdbcConnectionManager> m_pConnectionManager;

		BOOL ProcessLogEntries();
		BOOL ProcessMsgLogEntries();
	};

	DBThread m_dbt;
	Event m_evShutdown;
	BOOL m_fConfigured;
	DWORD m_dwLogTypes;
};

//==============================================================

#endif // _LOGGERDATABASE_H
