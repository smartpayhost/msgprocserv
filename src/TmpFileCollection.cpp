#include "stdafx.h"
#include "TmpFileCollection.h"

#include <stdio.h>

TmpFileCollection::~TmpFileCollection()
{
	for(iterator i = begin(); i != end(); i++)
	{
		::remove((*i).c_str());
	}
}
