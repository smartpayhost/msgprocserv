#include "stdafx.h"
#include "MessagePacket.h"

#include "NetworkHeader.h"
#include "Action.h"

//===================================================================

MessagePacket::MessagePacket()
:	m_eAction(Action::NONE)
{
}

//===================================================================

void MessagePacket::SetStyle(BitmapMsgStyle *pStyle)
{
	m_pRequest.SetStyle(pStyle);
	m_pResponse.SetStyle(pStyle);
}

//===================================================================

BOOL MessagePacket::CalcResponseMac()
{
	if(m_pRequest.GetMacKey().empty())
	{
		m_pRequest.CheckMac();
	}

	return m_pResponse.GenResponseMac(m_pRequest.GetMacKey(), m_pRequest.IsMacAnsi(), m_pRequest.IsMac64Bit());
}

//===================================================================
