#include "stdafx.h"
#include "MsgProcServ.h"
#include "version.h"

#include "Win32Service.h"
#include "MsgProcHandler.h"
#include "MsgProcCfg.h"
#include "Registry.h"
#include "MsgProcApp.h"
#include "getopt.h"

#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <conio.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef UNIT_TESTS
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#endif

#include "mdump.h"

using std::string;

//======================================================================

static MsgProcApp l_sApp;

//static MiniDumper l_sMiniDumper("MsgProcServ");	// removed to allow Windows Error Reporting to work

//======================================================================

static BOOL UninstallService()
{
	if(!l_sApp.UninstallService())
		return FALSE;

	string strRegKeyMain = string(REGKEY_ROOT) + l_sApp.GetName();

	Registry::DeleteKey(HKEY_LOCAL_MACHINE, strRegKeyMain.c_str());

	return TRUE;
}

//======================================================================

static BOOL InstallService(const char *szServiceExecutable, 
					const char *szMessageFile,
					const char *szConfigPath)
{
	if(!l_sApp.InstallService(szServiceExecutable))
		return FALSE;

	// setup registry

	string strRegKeyMain = string(REGKEY_ROOT) + l_sApp.GetName();

	if(!Registry::CreateKey(HKEY_LOCAL_MACHINE, strRegKeyMain.c_str()))
	{
		printf("Error creating registry key %s\n", strRegKeyMain.c_str());
	}
	else
	{
		RegistryKey sKey(HKEY_LOCAL_MACHINE, strRegKeyMain.c_str());

		sKey.SetString(REGVAL_CONFIGPATH, szConfigPath);
	}

	string strEventLog = string(REGKEY_EVENTLOGROOT) + l_sApp.GetName();

	if(!Registry::CreateKey(HKEY_LOCAL_MACHINE, strEventLog.c_str()))
	{
		printf("Error creating registry key %s\n", strEventLog.c_str());
	}
	else
	{
		RegistryKey sKey(HKEY_LOCAL_MACHINE, strEventLog.c_str());

		DWORD dwEventTypes = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE;

		sKey.SetString("EventMessageFile", szMessageFile, REG_EXPAND_SZ);
		sKey.SetDWORD("TypesSupported", dwEventTypes);
	}

	return TRUE;
}

//======================================================================

static void Usage(const char *szExe)
{
	printf("%s %s %s\n", szExe, VERSION, COPYRIGHT);
	printf("Usage:\n");
	printf("%s [options] <command>\n", szExe);
	printf("\n");
	printf("[options] can be any of:\n");
	printf("\t-C <configfile>\n");
	printf("\t-N <servicename>\n");
	printf("\n");
	printf("<command> is one of:\n");
	printf("\t install\n");
	printf("\t uninstall\n");
	printf("\t noservice\n");
	printf("\t start\n");
	printf("\t restart\n");
	printf("\t stop\n");
}

//======================================================================

#ifdef _DEBUG
#include <crtdbg.h>


static void AtExit()
{
	printf("Press any key...");
	_getch();
}
#endif

//======================================================================

//CWinApp theApp;
int mainmain(int argc, char **argv, char ** /*env*/)
{
	/*
	if(!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		printf("Fatal Error: MFC initialization failed");
		return 1;
	}
	*/

	switch(l_sApp.InitialSetup(argc, argv))
	{
	case 0:
		break;

	case 1:
		Usage(argv[0]);
		return 0;

	default:
		printf("Failed to setup application");
		return 1;
	}

	// process command or start service

	if(argc > 1)
	{
		if(optind >= argc)
		{
			Usage(l_sApp.GetName().c_str());
			return 1;
		}

		char *szCommand = argv[optind];

		if(_stricmp(szCommand, "noservice") == 0)
		{
			#ifdef _DEBUG
				_CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF );

				extern void DebugMain();

				DebugMain();
				atexit(AtExit);				
			#endif

			l_sApp.Run(FALSE, argc, argv);
		}
		else if(_stricmp(szCommand, "start") == 0)
		{
			printf("Starting service...\n");

			if(l_sApp.StartService())
			{
				printf("Service started.\n");
				return 0;
			}
			else
			{
				printf("Failed to start service.\n");
				return 1;
			}
		}
		else if(_stricmp(szCommand, "restart") == 0)
		{
			printf("Stopping service...\n");

			if(l_sApp.StopService())
			{
				printf("Service stopped.\n");
			}
			else
			{
				printf("Failed to stop service.\n");
				return 1;
			}

			printf("Starting service...\n");

			if(l_sApp.StartService())
			{
				printf("Service started.\n");
				return 0;
			}
			else
			{
				printf("Failed to start service.\n");
				return 1;
			}
		}
		else if(_stricmp(szCommand, "stop") == 0)
		{
			printf("Stopping service...\n");

			if(l_sApp.StopService())
			{
				printf("Service stopped.\n");
				return 0;
			}
			else
			{
				printf("Failed to stop service.\n");
				return 1;
			}
		}
		else if(_stricmp(szCommand, "uninstall") == 0)
		{
			printf("Uninstalling service...\n");

			if(UninstallService())
				return 0;
			else
				return 1;
		}	 
		else if(_stricmp(szCommand, "install") == 0)
		{
			printf("Installing service...\n");

			char exe[_MAX_PATH];
			char conf[_MAX_PATH];

			_fullpath(exe, argv[0], _MAX_PATH);
			_fullpath(conf, l_sApp.GetConfig().c_str(), _MAX_PATH);

			struct _stat statbuf;

			if(_stat(exe, &statbuf))
			{
				printf("Could not find executable %s\n", exe);
				return 1;
			}

			if(_stat(conf, &statbuf))
			{
				printf("Could not find config file %s\n", conf);
				return 1;
			}

			if(InstallService(exe, MSG_DLL_PATH, conf))
				return 0;
			else
				return 1;
		}
		else
		{
			Usage(argv[0]);
			return 1;
		}
	}
	else
	{
		l_sApp.Run(TRUE, argc, argv);
	}

	return 0;
}

#ifndef _UNIT_TESTS
int main(int argc, char **argv, char **env)
{
	return mainmain(argc, argv, env);
}
#endif
