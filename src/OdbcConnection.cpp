#include "stdafx.h"
#include "OdbcEnvironment.h"
#include "OdbcConnection.h"
#include "OdbcStatement.h"

//======================================================================

OdbcConnection::OdbcConnection()
:	m_hDbc(SQL_NULL_HDBC),
	m_fConnected(FALSE)
{
}

//======================================================================

OdbcConnection::~OdbcConnection()
{
	Release();
}

//======================================================================

BOOL OdbcConnection::Alloc()
{
	Log(LOG_DEBUG, "OdbcConnection(%p)::Alloc()", this);

	Release();

	m_hDbc = OdbcEnvironment::AllocConnection();

	return m_hDbc != SQL_NULL_HDBC;
}

//======================================================================

void OdbcConnection::Release()
{
	StatementVect::iterator it;
	
	Lock localLock(&m_Lockable);

	for(it = m_statements.begin(); it != m_statements.end(); ++it)
		(*it)->Release();

	m_statements.clear();

	if(m_hDbc != SQL_NULL_HDBC)
	{
		::SQLDisconnect(m_hDbc);
		::SQLFreeHandle(SQL_HANDLE_DBC, m_hDbc);
		m_hDbc = SQL_NULL_HDBC;
	}

	m_fConnected = FALSE;
}

//======================================================================

void OdbcConnection::Error()
{
	Log(LOG_DEBUG, "OdbcConnection(%p)::Error()", this);

	Release();
}

//======================================================================

BOOL OdbcConnection::HandleResult(SQLRETURN rc)
{
	if(rc == SQL_SUCCESS_WITH_INFO)
	{
		LogSQLInfo(LOG_INFO, m_hDbc);
	}
	else if(rc != SQL_SUCCESS)
	{
		LogSQLError(LOG_ERROR, m_hDbc);
		Error();
		return FALSE;
	}

	return TRUE;
}

//======================================================================

BOOL OdbcConnection::Connect(const char *szDSN,
							 const char *szUID,
							 const char *szAuthStr,
							 const char *szTraceFile)
{
	SZFN(OdbcConnection::Connect);

	Log(LOG_DEBUG, "OdbcConnection(%p)::Connect()", this);

	if(!Alloc())
		return FALSE;

	if(szTraceFile)
	{
		SQLRETURN rc = ::SQLSetConnectAttr(m_hDbc,
			SQL_ATTR_TRACEFILE, (SQLPOINTER)szTraceFile, SQL_NTS);

		if(!HandleResult(rc))
		{
			Log(LOG_ERROR, "%s: SQLSetConnectAttr failed (SQL_OPT_TRACEFILE)", szFn);
			return FALSE;
		}

		rc = ::SQLSetConnectAttr(m_hDbc,
			SQL_ATTR_TRACE, (SQLPOINTER)SQL_OPT_TRACE_ON, SQL_IS_UINTEGER);

		if(!HandleResult(rc))
		{
			Log(LOG_ERROR, "%s: SQLSetConnectAttr failed (SQL_OPT_TRACE)", szFn);
			return FALSE;
		}
	}

	SQLRETURN rc = ::SQLConnect(m_hDbc,
		(UCHAR *)szDSN,		SQL_NTS,
		(UCHAR *)szUID,		SQL_NTS,
		(UCHAR *)szAuthStr,	SQL_NTS);

	if(!HandleResult(rc))
	{
		Log(LOG_ERROR, "%s: SQLConnect failed", szFn);
		return FALSE;
	}

	m_fConnected = TRUE;

	return TRUE;
}

//======================================================================

HSTMT OdbcConnection::AllocStatement()
{
	Log(LOG_DEBUG, "OdbcConnection(%p)::AllocStatement()", this);

	HSTMT hstmt = SQL_NULL_HSTMT;

	if(m_hDbc == SQL_NULL_HDBC)
	{
		LogSQLError(LOG_ERROR, m_hDbc);
	}
	else
	{
		SQLRETURN rc = ::SQLAllocHandle(SQL_HANDLE_STMT, m_hDbc, &hstmt);

		HandleResult(rc);
	}

	return hstmt;
}

//======================================================================

void OdbcConnection::RegisterStatement(OdbcStatement *pStmt)
{
	Log(LOG_DEBUG, "OdbcConnection(%p)::RegisterStatement()", this);

	Lock localLock(&m_Lockable);
	m_statements.insert(pStmt);
}

//======================================================================

void OdbcConnection::UnregisterStatement(OdbcStatement *pStmt)
{
	Log(LOG_DEBUG, "OdbcConnection(%p)::UnregisterStatement()", this);

	Lock localLock(&m_Lockable);
	m_statements.erase(pStmt);
}

//======================================================================
