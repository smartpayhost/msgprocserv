#include "stdafx.h"
#include "DataSourceCollection.h"

#include "DataSourceRuleEntity.h"

#include <assert.h>

using std::string;

//======================================================================

DataSourceCollection::DataSourceCollection()
:	m_pMsgProcRules(NULL),
	m_pLogger(NULL)
{
}

//======================================================================

void DataSourceCollection::SetLogger(Logger *pLogger)
{
	m_pLogger = pLogger;

	DataSourceMap::iterator i;

	for(i = m_mapDataSources.begin(); i != m_mapDataSources.end(); i++)
		(*i).second->SetLogger(m_pLogger);
}

//======================================================================

void DataSourceCollection::SetMsgProcRuleCollection(MsgProcRuleCollection *pArray)
{
	m_pMsgProcRules = pArray;

	DataSourceMap::iterator i;

	for(i = m_mapDataSources.begin(); i != m_mapDataSources.end(); i++)
		(*i).second->SetMsgProcRuleCollection(m_pMsgProcRules);
}

//======================================================================

void DataSourceCollection::Register(RuleFile &rRuleFile)
{
	rRuleFile.RegisterEntityType(this);
}

//======================================================================

void DataSourceCollection::AddDataSource(const DataSource &rDataSource)
{
	boost::shared_ptr<DataSource> p(new DataSource(rDataSource));

	p->SetMsgProcRuleCollection(m_pMsgProcRules);
	p->SetLoggingName(rDataSource.GetName());
	p->SetLogger(m_pLogger);

	m_mapDataSources[rDataSource.GetName()] = p;
}

//======================================================================

BOOL DataSourceCollection::Export(string &rStrDest)
{
	DataSourceMap::const_iterator i;

	for(i = m_mapDataSources.begin(); i != m_mapDataSources.end(); i++)
	{
		string strTmp;

		BOOL fRetval = (*i).second->Export(strTmp);

		if(!fRetval)
			return FALSE;

		rStrDest += strTmp;
		rStrDest += '\n';
	}

	rStrDest += '\n';

	return TRUE;
}

//======================================================================

void DataSourceCollection::MergeFrom(const DataSourceCollection& rhs)
{
	for(DataSourceMap::const_iterator i = rhs.m_mapDataSources.begin(); i != rhs.m_mapDataSources.end(); i++)
	{
		AddDataSource(*((*i).second));
	}
}

//======================================================================

BOOL DataSourceCollection::IsStart(const Line &rLine) const
{
	if(rLine.size() != 2)
		return FALSE;

	if(rLine[0] == "DATA_SOURCE")
		return TRUE;

	return FALSE;
}

//======================================================================

RuleEntity* DataSourceCollection::MakeNew(const Line &rLine)
{
	DataSourceRuleEntity *pResult;

	pResult = new DataSourceRuleEntity;

	if(!pResult)
		return NULL;

	pResult->SetName(rLine[1]);

	pResult->SetDataSourceCollection(this);

	return pResult;
}

//======================================================================

const DataSource* DataSourceCollection::FindByName(const string& strName) const
{
	DataSourceMap::const_iterator i;

	i = m_mapDataSources.find(strName);

	if(i == m_mapDataSources.end())
		return NULL;
	else
		return (*i).second.get();
}

//======================================================================

BOOL DataSourceCollection::ConnectAll()
{
	DataSourceMap::iterator i;

	for(i = m_mapDataSources.begin(); i != m_mapDataSources.end(); i++)
	{
		if(!(*i).second->Connect())
			return FALSE;
	}

	return TRUE;
}

//======================================================================
