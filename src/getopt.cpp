#include "stdafx.h"

#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#define ERR(s, c)	if(opterr){\
	fputs(argv[0], stderr);\
	fputs(s, stderr);\
	fputc(c, stderr);\
	fputc('\n', stderr);}

int	opterr = 1;
int	optind = 1;
int	optopt;
char *optarg;

int getopt(int argc, char **argv, char *opts)
{
    static int sp = 1;
    static char **oldarg = NULL;
    int c;
    char *cp;

    if(oldarg != argv)	// new set of args to parse
    {
		sp = 1;
		optind = 1;
		opterr = 1;
		optopt = 0;
		oldarg = argv;
		optarg = NULL;
    }

    if(sp == 1)
    {
		if(optind >= argc || argv[optind][0] != '-' || argv[optind][1] == 0)
		{
			return EOF;
		}
		else if(strcmp(argv[optind], "--") == 0)
		{
			optind++;
			return EOF;
		}
    }

    optopt = c = argv[optind][sp];

    if(c == ':' || (cp=strchr(opts, c)) == NULL)
    {
		ERR (": illegal option -- ", c);

		if(argv[optind][++sp] == 0)
		{
			optind++;
			sp = 1;
		}

		return '?';
    }

    if(*++cp == ':')						// argument required
    {
		if(argv[optind][sp+1] != 0)			// argument joined to option character
		{
			optarg = &argv[optind++][sp+1];
		}
		else if (++optind >= argc)
		{
			ERR (": option requires an argument -- ", c);
			sp = 1;
			return '?';
		}
		else								// argument separated by space
		{
			optarg = argv[optind++];
		}

		sp = 1;
    }
    else
    {
		if(argv[optind][++sp] == '\0')		// if option by itself, move to next
		{
			sp = 1;
			optind++;
		}

		optarg=NULL;
    }

    return c;
}
