#include "stdafx.h"
#include "HostDescription.h"

#include "StrUtil.h"

using std::string;

//======================================================================

HostDescription::HostDescription()
:	m_eHeaderType(NetworkHeaderType::NHT_10BYTE),
	m_pMsgProcRules(NULL)
{
}

//======================================================================

BOOL HostDescription::Export(string &rStrDest)
{
	string strTmp;

	rStrDest = "UPSTREAM_HOST ";
	StrUtil::PurifyString(GetName(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "\tADDRESS ";
	StrUtil::PurifyString(GetAddr(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	rStrDest += "\tPORT ";
	StrUtil::PurifyString(GetPort(), strTmp);
	rStrDest += strTmp;
	rStrDest += '\n';

	switch(m_eHeaderType)
	{
	case NetworkHeaderType::NHT_LENTPDU:
		rStrDest += "\tHEADER_TYPE LENTPDU\n";
		break;

	case NetworkHeaderType::NHT_10BYTE:
	default:
		// output nothing
		break;
	}

	rStrDest += "UPSTREAM_HOST_END\n";

	return TRUE;
}

//======================================================================
