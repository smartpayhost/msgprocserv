#ifndef _MSGPROCRULEFILE_H
#define _MSGPROCRULEFILE_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _RULEFILE_H
#include "RuleFile.h"
#endif

#ifndef _MSGPROCRULECOLLECTION_H
#include "MsgProcRuleCollection.h"
#endif

class Logger;

//======================================================================

class MsgProcRuleFile : public RuleFile {
public:
	MsgProcRuleFile();

	virtual RuleFile* Clone() const;
	virtual void MergeFrom(const RuleFile* rfFrom);

	void Export(std::string &rStrDest);

	const MatchTemplateCollection&		GetMatchTemplateCollection() const		{ return m_rc.GetMatchTemplateCollection(); }
	const ReplyTemplateCollection&		GetReplyTemplateCollection() const		{ return m_rc.GetReplyTemplateCollection(); }
	const HostDescriptionCollection&	GetHostDescriptionCollection() const	{ return m_rc.GetHostDescriptionCollection(); }
	const DataSourceCollection&			GetDataSourceCollection() const			{ return m_rc.GetDataSourceCollection(); }
	const SQLMatchQueryCollection&		GetSQLMatchQueryCollection() const		{ return m_rc.GetSQLMatchQueryCollection(); }
	const SQLActionQueryCollection&		GetSQLActionQueryCollection() const		{ return m_rc.GetSQLActionQueryCollection(); }
	const SQLReplyQueryCollection&		GetSQLReplyQueryCollection() const		{ return m_rc.GetSQLReplyQueryCollection(); }

	MatchTemplateCollection&	GetMatchTemplateCollection()	{ return m_rc.GetMatchTemplateCollection(); }
	ReplyTemplateCollection&	GetReplyTemplateCollection()	{ return m_rc.GetReplyTemplateCollection(); }
	HostDescriptionCollection&	GetHostDescriptionCollection()	{ return m_rc.GetHostDescriptionCollection(); }
	DataSourceCollection&		GetDataSourceCollection()		{ return m_rc.GetDataSourceCollection(); }
	SQLMatchQueryCollection&	GetSQLMatchQueryCollection()	{ return m_rc.GetSQLMatchQueryCollection(); }
	SQLActionQueryCollection&	GetSQLActionQueryCollection()	{ return m_rc.GetSQLActionQueryCollection(); }
	SQLReplyQueryCollection&	GetSQLReplyQueryCollection()	{ return m_rc.GetSQLReplyQueryCollection(); }

	virtual void SetLogger(Logger *pLogger);

	virtual BOOL Check();

private:
	MsgProcRuleCollection m_rc;
};

//======================================================================

#endif // _MSGPROCRULEFILE_H
