#ifndef _LOGDEFAULTS_H
#define _LOGDEFAULTS_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//==============================================================

#define LOGPATH "c:\\MsgProcServ.log"
#define LOGLEVEL LOG_DEBUG
#define LOGNAME "MsgProcServ"

//==============================================================

#endif // _LOGDEFAULTS_H
