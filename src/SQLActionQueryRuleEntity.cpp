#include "stdafx.h"
#include "SQLActionQueryRuleEntity.h"

#include "SQLActionQueryCollection.h"
#include <assert.h>

using std::string;

//======================================================================

SQLActionQueryRuleEntity::SQLActionQueryRuleEntity()
:	m_fComplete(FALSE),
	m_pCollection(NULL)
{
}

//======================================================================

void SQLActionQueryRuleEntity::SetName(const string &strName)
{
	m_dsSQLActionQuery.SetName(strName);
}

//======================================================================

void SQLActionQueryRuleEntity::SetSQLActionQueryCollection(SQLActionQueryCollection *pCollection)
{
	m_pCollection = pCollection;
}

//----------------------------------------------------------------------

BOOL SQLActionQueryRuleEntity::ProcessDataSource(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsSQLActionQuery.SetDataSource(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL SQLActionQueryRuleEntity::ProcessQuery(const Line &rLine)
{
	if(rLine.size() < 2)
		return FALSE;

	m_dsSQLActionQuery.SetQuery(rLine[1]);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL SQLActionQueryRuleEntity::ProcessMatchQueryEnd(const Line & /*rLine*/)
{
	m_fComplete = TRUE;

	assert(m_pCollection);
	m_pCollection->AddSQLActionQuery(m_dsSQLActionQuery);

	return TRUE;
}

//----------------------------------------------------------------------

BOOL SQLActionQueryRuleEntity::ProcessLine(const Line &rLine)
{
	if(rLine[0] == "DATA_SOURCE")
	{
		return ProcessDataSource(rLine);
	}
	else if(rLine[0] == "QUERY")
	{
		return ProcessQuery(rLine);
	}
	else if(rLine[0] == "ACTION_QUERY_END")
	{
		return ProcessMatchQueryEnd(rLine);
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL SQLActionQueryRuleEntity::IsComplete() const
{
	return m_fComplete;
}

//======================================================================
