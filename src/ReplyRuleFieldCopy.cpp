#include "stdafx.h"
#include "ReplyRuleFieldCopy.h"

#include <assert.h>

using std::string;

//======================================================================

ReplyRuleFieldCopy::ReplyRuleFieldCopy(int iFieldNum, const BitmapMsgStyle *pStyle)
:	ReplyRuleField(iFieldNum, pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleFieldCopy::Clone() const
{
	return new ReplyRuleFieldCopy(*this);
}

//======================================================================
//
//	FIELD <field_num>
//	FIELD <field_num> COPY

BOOL ReplyRuleFieldCopy::Import(const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "FIELD") return FALSE;

	if(++i == rLine.end()) return FALSE;

	int iFieldNum = (WORD)strtol((*i).c_str(), NULL, 10);

	if((iFieldNum < 1)||(iFieldNum > 128))
		return FALSE;

	SetFieldNum(iFieldNum);

	if(++i == rLine.end()) return TRUE;

	if((*i) == "COPY")
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//======================================================================

BOOL ReplyRuleFieldCopy::Export(string &strDesc)
{
	StrUtil::Format(strDesc, "FIELD %d COPY", GetFieldNum());

	return TRUE;
}

//======================================================================

BOOL ReplyRuleFieldCopy::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	const BitmapMsgField *pOrigField = rOrigMsg.GetField(GetFieldNum());

	if(!pOrigField)
		return TRUE;

	BitmapMsgField *pField;

	pField = new BitmapMsgField(GetFieldNum(), rOrigMsg.GetMsgStyle());

	assert(pField);

	if(!pField)
		return FALSE;

	if(!pField->SetFieldFromString(pOrigField->GetAsciiRep()))
	{
		delete pField;
		return FALSE;
	}

	rNewMsg.SetField(pField);

	return TRUE;
}

//======================================================================
