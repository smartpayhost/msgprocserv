#ifndef _ODBCENVIRONMENT_H
#define _ODBCENVIRONMENT_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SQLLOGGINGOBJECT_H
#include "SQLLoggingObject.h"
#endif

#ifndef _SYNCHRO_H
#include "synchro.h"
#endif

#include <boost/utility.hpp>
#include <boost/smart_ptr.hpp>


class OdbcConnection;

//======================================================================

class OdbcEnvironment : public SQLLoggingObject, public boost::noncopyable {
public:
	~OdbcEnvironment();

	static HENV HEnv();
	static HDBC AllocConnection();

private:
	HENV m_hEnv;

	CritSect m_crit;

private:
	OdbcEnvironment();	// singleton, can only be instantiated inside instance()
	BOOL Alloc();
	void Release();
	
	static OdbcEnvironment& instance();

	HDBC impAllocConnection();
};

//======================================================================

#endif // _ODBCENVIRONMENT_H
