#ifndef _FORMAT_H
#define _FORMAT_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>

//==============================================================

class Format {
public:
	static void Buf2String(int iLen, const BYTE *pData, std::string &strDest, int iMinStringSize = 1);
	static void String2Buf(const std::string &csSrc, int *piLen, BYTE *pData);
	static void Buf2HexDump(int iLen, const BYTE *pData, std::string& strDest, int iWidth = 16);
};

//==============================================================

#endif //_FORMAT_H
