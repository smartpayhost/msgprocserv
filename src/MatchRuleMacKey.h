#ifndef _MATCHRULEMACKEY_H
#define _MATCHRULEMACKEY_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _MATCHRULE_H
#include "MatchRule.h"
#endif

#include <set>
#include <string>

class Message;
class BitmapMsg;

//==============================================================

class MatchRuleMacKey : public MatchRule {
public:
	MatchRuleMacKey();

	MatchRule* Clone() const;

	BOOL Check() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL DoesMatch(Message& rMsg) const;

private:
	BOOL GetKeysFromSQL(const std::string& strQuery, const BitmapMsg *pMsg, std::set<std::string>& Keys) const;
	BOOL GetKeysFromFile(const std::string& strFile, std::set<std::string>& Keys) const;

private:

	enum MATCH_WITH {
		MRMK_FIXED_DATA,
		MRMK_FILE,
		MRMK_SQL
	};

	MATCH_WITH m_eWith;

	std::string		m_strMatchData;
};

//==============================================================

#endif // _MATCHRULEMACKEY_H
