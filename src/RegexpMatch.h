#ifndef _REGEXPMATCH_H
#define _REGEXPMATCH_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>

//==============================================================

BOOL RegexpMatch(const std::string& strMatchData, const std::string& strTest);

//==============================================================

#endif // _REGEXPMATCH_H
