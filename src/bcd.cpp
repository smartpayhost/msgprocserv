#include "stdafx.h"
#include "bcd.h"

DWORD BCD::PackedBcd2Bin(DWORD dwBCD)
{
	DWORD dwBin;

	dwBin = 
		(((dwBCD >> 28) & 0x0F) * 10000000L) +
		(((dwBCD >> 24) & 0x0F) *  1000000L) +
		(((dwBCD >> 20) & 0x0F) *   100000L) +
		(((dwBCD >> 16) & 0x0F) *    10000L) +
		(((dwBCD >> 12) & 0x0F) *     1000L) +
		(((dwBCD >>  8) & 0x0F) *      100L) +
		(((dwBCD >>  4) & 0x0F) *       10L) +
		(((dwBCD      ) & 0x0F) *        1L);

	return dwBin;
}

DWORD BCD::Bin2PackedBcd(DWORD dwBin)
{
	DWORD dwBCD;

	dwBCD = 0L;

	dwBCD =
		(((dwBin / 10000000L) % 10) << 28L) +
		(((dwBin /  1000000L) % 10) << 24L) +
		(((dwBin /   100000L) % 10) << 20L) +
		(((dwBin /    10000L) % 10) << 16L) +
		(((dwBin /     1000L) % 10) << 12L) +
		(((dwBin /      100L) % 10) <<  8L) +
		(((dwBin /       10L) % 10) <<  4L) +
		(((dwBin            ) % 10)       );

	return dwBCD;
}

BYTE BCD::AscDigit2Bin(const char cChar)
{
	if(isdigit(cChar))				// handle 0-9
		return (BYTE)(cChar - '0');

	if(isxdigit(cChar))				// handle a-f, A-F
		return (BYTE)(tolower(cChar) - 'a' + 10);

	return (BYTE)(cChar - '0');	// handle <=>?;: for magstripe data
}

DWORD BCD::AscDec2Bin(const char *pcAsc, int iLen)
{
	DWORD dwBin;

	dwBin = 0;

	while(iLen--)
	{
		dwBin *= 10;
		dwBin += AscDigit2Bin(*pcAsc++);
	}

	return dwBin;
}

DWORD BCD::AscHex2Bin(const char *pcAsc, int iLen)
{
	DWORD dwBin;

	dwBin = 0;

	while(iLen--)
	{
		dwBin *= 16;
		dwBin += AscDigit2Bin(*pcAsc++);
	}

	return dwBin;
}

void BCD::Asc2PackedBcd(const char *pcAsc, BYTE *pBcd, int iLen)
{
	for(int i = 0; i < iLen; i++)
	{
		pBcd[i] = (BYTE)(
			(BCD::AscDigit2Bin(pcAsc[2*i]) << 4) +
			BCD::AscDigit2Bin(pcAsc[2*i+1]));
	}
}

char BCD::HexDigit2Asc(BYTE bHex)
{
	bHex &= 0x0F;

	if(bHex <= 9)
		return (char)(bHex + '0');
	else
		return (char)(bHex - 0x0A + 'A');
}

void BCD::Hex2Asc(const BYTE *pHex, char *pcAsc, int iLen)
{
	for(int i = 0; i < iLen; i++)
	{
		pcAsc[i*2    ] = HexDigit2Asc((BYTE)(pHex[i] >> 4));
		pcAsc[i*2 + 1] = HexDigit2Asc((BYTE)(pHex[i] & 0x0F));
	}
}

void BCD::Bin2LLBCD(DWORD dwBin, BYTE *pcMsg)
{
	DWORD dwBCD = BCD::Bin2PackedBcd(dwBin);
	pcMsg[0] = (BYTE)(dwBCD & 0xFF);
}

void BCD::Bin2LLLBCD(DWORD dwBin, BYTE *pcMsg)
{
	DWORD dwBCD = BCD::Bin2PackedBcd(dwBin);
	pcMsg[0] = (BYTE)((dwBCD >> 8) & 0x0F);
	pcMsg[1] = (BYTE)(dwBCD & 0xFF);
}

void BCD::Bin2LLAsc(DWORD dwBin, BYTE *pcMsg)
{
	DWORD dwBCD = BCD::Bin2PackedBcd(dwBin);
	pcMsg[0] = (BYTE)(((dwBCD >> 4) & 0x0F) + '0');
	pcMsg[1] = (BYTE)(((dwBCD     ) & 0x0F) + '0');
}

void BCD::Bin2LLLAsc(DWORD dwBin, BYTE *pcMsg)
{
	DWORD dwBCD = BCD::Bin2PackedBcd(dwBin);
	pcMsg[0] = (BYTE)(((dwBCD >> 8) & 0x0F) + '0');
	pcMsg[1] = (BYTE)(((dwBCD >> 4) & 0x0F) + '0');
	pcMsg[2] = (BYTE)(((dwBCD     ) & 0x0F) + '0');
}

DWORD BCD::LLBCD2Bin(const BYTE *pcMsg)
{
	DWORD dwBCD = (DWORD)(pcMsg[0]) & 0x000000FFL;
	return BCD::PackedBcd2Bin(dwBCD);
}

DWORD BCD::LLLBCD2Bin(const BYTE *pcMsg)
{
	DWORD dwBCD;
	dwBCD = (DWORD)(pcMsg[0]);
	dwBCD <<= 8;
	dwBCD += (DWORD)(pcMsg[1]);
	dwBCD &= 0x00000FFFL;

	return BCD::PackedBcd2Bin(dwBCD);
}

DWORD BCD::LLAsc2Bin(const BYTE *pcMsg)
{
	return BCD::AscDec2Bin((char *)pcMsg, 2);
}

DWORD BCD::LLLAsc2Bin(const BYTE *pcMsg)
{
	return BCD::AscDec2Bin((char *)pcMsg, 3);
}

