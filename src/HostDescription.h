#ifndef _HOSTDESCRIPTION_H
#define _HOSTDESCRIPTION_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

#ifndef _NETWORKHEADERTYPE_H
#include "NetworkHeaderType.h"
#endif

class MsgProcRuleCollection;

//======================================================================

class HostDescription : public LoggingObject 
{
public:
	HostDescription();

	BOOL Export(std::string &rStrDest);

	const std::string GetName()	const { return m_strName; }
	const std::string GetAddr()	const { return m_strAddr; }
	const std::string GetPort()	const { return m_strPort; }
	NetworkHeaderType GetHeaderType()  const { return m_eHeaderType; }

	void SetName(const std::string &strName)	{ m_strName = strName; }
	void SetAddr(const std::string &strAddr)	{ m_strAddr = strAddr; }
	void SetPort(const std::string &strPort)	{ m_strPort = strPort; }
	void SetHeaderType(NetworkHeaderType eHeaderType)	{ m_eHeaderType = eHeaderType; }

	inline const MsgProcRuleCollection *GetMsgProcRuleCollection() const { return m_pMsgProcRules; }
	inline void SetMsgProcRuleCollection(MsgProcRuleCollection *pArray) { m_pMsgProcRules = pArray; }

private:
	std::string m_strName;
	std::string m_strAddr;
	std::string m_strPort;
	NetworkHeaderType m_eHeaderType;

	MsgProcRuleCollection *m_pMsgProcRules;
};

//======================================================================

#endif // _HOSTDESCRIPTION_H
