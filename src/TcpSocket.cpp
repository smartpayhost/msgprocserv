#include "stdafx.h"
#include "TcpSocket.h"

#include <sstream>

using std::string;
using std::ostringstream;

//----------------------------------------------------------------------------------

void TcpSocket::SetHost(const string& strHostname, const string& strPort)
{
	m_strHost = strHostname;
	m_strPort = strPort;
}

//----------------------------------------------------------------------------------

SOCKRC TcpSocket::Open()
{
	SOCKET result;

	result = ::socket(PF_INET, SOCK_STREAM, 0);

	if(result == INVALID_SOCKET)
		return SOCKRC_ERROR;

	m_shSocket = result;

	if(SetNagle() != SOCKRC_OK)
		return SOCKRC_ERROR;

	if(SetBlocking(TRUE) != SOCKRC_OK)
		return SOCKRC_ERROR;

	return SOCKRC_OK;
}

//----------------------------------------------------------------------------------

SOCKRC TcpSocket::Connect()
{
	int result;
	struct sockaddr_in sin;

	sin.sin_family = AF_INET;
	if((sin.sin_addr.s_addr = ParseAddr(m_strHost)) == 0L)
	{
		return SOCKRC_ERROR;
	}

	if((sin.sin_port = ParsePort(m_strPort, "tcp")) == 0)
	{
		return SOCKRC_ERROR;
	}

	if(GetBlocking())
	{
		result = ::connect(m_shSocket, (struct sockaddr *)&sin, sizeof(sin));
	}
	else
	{
		SetBlocking(TRUE);
		result = ::connect(m_shSocket, (struct sockaddr *)&sin, sizeof(sin));

		if(result == SOCKET_ERROR)
		{
			result = ::WSAGetLastError();
			SetBlocking(FALSE);
			::WSASetLastError(result);
			result = SOCKET_ERROR;
		}
		else
		{
			SetBlocking(FALSE);
		}
	}

	if(result == SOCKET_ERROR)
		return SOCKRC_ERROR;

	SetConnected(TRUE);
	return SOCKRC_OK;
}

//----------------------------------------------------------------------------------

SOCKRC TcpSocket::Connect(const string& strHostname, const string& strPort)
{
	SetHost(strHostname, strPort);
	return Connect();
}

//----------------------------------------------------------------------------------

SOCKRC TcpSocket::Listen(const string& strPort)
{
	struct sockaddr_in sin;

	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = 0L;

	if((sin.sin_port = ParsePort(strPort, "tcp")) == 0)
	{
		return SOCKRC_ERROR;
	}

	if(::bind(m_shSocket, (struct sockaddr *)&sin, sizeof(sin)) == SOCKET_ERROR)
	{
		return SOCKRC_ERROR;
	}

	if(::listen(m_shSocket, 5) == SOCKET_ERROR)
	{
		return SOCKRC_ERROR;
	}
	
	return SOCKRC_OK;
}

//----------------------------------------------------------------------------------

SOCKET TcpSocket::Accept()
{
	return ::accept(m_shSocket, NULL, NULL);
}

//----------------------------------------------------------------------------------

SOCKET TcpSocket::Accept(unsigned long timeout)
{
	int result;

	result = WaitReadable(timeout);

	if(result != SOCKRC_OK)
		return INVALID_SOCKET;

	return Accept();
}

//----------------------------------------------------------------------------------

SOCKRC TcpSocket::SetNodelay()
{
	int true_int = 1;

	if(::setsockopt(m_shSocket, IPPROTO_TCP, TCP_NODELAY, 
		(char *)&true_int, sizeof(true_int)) == SOCKET_ERROR)
	{
		return SOCKRC_ERROR;
	}

	return SOCKRC_OK;
}

//----------------------------------------------------------------------------------

SOCKRC TcpSocket::SetNagle()
{
	int false_int = 0;

	if(::setsockopt(m_shSocket, IPPROTO_TCP, TCP_NODELAY, 
		(char *)&false_int, sizeof(false_int)) == SOCKET_ERROR)
	{
		return SOCKRC_ERROR;
	}

	return SOCKRC_OK;
}

//----------------------------------------------------------------------------------

string TcpSocket::GetPeerName()
{
	struct sockaddr_in name;
	int namelen = sizeof(name);
	char* nbuf;

	if(::getpeername(m_shSocket, (struct sockaddr *)&name, &namelen) != 0)
		return "";

	nbuf = inet_ntoa(name.sin_addr);

	if(!nbuf)
		return "";

	ostringstream os;

	os << nbuf << ":" << name.sin_port;

	return os.str();
}

//----------------------------------------------------------------------------------
