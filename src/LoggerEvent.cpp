#include "stdafx.h"
#include "LoggerEvent.h"

#include "LogDefaults.h"
#include "jpmsgdll.h"
#include "synchro.h"

#include <stdio.h>
#include <process.h>

#ifdef UNIT_TESTS
#include <boost/test/unit_test.hpp>
#endif

using std::string;

//======================================================================

LoggerEvent::LoggerEvent(const string& strLogName, int iLogLevel)
:	Logger(strLogName, iLogLevel)
{
}

//======================================================================

int LoggerEvent::vLog(enum LogLevel level, const char *fmt, va_list ap)
{
	Lock localLock(&m_Lockable);

	if(GetLogLevel() < level)
		return 0;

	//---------------------------------------------------------------

	char szBuf[1024];
	WORD wType;
	DWORD dwEventId;

	char *pPtr = szBuf;
	szBuf[sizeof(szBuf) - 1] = 0;
	int iBufLeft = sizeof(szBuf) - 1;
 
	int iLen = _snprintf_s(szBuf, sizeof(szBuf), _TRUNCATE, "%s[%d]: ", GetLogName(), _getpid());

	iBufLeft -= iLen;
	pPtr += iLen;

	_vsnprintf_s(pPtr, iBufLeft, _TRUNCATE, fmt, ap);

	//---------------------------------------------------------------

	if(level <= LOG_ERROR)
	{
		wType = EVENTLOG_ERROR_TYPE;
		dwEventId = MSG_ERROR;
	}
	else if(level <= LOG_WARNING)
	{
		wType = EVENTLOG_WARNING_TYPE;
		dwEventId = MSG_WARNING;
	}
	else
	{
		wType = EVENTLOG_INFORMATION_TYPE;
		dwEventId = MSG_INFO;
	}


	HANDLE hSource;

	hSource = ::RegisterEventSource(NULL, GetLogName());

	if(hSource)
	{
		const char *pszStrings[1];

		pszStrings[0] = szBuf;

		::ReportEvent(hSource,
			wType,
			0,
			dwEventId,
			NULL,
			1,
			0,
			pszStrings,
			NULL);

		::DeregisterEventSource(hSource);
	}

	return 0;
}

//======================================================================

#ifdef UNIT_TESTS

BOOST_AUTO_TEST_CASE(LoggerEvent__Log)
{
	char buf[500];
	int i;

	LoggerEvent theLogger("test", LOG_DEBUG);


	for(i = 0; i < (sizeof(buf)/sizeof(buf[0])); i++)
	{
		buf[i] = (char)i;
//		buf[i] = (char)'j';
		if(!buf[i])
			buf[i] = ' ';
	}
	
	buf[sizeof(buf) - 1] = 0;

//	theLogger.Log(LOG_DEBUG, "%s", &buf[0]);
	((Logger*)&theLogger)->Log(LOG_DEBUG, "%d %s", 1, buf);	// extra param needed to select correct overload
	BOOST_CHECK(true);
}

#endif
