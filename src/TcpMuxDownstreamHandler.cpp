#include "stdafx.h"
#include "TcpMuxDownstreamHandler.h"

#include "MuxUpstreamThread.h"
#include "TcpMuxThread.h"

//===========================================================

TcpMuxDownstreamHandler::TcpMuxDownstreamHandler(TcpMuxThread *pParent)
:	m_pUpstream(NULL),
	m_ppipe_mux_up(NULL),
	m_bIndex(0),
	m_bInstance(0),
	m_pParent(pParent),
	m_dwIdleTimeout(INFINITE)
{
}

//===========================================================

void TcpMuxDownstreamHandler::SetSocket(SOCKET s)
{
	m_s.SetSocketHandle(s);
	m_s.SetBlocking(TRUE);
	m_th.SetSocket(&m_s);
	m_peer = m_s.GetPeerName();
}

//-----------------------------------------------------------

void TcpMuxDownstreamHandler::SetPipeDst(DataPipe *pPipe)
{
	m_ppipe_mux_up = pPipe;
}

//-----------------------------------------------------------

DataPipe& TcpMuxDownstreamHandler::GetPipeSrc()
{
	return m_mdt.GetPipeUpSrc();
}

//===========================================================

void TcpMuxDownstreamHandler::SetThreadNumber(BYTE bIndex, BYTE bInstance)
{
	m_bIndex = bIndex;
	m_bInstance = bInstance;

	m_mdt.SetThreadNumber(m_bIndex, m_bInstance);
}

//===========================================================

BOOL TcpMuxDownstreamHandler::AttachToUpstream(MuxUpstreamThread *tmuThread)
{
	m_pUpstream = tmuThread;

	return m_pUpstream->AttachDownstream(this);
}

//===========================================================

void TcpMuxDownstreamHandler::SetIdleTimeout(DWORD dwTimeout)
{
	m_dwIdleTimeout = dwTimeout;
}

//===========================================================

DWORD TcpMuxDownstreamHandler::ThreadMemberFunc()
{
	SZFN(TcpMuxDownstreamHandler);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if(	(m_pUpstream == NULL) ||
		(m_ppipe_mux_up == NULL))
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	m_th.SetPipeDst(&m_nt.GetPipeSrc());		// TcpHandler sends upstream to NH1BlkThread
	m_nt.SetPipeDst(&m_mdt.GetPipeDnSrc());		// NH1BlkThread sends upstream to MuxDownstreamThread
	m_mdt.SetPipeUpDst(m_ppipe_mux_up);			// MuxDownstreamThread sends upstream to MuxUpstreamThread
	m_mdt.SetPipeDnDst(&m_th.GetPipeSrc());		// MuxDownstreamThread sends downstream to TcpHandler (no need for NH1BlkThread on downstream leg)

	AddChild(&m_th);
	AddChild(&m_nt);
	AddChild(&m_mdt);

	m_th.SetActivityEvent(&m_evDownstreamActivity);	// we want to monitor activity so we can detect idle connections and tear them down.

	SetupChildren();

	if(!m_th.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 2: failed to start tcp handler", szFn);
		ShutdownChildren();
		return 2;
	}

	if(!m_nt.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 3: failed to start nh1 handler", szFn);
		ShutdownChildren();
		return 3;
	}

	if(!m_mdt.StartThread())
	{
		Log(LOG_ERROR, "%s: ending, error 4: failed to start mdt handler", szFn);
		ShutdownChildren();
		return 4;
	}

	WaitableCollection wc;
	
	wc.push_back(GetShutdownEvent());
	wc.push_back(m_th);
	wc.push_back(m_nt);
	wc.push_back(m_mdt);
	wc.push_back(m_evDownstreamActivity);

	BOOL fExit = FALSE;

	while(!fExit)
	{
		DWORD e = wc.Wait(m_dwIdleTimeout);

		fExit = TRUE;

		switch(e)
		{
		case WAIT_OBJECT_0 + 1:
			Log(LOG_DEBUG, "%s: TCP handler child died", szFn);
			break;

		case WAIT_OBJECT_0 + 2:
			Log(LOG_DEBUG, "%s: NH1 handler child died", szFn);
			break;

		case WAIT_OBJECT_0 + 3:
			Log(LOG_DEBUG, "%s: MuxDownstreamThread handler child died", szFn);
			break;

		case WAIT_OBJECT_0 + 4:
			Log(LOG_DEBUG, "%s: activity detected", szFn);
			fExit = FALSE;
			break;

		case WAIT_OBJECT_0 + 0:
			Log(LOG_DEBUG, "%s: shutdown requested", szFn);
			break;

		case WAIT_TIMEOUT:
			Log(LOG_INFO, "%s: no activity detected for %lu seconds, closing downstream connection", szFn, m_dwIdleTimeout / 1000UL);
			break;

		default:
			Log(LOG_ERROR, "%s: unexpected WaitForMultipleObjects result", szFn);
			break;
		}
	}

	Log(LOG_DEBUG, "%s: shutting down children", szFn);

	m_pUpstream->DetachDownstream(m_bIndex);

	ShutdownChildren();

	m_s.Close();

	m_pParent->DownstreamEnding(this);

	Log(LOG_DEBUG, "%s: ending", szFn);

	return 0;
}
