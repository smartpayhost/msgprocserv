#ifndef _NETWORKHEADER_H
#define _NETWORKHEADER_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//==============================================================

class NetworkHeader {
public:
	NetworkHeader();

	BOOL IsShortHeader();
	void SetHeaderType(BOOL fShortHdr);	
	WORD GetNormalHeaderLength();
	WORD GetShortHeaderLength();	

	BOOL CopyFrom(const BYTE *pBuf, BOOL fShortHdr=FALSE);
	void CopyTo(BYTE *pBuf, BOOL fCheckForShortHdr=FALSE, BOOL fUseNormalHdr=FALSE);

	void SetDataLen(WORD wLen) { m_wDataLen = wLen; }
	WORD GetDataLen() { return m_wDataLen; }

	void SetThread(WORD wThread) { m_wThread = wThread; }
	WORD GetThread() { return m_wThread; }

	void SetConnection(WORD wConnection) { m_wConnection = wConnection; }
	WORD GetConnection() { return m_wConnection; }	

private:
	WORD	m_wHdrType;
	WORD	m_wHdrLen;
	WORD	m_wDataLen;
	WORD	m_wThread;
	WORD	m_wConnection;

	static const int SHORT_NETWORK_HDR;
	static const int NORMAL_NETWORK_HDR;
};

//==============================================================

#endif // _NETWORKHEADER_H
