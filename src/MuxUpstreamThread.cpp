#include "stdafx.h"
#include "MuxUpstreamThread.h"
#include "TcpMuxDownstreamHandler.h"

#include "ETSL6Header.h"
#include "ChinaUnionPayHeader.h"
#include "Format.h"
#include "MessagePacket.h"
#include "NetworkHeader.h"

#include <vector>
#include <string>

using std::string;
using std::vector;

//================================================================

MuxUpstreamThread::MuxUpstreamThread(MsgProcCfg &config)
:	m_pPipeUpDst(NULL),
	m_iMaxThreads(MAX_MUXDOWNSTREAMTHREADS),
	m_iTableFree(MAX_MUXDOWNSTREAMTHREADS),
	m_bIndex(0),
	m_bInstance(0),
	m_config(config)
{
	for(int i = 0; i < MAX_MUXDOWNSTREAMTHREADS; i++)
		m_pPipeDnDstTable[i] = NULL;
}

//================================================================

BOOL MuxUpstreamThread::SetMaxThreads(int iMaxThreads)
{
	if(iMaxThreads > MAX_MUXDOWNSTREAMTHREADS)
		return FALSE;
	
	m_iMaxThreads = iMaxThreads;
	m_iTableFree = m_iMaxThreads;

	return TRUE;
}

//================================================================

DataPipe& MuxUpstreamThread::GetPipeUpSrc()
{
	return m_pipeUpSrc;
}

//----------------------------------------------------------------

void MuxUpstreamThread::SetPipeUpDst(DataPipe *pPipe)
{
	m_pPipeUpDst = pPipe;
}

//----------------------------------------------------------------

BOOL MuxUpstreamThread::SetPipeDnDst(DataPipe *pPipe, BYTE &bIndex, BYTE &bInstance)
{
	if(pPipe == NULL)
		return FALSE;

	// Lock downstream pipe table

	m_lockPipeDnDstTable.Lock();

	// Find next empty position in table, if any

	if(m_iTableFree == 0)
	{
		m_lockPipeDnDstTable.Unlock();
		return FALSE;
	}

	while(m_pPipeDnDstTable[m_bIndex] != NULL)
	{
		m_bIndex++;
		if(m_bIndex == m_iMaxThreads)
		{
			m_bIndex = 0;
			m_bInstance++;
		}
	}

	// Change table

	m_pPipeDnDstTable[m_bIndex] = pPipe;
	m_iTableFree--;
	bIndex = m_bIndex;
	bInstance = m_bInstance;

	// Unlock downstream pipe table

	m_lockPipeDnDstTable.Unlock();

	return TRUE;
}

//----------------------------------------------------------------

BOOL MuxUpstreamThread::AttachDownstream(TcpMuxDownstreamHandler *tmdHandler)
{
	BOOL fResult;
	BYTE bIndex = 0;
	BYTE bInstance = 0;

	fResult = SetPipeDnDst(&tmdHandler->GetPipeSrc(), bIndex, bInstance);	// we will send to TcpMuxDownstreamHandler

	tmdHandler->SetPipeDst(&m_pipeDnSrc);				// tell TcpMuxDownstreamHandler to send to here
	tmdHandler->SetThreadNumber(bIndex, bInstance);

	return fResult;
}

//----------------------------------------------------------------

void MuxUpstreamThread::DetachDownstream(BYTE bIndex)
{
	// Lock downstream pipe table

	m_lockPipeDnDstTable.Lock();

	// Change table

	if(m_pPipeDnDstTable[bIndex] != NULL)
	{
		m_pPipeDnDstTable[bIndex] = NULL;
		m_iTableFree++;
	}

	// Unlock downstream pipe table

	m_lockPipeDnDstTable.Unlock();
}

//================================================================

void MuxUpstreamThread::LogReceivedUpstreamPacket(BYTE *pbBuffer, int iLen)
{
	MessagePacket rPacket;
	extern BitmapMsgStyle TemplateISO8583;

	rPacket.SetStyle(&TemplateISO8583);

	SZFN(MsgProcThread);

	if(!rPacket.m_pRequest.SetRawMsg(iLen, pbBuffer)) 
	{
		Log(LOG_WARNING, "%s: error in SetRawMsg", szFn);
		return;
	}

	if(!rPacket.m_pRequest.DecodeNetworkHeader(Message::NHT_NH1))
	{
		Log(LOG_WARNING, "%s: Failed to decode network header", szFn);
		return;
	}

	Log(LOG_INFO, "%s: Message received: Total len = %d, Network header len = %d, Body len = %d",
		szFn,
		rPacket.m_pRequest.GetRawLen(),
		rPacket.m_pRequest.GetNetworkHdrLen(),
		rPacket.m_pRequest.GetLenRemaining());

	if(!rPacket.m_pRequest.DecodeCommsHeader(Message::CHT_AUTOMATIC))
	{
		Log(LOG_WARNING, "%s: Failed to decode comms header", szFn);
		return;
	}

	if (GetConfig().GetEncryptionType() == Config::ETSL60)
	{
		if (*rPacket.m_pRequest.GetRawBMsg() == ENCRYPTION_INDICATOR)
		{	
			rPacket.m_pRequest.SetEtsl6Encrypted(TRUE);
			rPacket.m_pRequest.Etsl6BuildFakePacket();
		}
	}

	if(ChinaUnionPayHeader().IsChinaUnionPayHeader(rPacket.m_pRequest.GetRawBMsg()))
	{
		if(!rPacket.m_pRequest.GetEtsl6Encrypted())
		{
			rPacket.m_pRequest.SetChinaUnionPay(TRUE);
			rPacket.m_pRequest.ChinaUnionPayBuildFakePacket();
		}
	}

	if(!rPacket.m_pRequest.DecodeBMsg())
	{
		string strTmp;

		Format::Buf2HexDump(rPacket.m_pRequest.GetLenRemaining(),
			rPacket.m_pRequest.GetRawCommsHdr(), strTmp, 16);

		Log(LOG_WARNING, "%s: Failed to decode message: Len = %d, Data =\n%s", szFn, 
			rPacket.m_pRequest.GetLenRemaining(),
			strTmp.c_str());

		return;
	}

	string strTmp, strMessage;

	if (rPacket.m_pRequest.GetEtsl6Encrypted())
		rPacket.m_pRequest.m_sBMsg.Print(strTmp,BMPrintOption::BMPS_ETSL60);
	else
		rPacket.m_pRequest.m_sBMsg.Print(strTmp,BMPrintOption::BMPS_FULL);

	string strEnc = rPacket.m_pRequest.GetEtsl6Encrypted() ? "ETSL 6.0 encrypted " : "";

	string strCUP = rPacket.m_pRequest.GetChinaUnionPay() ? "CUP msg " : "";

	strMessage = string(szFn) + ": Received " + strEnc + strCUP + "response:\n\n" + strTmp;
	
	Log(LOG_INFO, "%s", strMessage.c_str());
}

//================================================================

DWORD MuxUpstreamThread::ThreadMemberFunc()
{
	SZFN(MuxUpstreamThread);

	Log(LOG_DEBUG, "%s: starting", szFn);

	if(	(m_pPipeUpDst == NULL) ||
		(GetActivityHandle() == NULL) ||
		(GetShutdownHandle() == NULL) )
	{
		Log(LOG_ERROR, "%s: ending, error 1: something not initialised at startup", szFn);
		return 1;
	}

	WaitableCollection wc;

	wc.push_back(m_pipeDnSrc);
	wc.push_back(m_pipeUpSrc);
	wc.push_back(GetShutdownEvent());

	int iLen;

	for(;;)
	{
		// wait for packet on pipe or shutdown

		DWORD e = wc.Wait();

		if(e == (WAIT_OBJECT_0 + 0))	// packet from downstream
		{
			// get packet from pipe

			BYTE *pbBuf = NULL;

			iLen = m_pipeDnSrc.GetChunkNC(&pbBuf);

			Log(LOG_DEBUG, "%s: packet received from downstream, size = %d", szFn, iLen);

			// Write to upstream pipe

			m_pPipeUpDst->PutNC(iLen, pbBuf);

			// signal activity semaphore

			Log(LOG_DEBUG, "%s: packet sent to upstream, size = %d", szFn, iLen);

			SignalActivity();
		}
		else if(e == (WAIT_OBJECT_0 + 1))	// packet from upstream
		{
			// get packet from pipe

			BYTE *pbBuf = NULL;

			iLen = m_pipeUpSrc.GetChunkNC(&pbBuf);

			Log(LOG_DEBUG, "%s: packet received from upstream, size = %d", szFn, iLen);

			if(iLen < 10)
			{
				Log(LOG_INFO, "%s: packet length invalid", szFn);
				delete[] pbBuf;
				continue;
			}

			// Get destination from header

			NetworkHeader nh;

			if(!nh.CopyFrom(pbBuf))
			{
				Log(LOG_INFO, "%s: packet header invalid", szFn);
				delete[] pbBuf;
				continue;
			}

			WORD wThread = nh.GetThread();

			int iIdx = (wThread >> 8) & 0xFF;

			if(iIdx >= m_iMaxThreads)
			{
				Log(LOG_INFO, "%s: packet header invalid", szFn);
				delete[] pbBuf;
				continue;
			}

			LogReceivedUpstreamPacket(pbBuf, iLen);

			// Lock downstream pipe table

			m_lockPipeDnDstTable.Lock();

			// Write to downstream pipe

			if(m_pPipeDnDstTable[iIdx] != NULL)
			{
				m_pPipeDnDstTable[iIdx]->PutNC(iLen, pbBuf);
				Log(LOG_DEBUG, "%s: packet sent to downstream, size = %d", szFn, iLen);
			}
			else
			{
				Log(LOG_INFO, "%s: packet received for disconnected downstream thread", szFn);
				delete[] pbBuf;
			}

			// Unlock downstream pipe table

			m_lockPipeDnDstTable.Unlock();

			// signal activity semaphore

			SignalActivity();
		}
		else if(e == (WAIT_OBJECT_0 + 2))
		{
			// shutdown thread

			Log(LOG_DEBUG, "%s: ending", szFn);
			return 0;
		}
		else
		{
			Log(LOG_ERROR, "%s: ending, error 2", szFn);
			return 2;
		}
	}
}
