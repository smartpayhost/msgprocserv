#include "stdafx.h"

#include "MAC.h"
#include "DesBlock.h"

//====================================================================

void MAC::NETS(DesBlock *pKey, int iDataLen, const BYTE *pData, BYTE *pResult)
{
	//	break message into 8 byte blocks (pad last with 0s)
	//	XOR all blocks together
	//	DES result

	//	DesBlock must have already been initialised with key in encrypt mode.

	int i;

	for(i = 0; i < 8; i++)
		pResult[i] = 0;

	for(i = 0; i < iDataLen; i++)
		pResult[i % 8] ^= pData[i];

	pKey->DoDes(pResult, pResult);
}

//====================================================================

void MAC::ANSI(DesBlock *pKey, int iDataLen, const BYTE *pData, BYTE *pResult)
{
	//	break message into 8 byte blocks (pad last with 0s)
	//	set result = 0
	//	for each block
	//		XOR block with result
	//		DES result

	int i;

	for(i = 0; i < 8; i++)
		pResult[i] = 0;

	for(i = 0; i < iDataLen; i++)
	{
		pResult[i % 8] ^= pData[i];

		if((i % 8) == 7)
			pKey->DoDes(pResult, pResult);
	}
	
	if((i % 8) != 0)	// do last block
		pKey->DoDes(pResult, pResult);
}

//====================================================================
