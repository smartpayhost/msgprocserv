#ifndef _MSGPROCSERV_H
#define _MSGPROCSERV_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SERVDEFAULTS_H
#include "ServDefaults.h"
#endif

//======================================================================

#define DEFAULT_RULE_FILE		"config\\msgprocserv.rules"

#define DEFAULT_LISTEN_PORT		"7000"

#define DEFAULT_MIN_THREADS		1
#define DEFAULT_MAX_THREADS		20

#define DEFAULT_DOWNSTREAM_IDLE_TIMEOUT	INFINITE
//#define DEFAULT_DOWNSTREAM_IDLE_TIMEOUT	30000

//======================================================================

#endif // _MSGPROCSERV_H
