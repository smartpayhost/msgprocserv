#ifndef _STRUTIL_H
#define _STRUTIL_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include <string>
#include <boost/utility.hpp>

#include "BitmapMsg.h"

//======================================================================

typedef std::string Token;

//======================================================================

class Line {
public:
	typedef std::vector<Token> LineContainer;

	typedef LineContainer::iterator iterator;
	typedef LineContainer::const_iterator const_iterator;
	typedef LineContainer::reference reference;
	typedef LineContainer::const_reference const_reference;
	typedef LineContainer::size_type size_type;

	iterator begin()             { return m_v.begin(); }
	const_iterator begin() const { return m_v.begin(); }
	iterator end()               { return m_v.end(); }
	const_iterator end() const   { return m_v.end(); }

	size_type size() const        { return m_v.size(); }

	void clear() { m_v.clear(); }

	void push_back(const_reference __x) { m_v.push_back(__x); }

	reference operator[](size_type __n) { return m_v.operator[](__n); }
	const_reference operator[](size_type __n) const { return m_v.operator[](__n); }

private:
	LineContainer m_v;
};

//======================================================================

class StrUtil : public boost::noncopyable {
public:
	StrUtil();
	~StrUtil();

	static BOOL StringToLine(const std::string &rStr, Line &rLine);
	static BOOL LineToString(const Line &rLine, std::string &rStr);

	static void ForcePurifyString(const std::string &rSrc, std::string &rDest);
	static void PurifyString(const std::string &rSrc, std::string &rDest);
	static BOOL IsStringPure(const std::string &rStr);
	static BOOL AddTokenToString(const Token &rToken, std::string &rStr);

	static BOOL Format(std::string &rStr, const char *szFmt, ...);

	static void trim( std::vector<std::string> &strings );
	static std::string FieldToString( const BitmapMsgField &msgField );
	static std::string VectorToString( const std::vector<BYTE> &source );
	static std::string trim( std::string &s );

private:
	BOOL IStringToLine(const std::string &rStr, Line &rLine);
	BOOL ILineToString(const Line &rLine, std::string &rStr);

	enum {
		WHITESPACE,
		FREETOKEN,
		DOUBLEQUOTE,
		DOUBLEQUOTE_ESC,
		DOUBLEQUOTE_HEX1,
		DOUBLEQUOTE_HEX2,
		SINGLEQUOTE,
		SINGLEQUOTE_ESC,
		COMMENT
	} m_eParseState;

	Token m_tokCurrent;
	Line m_lineCurrent;
	BYTE m_bHexByte;
	BOOL m_fComplete;

	BOOL TokeniseCharWhitespace(int c);
	BOOL TokeniseCharFreeToken(int c);
	BOOL TokeniseCharDoubleQuote(int c);
	BOOL TokeniseCharDoubleQuoteEsc(int c);
	BOOL TokeniseCharDoubleQuoteHex1(int c);
	BOOL TokeniseCharDoubleQuoteHex2(int c);
	BOOL TokeniseCharSingleQuote(int c);
	BOOL TokeniseCharSingleQuoteEsc(int c);
	BOOL TokeniseCharComment(int c);

	BOOL TokeniseChar(int c);

	void Reset();
	void ResetToken();
	void ResetLine();
	void EndToken();
	void EndLine();
};

//======================================================================

#endif // _STRUTIL_H
