#include "stdafx.h"
#include "SQLResultTable.h"

using std::string;
using std::vector;

//======================================================================

void SQLResultTable::AddRow(const vector<string>& rrRow)
{
	m_tblResult.push_back(rrRow);
}

//======================================================================

BOOL SQLResultTable::FindIn(const string& strTest, int iColumn)
{
	ResultTable::const_iterator i;

	for(i = m_tblResult.begin(); i != m_tblResult.end(); i++)
	{
		if(iColumn > (int)(*i).size())
			continue;

		if((*i)[iColumn] == strTest)
			return TRUE;
	}

	return FALSE;
}

//======================================================================

BOOL SQLResultTable::GetData(string &rResult, int iRow, int iCol)
{
	if(iRow >= GetNumRows())
		return FALSE;

	if(iCol >= GetNumCols())
		return FALSE;

	rResult = (m_tblResult[iRow])[iCol];

	return TRUE;
}

//======================================================================

void SQLResultTable::Erase()
{
	m_tblResult.clear();
}

//======================================================================

int SQLResultTable::GetNumCols() const
{
	if(m_tblResult.size())
	{
		return (int)m_tblResult[0].size();
	}
	else
		return 0;
}

//----------------------------------------------------------------------

int SQLResultTable::GetNumRows() const
{
	return (int)m_tblResult.size();
}

//======================================================================
