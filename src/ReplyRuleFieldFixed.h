#ifndef _REPLYRULEFIELDFIXED_H
#define _REPLYRULEFIELDFIXED_H

//==============================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _REPLYRULEFIELD_H
#include "ReplyRuleField.h"
#endif

//==============================================================

class ReplyRuleFieldFixed : public ReplyRuleField {
public:
	ReplyRuleFieldFixed(int iFieldNum, const BitmapMsgStyle *pStyle);

	ReplyRule* Clone() const;

	BOOL Import(const Line &rLine);
	BOOL Export(std::string &strDesc);

	BOOL BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const;

private:
	std::string	m_strFixedData;
};

//==============================================================

#endif // _REPLYRULEFIELDFIXED_H
