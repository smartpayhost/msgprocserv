#include "stdafx.h"
#include "ReplyRuleActionProg.h"

#include "ReplyRuleFieldFile.h"
#include "TmpFileCollection.h"

#include <fstream>
#include <iomanip>

#include <process.h>

using std::string;
using std::vector;
using std::ofstream;
using std::ios_base;

//======================================================================

ReplyRuleActionProg::ReplyRuleActionProg(const BitmapMsgStyle *pStyle)
:	ReplyRule(pStyle)
{
}

//---------------------------------------------------------

ReplyRule* ReplyRuleActionProg::Clone() const
{
	return new ReplyRuleActionProg(*this);
}

//======================================================================
//
//	PROG <cmdline>
//

BOOL ReplyRuleActionProg::Import(const Line &rLine)
{
	Line::const_iterator i;

	i = rLine.begin();

	if(i == rLine.end()) return FALSE;

	if((*i) != "PROG") return FALSE;

	if(++i == rLine.end()) return FALSE;

	m_strCmdline = *i;

	return TRUE;
}

//======================================================================
//
//	PROG <cmdline>
//

BOOL ReplyRuleActionProg::Export(string &strDesc)
{
	string strTmp;

	strDesc = "PROG " + m_strCmdline;

	return TRUE;
}

//======================================================================

BOOL ReplyRuleActionProg::BuildReplyField(BitmapMsg& rNewMsg, const BitmapMsg& rOrigMsg) const
{
	size_t i;
	BOOL fResult;
	int iResult;

	//-----------------------
	// Create source file(s)

	string strExpandedCmdLine;
	Line lineArgs;
	TmpFileCollection vectTmpFiles;
	vector<std::pair<int, string> > vectDstFiles;

	StrUtil::StringToLine(m_strCmdline, lineArgs);

	// look for %, $, & arguments and replace with filenames

	size_t iNumArgs = lineArgs.size();

	for(i = 0; i < iNumArgs; i++)
	{
		string strArg = lineArgs[i];

		if((strArg[0] == '%')||(strArg[0] == '$'))
		{
			const BitmapMsg *pMsg;

			if(strArg[0] == '%')
				pMsg = &rNewMsg;
			else
				pMsg = &rOrigMsg;

			int iField = atoi(strArg.c_str() + 1);

			if(((iField >= 1) && (iField <= 128)) || ((iField >= 200) && (iField <= 202)) || (iField == 999))
			{
				char tname[MAX_PATH];
				tmpnam_s(tname, sizeof(tname));
				string strSrcName(tname);

				ofstream of;

				of.open(strSrcName.c_str(), ios_base::out | ios_base::trunc | ios_base::binary);

				if(of.bad())
				{
					char szErr[100];

					strerror_s(szErr, sizeof(szErr), errno);

					Log(LOG_ERROR, "Failed to create temp file %s: %s", strSrcName.c_str(), szErr);
					return FALSE;
				}

				if((iField >= 1) && (iField <= 128))
				{
					const BitmapMsgField *pOrigField = pMsg->GetField(iField);

					if(pOrigField != NULL)
					{
						if(pOrigField->GetFieldLen() != 0)
						{
							of.write((const char *)(pOrigField->GetField()), pOrigField->GetFieldLen());

							if(of.bad())
							{
								char szErr[100];

								strerror_s(szErr, sizeof(szErr), errno);

								Log(LOG_ERROR, "Error writing to temp file %s: %s", strSrcName.c_str(), szErr);
								of.close();
								return FALSE;
							}
						}
					}
				}
				else if(iField == 200)
				{
					of << std::setw(4) << std::setfill('0') << pMsg->GetMsgType();

					if(of.bad())
					{
						char szErr[100];

						strerror_s(szErr, sizeof(szErr), errno);

						Log(LOG_ERROR, "Error writing to temp file %s: %s", strSrcName.c_str(), szErr);
						return FALSE;
					}
				}
				else if(iField == 201)
				{
					of << pMsg->GetPAN();
				}
				else if(iField == 202)
				{
					of << pMsg->GetExpiryYYMM();
				}
				else if(iField == 999)
				{
					of.write((const char *)(pMsg->GetRawMsg()), pMsg->GetMsgLen());

					if(of.bad())
					{
						char szErr[100];

						strerror_s(szErr, sizeof(szErr), errno);

						Log(LOG_ERROR, "Error writing to temp file %s: %s", strSrcName.c_str(), szErr);
						of.close();
						return FALSE;
					}
				}

				of.close();
				vectTmpFiles.push_back(strSrcName);
				lineArgs[i] = strSrcName;
			}
			else
			{
				Log(LOG_ERROR, "Invalid field number (%d) in cmdline", iField);
				return FALSE;
			}
		}
		else if(strArg[0] == '&')
		{
			int iField = atoi(strArg.c_str() + 1);

			if((iField < 1) || (iField > 128))
			{
				Log(LOG_ERROR, "Invalid field number (%d) in cmdline", iField);
				return FALSE;
			}

			char tname[MAX_PATH];
			tmpnam_s(tname, sizeof(tname));
			string strName(tname);
			vectTmpFiles.push_back(strName);
			vectDstFiles.push_back(std::pair<int, string>(iField, strName));
			lineArgs[i] = strName;
		}
	}
	
	const char **pArgs = new const char *[iNumArgs + 2];

	for(i = 0; i < iNumArgs; i++)
	{
		pArgs[i] = lineArgs[i].c_str();
	}

	pArgs[i] = NULL;

	//------------
	// Run program

	iResult = (int)_spawnv(_P_WAIT, pArgs[0], pArgs);

	delete [] pArgs;

	//--------------
	// Check results
	
	if(iResult == 0)
	{
		// Get results from optional destination files

		for(i = 0; i < (int)vectDstFiles.size(); i++)
		{
			int iField = vectDstFiles[i].first;
			string strDstName = vectDstFiles[i].second;

			if(!ReplyRuleFieldFile::BuildReplyFieldExtFile(rNewMsg, rOrigMsg, iField, strDstName))
			{
				char szErr[100];

				strerror_s(szErr, sizeof(szErr), errno);

				Log(LOG_WARNING, "Failed to open file %s: %s", strDstName.c_str(), szErr);
			}
		}

		fResult = TRUE;
	}
	else
	{
		Log(LOG_ERROR, "Failed to execute %s, return value = %d", m_strCmdline.c_str(), iResult);
		fResult = FALSE;
	}

	return fResult;
}

//======================================================================
