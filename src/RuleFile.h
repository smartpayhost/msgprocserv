#ifndef _RULEFILE_H
#define _RULEFILE_H

//======================================================================

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _LOGGINGOBJECT_H
#include "LoggingObject.h"
#endif

#ifndef _STRUTIL_H
#include "StrUtil.h"
#endif

#include <vector>
#include <boost/utility.hpp>

//======================================================================

class RuleEntity {
public:
	virtual ~RuleEntity() = 0;
	virtual BOOL ProcessLine(const Line &rLine) = 0;
	virtual BOOL IsComplete() const = 0;
};

//======================================================================

class RuleEntityDescriptor {
public:
	virtual ~RuleEntityDescriptor() = 0;
	virtual BOOL IsStart(const Line &rLine) const = 0;
	virtual RuleEntity *MakeNew(const Line &rLine) = 0;
};

//======================================================================

class RuleFile : public LoggingObject, public boost::noncopyable {
public:
	RuleFile();
	virtual ~RuleFile() = 0;

	virtual RuleFile* Clone() const = 0;
	virtual void MergeFrom(const RuleFile* prfFrom) = 0;

	virtual BOOL Load(const std::string& strFilename);

	virtual void Export(std::string &rStrDest) = 0;

	void RegisterEntityType(RuleEntityDescriptor *pDescriptor);

private:
	RuleEntity *MakeNewEntity(const Line &rLine);

	void Reset();
	BOOL TokeniseChar(int c);

private:
	typedef std::vector<RuleEntityDescriptor *> EntityDescriptorSequence;
	EntityDescriptorSequence m_seqTypes;

	typedef std::vector<std::pair<int, Line> > LineSequence;
	LineSequence m_seqLines;

	Line m_lineCurrent;
	std::string m_strCurrent;

	int m_iLineNumber;
};

//======================================================================

#endif // _RULEFILE_H
