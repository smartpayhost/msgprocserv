#ifndef _CONFIG_H
#define _CONFIG_H

#include <string>

namespace Config
{
	typedef enum
	{
		NO_TYPE,
		ETSL60
	} EncryptionType;

	static const std::string ETSL60_LABEL = "ETSL60";
};

#endif